package values;

import com.aleixpellisa.mdpa.workouter.network.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MockClient {
    private static Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    public static Retrofit retrofitClientMock = new Retrofit.Builder()
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(Constants.TEST_API_BASE)
            .build();
}
