package values;

import com.aleixpellisa.mdpa.workouter.builder.AuthBuilder;
import com.aleixpellisa.mdpa.workouter.builder.ProfileBuilder;
import com.aleixpellisa.mdpa.workouter.builder.SettingsBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.database.entity.Skill;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesKey;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.view.input.MeetDayTextInputType;

import java.util.List;

import static values.Events.fetchProfileEvent;
import static values.Events.sendEvaluationEvent;

public class Inputs {

    /**
     * Profile
     */

    public static final Auth auth = AuthBuilder.buildMockedAuth();

    public static final Settings settings = SettingsBuilder.buildMockedSettings();

    public static final Profile basicProfile = ProfileBuilder.buildBasicProfile();

    public static final String candidateId = ProfileBuilder.CANDIDATE_PROFILE_ID_MOCK;

    public static final String email = "test@email.com";
    public static final String wrongEmail = "NotAnEmail";
    public static final String password = "password123";
    public static final String encryptedPassword = "ef92b778bafe771e89245b89ecbc08a44a4e166c06659911881f383d4473e94f";
    public static final String wrongPassword = "pas";
    public static final String wrongConfirmPassword = "password1234";

    public static final String facebookAccessToken = "xxxx.yyyy.zzzz";

    public static final boolean acceptedTermsAndConditions = true;
    public static final boolean notAcceptedTermsAndConditions = false;

    public static final Profile profile = fetchProfileEvent.data;

    public static final String nameUpdated = ProfileBuilder.CANDIDATE_PROFILE_NAME_MOCK;
    public static final String firstNameUpdated = ProfileBuilder.CANDIDATE_PROFILE_FIRST_NAME_MOCK;
    public static final List<String> imagesUpdated = ProfileBuilder.CANDIDATE_PROFILE_IMAGES_MOCK;
    public static final int ageUpdated = ProfileBuilder.CANDIDATE_PROFILE_AGE_MOCK;
    public static final String genderUpdated = ProfileBuilder.CANDIDATE_PROFILE_GENDER_MOCK;
    public static final String preferenceUpdated = SettingsBuilder.SETTINGS_PREFERENCE_MOCK;
    public static final String cityUpdated = SettingsBuilder.SETTINGS_CITY_MOCK;

    public static final String descriptionUpdated = ProfileBuilder.CANDIDATE_PROFILE_DESCRIPTION_MOCK;
    public static final String currentJobUpdated = ProfileBuilder.CANDIDATE_PROFILE_CURRENT_JOB_MOCK;
    public static final String studiesUpdated = ProfileBuilder.CANDIDATE_PROFILE_STUDIES_MOCK;
    public static final String favSongUpdated = ProfileBuilder.CANDIDATE_PROFILE_FAV_SONG_MOCK;
    public static final List<Skill> skillsUpdated = ProfileBuilder.CANDIDATE_PROFILE_SKILLS_MOCK;

    public static final @PreferencesKey
    String updateSettingsKey = Settings.CITY_KEY;
    public static final Object updateSettingsValue = "CityUpdated";

    public static final MeetDayTextInputType emptyEmailInput = MeetDayTextInputType.email("");
    public static final MeetDayTextInputType emailInput = MeetDayTextInputType.email(email);
    public static final MeetDayTextInputType wrongEmailInput = MeetDayTextInputType.email(wrongEmail);
    public static final MeetDayTextInputType emptyPasswordInput = MeetDayTextInputType.password("");
    public static final MeetDayTextInputType passwordInput = MeetDayTextInputType.password(password);
    public static final MeetDayTextInputType wrongPasswordInput = MeetDayTextInputType.password(wrongPassword);
    public static final MeetDayTextInputType wrongConfirmPasswordInput = MeetDayTextInputType.password(wrongConfirmPassword);

    public static final MeetDayTextInputType profileEmptyTextInput = MeetDayTextInputType.required("");
    public static final MeetDayTextInputType profileNameTextInput = MeetDayTextInputType.required(nameUpdated);
    public static final MeetDayTextInputType profileFirstNameTextInput = MeetDayTextInputType.required(firstNameUpdated);
    public static final MeetDayTextInputType profileDescriptionTextInput = MeetDayTextInputType.notRequired(descriptionUpdated);
    public static final MeetDayTextInputType profileCurrentJobTextInput = MeetDayTextInputType.notRequired(currentJobUpdated);
    public static final MeetDayTextInputType profileStudiesTextInput = MeetDayTextInputType.notRequired(studiesUpdated);
    public static final MeetDayTextInputType profileFavSongTextInput = MeetDayTextInputType.notRequired(favSongUpdated);

    /**
     * Candidate
     */

    public static final boolean visible = true;
    public static final boolean notVisible = false;

    public static final String evaluation = sendEvaluationEvent.data.getEvaluationValue();
}
