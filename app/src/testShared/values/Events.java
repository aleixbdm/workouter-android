package values;

import com.aleixpellisa.mdpa.workouter.builder.AuthBuilder;
import com.aleixpellisa.mdpa.workouter.builder.CandidateBuilder;
import com.aleixpellisa.mdpa.workouter.builder.ChatBuilder;
import com.aleixpellisa.mdpa.workouter.builder.EvaluationBuilder;
import com.aleixpellisa.mdpa.workouter.builder.MessageBuilder;
import com.aleixpellisa.mdpa.workouter.builder.ProfileBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.model.MeetDayError;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.List;

public class Events {
    /**
     * Auth
     */
    public static final Event<Auth> loginEvent = Event.success(AuthBuilder.buildMockedAuth());
    public static final Event<Auth> loginErrorEvent = Event.error(new MeetDayError("loginError"));
    public static final Event<Auth> facebookLoginEvent = Event.success(AuthBuilder.buildMockedAuth());
    public static final Event<Auth> facebookLoginErrorEvent = Event.error(new MeetDayError("facebookLoginError"));
    public static final Event<Auth> createAccountEvent = Event.success(AuthBuilder.buildMockedAuth());
    public static final Event<Auth> createAccountErrorEvent = Event.error(new MeetDayError("createAccountError"));
    public static final Event<Auth> refreshTokenEvent = Event.success(AuthBuilder.buildMockedAuth());
    public static final Event<Auth> refreshTokenErrorEvent = Event.error(new MeetDayError("refreshTokenError"));

    /**
     * Profile
     */
    public static final Event<Profile> fetchProfileEvent = Event.success(ProfileBuilder.buildMockedProfile());
    public static final Event<Profile> fetchProfileErrorEvent = Event.error(new MeetDayError("fetchProfileError"));
    public static final Event<Profile> createProfileEvent = Event.success(ProfileBuilder.buildMockedProfile());
    public static final Event<Profile> createProfileErrorEvent = Event.error(new MeetDayError("createProfileError"));
    public static final Event<Profile> updateProfileEvent = Event.success(ProfileBuilder.buildMockedProfile());
    public static final Event<Profile> updateProfileErrorEvent = Event.error(new MeetDayError("updateProfileError"));
    public static final Event<Profile> updateSettingsEvent = Event.success(ProfileBuilder.buildMockedProfile());
    public static final Event<Profile> updateSettingsErrorEvent = Event.error(new MeetDayError("updateSettingsError"));
    public static final Event<Profile> deleteProfileEvent = Event.success(ProfileBuilder.buildMockedProfile());
    public static final Event<Profile> deleteProfileErrorEvent = Event.error(new MeetDayError("deleteProfileError"));
    public static final Event<Profile> fetchCandidateProfileEvent = Event.success(ProfileBuilder.buildMockedCandidateProfile());
    public static final Event<Profile> fetchCandidateProfileErrorEvent = Event.error(new MeetDayError("fetchCandidateProfileError"));

    /**
     * Candidate
     */
    public static final Event<List<Candidate>> fetchCandidatesEvent = Event.success(CandidateBuilder.buildMockedCandidateList());
    public static final Event<List<Candidate>> fetchEmptyCandidatesEvent = Event.success(CandidateBuilder.buildEmptyCandidateList());
    public static final Event<List<Candidate>> fetchCandidatesErrorEvent = Event.error(new MeetDayError("fetchCandidatesError"));

    /**
     * Evaluation
     */
    public static final Event<Evaluation> sendEvaluationEvent = Event.success(EvaluationBuilder.buildMockedEvaluation());
    public static final Event<Evaluation> sendEvaluationErrorEvent = Event.error(new MeetDayError("sendEvaluationErrorEvent"));

    /**
     * Chat
     */
    public static final Event<List<Chat>> fetchChatsEvent = Event.success(ChatBuilder.buildMockedChatList());
    public static final Event<List<Chat>> fetchChatsErrorEvent = Event.error(new MeetDayError("fetchChatsError"));

    /**
     * Message
     */
    public static final Event<List<Message>> fetchChatMessagesEvent = Event.success(MessageBuilder.buildMockedMessageList());
    public static final Event<List<Message>> fetchChatMessagesErrorEvent = Event.error(new MeetDayError("fetchChatMessagesError"));
    public static final Event<Message> sendMessageEvent = Event.success(MessageBuilder.buildMockedMessageList().get(0));
    public static final Event<Message> sendMessageErrorEvent = Event.error(new MeetDayError("sendMessageError"));
}
