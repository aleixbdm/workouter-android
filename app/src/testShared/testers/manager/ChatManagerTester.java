package testers.manager;

import android.arch.lifecycle.Observer;

import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesManager;
import com.aleixpellisa.mdpa.workouter.manager.impl.ChatManagerImpl;
import com.aleixpellisa.mdpa.workouter.manager.observer.EventObserver;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.network.rest.ChatRest;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.List;

import javax.inject.Inject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;


public class ChatManagerTester {

    private ChatManagerImpl manager;

    private PreferencesManager preferencesManager;

    private ChatRest chatRest;

    @Inject
    ChatManagerTester(ChatManagerImpl manager,
                      PreferencesManager preferencesManager,
                      ChatRest chatRest) {
        this.manager = manager;
        this.preferencesManager = preferencesManager;
        this.chatRest = chatRest;
    }

    /**
     * Preconditions
     */

    public void authInPreferences(final Auth auth) {
        when(preferencesManager.readAuth()).thenReturn(auth);
    }

    public void chatListInDatabase(final List<Chat> chatList) {
        manager.getChatListDatabaseObserver().onChanged(chatList);
    }

    public void chatMessageListInDatabase(final List<Message> chatMessageList) {
        manager.getChatMessageListDatabaseObserver().onChanged(chatMessageList);
    }

    /**
     * Observer responses
     */

    @SuppressWarnings("unchecked")
    public void fetchChatsReturnsChats(final Event<List<Chat>> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<List<Chat>, List<Chat>> observer = manager.getFetchChatsObserver();
                List<Chat> chatList = event.data;
                if (chatList != null) {
                    // Will execute an async task to save into database
                    manager.getChatListDatabaseObserver().onChanged(chatList);
                } else {
                    observer.onChanged(event);
                }
                return null;
            }
        }).when(chatRest).fetchChats(any(Observer.class));
    }

    @SuppressWarnings("unchecked")
    public void fetchChatMessagesReturnsChatMessages(String chatId, final Event<List<Message>> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<List<Message>, List<Message>> observer = manager.getFetchChatMessagesObserver();
                List<Message> messageList = event.data;
                if (messageList != null) {
                    // Will execute an async task to save into database
                    manager.getChatMessageListDatabaseObserver().onChanged(messageList);
                } else {
                    observer.onChanged(event);
                }
                return null;
            }
        }).when(chatRest).fetchChatMessages(eq(chatId), any(Observer.class));
    }

    @SuppressWarnings("unchecked")
    public void sendChatMessageReturnsChatMessage(String chatId, String messageText, final Event<Message> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<Message, Message> observer = manager.getSendChatMessageObserver();
                observer.onChanged(event);
                return null;
            }
        }).when(chatRest).sendMessage(eq(chatId), eq(messageText), any(Observer.class));
    }
}
