package testers.manager;

import android.arch.lifecycle.Observer;

import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesKey;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesManager;
import com.aleixpellisa.mdpa.workouter.manager.impl.ProfileManagerImpl;
import com.aleixpellisa.mdpa.workouter.manager.observer.EventObserver;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.network.rest.AuthRest;
import com.aleixpellisa.mdpa.workouter.network.rest.ProfileRest;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.List;

import javax.inject.Inject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;


public class ProfileManagerTester {

    private ProfileManagerImpl manager;

    private PreferencesManager preferencesManager;

    private AuthRest authRest;

    private ProfileRest profileRest;

    @Inject
    ProfileManagerTester(ProfileManagerImpl manager,
                         PreferencesManager preferencesManager,
                         AuthRest authRest,
                         ProfileRest profileRest) {
        this.manager = manager;
        this.preferencesManager = preferencesManager;
        this.authRest = authRest;
        this.profileRest = profileRest;
    }

    /**
     * Preconditions
     */

    public void authInPreferences(final Auth auth) {
        when(preferencesManager.readAuth()).thenReturn(auth);
    }

    public void settingsInPreferences(final Settings settings) {
        when(preferencesManager.readSettings()).thenReturn(settings);
    }

    public void profileInDatabase(final Profile profile) {
        manager.getProfileDatabaseObserver().onChanged(profile);
    }

    /**
     * Observer responses
     */

    @SuppressWarnings("unchecked")
    public void loginReturnsAuth(final Event<Auth> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<Auth, Profile> observer = manager.getLoginObserver();
                Auth auth = event.data;
                if (auth != null) {
                    when(preferencesManager.readAuth()).thenReturn(auth);
                }
                observer.onChanged(event);
                return null;
            }
        }).when(authRest).login(any(String.class), any(String.class), any(Observer.class));
    }

    @SuppressWarnings("unchecked")
    public void facebookLoginReturnsAuth(final Event<Auth> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<Auth, Profile> observer = manager.getFaceBookLoginObserver();
                Auth auth = event.data;
                if (auth != null) {
                    when(preferencesManager.readAuth()).thenReturn(auth);
                }
                observer.onChanged(event);
                return null;
            }
        }).when(authRest).facebookLogin(any(String.class), any(Observer.class));
    }

    @SuppressWarnings("unchecked")
    public void createAccountReturnsAuth(final Event<Auth> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<Auth, Profile> observer = manager.getCreateAccountObserver();
                Auth auth = event.data;
                if (auth != null) {
                    when(preferencesManager.readAuth()).thenReturn(auth);
                }
                observer.onChanged(event);
                return null;
            }
        }).when(authRest).createAccount(any(String.class), any(String.class), any(Observer.class));
    }

    @SuppressWarnings("unchecked")
    public void fetchProfileReturnsProfile(final Event<Profile> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<Profile, Profile> observer = manager.getFetchProfileObserver();
                Profile profile = event.data;
                if (profile != null) {
                    // Will execute an async task to save into database
                    manager.getProfileDatabaseObserver().onChanged(profile);
                } else {
                    observer.onChanged(event);
                }
                return null;
            }
        }).when(profileRest).fetchProfile(any(Observer.class));
    }

    @SuppressWarnings("unchecked")
    public void createProfileReturnsProfileCreated(String name, String firstName, List<String> imageUrls,
                                                   Integer age, @Profile.Gender String gender,
                                                   @Settings.Preference String preference, String city,
                                                   final Event<Profile> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<Profile, Profile> observer = manager.getCreateProfileObserver();
                Profile profile = event.data;
                if (profile != null) {
                    // Will execute an async task to save into database
                    manager.getProfileDatabaseObserver().onChanged(profile);
                } else {
                    observer.onChanged(event);
                }
                return null;
            }
        }).when(profileRest).createProfile(eq(name),
                eq(firstName), eq(imageUrls), eq(age), eq(gender),
                eq(preference), eq(city), any(Observer.class));
    }

    @SuppressWarnings("unchecked")
    public void updateProfileReturnsProfileUpdated(final Event<Profile> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<Profile, Profile> observer = manager.getUpdateProfileObserver();
                Profile profile = event.data;
                if (profile != null) {
                    // Will execute an async task to save into database
                    manager.getProfileDatabaseObserver().onChanged(profile);
                } else {
                    observer.onChanged(event);
                }
                return null;
            }
        }).when(profileRest).updateProfile(eq(event.data), any(Observer.class));
    }

    @SuppressWarnings("unchecked")
    public void updateSettingsReturnsProfileUpdated(@PreferencesKey String key, Object value, final Event<Profile> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<Profile, Profile> observer = manager.getUpdateSettingsObserver();
                Profile profile = event.data;
                if (profile != null) {
                    // Will execute an async task to save into database
                    manager.getProfileDatabaseObserver().onChanged(profile);
                } else {
                    observer.onChanged(event);
                }
                return null;
            }
        }).when(profileRest).updateSettings(eq(key), eq(value), any(Observer.class));
    }

    @SuppressWarnings("unchecked")
    public void deleteProfileReturnsProfileDeleted(final Event<Profile> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<Profile, Profile> observer = manager.getDeleteProfileObserver();
                observer.onChanged(event);
                return null;
            }
        }).when(profileRest).deleteProfile(any(Observer.class));
    }

    @SuppressWarnings("unchecked")
    public void fetchCandidateProfileReturnsCandidateProfile(String candidateId, final Event<Profile> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<Profile, Profile> observer = manager.getFetchCandidateProfileObserver();
                observer.onChanged(event);
                return null;
            }
        }).when(profileRest).fetchCandidateProfile(eq(candidateId), any(Observer.class));
    }
}
