package testers.manager;

import android.arch.lifecycle.Observer;

import com.aleixpellisa.mdpa.workouter.manager.impl.CandidateManagerImpl;
import com.aleixpellisa.mdpa.workouter.manager.observer.EventObserver;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.network.rest.CandidateRest;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.List;

import javax.inject.Inject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;


public class CandidateManagerTester {

    private CandidateManagerImpl manager;

    private CandidateRest candidateRest;

    @Inject
    CandidateManagerTester(CandidateManagerImpl manager,
                           CandidateRest candidateRest) {
        this.manager = manager;
        this.candidateRest = candidateRest;
    }

    /**
     * Observer responses
     */

    @SuppressWarnings("unchecked")
    public void fetchCandidatesReturnsCandidates(final Event<List<Candidate>> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<List<Candidate>, List<Candidate>> observer = manager.getFetchCandidatesObserver();
                observer.onChanged(event);
                return null;
            }
        }).when(candidateRest).fetchCandidates(any(Observer.class));
    }

    @SuppressWarnings("unchecked")
    public void sendEvaluationReturnsEvaluation(String candidateId, @Evaluation.EvaluationValue String evaluation, final Event<Evaluation> event) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                EventObserver<Evaluation, Evaluation> observer = manager.getSendEvaluationObserver();
                observer.onChanged(event);
                return null;
            }
        }).when(candidateRest).sendEvaluation(eq(candidateId), eq(evaluation), any(Observer.class));
    }
}
