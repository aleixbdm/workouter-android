package testers.viewmodel;

import com.aleixpellisa.mdpa.workouter.manager.CandidateManager;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;

import java.util.List;

import javax.inject.Inject;

import static org.mockito.Mockito.when;

public class CandidateViewModelUnitTester extends CandidateViewModelTester {

    @Inject
    CandidateViewModelUnitTester(ProfileManager profileManager,
                                 CandidateManager candidateManager,
                                 EventLiveData<Event<List<Candidate>>> candidateListLiveData,
                                 EventLiveData<Event<Evaluation>> evaluationLiveData) {
        this.profileManager = profileManager;
        this.candidateManager = candidateManager;
        this.candidateListLiveData = candidateListLiveData;
        this.evaluationLiveData = evaluationLiveData;
    }

    /**
     * Preconditions
     */

    public void candidateListFromManager(Event<List<Candidate>> event) {
        when(candidateListLiveData.getValue()).thenReturn(event);
        when(candidateManager.getCandidateList()).thenReturn(candidateListLiveData);
    }

    public void evaluationFromManager(Event<Evaluation> event) {
        when(evaluationLiveData.getValue()).thenReturn(event);
        when(candidateManager.getEvaluation()).thenReturn(evaluationLiveData);
    }
}
