package testers.viewmodel;

import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

public interface IProfileViewModelTester {
    void authFromManager(Auth auth);

    void settingsFromManager(Settings settings);

    void profileFromManager(Event<Profile> event);

    void candidateProfileFromManager(Event<Profile> event);
}
