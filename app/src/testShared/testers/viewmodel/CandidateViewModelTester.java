package testers.viewmodel;

import com.aleixpellisa.mdpa.workouter.builder.SettingsBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.manager.CandidateManager;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;

import java.util.List;

import static org.mockito.Mockito.when;

abstract class CandidateViewModelTester implements ICandidateViewModelTester {

    ProfileManager profileManager;

    CandidateManager candidateManager;

    EventLiveData<Event<List<Candidate>>> candidateListLiveData;

    EventLiveData<Event<Evaluation>> evaluationLiveData;

    /**
     * Preconditions
     */

    @Override
    public void isVisibleFromManager(boolean isVisible) {
        Settings settings = SettingsBuilder.buildMockedSettings();
        settings.setVisible(isVisible);
        when(profileManager.getSettings()).thenReturn(settings);
    }
}
