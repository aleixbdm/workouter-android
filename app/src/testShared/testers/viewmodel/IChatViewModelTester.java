package testers.viewmodel;

import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.List;

public interface IChatViewModelTester {
    void chatListFromManager(Event<List<Chat>> event);

    void chatMessageListFromManager(Event<List<Message>> event);

    void chatMessageFromManager(Event<Message> event);

    void setCurrentChatId(String currentChatId);
}
