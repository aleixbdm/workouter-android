package testers.viewmodel;

import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.manager.ChatManager;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;

import java.util.List;

import static org.mockito.Mockito.when;

public class ChatViewModelInstrumentedTester extends ChatViewModelTester {

    public ChatViewModelInstrumentedTester(ChatManager chatManager) {
        this.chatManager = chatManager;
        this.chatListLiveData = new EventLiveData<>();
        this.chatMessageListLiveData = new EventLiveData<>();
        this.chatMessageLiveData = new EventLiveData<>();

        when(chatManager.getChatList()).thenReturn(chatListLiveData);
        when(chatManager.getChatMessageList()).thenReturn(chatMessageListLiveData);
        when(chatManager.getChatMessage()).thenReturn(chatMessageLiveData);
    }

    /**
     * Preconditions
     */

    @Override
    public void chatListFromManager(Event<List<Chat>> event) {
        chatListLiveData.postValue(event);
    }

    @Override
    public void chatMessageListFromManager(Event<List<Message>> event) {
        chatMessageListLiveData.postValue(event);
    }

    @Override
    public void chatMessageFromManager(Event<Message> event) {
        chatMessageLiveData.postValue(event);
    }
}
