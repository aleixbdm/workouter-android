package testers.viewmodel;

import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;

import static org.mockito.Mockito.when;

abstract class ProfileViewModelTester implements IProfileViewModelTester {

    ProfileManager profileManager;

    EventLiveData<Event<Profile>> profileLiveData;

    EventLiveData<Event<Profile>> candidateProfileLiveData;

    /**
     * Preconditions
     */

    @Override
    public void authFromManager(final Auth auth) {
        when(profileManager.getAuth()).thenReturn(auth);
    }

    @Override
    public void settingsFromManager(final Settings settings) {
        when(profileManager.getSettings()).thenReturn(settings);
    }
}
