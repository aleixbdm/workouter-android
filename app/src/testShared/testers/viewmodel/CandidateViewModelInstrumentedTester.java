package testers.viewmodel;

import com.aleixpellisa.mdpa.workouter.manager.CandidateManager;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;

import java.util.List;

import static org.mockito.Mockito.when;

public class CandidateViewModelInstrumentedTester extends CandidateViewModelTester {

    public CandidateViewModelInstrumentedTester(ProfileManager profileManager,
                                                CandidateManager candidateManager) {
        this.profileManager = profileManager;
        this.candidateManager = candidateManager;
        this.candidateListLiveData = new EventLiveData<>();
        this.evaluationLiveData = new EventLiveData<>();

        when(candidateManager.getCandidateList()).thenReturn(candidateListLiveData);
        when(candidateManager.getEvaluation()).thenReturn(evaluationLiveData);
    }

    /**
     * Preconditions
     */

    @Override
    public void candidateListFromManager(Event<List<Candidate>> event) {
        candidateListLiveData.postValue(event);
    }

    @Override
    public void evaluationFromManager(Event<Evaluation> event) {
        evaluationLiveData.postValue(event);
    }
}
