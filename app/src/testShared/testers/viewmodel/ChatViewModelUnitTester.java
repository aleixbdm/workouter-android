package testers.viewmodel;

import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.manager.ChatManager;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;
import com.aleixpellisa.mdpa.workouter.view.model.ChatViewModel;

import java.util.List;

import javax.inject.Inject;

import static org.mockito.Mockito.when;

public class ChatViewModelUnitTester extends ChatViewModelTester {

    @Inject
    ChatViewModelUnitTester(ChatViewModel chatViewModel,
                            ChatManager chatManager,
                            EventLiveData<Event<List<Chat>>> chatListLiveData,
                            EventLiveData<Event<List<Message>>> chatMessageListLiveData,
                            EventLiveData<Event<Message>> chatMessageLiveData) {
        this.chatViewModel = chatViewModel;
        this.chatManager = chatManager;
        this.chatListLiveData = chatListLiveData;
        this.chatMessageListLiveData = chatMessageListLiveData;
        this.chatMessageLiveData = chatMessageLiveData;
    }

    /**
     * Preconditions
     */

    @Override
    public void chatListFromManager(Event<List<Chat>> event) {
        when(chatListLiveData.getValue()).thenReturn(event);
        when(chatManager.getChatList()).thenReturn(chatListLiveData);
    }

    @Override
    public void chatMessageListFromManager(Event<List<Message>> event) {
        when(chatMessageListLiveData.getValue()).thenReturn(event);
        when(chatManager.getChatMessageList()).thenReturn(chatMessageListLiveData);
    }

    @Override
    public void chatMessageFromManager(Event<Message> event) {
        when(chatMessageLiveData.getValue()).thenReturn(event);
        when(chatManager.getChatMessage()).thenReturn(chatMessageLiveData);
    }
}
