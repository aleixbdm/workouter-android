package testers.viewmodel;

import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;

import static org.mockito.Mockito.when;

public class ProfileViewModelInstrumentedTester extends ProfileViewModelTester {

    public ProfileViewModelInstrumentedTester(ProfileManager profileManager) {
        this.profileManager = profileManager;
        this.profileLiveData = new EventLiveData<>();
        this.candidateProfileLiveData = new EventLiveData<>();

        when(profileManager.getProfile()).thenReturn(profileLiveData);
        when(profileManager.getCandidateProfile()).thenReturn(candidateProfileLiveData);
    }

    /**
     * Preconditions
     */

    @Override
    public void profileFromManager(Event<Profile> event) {
        profileLiveData.postValue(event);
    }

    @Override
    public void candidateProfileFromManager(Event<Profile> event) {
        candidateProfileLiveData.postValue(event);
    }

}
