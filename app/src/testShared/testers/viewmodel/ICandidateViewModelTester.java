package testers.viewmodel;

import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.List;

public interface ICandidateViewModelTester {
    void isVisibleFromManager(boolean isVisible);

    void candidateListFromManager(Event<List<Candidate>> event);

    void evaluationFromManager(Event<Evaluation> event);
}
