package testers.viewmodel;

import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.manager.ChatManager;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;
import com.aleixpellisa.mdpa.workouter.view.model.ChatViewModel;

import java.util.List;

abstract class ChatViewModelTester implements IChatViewModelTester {

    ChatViewModel chatViewModel;

    ChatManager chatManager;

    EventLiveData<Event<List<Chat>>> chatListLiveData;

    EventLiveData<Event<List<Message>>> chatMessageListLiveData;

    EventLiveData<Event<Message>> chatMessageLiveData;

    /**
     * Preconditions
     */

    public void setCurrentChatId(String currentChatId) {
        chatViewModel.setCurrentChatId(currentChatId);
    }

}
