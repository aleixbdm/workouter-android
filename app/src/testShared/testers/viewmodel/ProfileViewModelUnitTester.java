package testers.viewmodel;

import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;

import javax.inject.Inject;

import static org.mockito.Mockito.when;

public class ProfileViewModelUnitTester extends ProfileViewModelTester {

    @Inject
    ProfileViewModelUnitTester(ProfileManager profileManager,
                               EventLiveData<Event<Profile>> profileLiveData,
                               EventLiveData<Event<Profile>> candidateProfileLiveData) {
        this.profileManager = profileManager;
        this.profileLiveData = profileLiveData;
        this.candidateProfileLiveData = candidateProfileLiveData;
    }

    /**
     * Preconditions
     */

    @Override
    public void profileFromManager(Event<Profile> event) {
        when(profileLiveData.getValue()).thenReturn(event);
        when(profileManager.getProfile()).thenReturn(profileLiveData);
    }

    @Override
    public void candidateProfileFromManager(Event<Profile> event) {
        when(candidateProfileLiveData.getValue()).thenReturn(event);
        when(profileManager.getCandidateProfile()).thenReturn(candidateProfileLiveData);
    }
}
