package testers.network;

import com.aleixpellisa.mdpa.workouter.builder.AuthBuilder;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.network.callback.RetrofitCallback;
import com.aleixpellisa.mdpa.workouter.network.response.AuthResponse;
import com.aleixpellisa.mdpa.workouter.network.rest.impl.AuthRestRetrofit;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;


public class AuthRestTester extends RestTester {

    private AuthRestRetrofit retrofit;

    public AuthRestTester(AuthRestRetrofit retrofit) {
        this.retrofit = retrofit;
    }

    /**
     * Callback responses
     */

    public void loginReturnsAuth(Event<Auth> event) {
        makeCallbackReturnAuth(retrofit.getLoginCallback(), event);
    }

    public void facebookLoginReturnsAuth(Event<Auth> event) {
        makeCallbackReturnAuth(retrofit.getFacebookLoginCallback(), event);
    }

    public void createAccountReturnsAuth(Event<Auth> event) {
        makeCallbackReturnAuth(retrofit.getRegisterCallback(), event);
    }

    public void refreshTokenReturnsAuth(Event<Auth> event) {
        makeCallbackReturnAuth(retrofit.getRefreshCallback(), event);
    }

    private void makeCallbackReturnAuth(RetrofitCallback<AuthResponse, Auth> callback, Event<Auth> event) {
        switch (event.status) {
            case Event.SUCCESS:
                Auth auth = event.data;
                AuthResponse response = new AuthResponse();
                response.authSchema = new AuthBuilder(auth).buildSchema();
                callback.onSuccess(response);
                break;
            case Event.ERROR:
            case Event.AUTH_ERROR:
                callback.onError(event.meetDayError);
                break;
        }
    }

}
