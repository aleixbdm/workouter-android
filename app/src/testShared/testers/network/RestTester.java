package testers.network;

import android.arch.lifecycle.Observer;

import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import org.mockito.ArgumentCaptor;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class RestTester {

    /**
     * Event Observer
     */

    @SuppressWarnings("unchecked")
    public <T> Event<T> captureEventFromResponseObserver(Observer<Event<T>> responseObserver) {
        ArgumentCaptor<Event<T>> argument = ArgumentCaptor.forClass(Event.class);
        verify(responseObserver, times(1)).onChanged(argument.capture());
        return argument.getValue();
    }

}
