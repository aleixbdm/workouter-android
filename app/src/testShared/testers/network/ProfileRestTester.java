package testers.network;

import com.aleixpellisa.mdpa.workouter.builder.ProfileBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.network.callback.RetrofitCallback;
import com.aleixpellisa.mdpa.workouter.network.response.ProfileResponse;
import com.aleixpellisa.mdpa.workouter.network.rest.impl.ProfileRestRetrofit;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;


public class ProfileRestTester extends RestTester {

    private ProfileRestRetrofit retrofit;

    public ProfileRestTester(ProfileRestRetrofit retrofit) {
        this.retrofit = retrofit;
    }

    /**
     * Callback responses
     */

    public void fetchProfileReturnsProfile(Event<Profile> event) {
        makeCallbackReturnProfile(retrofit.getFetchProfileCallback(), event);
    }

    public void createProfileReturnsProfile(Event<Profile> event) {
        makeCallbackReturnProfile(retrofit.getCreateProfileCallback(), event);
    }

    public void updateProfileReturnsProfile(Event<Profile> event) {
        makeCallbackReturnProfile(retrofit.getUpdateProfileCallback(), event);
    }

    public void updateSettingsReturnsProfile(Event<Profile> event) {
        makeCallbackReturnProfile(retrofit.getUpdateSettingsCallback(), event);
    }

    public void deleteProfileReturnsProfile(Event<Profile> event) {
        makeCallbackReturnProfile(retrofit.getDeleteProfileCallback(), event);
    }

    public void fetchCandidateProfileReturnsProfile(Event<Profile> event) {
        makeCallbackReturnProfile(retrofit.getFetchCandidateProfileCallback(), event);
    }

    private void makeCallbackReturnProfile(RetrofitCallback<ProfileResponse, Profile> callback, Event<Profile> event) {
        switch (event.status) {
            case Event.SUCCESS:
                Profile profile = event.data;
                ProfileResponse response = new ProfileResponse();
                response.profileSchema = new ProfileBuilder(profile).buildSchema();
                callback.onSuccess(response);
                break;
            case Event.ERROR:
            case Event.AUTH_ERROR:
                callback.onError(event.meetDayError);
                break;
        }
    }

}
