package testers.network;

import com.aleixpellisa.mdpa.workouter.builder.ChatBuilder;
import com.aleixpellisa.mdpa.workouter.builder.MessageBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.network.callback.RetrofitCallback;
import com.aleixpellisa.mdpa.workouter.network.response.ChatResponse;
import com.aleixpellisa.mdpa.workouter.network.response.MessageResponse;
import com.aleixpellisa.mdpa.workouter.network.rest.impl.ChatRestRetrofit;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.ArrayList;
import java.util.List;


public class ChatRestTester extends RestTester {

    private ChatRestRetrofit retrofit;

    public ChatRestTester(ChatRestRetrofit retrofit) {
        this.retrofit = retrofit;
    }

    /**
     * Callback responses
     */

    public void fetchChatsReturnsChats(Event<List<Chat>> event) {
        makeCallbackReturnChats(retrofit.getGetChatsCallback(), event);
    }

    public void fetchChatMessagesReturnsChatMessages(Event<List<Message>> event) {
        makeCallbackReturnChatMessages(retrofit.getGetChatMessagesCallback(), event);
    }

    public void sendMessageReturnsMessage(Event<Message> event) {
        makeCallbackReturnChatMessage(retrofit.getSendMessageCallback(), event);
    }

    private void makeCallbackReturnChats(RetrofitCallback<List<ChatResponse>, List<Chat>> callback, Event<List<Chat>> event) {
        switch (event.status) {
            case Event.SUCCESS:
                List<Chat> chats = event.data;
                List<ChatResponse> response = new ArrayList<>();
                for (Chat chat : chats) {
                    ChatResponse chatResponse = new ChatResponse();
                    chatResponse.chatSchema = new ChatBuilder(chat).buildSchema();
                    response.add(chatResponse);
                }
                callback.onSuccess(response);
                break;
            case Event.ERROR:
            case Event.AUTH_ERROR:
                callback.onError(event.meetDayError);
                break;
        }
    }

    private void makeCallbackReturnChatMessages(RetrofitCallback<List<MessageResponse>, List<Message>> callback, Event<List<Message>> event) {
        switch (event.status) {
            case Event.SUCCESS:
                List<Message> messages = event.data;
                List<MessageResponse> response = new ArrayList<>();
                for (Message message : messages) {
                    MessageResponse messageResponse = new MessageResponse();
                    messageResponse.messageSchema = new MessageBuilder(message).buildSchema();
                    response.add(messageResponse);
                }
                callback.onSuccess(response);
                break;
            case Event.ERROR:
            case Event.AUTH_ERROR:
                callback.onError(event.meetDayError);
                break;
        }
    }

    private void makeCallbackReturnChatMessage(RetrofitCallback<MessageResponse, Message> callback, Event<Message> event) {
        switch (event.status) {
            case Event.SUCCESS:
                Message message = event.data;
                MessageResponse response = new MessageResponse();
                response.messageSchema = new MessageBuilder(message).buildSchema();
                callback.onSuccess(response);
                break;
            case Event.ERROR:
            case Event.AUTH_ERROR:
                callback.onError(event.meetDayError);
                break;
        }
    }

}
