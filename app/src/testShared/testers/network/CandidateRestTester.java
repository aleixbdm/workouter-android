package testers.network;

import com.aleixpellisa.mdpa.workouter.builder.CandidateBuilder;
import com.aleixpellisa.mdpa.workouter.builder.EvaluationBuilder;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.network.callback.RetrofitCallback;
import com.aleixpellisa.mdpa.workouter.network.response.CandidateResponse;
import com.aleixpellisa.mdpa.workouter.network.response.EvaluationResponse;
import com.aleixpellisa.mdpa.workouter.network.rest.impl.CandidateRestRetrofit;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.ArrayList;
import java.util.List;


public class CandidateRestTester extends RestTester {

    private CandidateRestRetrofit retrofit;

    public CandidateRestTester(CandidateRestRetrofit retrofit) {
        this.retrofit = retrofit;
    }

    /**
     * Callback responses
     */

    public void fetchCandidatesReturnsCandidates(Event<List<Candidate>> event) {
        makeCallbackReturnCandidates(retrofit.getDiscoverCallback(), event);
    }

    public void sendEvaluationReturnsEvaluation(Event<Evaluation> event) {
        makeCallbackReturnEvaluation(retrofit.getEvaluateCallback(), event);
    }

    private void makeCallbackReturnCandidates(RetrofitCallback<List<CandidateResponse>, List<Candidate>> callback, Event<List<Candidate>> event) {
        switch (event.status) {
            case Event.SUCCESS:
                List<Candidate> candidates = event.data;
                List<CandidateResponse> response = new ArrayList<>();
                for (Candidate candidate : candidates) {
                    CandidateResponse candidateResponse = new CandidateResponse();
                    candidateResponse.candidateSchema = new CandidateBuilder(candidate).buildSchema();
                    response.add(candidateResponse);
                }
                callback.onSuccess(response);
                break;
            case Event.ERROR:
            case Event.AUTH_ERROR:
                callback.onError(event.meetDayError);
                break;
        }
    }

    private void makeCallbackReturnEvaluation(RetrofitCallback<EvaluationResponse, Evaluation> callback, Event<Evaluation> event) {
        switch (event.status) {
            case Event.SUCCESS:
                Evaluation evaluation = event.data;
                EvaluationResponse response = new EvaluationResponse();
                response.evaluationSchema = new EvaluationBuilder(evaluation).buildSchema();
                callback.onSuccess(response);
                break;
            case Event.ERROR:
            case Event.AUTH_ERROR:
                callback.onError(event.meetDayError);
                break;
        }
    }

}
