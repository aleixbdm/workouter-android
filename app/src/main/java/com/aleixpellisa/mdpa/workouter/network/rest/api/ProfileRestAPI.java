package com.aleixpellisa.mdpa.workouter.network.rest.api;

import com.aleixpellisa.mdpa.workouter.network.request.CreateProfileRequest;
import com.aleixpellisa.mdpa.workouter.network.request.UpdateProfileRequest;
import com.aleixpellisa.mdpa.workouter.network.request.UpdateSettingsRequest;
import com.aleixpellisa.mdpa.workouter.network.response.ProfileResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ProfileRestAPI {
    @GET("profile")
    Call<ProfileResponse> getProfile();

    @PUT("profile/basic")
    @Headers("Content-Type: application/json")
    Call<ProfileResponse> createProfile(@Body CreateProfileRequest createProfileRequest);

    @PUT("profile")
    @Headers("Content-Type: application/json")
    Call<ProfileResponse> updateProfile(@Body UpdateProfileRequest updateProfileRequest);

    @PATCH("profile/settings")
    Call<ProfileResponse> updateSettings(@Body UpdateSettingsRequest updateSettingsRequest);

    @DELETE("profile")
    Call<ProfileResponse> deleteProfile();

    @GET("users/{candidateId}")
    Call<ProfileResponse> getCandidateProfile(@Path("candidateId") String candidateId);
}
