package com.aleixpellisa.mdpa.workouter.network.rest;

import android.arch.lifecycle.Observer;

import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesKey;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.List;

public interface ProfileRest {
    void fetchProfile(Observer<Event<Profile>> profileObserver);

    void createProfile(String name, String firstName, List<String> imageUrls, Integer age,
                       String gender, @Settings.Preference String preference, String city,
                       final Observer<Event<Profile>> responseObserver);

    void updateProfile(Profile profile, Observer<Event<Profile>> profileObserver);

    void updateSettings(@PreferencesKey String key, Object value, Observer<Event<Profile>> profileObserver);

    void deleteProfile(Observer<Event<Profile>> profileObserver);

    void fetchCandidateProfile(String candidateId, Observer<Event<Profile>> profileObserver);
}
