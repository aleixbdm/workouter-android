package com.aleixpellisa.mdpa.workouter.builder;

import android.content.Context;
import android.content.res.Resources;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.network.schema.MessageSchema;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MessageBuilder extends BaseBuilder {

    public static final List<MessageSchema> MESSAGE_SCHEMA_LIST_MOCK = new ArrayList<MessageSchema>() {{
        MessageSchema messageSchema1 = new MessageSchema();
        messageSchema1.id = "MessageId1";
        messageSchema1.chatId = "ChatId1";
        messageSchema1.senderUserId = "SenderId1";
        messageSchema1.messageText = "Text1";
        messageSchema1.createdAt = Calendar.getInstance().getTimeInMillis();
        add(messageSchema1);

        MessageSchema messageSchema2 = new MessageSchema();
        messageSchema2.id = "MessageId2";
        messageSchema2.chatId = "ChatId2";
        messageSchema2.senderUserId = "SenderId2";
        messageSchema2.messageText = "Text2";
        messageSchema2.createdAt = Calendar.getInstance().getTimeInMillis();
        add(messageSchema2);
    }};

    private List<MessageSchema> messageSchemaList;

    private String id;
    private String chatId;
    private String senderUserId;
    private String messageText;
    private Long createdAt;

    public MessageBuilder() {
        messageSchemaList = new ArrayList<>();
    }

    public MessageBuilder(List<MessageSchema> messageSchemaList) {
        this.messageSchemaList = messageSchemaList;
    }

    public MessageBuilder(MessageSchema messageSchema) {
        if (messageSchema == null) return;
        this.id = messageSchema.id;
        this.chatId = messageSchema.chatId;
        this.senderUserId = messageSchema.senderUserId;
        this.messageText = messageSchema.messageText;
        this.createdAt = messageSchema.createdAt;
    }

    public MessageBuilder(Resources resources) {
        super.setResources(resources);
        messageSchemaList = this.getSchemaListFromFile(R.raw.messages, MessageSchema.class);
    }

    public MessageBuilder(Message message) {
        if (message == null) return;
        this.id = message.getId();
        this.chatId = message.getChatId();
        this.senderUserId = message.getSenderUserId();
        this.messageText = message.getMessageText();
        this.createdAt = message.getCreatedAt();
    }

    public MessageBuilder(String senderUserId, String messageText) {
        // Ids are not important when constructing message (usually needed for UI).
        // See more at ChatManagerImpl.sendMessage
        this.id = "someMessageId";
        this.chatId = "someChatId";
        this.senderUserId = senderUserId;
        this.messageText = messageText;
        this.createdAt = Calendar.getInstance().getTimeInMillis();
    }

    public Message build() {
        return new Message(id, chatId, senderUserId, messageText, createdAt);
    }

    public MessageSchema buildSchema() {
        Message message = build();
        MessageSchema messageSchema = new MessageSchema();
        messageSchema.id = message.getId();
        messageSchema.senderUserId = message.getSenderUserId();
        messageSchema.messageText = message.getMessageText();
        messageSchema.createdAt = message.getCreatedAt();
        return messageSchema;
    }

    public List<Message> buildList() {
        List<Message> messageList = new ArrayList<>();
        if (messageSchemaList != null) {
            for (MessageSchema messageSchema : messageSchemaList) {
                messageList.add(new Message(
                        messageSchema.id,
                        messageSchema.chatId,
                        messageSchema.senderUserId,
                        messageSchema.messageText,
                        messageSchema.createdAt
                ));
            }
        }
        return messageList;
    }

    public static List<Message> buildEmptyMessageList() {
        MessageBuilder messageBuilder =
                new MessageBuilder();
        return messageBuilder.buildList();
    }

    public static Message buildFirstAppMessage(Context context) {
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder.id = Message.APP_MESSAGE_ID;
        messageBuilder.senderUserId = Message.APP_MESSAGE_ID;
        messageBuilder.messageText = context.getString(R.string.message_app_text);
        messageBuilder.createdAt = Calendar.getInstance().getTimeInMillis();
        return messageBuilder.build();
    }

    public static Message buildDayMessage(Date createdAtDate) {
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder.id = Message.APP_MESSAGE_ID;
        messageBuilder.senderUserId = Message.APP_MESSAGE_ID;
        PrettyTime p = new PrettyTime();
        messageBuilder.messageText = p.format(createdAtDate);
        messageBuilder.createdAt = createdAtDate.getTime();
        return messageBuilder.build();
    }

    public static List<Message> buildMockedMessageList() {
        MessageBuilder messageBuilder =
                new MessageBuilder(MESSAGE_SCHEMA_LIST_MOCK);
        return messageBuilder.buildList();
    }

}
