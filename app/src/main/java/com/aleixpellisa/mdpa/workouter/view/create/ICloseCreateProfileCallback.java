package com.aleixpellisa.mdpa.workouter.view.create;

public interface ICloseCreateProfileCallback {

    void closeCreateProfile();

}
