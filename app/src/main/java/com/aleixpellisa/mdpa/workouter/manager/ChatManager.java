package com.aleixpellisa.mdpa.workouter.manager;

import android.arch.lifecycle.LiveData;

import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.List;

public interface ChatManager {

    void startObservers();

    LiveData<Event<List<Chat>>> getChatList();

    LiveData<Event<List<Message>>> getChatMessageList();

    LiveData<Event<Message>> getChatMessage();

    List<Chat> getChatListFiltered(List<Chat> chatList, String filter);

    void fetchChats();

    void enterChat(String chatId);

    void fetchChatMessages(String chatId);

    void sendMessage(String chatId, String messageText);

    void leaveChat();

    void clearAll();
}
