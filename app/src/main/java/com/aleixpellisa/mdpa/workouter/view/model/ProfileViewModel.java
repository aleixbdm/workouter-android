package com.aleixpellisa.mdpa.workouter.view.model;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.aleixpellisa.mdpa.workouter.App;
import com.aleixpellisa.mdpa.workouter.builder.ProfileBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.database.entity.Skill;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesKey;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.view.input.ErrorChecker;
import com.aleixpellisa.mdpa.workouter.view.input.MeetDayTextInputType;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.google.common.base.Charsets;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import toothpick.Scope;
import toothpick.Toothpick;

@Singleton
public class ProfileViewModel extends BaseViewModel {

    @Inject
    ProfileManager profileManager;

    @Inject
    public ProfileViewModel(@NonNull Application application) {
        super(application);
        // Supporting tests
        if (application instanceof App) {
            Scope scope = Toothpick.openScopes(getApplication(), this);
            Toothpick.inject(this, scope);
            profileManager.startObservers();
        }
    }

    public Auth getAuth() {
        return profileManager.getAuth();
    }

    public Settings getSettings() {
        return profileManager.getSettings();
    }

    public LiveData<Event<Profile>> getProfile() {
        return profileManager.getProfile();
    }

    public LiveData<Event<Profile>> getCandidateProfile() {
        return profileManager.getCandidateProfile();
    }

    public void attemptLogin(MeetDayTextInputType inputEmail, MeetDayTextInputType inputPassword) throws IllegalArgumentException {
        checkInputErrors(inputEmail);
        checkInputErrors(inputPassword);

        profileManager.login(
                inputEmail.getValue(),
                encryptPassword(inputPassword.getValue())
        );
    }

    public void attemptFacebookLogin(String accessToken) {
        profileManager.facebookLogin(accessToken);
    }

    public void attemptCreateAccount(MeetDayTextInputType inputEmail,
                                     MeetDayTextInputType inputPassword,
                                     MeetDayTextInputType inputConfirmPassword,
                                     boolean acceptedTermsAndConditions) throws IllegalArgumentException {
        checkInputErrors(inputEmail);
        checkInputErrors(inputPassword);
        checkInputErrors(inputConfirmPassword);
        checkMatchesPasswords(inputPassword.getValue(), inputConfirmPassword.getValue());
        checkTermsAndConditions(acceptedTermsAndConditions);

        profileManager.createAccount(
                inputEmail.getValue(),
                encryptPassword(inputPassword.getValue())
        );
    }

    public void fetchProfile() {
        profileManager.fetchProfile();
    }

    public boolean isProfileBasic(Profile profile) {
        return profileManager.isProfileBasic(profile);
    }

    public void fetchCandidateProfile(String candidateId) throws IllegalArgumentException {
        checkCandidateId(candidateId);

        profileManager.fetchCandidateProfile(candidateId);
    }

    public void attemptCreateProfile(String name,
                                     String firstName,
                                     List<String> imageUrls,
                                     int age,
                                     @Profile.Gender String gender,
                                     @Settings.Preference String preference,
                                     String city) throws IllegalArgumentException {
        MeetDayTextInputType meetDayTextInputType = new MeetDayTextInputType();
        meetDayTextInputType.setRequired(true);

        meetDayTextInputType.setValue(name);
        checkInputErrors(meetDayTextInputType);

        meetDayTextInputType.setValue(firstName);
        checkInputErrors(meetDayTextInputType);

        for (String imageUrl : imageUrls) {
            meetDayTextInputType.setValue(imageUrl);
            checkInputErrors(meetDayTextInputType);
        }

        checkAge(age);

        meetDayTextInputType.setValue(gender);
        checkInputErrors(meetDayTextInputType);

        meetDayTextInputType.setValue(preference);
        checkInputErrors(meetDayTextInputType);

        meetDayTextInputType.setValue(city);
        checkInputErrors(meetDayTextInputType);

        profileManager.createProfile(name, firstName,
                imageUrls, age, gender, preference, city);
    }

    public void attemptUpdateProfile(@NonNull Profile currentProfile, List<String> imagesUpdated, MeetDayTextInputType profileNameTextInput,
                                     MeetDayTextInputType profileFirstNameTextInput, int ageUpdated,
                                     String genderUpdated, MeetDayTextInputType profileDescriptionTextInput,
                                     MeetDayTextInputType profileCurrentJobTextInput,
                                     MeetDayTextInputType profileStudiesTextInput,
                                     MeetDayTextInputType profileFavSongTextInput,
                                     List<Skill> skillsUpdated) throws IllegalArgumentException {
        ProfileBuilder profileBuilder = new ProfileBuilder(currentProfile);
        Profile profileToUpdate = profileBuilder.build();

        checkInputErrors(profileNameTextInput);
        checkInputErrors(profileFirstNameTextInput);
        checkInputErrors(profileDescriptionTextInput);
        checkInputErrors(profileCurrentJobTextInput);
        checkInputErrors(profileStudiesTextInput);
        checkInputErrors(profileFavSongTextInput);

        profileToUpdate.setImageUrls(imagesUpdated);
        profileToUpdate.setName(profileNameTextInput.getValue());
        profileToUpdate.setFirstName(profileFirstNameTextInput.getValue());
        profileToUpdate.setAge(ageUpdated);
        profileToUpdate.setGender(genderUpdated);
        profileToUpdate.setDescription(profileDescriptionTextInput.getValue());
        profileToUpdate.setCurrentJob(profileCurrentJobTextInput.getValue());
        profileToUpdate.setStudies(profileStudiesTextInput.getValue());
        profileToUpdate.setFavouriteSong(profileFavSongTextInput.getValue());
        profileToUpdate.setSkills(skillsUpdated);

        profileManager.updateProfile(profileToUpdate);
    }

    public void attemptUpdateSettings(@PreferencesKey String key, Object value) throws IllegalArgumentException {
        checkSettingsValue(value);

        profileManager.updateSettings(key, value);
    }

    public void deleteProfile() {
        profileManager.deleteProfile();
    }

    public void clearAll() {
        profileManager.clearAll();
    }

    /**
     * Checks
     */

    private void checkMatchesPasswords(String password, String passwordToBeConfirmed) {
        ErrorChecker errorChecker = new ErrorChecker();
        if (!errorChecker.checkIfMatchesPasswords(password, passwordToBeConfirmed)) {
            throw new IllegalArgumentException();
        }
    }

    private void checkTermsAndConditions(boolean acceptedTermsAndConditions) throws IllegalArgumentException {
        ErrorChecker errorChecker = new ErrorChecker();
        if (!errorChecker.checkIfAcceptedTerms(acceptedTermsAndConditions)) {
            throw new IllegalArgumentException();
        }
    }

    private String encryptPassword(String password) {
        Hasher hasher = Hashing.sha256().newHasher();
        hasher.putString(password, Charsets.UTF_8);
        return hasher.hash().toString();
    }

    private void checkAge(int age) throws IllegalArgumentException {
        ErrorChecker errorChecker = new ErrorChecker();
        if (!errorChecker.checkIfAgeValueIsCorrect(age)) {
            throw new IllegalArgumentException();
        }
    }

    private void checkCandidateId(String candidateId) throws IllegalArgumentException {
        ErrorChecker errorChecker = new ErrorChecker();
        if (!errorChecker.checkIfCandidateIdIsCorrect(candidateId)) {
            throw new IllegalArgumentException();
        }
    }

    private void checkSettingsValue(Object value) throws IllegalArgumentException {
        ErrorChecker errorChecker = new ErrorChecker();
        if (!errorChecker.checkIfSettingsValueIsCorrect(value)) {
            throw new IllegalArgumentException();
        }
    }

}
