package com.aleixpellisa.mdpa.workouter.builder;

import com.aleixpellisa.mdpa.workouter.database.entity.UserChat;
import com.aleixpellisa.mdpa.workouter.network.schema.UserChatSchema;

public class UserChatBuilder extends BaseBuilder {

    public static final String USER_CHAT_ID_MOCK = "UserChatId1";
    public static final String USER_CHAT_NAME_MOCK = "Name1";
    public static final String USER_CHAT_FIRST_NAME_MOCK = "FirstName1";
    public static final String USER_CHAT_IMAGE_MOCK = "image1.1";

    private String id;
    private String name;
    private String firstName;
    private String imageUrl;

    public UserChatBuilder(String id, String name, String firstName, String imageUrl) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.imageUrl = imageUrl;
    }

    public UserChatBuilder(UserChat userChat) {
        if (userChat == null) return;
        this.id = userChat.getId();
        this.name = userChat.getName();
        this.firstName = userChat.getFirstName();
        this.imageUrl = userChat.getImageUrl();
    }

    public UserChatBuilder(UserChatSchema userChatSchema) {
        this.id = userChatSchema.id;
        this.name = userChatSchema.name;
        this.firstName = userChatSchema.firstName;
        this.imageUrl = userChatSchema.imageUrl;
    }

    public UserChat build() {
        return new UserChat(id, name, firstName, imageUrl);
    }

    public UserChatSchema buildSchema() {
        UserChatSchema userChatSchema = new UserChatSchema();
        userChatSchema.id = id;
        userChatSchema.name = name;
        userChatSchema.firstName = firstName;
        userChatSchema.imageUrl = imageUrl;
        return userChatSchema;
    }

    public static UserChatSchema buildMockedUserChatSchema() {
        UserChatBuilder userChatBuilder =
                new UserChatBuilder(USER_CHAT_ID_MOCK, USER_CHAT_NAME_MOCK, USER_CHAT_FIRST_NAME_MOCK,
                        USER_CHAT_IMAGE_MOCK);
        return userChatBuilder.buildSchema();
    }

}
