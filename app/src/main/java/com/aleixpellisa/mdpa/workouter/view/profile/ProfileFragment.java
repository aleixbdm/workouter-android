package com.aleixpellisa.mdpa.workouter.view.profile;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.view.BaseFragment;
import com.aleixpellisa.mdpa.workouter.view.main.IBottomTabNavigation;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;

public class ProfileFragment extends BaseFragment {

    public static final String TAG = "ProfileFragmentTag";

    ProfileViewModel profileViewModel;

    private ProfileView profileView;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        setupView(view);

        return view;
    }

    @Override
    public void setupView(View view) {
        super.setupView(view);

        initViewModel();

        profileView = new ProfileView(getActivity(), view, profileViewModel, null,
                false, false, false);

        notifyFragmentReady();

        profileViewModel.fetchProfile();
    }

    private void initViewModel() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            profileViewModel = ViewModelProviders.of(activity).get(ProfileViewModel.class);
        }
    }

    private void notifyFragmentReady() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            try {
                IBottomTabNavigation bottomTabNavigationCallback = (IBottomTabNavigation) activity;
                bottomTabNavigationCallback.fragmentReady(TAG);
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement IBottomTabNavigation");
            }
        }
    }

    public void addProfile(Profile profile) {
        profileView.setupProfileView(profile);
    }

}