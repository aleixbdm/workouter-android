package com.aleixpellisa.mdpa.workouter.builder;

import android.content.res.Resources;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Skill;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.network.schema.CandidateSchema;
import com.aleixpellisa.mdpa.workouter.network.schema.SkillSchema;

import java.util.ArrayList;
import java.util.List;

public class CandidateBuilder extends BaseBuilder {

    public static final List<CandidateSchema> CANDIDATE_SCHEMA_LIST_MOCK = new ArrayList<CandidateSchema>() {{
        CandidateSchema candidateSchemaMale = new CandidateSchema();
        candidateSchemaMale.id = "CandidateId1";
        candidateSchemaMale.name = "Male1";
        candidateSchemaMale.imageUrls = new ArrayList<String>() {{
            add("image1.1");
            add("image1.2");
        }};
        candidateSchemaMale.age = 21;
        candidateSchemaMale.skills = new ArrayList<>();
        candidateSchemaMale.city = "City1";
        add(candidateSchemaMale);

        CandidateSchema candidateSchemaFemale = new CandidateSchema();
        candidateSchemaFemale.id = "CandidateId2";
        candidateSchemaFemale.name = "Female2";
        candidateSchemaFemale.imageUrls = new ArrayList<String>() {{
            add("image2.1");
        }};
        candidateSchemaFemale.age = 22;
        candidateSchemaFemale.skills = new ArrayList<SkillSchema>() {{
            for (Skill skill : SkillBuilder.buildMockedSkillList()) {
                add(new SkillBuilder(skill).buildSchema());
            }
        }};
        candidateSchemaFemale.city = "City2";
        add(candidateSchemaFemale);
    }};

    private List<CandidateSchema> candidateSchemaList;

    public String id;
    public String name;
    public List<String> imageUrls;
    public Integer age;
    public List<Skill> skills;
    public String city;

    public CandidateBuilder() {
        candidateSchemaList = new ArrayList<>();
    }

    public CandidateBuilder(List<CandidateSchema> candidateSchemaList) {
        this.candidateSchemaList = candidateSchemaList;
    }

    public CandidateBuilder(Resources resources) {
        super.setResources(resources);
        candidateSchemaList = this.getSchemaListFromFile(R.raw.candidates, CandidateSchema.class);
    }

    public CandidateBuilder(Candidate candidate) {
        if (candidate == null) return;
        this.id = candidate.getId();
        this.name = candidate.getName();
        this.imageUrls = candidate.getImageUrls();
        this.age = candidate.getAge();
        this.skills = candidate.getSkills();
    }

    public CandidateSchema buildSchema() {
        Candidate candidate = build();
        CandidateSchema candidateSchema = new CandidateSchema();
        candidateSchema.id = candidate.getId();
        candidateSchema.name = candidate.getName();
        candidateSchema.imageUrls = candidate.getImageUrls();
        candidateSchema.age = candidate.getAge();
        for (Skill skill : candidate.getSkills()) {
            SkillBuilder skillBuilder = new SkillBuilder(skill);
            if (candidateSchema.skills == null) candidateSchema.skills = new ArrayList<>();
            candidateSchema.skills.add(skillBuilder.buildSchema());
        }
        candidateSchema.city = candidate.getCity();
        return candidateSchema;
    }

    public Candidate build() {
        return new Candidate(id, name, imageUrls, age, skills, city);
    }

    public List<Candidate> buildList() {
        List<Candidate> candidateList = new ArrayList<>();
        if (candidateSchemaList != null) {
            for (CandidateSchema candidateSchema : candidateSchemaList) {
                List<Skill> skills = new ArrayList<>();
                if (candidateSchema.skills != null) {
                    for (SkillSchema skillSchema : candidateSchema.skills) {
                        skills.add(new SkillBuilder(skillSchema).build());
                    }
                }
                candidateList.add(new Candidate(
                        candidateSchema.id,
                        candidateSchema.name,
                        candidateSchema.imageUrls,
                        candidateSchema.age,
                        skills,
                        candidateSchema.city
                ));
            }
        }
        return candidateList;
    }

    public static List<Candidate> buildEmptyCandidateList() {
        CandidateBuilder candidateBuilder =
                new CandidateBuilder();
        return candidateBuilder.buildList();
    }

    public static List<Candidate> buildMockedCandidateList() {
        CandidateBuilder candidateBuilder =
                new CandidateBuilder(CANDIDATE_SCHEMA_LIST_MOCK);
        return candidateBuilder.buildList();
    }

}
