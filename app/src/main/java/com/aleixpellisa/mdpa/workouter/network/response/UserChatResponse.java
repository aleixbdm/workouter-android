package com.aleixpellisa.mdpa.workouter.network.response;

import com.aleixpellisa.mdpa.workouter.network.schema.UserChatSchema;
import com.google.gson.annotations.SerializedName;

public class UserChatResponse extends BaseResponse {
    @SerializedName("user_chat")
    public UserChatSchema userChatSchema;
}
