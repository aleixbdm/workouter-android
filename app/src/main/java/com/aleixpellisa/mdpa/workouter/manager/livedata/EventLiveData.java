package com.aleixpellisa.mdpa.workouter.manager.livedata;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.concurrent.atomic.AtomicBoolean;

public class EventLiveData<T> extends MediatorLiveData<T> {

    private final AtomicBoolean read = new AtomicBoolean(true);

    @MainThread
    public void observe(@NonNull LifecycleOwner owner, @NonNull final Observer<T> observer) {
        super.observe(owner, new Observer<T>() {
            @Override
            public void onChanged(@Nullable T t) {
                if (read.compareAndSet(false, true)) {
                    observer.onChanged(t);
                }
            }
        });
    }

    @MainThread
    public void setValue(@Nullable T t) {
        read.set(false);
        super.setValue(t);
    }

    @MainThread
    public void call() {
        setValue(null);
    }
}
