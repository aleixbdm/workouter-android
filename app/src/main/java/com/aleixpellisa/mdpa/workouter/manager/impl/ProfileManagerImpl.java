package com.aleixpellisa.mdpa.workouter.manager.impl;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.aleixpellisa.mdpa.workouter.database.MeetDayDatabase;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesKey;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesManager;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.manager.observer.EventObserver;
import com.aleixpellisa.mdpa.workouter.manager.refresher.TokenRefresher;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.model.MeetDayError;
import com.aleixpellisa.mdpa.workouter.network.interceptor.AuthTokenInterceptor;
import com.aleixpellisa.mdpa.workouter.network.rest.AuthRest;
import com.aleixpellisa.mdpa.workouter.network.rest.ProfileRest;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;
import com.google.common.annotations.VisibleForTesting;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ProfileManagerImpl implements ProfileManager {

    private PreferencesManager preferencesManager;

    private MeetDayDatabase meetDayDatabase;

    private AuthTokenInterceptor authTokenInterceptor;

    @Inject
    AuthRest authRest;

    @Inject
    ProfileRest profileRest;

    @Inject
    TokenRefresher tokenRefresher;

    private LiveData<Profile> profileDbSource;
    private EventLiveData<Event<Profile>> profileLiveData;
    private EventLiveData<Event<Profile>> candidateProfileLiveData;

    private Observer<Profile> profileDatabaseObserver;
    private EventObserver<Auth, Profile> loginObserver;
    private EventObserver<Auth, Profile> faceBookLoginObserver;
    private EventObserver<Auth, Profile> createAccountObserver;
    private EventObserver<Profile, Profile> fetchProfileObserver;
    private EventObserver<Profile, Profile> createProfileObserver;
    private EventObserver<Profile, Profile> updateProfileObserver;
    private EventObserver<Profile, Profile> updateSettingsObserver;
    private EventObserver<Profile, Profile> deleteProfileObserver;
    private EventObserver<Profile, Profile> fetchCandidateProfileObserver;

    @Inject
    ProfileManagerImpl(MeetDayDatabase meetDayDatabase, PreferencesManager preferencesManager,
                       AuthTokenInterceptor authTokenInterceptor) {
        this.meetDayDatabase = meetDayDatabase;
        this.preferencesManager = preferencesManager;
        this.authTokenInterceptor = authTokenInterceptor;
        this.profileLiveData = new EventLiveData<>();
        this.candidateProfileLiveData = new EventLiveData<>();

        Auth auth = this.getAuth();
        if (auth != null) {
            this.authTokenInterceptor.setAccessToken(auth.getAccessToken());
        }
    }

    @Override
    public void startObservers() {
        profileDbSource = meetDayDatabase.getProfileDao().getProfile();
        profileDatabaseObserver = new Observer<Profile>() {
            @Override
            public void onChanged(@Nullable Profile profile) {
                if (profile != null) {
                    profileLiveData.setValue(Event.success(profile));
                } else {
                    Event<Profile> profileEvent = Event.error(new MeetDayError("database profile empty"));
                    profileLiveData.setValue(profileEvent);
                }
            }
        };
        profileLiveData.addSource(profileDbSource, profileDatabaseObserver);
    }

    @Override
    public Auth getAuth() {
        // retrieve from preferences
        return preferencesManager.readAuth();
    }

    @Override
    public Settings getSettings() {
        // retrieve from preferences
        return preferencesManager.readSettings();
    }

    @Override
    public LiveData<Event<Profile>> getProfile() {
        return profileLiveData;
    }

    @Override
    public LiveData<Event<Profile>> getCandidateProfile() {
        return candidateProfileLiveData;
    }

    @Override
    public void login(final String email, String password) {
        loginObserver = new EventObserver<Auth, Profile>(null, profileLiveData) {
            @Override
            public void onSuccess(Event<Auth> event) {
                saveAuth(event.data);

                // Fetch profile
                fetchProfile();
            }
        };
        authRest.login(email, password, loginObserver);
    }

    @Override
    public void facebookLogin(String accessToken) {
        faceBookLoginObserver = new EventObserver<Auth, Profile>(null, profileLiveData) {
            @Override
            public void onSuccess(Event<Auth> event) {
                saveAuth(event.data);

                // Fetch profile
                fetchProfile();
            }
        };
        authRest.facebookLogin(accessToken, faceBookLoginObserver);
    }

    @Override
    public void createAccount(String email, String password) {
        createAccountObserver = new EventObserver<Auth, Profile>(null, profileLiveData) {
            @Override
            public void onSuccess(Event<Auth> event) {
                saveAuth(event.data);

                // Fetch profile
                fetchProfile();
            }
        };
        authRest.createAccount(email, password, createAccountObserver);
    }

    @Override
    public void fetchProfile() {
        Callable refreshToken = tokenRefresher.refreshTokenCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                fetchProfile();
                return null;
            }
        }, profileLiveData);
        fetchProfileObserver = new EventObserver<Profile, Profile>
                (refreshToken, profileLiveData) {
            @Override
            public void onSuccess(Event<Profile> event) {
                saveProfile(event.data);
            }
        };
        profileRest.fetchProfile(fetchProfileObserver);
    }

    @Override
    public boolean isProfileBasic(Profile profile) {
        return profile == null || profile.getSkills() == null || profile.getSkills().isEmpty();
    }

    @Override
    public void createProfile(final String name, final String firstName, final List<String> imageUrls,
                              final Integer age, final @Profile.Gender String gender,
                              final @Settings.Preference String preference, final String city) {
        Callable refreshToken = tokenRefresher.refreshTokenCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                createProfile(name, firstName,
                        imageUrls, age, gender, preference, city);
                return null;
            }
        }, profileLiveData);
        createProfileObserver = new EventObserver<Profile, Profile>
                (refreshToken, profileLiveData) {
            @Override
            public void onSuccess(Event<Profile> event) {
                saveProfile(event.data);
            }
        };
        profileRest.createProfile(name, firstName,
                imageUrls, age, gender, preference, city, createProfileObserver);
    }

    @Override
    public void updateProfile(final Profile profile) {
        Callable refreshToken = tokenRefresher.refreshTokenCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                updateProfile(profile);
                return null;
            }
        }, profileLiveData);
        updateProfileObserver = new EventObserver<Profile, Profile>
                (refreshToken, profileLiveData) {
            @Override
            public void onSuccess(Event<Profile> event) {
                saveProfile(event.data);
            }
        };
        profileRest.updateProfile(profile, updateProfileObserver);
    }

    @Override
    public void updateSettings(final @PreferencesKey String key, final Object value) {
        Callable refreshToken = tokenRefresher.refreshTokenCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                updateSettings(key, value);
                return null;
            }
        }, profileLiveData);
        updateSettingsObserver = new EventObserver<Profile, Profile>
                (refreshToken, profileLiveData) {
            @Override
            public void onSuccess(Event<Profile> event) {
                saveProfile(event.data);
            }
        };
        profileRest.updateSettings(key, value, updateSettingsObserver);
    }

    @Override
    public void deleteProfile() {
        Callable refreshToken = tokenRefresher.refreshTokenCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                deleteProfile();
                return null;
            }
        }, profileLiveData);
        deleteProfileObserver = new EventObserver<Profile, Profile>
                (refreshToken, profileLiveData) {
            @Override
            public void onSuccess(Event<Profile> event) {
                // Sync profile
                profileLiveData.postValue(event);
            }
        };
        profileRest.deleteProfile(deleteProfileObserver);
    }

    @Override
    public void fetchCandidateProfile(final String candidateId) {
        Callable refreshToken = tokenRefresher.refreshTokenCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                fetchCandidateProfile(candidateId);
                return null;
            }
        }, candidateProfileLiveData);
        fetchCandidateProfileObserver = new EventObserver<Profile, Profile>
                (refreshToken, candidateProfileLiveData) {
            @Override
            public void onSuccess(Event<Profile> event) {
                // Sync candidate profile
                candidateProfileLiveData.postValue(event);
            }
        };
        profileRest.fetchCandidateProfile(candidateId, fetchCandidateProfileObserver);
    }

    @Override
    public void clearAll() {
        profileLiveData.removeSource(profileDbSource);
        preferencesManager.clearAll();
        authTokenInterceptor.setAccessToken(null);
        DeleteDatabase deleteDatabase = new DeleteDatabase();
        deleteDatabase.setDatabaseReference(meetDayDatabase);
        deleteDatabase.execute();
    }

    private void saveAuth(@NonNull Auth auth) {
        preferencesManager.saveAuth(auth);
        authTokenInterceptor.setAccessToken(auth.getAccessToken());
    }

    private void saveProfile(@NonNull final Profile profile) {
        // Save settings
        preferencesManager.saveSettings(profile.getSettings());

        // Save profile
        new Thread(new Runnable() {
            public void run() {
                meetDayDatabase.getProfileDao().refreshDatabase(profile);
            }
        }).start();
    }

    /**
     * AsyncTask section
     */

    private static class DeleteDatabase extends AsyncTask<Void, Void, Void> {

        private MeetDayDatabase databaseReference;

        @Override
        protected Void doInBackground(Void... voids) {
            databaseReference.getProfileDao().deleteAll();
            return null;
        }

        void setDatabaseReference(MeetDayDatabase databaseReference) {
            this.databaseReference = databaseReference;
        }
    }

    /**
     * Testing section
     */

    @VisibleForTesting
    public Observer<Profile> getProfileDatabaseObserver() {
        return profileDatabaseObserver;
    }

    @VisibleForTesting
    public EventObserver<Auth, Profile> getLoginObserver() {
        return loginObserver;
    }

    @VisibleForTesting
    public EventObserver<Auth, Profile> getFaceBookLoginObserver() {
        return faceBookLoginObserver;
    }

    @VisibleForTesting
    public EventObserver<Auth, Profile> getCreateAccountObserver() {
        return createAccountObserver;
    }

    @VisibleForTesting
    public EventObserver<Profile, Profile> getFetchProfileObserver() {
        return fetchProfileObserver;
    }

    @VisibleForTesting
    public EventObserver<Profile, Profile> getCreateProfileObserver() {
        return createProfileObserver;
    }

    @VisibleForTesting
    public EventObserver<Profile, Profile> getUpdateProfileObserver() {
        return updateProfileObserver;
    }

    @VisibleForTesting
    public EventObserver<Profile, Profile> getUpdateSettingsObserver() {
        return updateSettingsObserver;
    }

    @VisibleForTesting
    public EventObserver<Profile, Profile> getDeleteProfileObserver() {
        return deleteProfileObserver;
    }

    @VisibleForTesting
    public EventObserver<Profile, Profile> getFetchCandidateProfileObserver() {
        return fetchCandidateProfileObserver;
    }
}
