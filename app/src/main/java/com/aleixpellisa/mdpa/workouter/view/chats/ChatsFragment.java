package com.aleixpellisa.mdpa.workouter.view.chats;

import android.app.SearchManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.view.BaseFragment;
import com.aleixpellisa.mdpa.workouter.view.main.IBottomTabNavigation;
import com.aleixpellisa.mdpa.workouter.view.model.ChatViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ChatsFragment extends BaseFragment {

    public static final String TAG = "ChatsFragmentTag";

    ChatViewModel chatViewModel;

    @BindView(R.id.chats_fragment_chats_list)
    RecyclerView chatRecyclerView;

    private ChatListAdapter chatListAdapter;

    public static ChatsFragment newInstance() {
        return new ChatsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chats, container, false);

        setupView(view);

        return view;
    }

    @Override
    public void setupView(View view) {
        super.setupView(view);

        initViewModel();

        chatListAdapter = new ChatListAdapter(getContext(), new ArrayList<Chat>());
        chatRecyclerView.setAdapter(chatListAdapter);
        chatRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        notifyFragmentReady();

        chatViewModel.fetchChats();
    }

    private void initViewModel() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            chatViewModel = ViewModelProviders.of(activity).get(ChatViewModel.class);
        }
    }

    private void notifyFragmentReady() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            try {
                IBottomTabNavigation bottomTabNavigationCallback = (IBottomTabNavigation) activity;
                bottomTabNavigationCallback.fragmentReady(TAG);
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement IBottomTabNavigation");
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.chats_fragment_search_menu, menu);

        setupMenu(menu);

    }

    private void setupMenu(Menu menu) {
        if (getActivity() != null) {
            SearchManager searchManager =
                    (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            if (searchManager != null) {
                SearchView searchView =
                        (SearchView) menu.findItem(R.id.chats_fragment_search_bar).getActionView();
                searchView.setSearchableInfo(
                        searchManager.getSearchableInfo(getActivity().getComponentName()));
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        // do your search
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        // do your search on change or save the last string or...
                        chatViewModel.applyChatsFilter(s);
                        return false;
                    }
                });
            }
        }
    }

    public void addChats(List<Chat> chatList) {
        if (chatListAdapter != null) {
            chatListAdapter.setValues(chatList);
            chatListAdapter.notifyDataSetChanged();
        }
    }

}