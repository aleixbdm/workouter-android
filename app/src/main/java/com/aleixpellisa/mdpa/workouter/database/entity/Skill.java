package com.aleixpellisa.mdpa.workouter.database.entity;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Skill {

    public static final String SKILL_SPORTS = "sports";
    public static final String SKILL_GEEK = "geek";
    public static final String SKILL_DANCE = "dance";
    public static final String SKILL_TRAVEL = "travel";
    public static final String SKILL_PETS = "pets";
    public static final String SKILL_GREEN = "green";

    public static final int MIN_VALUE = 0;
    public static final int MAX_VALUE = 2;

    @StringDef({SKILL_SPORTS, SKILL_GEEK, SKILL_DANCE, SKILL_TRAVEL, SKILL_PETS, SKILL_GREEN })
    @Retention(RetentionPolicy.SOURCE)
    public @interface Category {
    }

    private Integer value;

    private @Category
    String category;

    public Skill(Integer value, String category) {
        this.value = value;
        this.category = category;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
