package com.aleixpellisa.mdpa.workouter.network.response;

import com.aleixpellisa.mdpa.workouter.network.schema.CandidateSchema;
import com.google.gson.annotations.SerializedName;

public class CandidateResponse extends BaseResponse {
    @SerializedName("candidate")
    public CandidateSchema candidateSchema;
}
