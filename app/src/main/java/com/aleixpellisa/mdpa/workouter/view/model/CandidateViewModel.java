package com.aleixpellisa.mdpa.workouter.view.model;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.aleixpellisa.mdpa.workouter.App;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.manager.CandidateManager;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.view.input.ErrorChecker;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import toothpick.Scope;
import toothpick.Toothpick;

@Singleton
public class CandidateViewModel extends BaseViewModel {

    @Inject
    ProfileManager profileManager;

    @Inject
    CandidateManager candidateManager;

    @Inject
    public CandidateViewModel(@NonNull Application application) {
        super(application);
        // Supporting tests
        if (application instanceof App) {
            Scope scope = Toothpick.openScopes(getApplication(), this);
            Toothpick.inject(this, scope);
        }
    }

    public LiveData<Event<List<Candidate>>> getCandidateList() {
        return candidateManager.getCandidateList();
    }

    public LiveData<Event<Evaluation>> getEvaluation() {
        return candidateManager.getEvaluation();
    }

    public void attemptDownloadCandidates() throws IllegalAccessException {
        Settings settings = profileManager.getSettings();
        Boolean value = settings != null ? settings.isVisible() : null;
        boolean isVisible = value != null ? value : false;
        checkIsVisible(isVisible);

        candidateManager.fetchCandidates();
    }

    public void sendEvaluation(String candidateId, @Evaluation.EvaluationValue String evaluation) {
        candidateManager.sendEvaluation(candidateId, evaluation);
    }

    private void checkIsVisible(boolean isVisible) throws IllegalAccessException {
        ErrorChecker errorChecker = new ErrorChecker();
        if (!errorChecker.checkIfIsVisible(isVisible)) {
            throw new IllegalAccessException();
        }
    }

    public void clearAll() {
        candidateManager.clearAll();
    }

}
