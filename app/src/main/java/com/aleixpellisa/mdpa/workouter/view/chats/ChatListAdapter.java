package com.aleixpellisa.mdpa.workouter.view.chats;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.UserChat;
import com.aleixpellisa.mdpa.workouter.view.main.IBottomTabNavigation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> {

    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layout;
        ImageView image;
        TextView userName;
        TextView message;
        TextView timestamp;
        ImageView indicatorImage;
        TextView indicatorNumber;

        ViewHolder(View itemView) {
            super(itemView);

            layout = itemView.findViewById(R.id.chat_item_layout);
            image = itemView.findViewById(R.id.chat_item_image);
            userName = itemView.findViewById(R.id.chat_item_user_name);
            message = itemView.findViewById(R.id.chat_item_message);
            timestamp = itemView.findViewById(R.id.chat_item_timestamp);
            indicatorImage = itemView.findViewById(R.id.chat_item_indicator_image);
            indicatorNumber = itemView.findViewById(R.id.chat_item_indicator_number);

        }
    }

    private final Context context;
    private List<Chat> values;

    private IBottomTabNavigation bottomTabNavigationCallback;

    ChatListAdapter(Context context, List<Chat> values) {
        this.context = context;
        this.values = values;

        try {
            bottomTabNavigationCallback = (IBottomTabNavigation) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IBottomTabNavigation");
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.chat_item_layout, parent, false);

        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Chat chat = values.get(position);

        if (position % 2 == 1) {
            holder.layout.setBackground(context.getResources().getDrawable(R.drawable.chat_item_light_selector));
        } else {
            holder.layout.setBackground(context.getResources().getDrawable(R.drawable.chat_item_transparent_selector));
        }

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomTabNavigationCallback.goToChat(chat);
            }
        });

        UserChat userChat = chat.getUserChat();
        if (userChat != null) {
            Glide.with(context).load(userChat.getImageUrl())
                    .apply(RequestOptions.circleCropTransform())
                    .into(holder.image);

            holder.userName.setText(userChat.getName());
        }
        holder.message.setText(chat.getLastMessage());
        holder.timestamp.setText(chat.getCurrentFormattedDate());

        /* if (chat.isIndicatorImage()) {
            holder.indicatorImage.setVisibility(View.VISIBLE);
        } else {
            holder.indicatorImage.setVisibility(View.GONE);
        }

        if (chat.getIndicatorNumber() > 0) {
            holder.indicatorNumber.setVisibility(View.VISIBLE);
            holder.indicatorNumber.setText(String.valueOf(chat.getIndicatorNumber()));
        } else {
            holder.indicatorNumber.setVisibility(View.GONE);
        } */
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    void setValues(List<Chat> values) {
        this.values = values;
    }

}
