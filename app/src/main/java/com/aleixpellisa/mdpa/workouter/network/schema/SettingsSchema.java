package com.aleixpellisa.mdpa.workouter.network.schema;

import com.google.gson.annotations.SerializedName;

public class SettingsSchema extends BaseSchema {

    @SerializedName("preference")
    public String preference;

    @SerializedName("city")
    public String city;

    @SerializedName("search_range")
    public Integer searchRange;

    @SerializedName("year_range_min")
    public Integer yearRangeMin;

    @SerializedName("year_range_max")
    public Integer yearRangeMax;

    @SerializedName("visible")
    public Boolean visible;

}
