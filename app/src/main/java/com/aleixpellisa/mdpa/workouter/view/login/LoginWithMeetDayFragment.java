package com.aleixpellisa.mdpa.workouter.view.login;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.view.dialog.DialogHandler;
import com.aleixpellisa.mdpa.workouter.view.input.MeetDayTextInputLayout;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginWithMeetDayFragment extends LoginBaseFragment {

    static final String TAG = "LoginWithMeetDayFragmentTag";

    @Inject
    DialogHandler dialogHandler;

    ProfileViewModel profileViewModel;

    @BindView(R.id.login_with_md_email_text_input)
    TextInputLayout emailTextInput;
    @BindView(R.id.login_with_md_password_text_input)
    TextInputLayout passwordTextInput;

    private MeetDayTextInputLayout meetDayEmailTextInput;
    private MeetDayTextInputLayout meetDayPasswordTextInput;

    public static LoginWithMeetDayFragment newInstance() {
        return new LoginWithMeetDayFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_with_meetday, container, false);

        setupView(view);

        return view;
    }

    @Override
    public void setupView(View view) {
        super.setupView(view);

        initViewModel();

        meetDayEmailTextInput = new MeetDayTextInputLayout(emailTextInput);
        meetDayPasswordTextInput = new MeetDayTextInputLayout(passwordTextInput);

        showKeyboard(meetDayEmailTextInput.getEditText());
        meetDayEmailTextInput.initTextInputLayout(
                MeetDayTextInputLayout.LOGIN_CREDENTIALS_EMAIL_FIELD_KEY);

        meetDayPasswordTextInput.initTextInputLayout(
                MeetDayTextInputLayout.LOGIN_CREDENTIALS_PASSWORD_FIELD_KEY);

        meetDayPasswordTextInput.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
    }

    private void initViewModel() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            profileViewModel = ViewModelProviders.of(activity).get(ProfileViewModel.class);
        }
    }

    @OnClick(R.id.login_with_md_back_button)
    public void backButtonClicked() {
        hideKeyboard(getView());
        fragmentToActivityCallback.returnFromLoginWithMD();
    }

    @OnClick(R.id.login_with_md_login_button)
    public void attemptLogin() {
        try {
            dialogHandler.showProgressDialog();

            profileViewModel.attemptLogin(
                    meetDayEmailTextInput.retrieveMeetDayTextInputType(),
                    meetDayPasswordTextInput.retrieveMeetDayTextInputType()
            );
        } catch (IllegalArgumentException ex) {
            dialogHandler.hideProgressDialog();

            meetDayEmailTextInput.showErrorsIfNeeded();
            meetDayPasswordTextInput.showErrorsIfNeeded();
        }
    }


}