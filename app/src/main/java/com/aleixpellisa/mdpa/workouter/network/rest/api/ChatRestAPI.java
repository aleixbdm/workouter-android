package com.aleixpellisa.mdpa.workouter.network.rest.api;

import com.aleixpellisa.mdpa.workouter.network.request.SendMessageRequest;
import com.aleixpellisa.mdpa.workouter.network.response.ChatResponse;
import com.aleixpellisa.mdpa.workouter.network.response.MessageResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ChatRestAPI {
    @GET("chats")
    Call<List<ChatResponse>> getChats();

    @GET("chats/{chatId}/messages")
    Call<List<MessageResponse>> getChatMessages(@Path("chatId") String chatId, @Query("limit") int limit);

    @POST("chats/{chatId}/messages")
    Call<MessageResponse> sendMessage(@Path("chatId") String chatId, @Body SendMessageRequest sendMessageRequest);
}
