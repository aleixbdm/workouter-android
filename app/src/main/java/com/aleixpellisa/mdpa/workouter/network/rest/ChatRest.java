package com.aleixpellisa.mdpa.workouter.network.rest;

import android.arch.lifecycle.Observer;

import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.List;

public interface ChatRest {
    void fetchChats(Observer<Event<List<Chat>>> chatsObserver);

    void fetchChatMessages(String chatId, Observer<Event<List<Message>>> chatMessagesObserver);

    void sendMessage(String chatId, String messageText, Observer<Event<Message>> chatMessageObserver);
}
