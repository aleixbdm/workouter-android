package com.aleixpellisa.mdpa.workouter.builder;

import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.network.schema.CreateProfileSchema;

import java.util.List;

public class BasicProfileBuilder {

    private String name;
    private String firstName;
    private List<String> imageUrls;
    private Integer age;
    private @Profile.Gender
    String gender;
    private @Settings.Preference
    String preference;
    private String city;

    public BasicProfileBuilder(String name, String firstName,
                               List<String> imageUrls, Integer age, @Profile.Gender String gender,
                               @Settings.Preference String preference, String city) {
        this.name = name;
        this.firstName = firstName;
        this.imageUrls = imageUrls;
        this.age = age;
        this.gender = gender;
        this.preference = preference;
        this.city = city;
    }

    public CreateProfileSchema buildSchema() {
        CreateProfileSchema createProfileSchema = new CreateProfileSchema();
        createProfileSchema.name = name;
        createProfileSchema.firstName = firstName;
        createProfileSchema.imageUrls = imageUrls;
        createProfileSchema.age = age;
        createProfileSchema.gender = gender;
        createProfileSchema.preference = preference;
        createProfileSchema.city = city;
        return createProfileSchema;
    }

}
