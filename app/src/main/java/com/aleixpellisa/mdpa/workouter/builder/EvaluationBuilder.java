package com.aleixpellisa.mdpa.workouter.builder;

import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.network.schema.EvaluationSchema;

public class EvaluationBuilder extends BaseBuilder {

    public static final String EVALUATION_ID_MOCK = "id1";
    public static final String EVALUATION_SENDER_USER_ID_MOCK = "senderUserId1";
    public static final String EVALUATION_RECEIVER_USER_ID_MOCK = "receiverUserId1";
    public static final String EVALUATION_EVALUATION_VALUE_MOCK = Evaluation.EVALUATION_LIKE;

    private String id;
    private String senderUserId;
    private String receiverUserId;
    private String evaluationValue;

    public EvaluationBuilder(String id, String senderUserId, String receiverUserId, String evaluationValue) {
        this.id = id;
        this.senderUserId = senderUserId;
        this.receiverUserId = receiverUserId;
        this.evaluationValue = evaluationValue;
    }

    public EvaluationBuilder(EvaluationSchema evaluationSchema) {
        this.id = evaluationSchema.id;
        this.senderUserId = evaluationSchema.senderUserId;
        this.receiverUserId = evaluationSchema.receiverUserId;
        this.evaluationValue = evaluationSchema.evaluationValue;
    }

    public EvaluationBuilder(Evaluation evaluation) {
        this.id = evaluation.getId();
        this.senderUserId = evaluation.getSenderUserId();
        this.receiverUserId = evaluation.getReceiverUserId();
        this.evaluationValue = evaluation.getEvaluationValue();
    }

    public Evaluation build() {
        return new Evaluation(id, senderUserId, receiverUserId, evaluationValue);
    }

    public EvaluationSchema buildSchema() {
        Evaluation evaluation = build();
        EvaluationSchema evaluationSchema = new EvaluationSchema();
        evaluationSchema.id = evaluation.getId();
        evaluationSchema.senderUserId = evaluation.getSenderUserId();
        evaluationSchema.receiverUserId = evaluation.getReceiverUserId();
        evaluationSchema.evaluationValue = evaluation.getEvaluationValue();
        return evaluationSchema;
    }

    public static Evaluation buildMockedEvaluation() {
        EvaluationBuilder evaluationBuilder =
                new EvaluationBuilder(EVALUATION_ID_MOCK, EVALUATION_SENDER_USER_ID_MOCK,
                        EVALUATION_RECEIVER_USER_ID_MOCK, EVALUATION_EVALUATION_VALUE_MOCK);
        return evaluationBuilder.build();
    }

}
