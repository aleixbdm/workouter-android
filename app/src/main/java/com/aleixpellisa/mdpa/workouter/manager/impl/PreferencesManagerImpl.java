package com.aleixpellisa.mdpa.workouter.manager.impl;

import android.content.SharedPreferences;

import com.aleixpellisa.mdpa.workouter.builder.AuthBuilder;
import com.aleixpellisa.mdpa.workouter.builder.SettingsBuilder;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesKey;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesManager;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PreferencesManagerImpl implements PreferencesManager {

    public static final String SHARED_PREFERENCES_NAME = "MeetDayPrefs";

    @Inject
    SharedPreferences sharedPreferences;

    PreferencesManagerImpl() {
    }

    @Override
    public void save(@PreferencesKey String key, Object value) {
        if (value == null) {
            return;
        }
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        switch (key) {
            case Auth.ACCESS_TOKEN:
            case Auth.USER_ID:
            case Settings.PREFERENCE_KEY:
            case Settings.CITY_KEY:
                sharedPreferencesEditor.putString(key, (String) value);
                break;
            case Settings.SEARCH_RANGE_KEY:
            case Settings.YEAR_RANGE_MIN_KEY:
            case Settings.YEAR_RANGE_MAX_KEY:
                sharedPreferencesEditor.putInt(key, (int) value);
                break;
            case Settings.VISIBLE_KEY:
                sharedPreferencesEditor.putBoolean(key, (boolean) value);
                break;
        }
        sharedPreferencesEditor.apply();
    }

    @Override
    public void saveAuth(Auth auth) {
        if (auth == null) {
            return;
        }
        this.save(Auth.ACCESS_TOKEN, auth.getAccessToken());
        this.save(Auth.USER_ID, auth.getUserId());
    }

    @Override
    public void saveSettings(Settings settings) {
        if (settings == null) {
            return;
        }
        this.save(Settings.PREFERENCE_KEY, settings.getPreference());
        this.save(Settings.CITY_KEY, settings.getCity());
        this.save(Settings.SEARCH_RANGE_KEY, settings.getSearchRange());
        this.save(Settings.YEAR_RANGE_MIN_KEY, settings.getYearRangeMin());
        this.save(Settings.YEAR_RANGE_MAX_KEY, settings.getYearRangeMax());
        this.save(Settings.VISIBLE_KEY, settings.isVisible());
    }

    @Override
    public Object read(@PreferencesKey String key) {
        switch (key) {
            case Settings.VISIBLE_KEY:
                return sharedPreferences.getBoolean(key, false);
            case Auth.ACCESS_TOKEN:
            case Auth.USER_ID:
            case Settings.PREFERENCE_KEY:
            case Settings.CITY_KEY:
                return sharedPreferences.getString(key, null);
            case Settings.SEARCH_RANGE_KEY:
            case Settings.YEAR_RANGE_MIN_KEY:
            case Settings.YEAR_RANGE_MAX_KEY:
                return sharedPreferences.getInt(key, 0);
        }
        return null;
    }

    @Override
    public Auth readAuth() {
        String accessToken = (String) read(Auth.ACCESS_TOKEN);
        String userId = (String) read(Auth.USER_ID);
        AuthBuilder authBuilder = new AuthBuilder(accessToken, userId);
        return authBuilder.build();
    }

    @Override
    public Settings readSettings() {
        String preference = (String) read(Settings.PREFERENCE_KEY);
        String city = (String) read(Settings.CITY_KEY);
        Integer searchRange = (Integer) read(Settings.SEARCH_RANGE_KEY);
        searchRange = searchRange != 0 ? searchRange : null;
        Integer yearRangeMin = (Integer) read(Settings.YEAR_RANGE_MIN_KEY);
        yearRangeMin = yearRangeMin != 0 ? yearRangeMin : null;
        Integer yearRangeMax = (Integer) read(Settings.YEAR_RANGE_MAX_KEY);
        yearRangeMax = yearRangeMax != 0 ? yearRangeMax : null;
        Boolean visible = (Boolean) read(Settings.VISIBLE_KEY);
        SettingsBuilder settingsBuilder = new SettingsBuilder(preference, city, searchRange,
                yearRangeMin, yearRangeMax, visible);
        return settingsBuilder.build();
    }

    @Override
    public void clearAll() {
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.clear();
        sharedPreferencesEditor.apply();
    }

}
