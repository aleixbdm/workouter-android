package com.aleixpellisa.mdpa.workouter.view.model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.aleixpellisa.mdpa.workouter.view.input.ErrorChecker;
import com.aleixpellisa.mdpa.workouter.view.input.MeetDayTextInputType;

import toothpick.Toothpick;

public class BaseViewModel extends AndroidViewModel {

    BaseViewModel(@NonNull Application application) {
        super(application);
    }

    void checkInputErrors(MeetDayTextInputType meetDayTextInputType) throws IllegalArgumentException {
        ErrorChecker errorChecker = new ErrorChecker();
        if (meetDayTextInputType == null || errorChecker.inputHasErrors(meetDayTextInputType)) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    protected void onCleared() {
        Toothpick.closeScope(this);
        super.onCleared();
    }
}
