package com.aleixpellisa.mdpa.workouter.view.settings;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.view.BaseFragment;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;

public class SettingsFragment extends BaseFragment {

    public static final String TAG = "SettingsFragmentTag";

    ProfileViewModel profileViewModel;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        setupView(view);

        return view;
    }

    @Override
    public void setupView(View view) {
        super.setupView(view);

        initViewModel();

        SettingsView settingsView = new SettingsView(getActivity(), view, profileViewModel);
        settingsView.setupView(profileViewModel.getSettings());
    }

    private void initViewModel() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            profileViewModel = ViewModelProviders.of(activity).get(ProfileViewModel.class);
        }
    }
}