package com.aleixpellisa.mdpa.workouter.database.entity;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Settings {

    public static final String PREFERENCE_KEY = "preference";
    public static final String CITY_KEY = "city";
    public static final String SEARCH_RANGE_KEY = "search_range";
    public static final String YEAR_RANGE_MIN_KEY = "year_range_min";
    public static final String YEAR_RANGE_MAX_KEY = "year_range_max";
    public static final String VISIBLE_KEY = "visible";

    public static final String PREFERENCE_MALE = "Male";
    public static final String PREFERENCE_FEMALE = "Female";
    public static final String PREFERENCE_ALL = "All";

    @StringDef({PREFERENCE_MALE, PREFERENCE_FEMALE, PREFERENCE_ALL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Preference {
    }

    private @Preference
    String preference;

    private String city;

    private Integer searchRange;

    private Integer yearRangeMin;

    private Integer yearRangeMax;

    private Boolean visible;

    public Settings(@Preference String preference, String city, Integer searchRange, Integer yearRangeMin, Integer yearRangeMax, Boolean visible) {
        this.preference = preference;
        this.city = city;
        this.searchRange = searchRange;
        this.yearRangeMin = yearRangeMin;
        this.yearRangeMax = yearRangeMax;
        this.visible = visible;
    }

    public @Preference
    String getPreference() {
        return preference;
    }

    public void setPreference(@Preference String preference) {
        this.preference = preference;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getSearchRange() {
        return searchRange;
    }

    public void setSearchRange(Integer searchRange) {
        this.searchRange = searchRange;
    }

    public Integer getYearRangeMin() {
        return yearRangeMin;
    }

    public void setYearRangeMin(Integer yearRangeMin) {
        this.yearRangeMin = yearRangeMin;
    }

    public Integer getYearRangeMax() {
        return yearRangeMax;
    }

    public void setYearRangeMax(Integer yearRangeMax) {
        this.yearRangeMax = yearRangeMax;
    }

    public Boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
