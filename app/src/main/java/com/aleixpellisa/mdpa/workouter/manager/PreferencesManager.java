package com.aleixpellisa.mdpa.workouter.manager;

import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;

public interface PreferencesManager {

    void save(@PreferencesKey String key, Object value);

    void saveAuth(Auth auth);

    void saveSettings(Settings settings);

    Object read(@PreferencesKey String key);

    Auth readAuth();

    Settings readSettings();

    void clearAll();

}
