package com.aleixpellisa.mdpa.workouter.manager;

import android.arch.lifecycle.LiveData;

import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.List;

public interface CandidateManager {

    LiveData<Event<List<Candidate>>> getCandidateList();

    LiveData<Event<Evaluation>> getEvaluation();

    void fetchCandidates();

    void sendEvaluation(String candidateId, @Evaluation.EvaluationValue String evaluation);

    void clearAll();

}
