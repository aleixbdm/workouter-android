package com.aleixpellisa.mdpa.workouter.view.main;

import android.annotation.SuppressLint;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;

import java.lang.reflect.Field;

/**
 * Workaround to disable the shift mode with the bottom bar found on internet.
 * Credits: https://stackoverflow.com/a/40189977
 */

class BottomNavigationViewHelper {

    @SuppressLint("RestrictedApi")
    static void disableShiftMode(BottomNavigationView view) {

        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("[BottomNavigationVH]", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("[BottomNavigationVH]", "Unable to change value of shift mode", e);
        }
    }
}
