package com.aleixpellisa.mdpa.workouter.network.response;

import com.aleixpellisa.mdpa.workouter.network.schema.EvaluationSchema;
import com.google.gson.annotations.SerializedName;

public class EvaluationResponse extends BaseResponse {
    @SerializedName("evaluation")
    public EvaluationSchema evaluationSchema;
}
