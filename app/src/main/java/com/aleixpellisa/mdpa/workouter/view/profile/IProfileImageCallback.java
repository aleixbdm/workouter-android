package com.aleixpellisa.mdpa.workouter.view.profile;

public interface IProfileImageCallback {

    void imageAdded(String url);

}
