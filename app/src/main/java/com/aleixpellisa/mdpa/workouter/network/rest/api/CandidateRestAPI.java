package com.aleixpellisa.mdpa.workouter.network.rest.api;

import com.aleixpellisa.mdpa.workouter.network.request.EvaluationRequest;
import com.aleixpellisa.mdpa.workouter.network.response.CandidateResponse;
import com.aleixpellisa.mdpa.workouter.network.response.EvaluationResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface CandidateRestAPI {
    @GET("discover")
    Call<List<CandidateResponse>> discover(@Query("limit") int limit);

    @POST("evaluate")
    Call<EvaluationResponse> evaluate(@Body EvaluationRequest evaluationRequest);
}
