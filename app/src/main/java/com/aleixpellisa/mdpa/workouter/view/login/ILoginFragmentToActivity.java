package com.aleixpellisa.mdpa.workouter.view.login;

public interface ILoginFragmentToActivity {

    void goToLoginWithMeetDay();

    void goToLoginCreateAccount();

    void returnFromLoginWithMD();

    void returnFromCreateAccount();

}
