package com.aleixpellisa.mdpa.workouter.manager.observer;

import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

public interface IEventObserver<E> {
    void onSuccess(Event<E> event);

    void onError(Event<E> event);
}
