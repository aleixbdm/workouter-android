package com.aleixpellisa.mdpa.workouter.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import com.aleixpellisa.mdpa.workouter.database.entity.Chat;

import java.util.List;

@Dao
public abstract class ChatDao {
    @Query("Select * from chat order by updatedAt desc")
    public abstract LiveData<List<Chat>> getChats();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    protected abstract void insert(List<Chat> chatList);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    protected abstract void update(List<Chat> chatList);

    @Transaction
    public void upsert(List<Chat> chatList) {
        insert(chatList);
        update(chatList);
    }

    @Query("Delete from chat")
    public abstract void deleteAll();
}
