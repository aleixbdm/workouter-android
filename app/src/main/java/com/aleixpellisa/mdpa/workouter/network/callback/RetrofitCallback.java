package com.aleixpellisa.mdpa.workouter.network.callback;

import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;

import com.aleixpellisa.mdpa.workouter.model.MeetDayError;
import com.aleixpellisa.mdpa.workouter.network.response.ErrorResponse;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class RetrofitCallback<R, E> implements Callback<R>, IRetrofitCallback<R> {

    private final Observer<Event<E>> observer;

    protected Event<E> event = Event.error(new MeetDayError("server error"));

    protected RetrofitCallback(Observer<Event<E>> observer) {
        this.observer = observer;
    }

    @Override
    public void onResponse(@NonNull Call<R> call, @NonNull Response<R> response) {
        R responseBody = response.body();
        if (response.isSuccessful() && responseBody != null) {
            onSuccess(responseBody);
        } else {
            onError(errorParser(response.errorBody()));
        }
    }

    @Override
    public void onError(@NonNull MeetDayError meetDayError) {
        if (!meetDayError.isAuthenticated()) {
            event = Event.authError(meetDayError);
        } else {
            event = Event.error(meetDayError);
        }
        observer.onChanged(event);
    }

    @Override
    public void onFailure(@NonNull Call<R> call, @NonNull Throwable t) {
        event = Event.error(new MeetDayError(t.getMessage()));
        observer.onChanged(event);
    }

    private MeetDayError errorParser(ResponseBody errorBody) {
        try {
            if (errorBody != null) {
                String content = errorBody.string();
                ErrorResponse errorResponse = ErrorResponse.fromJson(content, ErrorResponse.class);
                if (errorResponse.error != null) {
                    return new MeetDayError(errorResponse.error.message);
                } else if (errorResponse.authErrorSchema != null) {
                    return new MeetDayError(errorResponse.authErrorSchema.message, false);
                }
            }
        } catch (IOException ignored) {
        }
        return event.meetDayError;
    }
}
