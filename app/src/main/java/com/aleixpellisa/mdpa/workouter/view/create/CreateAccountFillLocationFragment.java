package com.aleixpellisa.mdpa.workouter.view.create;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.builder.SettingsBuilder;

import butterknife.BindView;

public class CreateAccountFillLocationFragment extends CreateAccountBaseFragment {

    static final String TAG = "CreateAccountFillLocationFragmentTag";

    @BindView(R.id.create_account_fill_location_fragment_location_switch)
    Switch locationSwitch;
    @BindView(R.id.create_account_fill_location_fragment_city_text_view)
    TextView cityTextView;

    public Button leftButton;
    public Button rightButton;

    private String city = SettingsBuilder.SETTINGS_CITY_MOCK;

    public static CreateAccountFillLocationFragment newInstance(Button leftButton, Button rightButton) {
        CreateAccountFillLocationFragment createAccountFillAgeFragment = new CreateAccountFillLocationFragment();
        createAccountFillAgeFragment.setArguments(leftButton, rightButton);
        return createAccountFillAgeFragment;
    }

    public void setArguments(Button leftButton, Button rightButton) {
        this.leftButton = leftButton;
        this.rightButton = rightButton;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_account_fill_location, container, false);

        setupView(view);

        return view;
    }

    @Override
    public void setupView(final View view) {
        super.setupView(view);

        initViewComponents();
        configureBottomButtons();
    }

    private void initViewComponents() {
        // Check if location permission is granted
        locationSwitch.setChecked(true);
        locationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                // Permissions
            }
        });

        cityTextView.setText(city);
    }

    private void configureBottomButtons() {
        leftButton.setText(R.string.input_text_layout_action_back);
        leftButton.setVisibility(View.VISIBLE);
        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentToActivityCallback.returnFromFillLocation();
            }
        });

        rightButton.setText(R.string.create_account_continue_button);
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentToActivityCallback.goToFinishCreate(city);
            }
        });
    }

}