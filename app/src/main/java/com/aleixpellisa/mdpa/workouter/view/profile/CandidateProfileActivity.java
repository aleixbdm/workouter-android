package com.aleixpellisa.mdpa.workouter.view.profile;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.view.BasePushActivity;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;

public class CandidateProfileActivity extends BasePushActivity implements IEvaluationCallback {

    public static final int REQUEST_CODE = 100;
    public static final String FROM_CANDIDATE_CARD_KEY = "FromCandidateCard";
    public static final String FROM_CANDIDATE_CHAT_KEY = "FromCandidateChat";

    public static final String LIKE_PRESSED_KEY = "LikePressed";
    public static final String DISLIKE_PRESSED_KEY = "DislikePressed";

    ProfileViewModel profileViewModel;

    private Candidate candidate;
    private boolean isFromCandidateCard;
    private boolean isFromCandidateChat;

    private ProfileView profileView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle data = getIntent().getExtras();
        if (data != null) {
            candidate = data.getParcelable(Candidate.PARCEL_KEY);
            isFromCandidateCard = data.getBoolean(FROM_CANDIDATE_CARD_KEY);
            isFromCandidateChat = data.getBoolean(FROM_CANDIDATE_CHAT_KEY);
        }

        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        setupView(R.layout.fragment_profile);
    }

    @Override
    public void setupView(int layoutResID) {
        super.setupView(layoutResID);

        setTitle(this.getString(R.string.profile_candidate_title, candidate.getName()));

        profileView =
                new ProfileView(this, getWindow().findViewById(android.R.id.content),
                        profileViewModel, null, isFromCandidateCard, isFromCandidateChat, false);

        profileView.setupCandidateProfileView(candidate);

        profileViewModel.fetchCandidateProfile(candidate.getId());

        setupProfileViewModel();
    }

    private void setupProfileViewModel() {
        profileViewModel.getCandidateProfile().observe(this, new Observer<Event<Profile>>() {
            @Override
            public void onChanged(@Nullable Event<Profile> event) {
                if (event != null) {
                    switch (event.status) {
                        case Event.SUCCESS:
                            retrievedCandidateProfile(event.data);
                            break;
                        case Event.ERROR:
                        case Event.AUTH_ERROR:
                            break;
                    }
                }
            }
        });
    }

    private void retrievedCandidateProfile(Profile profile) {
        profileView.setupCandidateProfileView(profile);
    }

    @Override
    public void likePressed() {
        Intent intent = new Intent();
        intent.putExtra(LIKE_PRESSED_KEY, true);
        setResult(REQUEST_CODE, intent);
        onBackPressed();
    }

    @Override
    public void dislikePressed() {
        Intent intent = new Intent();
        intent.putExtra(DISLIKE_PRESSED_KEY, true);
        setResult(REQUEST_CODE, intent);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        removeObservers();
    }

    private void removeObservers() {
        profileViewModel.getCandidateProfile().removeObservers(this);
    }
}
