package com.aleixpellisa.mdpa.workouter.network.request;

import android.support.annotation.Keep;

import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.google.gson.annotations.SerializedName;

@Keep
public class EvaluationRequest {
    @SerializedName("candidate_id")
    public String candidateId;

    @SerializedName("evaluation_value")
    public @Evaluation.EvaluationValue
    String evaluationValue;
}

