package com.aleixpellisa.mdpa.workouter.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Keep;

import com.aleixpellisa.mdpa.workouter.database.entity.Skill;

import java.util.ArrayList;
import java.util.List;

@Keep
public class Candidate extends BaseModel implements Parcelable {

    private String id;

    private String name;

    private List<String> imageUrls;

    private Integer age;

    private List<Skill> skills;

    private String city;

    public Candidate(String id, String name, List<String> imageUrls, Integer age, List<Skill> skills, String city) {
        this.id = id;
        this.name = name;
        this.imageUrls = imageUrls;
        this.age = age;
        this.skills = skills;
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Custom getters
     */

    public List<Skill> getTopSkills() {
        ArrayList<Skill> topSkills = new ArrayList<>();
        for (Skill skill : skills) {
            if (skill.getValue().equals(Skill.MAX_VALUE)) {
                topSkills.add(skill);
            }
        }
        return topSkills;
    }

    /**
     * Parcelable part
     **/

    public static final String PARCEL_KEY = "Candidate";

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Candidate createFromParcel(Parcel in) {
            return new Candidate(in);
        }

        public Candidate[] newArray(int size) {
            return new Candidate[size];
        }
    };

    public Candidate(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.imageUrls = new ArrayList<>();
        in.readStringList(this.imageUrls);
        this.age = in.readInt();
        this.city = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeStringList(imageUrls);
        parcel.writeInt(age);
        parcel.writeString(city);
    }
}
