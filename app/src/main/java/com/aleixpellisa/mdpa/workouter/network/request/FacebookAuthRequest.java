package com.aleixpellisa.mdpa.workouter.network.request;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class FacebookAuthRequest {
    @SerializedName("access_token")
    public String facebookAccessToken;
}

