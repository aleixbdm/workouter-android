package com.aleixpellisa.mdpa.workouter.network.request;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class SendMessageRequest {
    @SerializedName("message_text")
    public String messageText;
}

