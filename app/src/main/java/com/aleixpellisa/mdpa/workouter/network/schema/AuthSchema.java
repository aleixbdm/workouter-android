package com.aleixpellisa.mdpa.workouter.network.schema;

import com.google.gson.annotations.SerializedName;

public class AuthSchema extends BaseSchema {
    @SerializedName("access_token")
    public String accessToken;

    @SerializedName("user_id")
    public String userId;
}
