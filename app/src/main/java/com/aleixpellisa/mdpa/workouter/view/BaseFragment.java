package com.aleixpellisa.mdpa.workouter.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import butterknife.ButterKnife;
import toothpick.Scope;
import toothpick.Toothpick;

public class BaseFragment extends Fragment {

    private Scope scope;

    private void beforeCreate(Activity owner) {
        if (owner != null) {
            scope = Toothpick.openScopes(owner.getApplication(), owner, this);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        this.beforeCreate(getActivity());
        super.onCreate(savedInstanceState);
        if (scope != null) {
            Toothpick.inject(this, scope);
        }
    }

    public void setupView(View view) {
        ButterKnife.bind(this, view);
    }

    public void showKeyboard(View view) {
        if (view != null && getActivity() != null) {
            view.requestFocus();
            InputMethodManager imm = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.showSoftInput(view, 0);
            }
        }
    }

    public void hideKeyboard(View view) {
        if (view != null && getActivity() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

}