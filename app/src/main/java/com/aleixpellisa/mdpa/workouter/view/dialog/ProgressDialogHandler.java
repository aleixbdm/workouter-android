package com.aleixpellisa.mdpa.workouter.view.dialog;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class ProgressDialogHandler {

    public static final String TAG = "ProgressDialog";
    private FragmentManager fragmentManager;
    private ProgressDialogFragment progressDialogFragment;
    private boolean isVisible;

    public ProgressDialogHandler(@NonNull FragmentActivity fragmentActivity) {
        fragmentManager = fragmentActivity.getSupportFragmentManager();
    }

    public void show(String message) {
        progressDialogFragment = ProgressDialogFragment.newInstance(message);
        progressDialogFragment.show(fragmentManager, TAG);
        isVisible = true;
    }

    public void hide() {
        if (progressDialogFragment != null) {
            progressDialogFragment.dismiss();
            isVisible = false;
        }
    }

    public boolean isVisible() {
        return isVisible;
    }
}
