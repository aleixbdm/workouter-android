package com.aleixpellisa.mdpa.workouter.network.response;

import com.aleixpellisa.mdpa.workouter.network.schema.ErrorSchema;
import com.aleixpellisa.mdpa.workouter.network.schema.AuthErrorSchema;
import com.google.gson.annotations.SerializedName;

public class ErrorResponse extends BaseResponse {
    @SerializedName("error")
    public ErrorSchema error;

    @SerializedName("auth_error")
    public AuthErrorSchema authErrorSchema;
}
