package com.aleixpellisa.mdpa.workouter.view.input;

import android.content.Context;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.model.Credentials;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.regex.Pattern;

public class ErrorChecker {

    private static final Pattern EMAIL_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );


    static final int NO_ERROR_KEY = 0;
    static final int IS_REQUIRED_ERROR_KEY = 1;
    static final int INCORRECT_EMAIL_ERROR_KEY = 2;
    static final int SHORT_PASSWORD_ERROR_KEY = 3;
    static final int PASSWORD_NOT_MATCH_ERROR_KEY = 4;
    static final int MUST_ACCEPT_TERMS_ERROR_KEY = 5;
    static final int AGE_ERROR_KEY = 6;
    static final int IS_NOT_VISIBLE_ERROR_KEY = 7;
    static final int WRONG_CANDIDATE_ID_ERROR_KEY = 8;
    static final int WRONG_SETTINGS_VALUE_ERROR_KEY = 9;

    @IntDef({NO_ERROR_KEY, IS_REQUIRED_ERROR_KEY,
            INCORRECT_EMAIL_ERROR_KEY, SHORT_PASSWORD_ERROR_KEY,
            PASSWORD_NOT_MATCH_ERROR_KEY, MUST_ACCEPT_TERMS_ERROR_KEY,
            AGE_ERROR_KEY, IS_NOT_VISIBLE_ERROR_KEY, WRONG_CANDIDATE_ID_ERROR_KEY,
            WRONG_SETTINGS_VALUE_ERROR_KEY})
    @Retention(RetentionPolicy.SOURCE)
    @interface ErrorKey {
    }

    private Context context;
    private TextView view;

    public ErrorChecker() {
    }

    public ErrorChecker(Context context, TextView view) {
        this.context = context;
        this.view = view;
    }

    public boolean inputHasErrors(@NonNull MeetDayTextInputType meetDayTextInputType) {
        return getErrorKey(meetDayTextInputType) != NO_ERROR_KEY;
    }

    @ErrorKey
    int getErrorKey(MeetDayTextInputType meetDayTextInputType) {
        String value = meetDayTextInputType.getValue();

        // Check required
        if (value == null || meetDayTextInputType.isRequired() && value.isEmpty()) {
            return IS_REQUIRED_ERROR_KEY;
        }

        // Check email
        if (meetDayTextInputType.isEmail() && !isEmailCorrect(value)) {
            return INCORRECT_EMAIL_ERROR_KEY;
        }

        // Check password
        if (meetDayTextInputType.isPassword() && !isPasswordValid(value)) {
            return SHORT_PASSWORD_ERROR_KEY;
        }

        return NO_ERROR_KEY;
    }

    private boolean isEmailCorrect(String email) {
        return EMAIL_PATTERN.matcher(email).matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() > Credentials.MIN_PASSWORD_LENGTH;
    }

    void refreshUIErrors(Context context, TextView view, boolean isFocusableOnChange, @ErrorKey int errorKey) {
        // Reset errors
        view.setError(null);

        switch (errorKey) {
            case NO_ERROR_KEY:
                break;
            case INCORRECT_EMAIL_ERROR_KEY:
                setUIError(context.getString(R.string.input_text_layout_error_email_incorrect), isFocusableOnChange);
                break;
            case IS_REQUIRED_ERROR_KEY:
                setUIError(context.getString(R.string.input_text_layout_error_field_required), isFocusableOnChange);
                break;
            case SHORT_PASSWORD_ERROR_KEY:
                setUIError(context.getString(R.string.input_text_layout_error_password_short), isFocusableOnChange);
                break;
            case PASSWORD_NOT_MATCH_ERROR_KEY:
                setUIError(context.getString(R.string.login_create_account_error_passwords_match), true);
                break;
            case MUST_ACCEPT_TERMS_ERROR_KEY:
                setUIError(context.getString(R.string.login_create_account_error_must_accept_terms), true);
            case AGE_ERROR_KEY:
            case IS_NOT_VISIBLE_ERROR_KEY:
            case WRONG_CANDIDATE_ID_ERROR_KEY:
            case WRONG_SETTINGS_VALUE_ERROR_KEY:
                break;
        }
    }

    private void setUIError(String errorMessage, boolean isFocusableOnChange) {
        view.setError(errorMessage);
        if (!isFocusableOnChange) {
            View focusViewWithError = view;
            focusViewWithError.requestFocus();
        }
    }

    // Custom errors

    // Matches passwords

    public boolean checkIfMatchesPasswords(String password, String passwordToBeConfirmed) {
        return matchesPasswordsErrorKey(password, passwordToBeConfirmed) == NO_ERROR_KEY;
    }

    public void refreshMatchesPasswordsUIError(MeetDayTextInputType passwordInputType,
                                               MeetDayTextInputType passwordToBeConfirmedInputType) {
        @ErrorChecker.ErrorKey int errorKeyConfirmPassword = getErrorKey(passwordToBeConfirmedInputType);
        @ErrorChecker.ErrorKey int errorKey;
        if (errorKeyConfirmPassword == NO_ERROR_KEY) {
            errorKey = matchesPasswordsErrorKey(
                    passwordInputType.getValue(), passwordToBeConfirmedInputType.getValue());
        } else {
            errorKey = errorKeyConfirmPassword;
        }
        refreshUIErrors(context, view, true, errorKey);
    }

    private @ErrorKey
    int matchesPasswordsErrorKey(String password, String passwordToBeConfirmed) {
        return password.equals(passwordToBeConfirmed) ? NO_ERROR_KEY : PASSWORD_NOT_MATCH_ERROR_KEY;
    }

    // Accepted terms

    public boolean checkIfAcceptedTerms(boolean accepted) {
        return termsAreAcceptedErrorKey(accepted) == NO_ERROR_KEY;
    }

    public void refreshTermsAcceptedUIError(boolean accepted) {
        @ErrorChecker.ErrorKey int errorKey = termsAreAcceptedErrorKey(accepted);
        refreshUIErrors(context, view, true, errorKey);
    }

    private @ErrorKey
    int termsAreAcceptedErrorKey(boolean checked) {
        return checked ? NO_ERROR_KEY : MUST_ACCEPT_TERMS_ERROR_KEY;
    }

    // Settings value
    public boolean checkIfAgeValueIsCorrect(int value) {
        return ageValueErrorKey(value) == NO_ERROR_KEY;
    }

    private @ErrorKey
    int ageValueErrorKey(int value) {
        return value >= Profile.MIN_AGE && value <= Profile.MAX_AGE ? NO_ERROR_KEY : AGE_ERROR_KEY;
    }

    // Is visible
    public boolean checkIfIsVisible(boolean isVisible) {
        return isVisibleErrorKey(isVisible) == NO_ERROR_KEY;
    }

    private @ErrorKey
    int isVisibleErrorKey(boolean isVisible) {
        return isVisible ? NO_ERROR_KEY : IS_NOT_VISIBLE_ERROR_KEY;
    }

    // Candidate id
    public boolean checkIfCandidateIdIsCorrect(String candidateId) {
        return candidateIdErrorKey(candidateId) == NO_ERROR_KEY;
    }

    private @ErrorKey
    int candidateIdErrorKey(String candidateId) {
        return candidateId != null && !candidateId.isEmpty() ? NO_ERROR_KEY : WRONG_CANDIDATE_ID_ERROR_KEY;
    }

    // Settings value
    public boolean checkIfSettingsValueIsCorrect(Object value) {
        return settingsValueErrorKey(value) == NO_ERROR_KEY;
    }

    private @ErrorKey
    int settingsValueErrorKey(Object value) {
        return value != null ? NO_ERROR_KEY : WRONG_SETTINGS_VALUE_ERROR_KEY;
    }

}
