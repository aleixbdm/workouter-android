package com.aleixpellisa.mdpa.workouter.view.create;

import android.content.Context;

import com.aleixpellisa.mdpa.workouter.view.BaseFragment;

public class CreateAccountBaseFragment extends BaseFragment {

    public ICreateAccountFragmentToActivity fragmentToActivityCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentToActivityCallback = (ICreateAccountFragmentToActivity) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement ICreateAccountFragmentToActivity");
        }
    }

    @Override
    public void onDetach() {
        fragmentToActivityCallback = null;
        super.onDetach();
    }

}
