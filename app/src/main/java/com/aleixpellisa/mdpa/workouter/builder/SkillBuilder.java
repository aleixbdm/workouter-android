package com.aleixpellisa.mdpa.workouter.builder;

import com.aleixpellisa.mdpa.workouter.database.entity.Skill;
import com.aleixpellisa.mdpa.workouter.network.schema.SkillSchema;

import java.util.ArrayList;
import java.util.List;

public class SkillBuilder extends BaseBuilder {

    public static final List<SkillSchema> SKILL_SCHEMA_LIST_MOCK = new ArrayList<SkillSchema>() {{
        SkillSchema skillSchemaSports = new SkillSchema();
        skillSchemaSports.value = 2;
        skillSchemaSports.category = Skill.SKILL_SPORTS;
        add(skillSchemaSports);

        SkillSchema skillSchemaGeek = new SkillSchema();
        skillSchemaGeek.value = 2;
        skillSchemaGeek.category = Skill.SKILL_GEEK;
        add(skillSchemaGeek);
    }};

    private List<SkillSchema> skillSchemaList;

    private Integer value;
    private @Skill.Category
    String category;

    public SkillBuilder() {
        skillSchemaList = new ArrayList<>();
    }

    public SkillBuilder(SkillSchema skillSchema) {
        if (skillSchema == null) return;
        this.value = skillSchema.value;
        this.category = skillSchema.category;
    }

    public SkillBuilder(List<SkillSchema> skillSchemaList) {
        this.skillSchemaList = skillSchemaList;
    }

    public SkillBuilder(Integer value, @Skill.Category String category) {
        this.value = value;
        this.category = category;
    }

    public SkillBuilder(Skill skill) {
        if (skill == null) return;
        this.value = skill.getValue();
        this.category = skill.getCategory();
    }

    public Skill build() {
        return new Skill(value, category);
    }

    public SkillSchema buildSchema() {
        Skill skill = build();
        SkillSchema skillSchema = new SkillSchema();
        skillSchema.value = skill.getValue();
        skillSchema.category = skill.getCategory();
        return skillSchema;
    }

    public List<Skill> buildList() {
        List<Skill> skillList = new ArrayList<>();
        if (skillSchemaList != null) {
            for (SkillSchema skillSchema : skillSchemaList) {
                skillList.add(new Skill(
                        skillSchema.value,
                        skillSchema.category
                ));
            }
        }
        return skillList;
    }

    public static List<Skill> buildInitialSkillList() {
        List<SkillSchema> initialSkillList = new ArrayList<SkillSchema>() {{
            SkillSchema skillSchemaSports = new SkillSchema();
            skillSchemaSports.value = 1;
            skillSchemaSports.category = Skill.SKILL_SPORTS;
            add(skillSchemaSports);

            SkillSchema skillSchemaGeek = new SkillSchema();
            skillSchemaGeek.value = 1;
            skillSchemaGeek.category = Skill.SKILL_GEEK;
            add(skillSchemaGeek);

            SkillSchema skillSchemaDance = new SkillSchema();
            skillSchemaDance.value = 1;
            skillSchemaDance.category = Skill.SKILL_DANCE;
            add(skillSchemaDance);

            SkillSchema skillSchemaTravel = new SkillSchema();
            skillSchemaTravel.value = 1;
            skillSchemaTravel.category = Skill.SKILL_TRAVEL;
            add(skillSchemaTravel);

            SkillSchema skillSchemaPets = new SkillSchema();
            skillSchemaPets.value = 1;
            skillSchemaPets.category = Skill.SKILL_PETS;
            add(skillSchemaPets);

            SkillSchema skillSchemaGreen = new SkillSchema();
            skillSchemaGreen.value = 1;
            skillSchemaGreen.category = Skill.SKILL_GREEN;
            add(skillSchemaGreen);
        }};
        SkillBuilder skillBuilder =
                new SkillBuilder(initialSkillList);
        return skillBuilder.buildList();
    }

    public static List<Skill> buildMockedSkillList() {
        SkillBuilder skillBuilder =
                new SkillBuilder(SKILL_SCHEMA_LIST_MOCK);
        return skillBuilder.buildList();
    }

}
