package com.aleixpellisa.mdpa.workouter.view.login;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.view.dialog.DialogHandler;
import com.aleixpellisa.mdpa.workouter.view.input.ErrorChecker;
import com.aleixpellisa.mdpa.workouter.view.input.MeetDayTextInputLayout;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginCreateAccountFragment extends LoginBaseFragment {

    static final String TAG = "LoginCreateAccountFragmentTag";

    @Inject
    DialogHandler dialogHandler;

    ProfileViewModel profileViewModel;

    @BindView(R.id.login_create_account_email_text_input)
    TextInputLayout emailTextInput;
    @BindView(R.id.login_create_account_password_text_input)
    TextInputLayout passwordTextInput;
    @BindView(R.id.login_create_account_confirm_password_text_input)
    TextInputLayout confirmPasswordTextInput;
    @BindView(R.id.login_create_account_register_accept_terms_checkbox)
    CheckBox acceptTermsCheckbox;
    @BindView(R.id.login_create_account_register_accept_terms_text_view)
    TextView acceptTermsTextView;

    private MeetDayTextInputLayout meetDayEmailTextInput;
    private MeetDayTextInputLayout meetDayPasswordTextInput;
    private MeetDayTextInputLayout meetDayConfirmPasswordTextInput;

    public static LoginCreateAccountFragment newInstance() {
        return new LoginCreateAccountFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_create_account, container, false);

        setupView(view);

        return view;
    }

    @Override
    public void setupView(View view) {
        super.setupView(view);

        initViewModel();

        meetDayEmailTextInput = new MeetDayTextInputLayout(emailTextInput);
        meetDayPasswordTextInput = new MeetDayTextInputLayout(passwordTextInput);
        meetDayConfirmPasswordTextInput = new MeetDayTextInputLayout(confirmPasswordTextInput);

        meetDayEmailTextInput.initTextInputLayout(
                MeetDayTextInputLayout.CREATE_CREDENTIALS_EMAIL_FIELD_KEY);

        meetDayPasswordTextInput.initTextInputLayout(
                MeetDayTextInputLayout.CREATE_CREDENTIALS_PASSWORD_FIELD_KEY);

        meetDayConfirmPasswordTextInput.initTextInputLayout(
                MeetDayTextInputLayout.CREATE_CREDENTIALS_CONFIRM_PASSWORD_FIELD_KEY);

        acceptTermsTextView.setText(getText(R.string.login_create_account_accept_terms));
        acceptTermsTextView.setMovementMethod(LinkMovementMethod.getInstance());

        meetDayConfirmPasswordTextInput.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptCreate();
                    return true;
                }
                return false;
            }
        });
    }

    private void initViewModel() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            profileViewModel = ViewModelProviders.of(activity).get(ProfileViewModel.class);
        }
    }

    @OnClick(R.id.login_create_account_back_button)
    public void backButtonClicked() {
        hideKeyboard(getView());
        fragmentToActivityCallback.returnFromCreateAccount();
    }

    @OnClick(R.id.login_create_account_create_button)
    public void attemptCreate() {
        try {
            dialogHandler.showProgressDialog();

            profileViewModel.attemptCreateAccount(
                    meetDayEmailTextInput.retrieveMeetDayTextInputType(),
                    meetDayPasswordTextInput.retrieveMeetDayTextInputType(),
                    meetDayConfirmPasswordTextInput.retrieveMeetDayTextInputType(),
                    acceptTermsCheckbox.isChecked()
            );
        } catch (IllegalArgumentException ex) {
            dialogHandler.hideProgressDialog();

            meetDayEmailTextInput.showErrorsIfNeeded();
            meetDayPasswordTextInput.showErrorsIfNeeded();
            meetDayConfirmPasswordTextInput.showErrorsIfNeeded();

            // Extra validation
            ErrorChecker errorCheckerConfirmPassword = new ErrorChecker(getContext(),
                    meetDayConfirmPasswordTextInput.getEditText());
            errorCheckerConfirmPassword.refreshMatchesPasswordsUIError(
                    meetDayPasswordTextInput.retrieveMeetDayTextInputType(),
                    meetDayConfirmPasswordTextInput.retrieveMeetDayTextInputType()
            );

            ErrorChecker errorCheckerCheckboxChecked = new ErrorChecker(getContext(),
                    acceptTermsCheckbox);
            errorCheckerCheckboxChecked.refreshTermsAcceptedUIError(acceptTermsCheckbox.isChecked());
        }
    }

}