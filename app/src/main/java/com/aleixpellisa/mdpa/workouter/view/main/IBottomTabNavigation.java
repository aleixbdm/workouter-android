package com.aleixpellisa.mdpa.workouter.view.main;

import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.model.Candidate;

public interface IBottomTabNavigation {

    void fragmentReady(String fragmentTag);

    void goToDiscover();

    void goToChats();

    void goToChat(Chat chat);

    void goToProfile();

    void goToCandidateProfileFromCard(Candidate candidate);

    void goToSettings();

    void goToPrivacyPolicy();

    void share();

    void logout();

    void deleteAccount();

}
