package com.aleixpellisa.mdpa.workouter.network.rest.api;

import com.aleixpellisa.mdpa.workouter.network.request.AuthRequest;
import com.aleixpellisa.mdpa.workouter.network.request.FacebookAuthRequest;
import com.aleixpellisa.mdpa.workouter.network.request.RefreshTokenRequest;
import com.aleixpellisa.mdpa.workouter.network.response.AuthResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AuthRestAPI {
    @POST("login")
    Call<AuthResponse> login(@Body AuthRequest authRequest);

    @POST("login/facebook")
    Call<AuthResponse> facebookLogin(@Body FacebookAuthRequest facebookAuthRequest);

    @POST("register")
    Call<AuthResponse> register(@Body AuthRequest authRequest);

    @POST("token")
    Call<AuthResponse> refreshToken(@Body RefreshTokenRequest refreshTokenRequest);
}
