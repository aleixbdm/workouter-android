package com.aleixpellisa.mdpa.workouter.network.schema;

import com.google.gson.annotations.SerializedName;

public class EvaluationSchema extends BaseSchema {

    @SerializedName("id")
    public String id;

    @SerializedName("sender_user_id")
    public String senderUserId;

    @SerializedName("receiver_user_id")
    public String receiverUserId;

    @SerializedName("evaluation_value")
    public String evaluationValue;
}
