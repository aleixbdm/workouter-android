package com.aleixpellisa.mdpa.workouter.network.schema;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileSchema extends BaseSchema {

    @SerializedName("id")
    public String id;

    @SerializedName("email")
    public String email;

    @SerializedName("name")
    public String name;

    @SerializedName("first_name")
    public String firstName;

    @SerializedName("image_urls")
    public List<String> imageUrls;

    @SerializedName("age")
    public Integer age;

    @SerializedName("gender")
    public String gender;

    @SerializedName("description")
    public String description;

    @SerializedName("current_job")
    public String currentJob;

    @SerializedName("studies")
    public String studies;

    @SerializedName("favourite_song")
    public String favouriteSong;

    @SerializedName("skills")
    public List<SkillSchema> skills;

    @SerializedName("settings")
    public SettingsSchema settings;

}
