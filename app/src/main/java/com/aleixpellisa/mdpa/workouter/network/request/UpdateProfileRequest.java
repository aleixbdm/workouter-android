package com.aleixpellisa.mdpa.workouter.network.request;

import android.support.annotation.Keep;

import com.aleixpellisa.mdpa.workouter.network.schema.ProfileSchema;
import com.google.gson.annotations.SerializedName;

@Keep
public class UpdateProfileRequest {
    @SerializedName("user")
    public ProfileSchema profileSchema;
}

