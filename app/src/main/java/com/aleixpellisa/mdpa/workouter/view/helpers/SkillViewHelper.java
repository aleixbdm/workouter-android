package com.aleixpellisa.mdpa.workouter.view.helpers;

import com.aleixpellisa.mdpa.workouter.R;

import static com.aleixpellisa.mdpa.workouter.database.entity.Skill.Category;
import static com.aleixpellisa.mdpa.workouter.database.entity.Skill.SKILL_DANCE;
import static com.aleixpellisa.mdpa.workouter.database.entity.Skill.SKILL_GEEK;
import static com.aleixpellisa.mdpa.workouter.database.entity.Skill.SKILL_GREEN;
import static com.aleixpellisa.mdpa.workouter.database.entity.Skill.SKILL_PETS;
import static com.aleixpellisa.mdpa.workouter.database.entity.Skill.SKILL_SPORTS;
import static com.aleixpellisa.mdpa.workouter.database.entity.Skill.SKILL_TRAVEL;

public class SkillViewHelper {

    private @Category
    String skillCategory;

    public SkillViewHelper(@Category String skillCategory) {
        this.skillCategory = skillCategory;
    }

    public int getSkillLayout() {
        switch (skillCategory) {
            case SKILL_SPORTS:
                return R.id.layout_profile_skill_sports;
            case SKILL_GEEK:
                return R.id.layout_profile_skill_geek;
            case SKILL_DANCE:
                return R.id.layout_profile_skill_dance;
            case SKILL_TRAVEL:
                return R.id.layout_profile_skill_travel;
            case SKILL_PETS:
                return R.id.layout_profile_skill_pets;
            case SKILL_GREEN:
                return R.id.layout_profile_skill_green;
        }
        // Should not get here
        return 0;
    }

    public int getSkillIcon() {
        switch (skillCategory) {
            case SKILL_SPORTS:
                return R.drawable.ic_skill_sport;
            case SKILL_GEEK:
                return R.drawable.ic_skill_geek;
            case SKILL_DANCE:
                return R.drawable.ic_skill_dance;
            case SKILL_TRAVEL:
                return R.drawable.ic_skill_travel;
            case SKILL_PETS:
                return R.drawable.ic_skill_pet;
            case SKILL_GREEN:
                return R.drawable.ic_skill_green;
        }
        // Should not get here
        return 0;
    }

    public int getSkillName() {
        switch (skillCategory) {
            case SKILL_SPORTS:
                return R.string.profile_sports_skill;
            case SKILL_GEEK:
                return R.string.profile_geek_skill;
            case SKILL_DANCE:
                return R.string.profile_dance_skill;
            case SKILL_TRAVEL:
                return R.string.profile_travel_skill;
            case SKILL_PETS:
                return R.string.profile_pets_skill;
            case SKILL_GREEN:
                return R.string.profile_green_skill;
        }
        // Should not get here
        return 0;
    }

}
