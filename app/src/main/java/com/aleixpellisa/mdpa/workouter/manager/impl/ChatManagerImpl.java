package com.aleixpellisa.mdpa.workouter.manager.impl;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.aleixpellisa.mdpa.workouter.builder.MessageBuilder;
import com.aleixpellisa.mdpa.workouter.database.MeetDayDatabase;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.manager.ChatManager;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesManager;
import com.aleixpellisa.mdpa.workouter.manager.observer.EventObserver;
import com.aleixpellisa.mdpa.workouter.manager.refresher.DataRefresher;
import com.aleixpellisa.mdpa.workouter.manager.refresher.TokenRefresher;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.model.MeetDayError;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.network.rest.ChatRest;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ChatManagerImpl implements ChatManager {

    private PreferencesManager preferencesManager;

    private MeetDayDatabase meetDayDatabase;

    @Inject
    ChatRest chatRest;

    @Inject
    TokenRefresher tokenRefresher;

    @Inject
    DataRefresher chatsDataRefresher;

    @Inject
    DataRefresher chatMessagesDataRefresher;

    private LiveData<List<Chat>> chatListDbSource;
    private LiveData<List<Message>> chatMessageListDbSource;
    private EventLiveData<Event<List<Chat>>> chatListLiveData;
    private EventLiveData<Event<List<Message>>> chatMessageListLiveData;
    private EventLiveData<Event<Message>> chatMessageLiveData;

    private Observer<List<Chat>> chatListDatabaseObserver;
    private Observer<List<Message>> chatMessageListDatabaseObserver;
    private EventObserver<List<Chat>, List<Chat>> fetchChatsObserver;
    private EventObserver<List<Message>, List<Message>> fetchChatMessagesObserver;
    private EventObserver<Message, Message> sendChatMessageObserver;

    @Inject
    ChatManagerImpl(MeetDayDatabase meetDayDatabase, PreferencesManager preferencesManager) {
        this.meetDayDatabase = meetDayDatabase;
        this.preferencesManager = preferencesManager;
        chatListLiveData = new EventLiveData<>();
        chatMessageListLiveData = new EventLiveData<>();
        chatMessageLiveData = new EventLiveData<>();
    }

    @Override
    public void startObservers() {
        chatListDbSource = meetDayDatabase.getChatDao().getChats();
        chatListDatabaseObserver = new Observer<List<Chat>>() {
            @Override
            public void onChanged(@Nullable List<Chat> chatList) {
                if (chatList != null) {
                    chatListLiveData.setValue(Event.success(chatList));
                } else {
                    Event<List<Chat>> chatListEvent = Event.error(new MeetDayError("database chat list empty"));
                    chatListLiveData.setValue(chatListEvent);
                }
            }
        };
        chatListLiveData.addSource(chatListDbSource, chatListDatabaseObserver);
    }

    @Override
    public LiveData<Event<List<Chat>>> getChatList() {
        return chatListLiveData;
    }

    @Override
    public LiveData<Event<List<Message>>> getChatMessageList() {
        return chatMessageListLiveData;
    }

    @Override
    public LiveData<Event<Message>> getChatMessage() {
        return chatMessageLiveData;
    }

    @Override
    public List<Chat> getChatListFiltered(List<Chat> chatList, final String filter) {
        if (!filter.isEmpty()) {
            FluentIterable<Chat> chatsFiltered = FluentIterable.from(chatList).filter(new Predicate<Chat>() {
                @ParametersAreNonnullByDefault
                public boolean apply(Chat input) {
                    return input.getUserChat().getName().contains(filter);
                }
            });
            return chatsFiltered.toList();
        }
        return chatListLiveData.getValue() != null ? chatListLiveData.getValue().data : new ArrayList<Chat>();
    }

    @Override
    public void fetchChats() {
        refreshChats();

        Callable refreshToken = tokenRefresher.refreshTokenCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                fetchChats();
                return null;
            }
        }, chatListLiveData);
        fetchChatsObserver = new EventObserver<List<Chat>, List<Chat>>
                (refreshToken, chatListLiveData) {
            @Override
            public void onSuccess(Event<List<Chat>> event) {
                saveChats(event.data);
            }
        };
        chatRest.fetchChats(fetchChatsObserver);
    }

    @Override
    public void enterChat(String chatId) {
        chatMessageListDbSource = meetDayDatabase.getMessageDao().getMessagesForChat(chatId);
        chatMessageListDatabaseObserver = new Observer<List<Message>>() {
            @Override
            public void onChanged(@Nullable List<Message> messageList) {
                if (messageList != null) {
                    chatMessageListLiveData.setValue(Event.success(messageList));
                } else {
                    Event<List<Message>> chatMessageListEvent = Event.error(new MeetDayError("database chat message list empty"));
                    chatMessageListLiveData.setValue(chatMessageListEvent);
                }
            }
        };
        chatMessageListLiveData.addSource(chatMessageListDbSource, chatMessageListDatabaseObserver);
    }

    @Override
    public void fetchChatMessages(final String chatId) {
        refreshChatMessages(chatId);

        Callable refreshToken = tokenRefresher.refreshTokenCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                fetchChatMessages(chatId);
                return null;
            }
        }, chatMessageListLiveData);
        fetchChatMessagesObserver = new EventObserver<List<Message>, List<Message>>
                (refreshToken, chatMessageListLiveData) {
            @Override
            public void onSuccess(Event<List<Message>> event) {
                saveChatMessages(event.data);
            }
        };
        chatRest.fetchChatMessages(chatId, fetchChatMessagesObserver);
    }

    @Override
    public void sendMessage(final String chatId, final String messageText) {
        Callable refreshToken = tokenRefresher.refreshTokenCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                sendMessage(chatId, messageText);
                return null;
            }
        }, chatMessageLiveData);
        sendChatMessageObserver = new EventObserver<Message, Message>
                (refreshToken, chatMessageLiveData) {
            @Override
            public void onSuccess(Event<Message> event) {
                // Nothing to do
            }
        };
        chatRest.sendMessage(chatId, messageText, sendChatMessageObserver);

        // Also sync data as already send
        Auth auth = preferencesManager.readAuth();
        if (auth != null) {
            String userId = preferencesManager.readAuth().getUserId();
            chatMessageLiveData.postValue(Event.success(new MessageBuilder(userId, messageText).build()));
        }
    }

    @Override
    public void leaveChat() {
        chatMessageListLiveData.removeSource(chatMessageListDbSource);
        stopRefreshChatMessages();
    }

    @Override
    public void clearAll() {
        chatListLiveData.removeSource(chatListDbSource);
        stopRefreshChats();
        DeleteDatabase deleteDatabase = new DeleteDatabase();
        deleteDatabase.setDatabaseReference(meetDayDatabase);
        deleteDatabase.execute();
    }

    private void refreshChats() {
        chatsDataRefresher.refreshData(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                fetchChats();
                return null;
            }
        });
    }

    private void refreshChatMessages(final String chatId) {
        chatMessagesDataRefresher.refreshData(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                fetchChatMessages(chatId);
                return null;
            }
        });
    }

    private void stopRefreshChats() {
        chatsDataRefresher.stop();
    }

    private void stopRefreshChatMessages() {
        chatMessagesDataRefresher.stop();
    }

    private void saveChats(@NonNull final List<Chat> chatList) {
        new Thread(new Runnable() {
            public void run() {
                meetDayDatabase.getChatDao().upsert(chatList);
            }
        }).start();
    }

    private void saveChatMessages(@NonNull final List<Message> messageList) {
        new Thread(new Runnable() {
            public void run() {
                meetDayDatabase.getMessageDao().insert(messageList);
            }
        }).start();
    }

    /**
     * AsyncTask section
     */

    private static class DeleteDatabase extends AsyncTask<Void, Void, Void> {

        private MeetDayDatabase databaseReference;

        @Override
        protected Void doInBackground(Void... voids) {
            databaseReference.getChatDao().deleteAll();
            return null;
        }

        void setDatabaseReference(MeetDayDatabase databaseReference) {
            this.databaseReference = databaseReference;
        }
    }

    /**
     * Testing section
     */

    @VisibleForTesting
    public Observer<List<Chat>> getChatListDatabaseObserver() {
        return chatListDatabaseObserver;
    }

    public Observer<List<Message>> getChatMessageListDatabaseObserver() {
        return chatMessageListDatabaseObserver;
    }

    @VisibleForTesting
    public EventObserver<List<Chat>, List<Chat>> getFetchChatsObserver() {
        return fetchChatsObserver;
    }

    @VisibleForTesting
    public EventObserver<List<Message>, List<Message>> getFetchChatMessagesObserver() {
        return fetchChatMessagesObserver;
    }

    @VisibleForTesting
    public EventObserver<Message, Message> getSendChatMessageObserver() {
        return sendChatMessageObserver;
    }
}
