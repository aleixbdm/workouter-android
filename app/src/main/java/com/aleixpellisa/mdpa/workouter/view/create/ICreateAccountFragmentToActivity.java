package com.aleixpellisa.mdpa.workouter.view.create;

import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;

public interface ICreateAccountFragmentToActivity {

    void fragmentReady(String fragmentTag);

    void setEnabledRightButton(boolean enabled);

    void cancelCreate();

    void goToFillAge(String name, String firstName, String image);

    void goToFillLocation(int age, @Profile.Gender String gender, @Settings.Preference String preference);

    void goToFinishCreate(String city);

    void attemptFinishCreate();

    void returnFromFillLocation();

    void returnFromFinishCreate();

}
