package com.aleixpellisa.mdpa.workouter.view.settings;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.builder.SettingsBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesKey;
import com.aleixpellisa.mdpa.workouter.view.dialog.CloseDialog;
import com.aleixpellisa.mdpa.workouter.view.main.IBottomTabNavigation;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;
import com.appyvet.materialrangebar.RangeBar;

class SettingsView {

    private Context context;
    private View view;

    private ProfileViewModel profileViewModel;

    private ImageButton genderMaleImageView;
    private ImageButton genderFemaleImageView;
    private ImageButton genderAllImageView;

    private int searchRangeUpdated;
    private boolean isUpdatingSearchRange = false;

    private int minYearRangeUpdated;
    private int maxYearRangeUpdated;
    private boolean isUpdatingYearRange = false;

    private IBottomTabNavigation bottomTabNavigationCallback;

    SettingsView(Activity activity, View view, ProfileViewModel profileViewModelParam) {
        this.context = activity;
        this.view = view;
        this.profileViewModel = profileViewModelParam;

        try {
            bottomTabNavigationCallback = (IBottomTabNavigation) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IBottomTabNavigation");
        }

    }

    void setupView(Settings settings) {
        initSettingsView(settings);
    }

    private void initSettingsView(Settings settings) {
        if (settings != null) {
            initLocation(settings.getCity());
            initSearchRange(settings.getSearchRange());
            initPreference(settings.getPreference());
            initYearRange(settings.getYearRangeMin(), settings.getYearRangeMax());
            initVisible(settings.isVisible());
        }
        initNotifications();
        initPrivacyTermsSection();
        initBottomButtonsSection();
    }

    private void initLocation(String city) {
        city = city != null ? city : SettingsBuilder.SETTINGS_CITY_MOCK;

        Switch locationSwitch = view.findViewById(R.id.settings_fragment_location_switch);
        TextView cityTextView = view.findViewById(R.id.settings_fragment_city_text_view);

        // Check if location permission is granted
        locationSwitch.setChecked(true);
        locationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                // Permissions
            }
        });

        cityTextView.setText(city);
    }

    private void updateLocation(String city) {
        updateSettings(Settings.CITY_KEY, city);
    }

    private void initSearchRange(Integer searchRange) {
        searchRange = searchRange != null ? searchRange : SettingsBuilder.SETTINGS_SEARCH_RANGE_MOCK;
        RangeBar searchRangeBar = view.findViewById(R.id.settings_fragment_search_range_bar);
        searchRangeBar.setSeekPinByValue(searchRange);
        searchRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex, String leftPinValue, String rightPinValue) {
                searchRangeUpdated = Integer.valueOf(rightPinValue);
                updateSearchRange();
            }

        });
    }

    private void updateSearchRange() {
        if (!isUpdatingSearchRange) {
            isUpdatingSearchRange = true;
            new Thread(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(1000);
                        updateSettings(Settings.SEARCH_RANGE_KEY, searchRangeUpdated);
                        isUpdatingSearchRange = false;
                    } catch (Exception ignored) {
                    }
                }
            }).start();
        }
    }

    private void initPreference(String preference) {
        preference = preference != null ? preference : SettingsBuilder.SETTINGS_PREFERENCE_MOCK;

        genderMaleImageView = view.findViewById(R.id.settings_fragment_gender_male_image_view);
        genderFemaleImageView = view.findViewById(R.id.settings_fragment_gender_female_image_view);
        genderAllImageView = view.findViewById(R.id.settings_fragment_gender_all_image_view);

        genderMaleImageView.setActivated(preference.equals(Settings.PREFERENCE_MALE));
        genderFemaleImageView.setActivated(preference.equals(Settings.PREFERENCE_FEMALE));
        genderAllImageView.setActivated(preference.equals(Settings.PREFERENCE_ALL));

        genderMaleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePreference(Settings.PREFERENCE_MALE);
            }
        });

        genderFemaleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePreference(Settings.PREFERENCE_FEMALE);
            }
        });

        genderAllImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePreference(Settings.PREFERENCE_ALL);
            }
        });

    }

    private void updatePreference(@Settings.Preference String preference) {
        genderMaleImageView.setActivated(preference.equals(Settings.PREFERENCE_MALE));
        genderFemaleImageView.setActivated(preference.equals(Settings.PREFERENCE_FEMALE));
        genderAllImageView.setActivated(preference.equals(Settings.PREFERENCE_ALL));
        updateSettings(Settings.PREFERENCE_KEY, preference);
    }

    private void initYearRange(Integer minValue, Integer maxValue) {
        minValue = minValue != null ? minValue : SettingsBuilder.SETTINGS_YEAR_RANGE_MIN_MOCK;
        maxValue = maxValue != null ? maxValue : SettingsBuilder.SETTINGS_YEAR_RANGE_MAX_MOCK;

        RangeBar yearRangeBar = view.findViewById(R.id.settings_fragment_year_range_bar);
        yearRangeBar.setRangePinsByValue(minValue, maxValue);
        yearRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex, String leftPinValue, String rightPinValue) {
                int min = Integer.valueOf(leftPinValue);
                int max = Integer.valueOf(rightPinValue);
                if (min <= max) {
                    minYearRangeUpdated = min;
                    maxYearRangeUpdated = max;
                    updateYearRange();
                } else {
                    minYearRangeUpdated = max;
                    maxYearRangeUpdated = min;
                    updateYearRange();
                }
            }

        });
    }

    private void updateYearRange() {
        if (!isUpdatingYearRange) {
            isUpdatingYearRange = true;
            new Thread(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(1000);
                        updateSettings(Settings.YEAR_RANGE_MIN_KEY, minYearRangeUpdated);
                        updateSettings(Settings.YEAR_RANGE_MAX_KEY, maxYearRangeUpdated);
                        isUpdatingYearRange = false;
                    } catch (Exception ignored) {
                    }
                }
            }).start();
        }
    }

    private void initVisible(Boolean isVisible) {
        isVisible = isVisible != null ? isVisible : SettingsBuilder.SETTINGS_VISIBLE_MOCK;

        Switch visibleSwitch = view.findViewById(R.id.settings_fragment_visible_switch);

        visibleSwitch.setChecked(isVisible);
        visibleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                updateVisible(b);
            }
        });
    }

    private void updateVisible(boolean visible) {
        updateSettings(Settings.VISIBLE_KEY, visible);
    }

    private void initNotifications() {
        Switch newMatchesSwitch = view.findViewById(R.id.settings_fragment_new_matches_switch);
        Switch newMessagesSwitch = view.findViewById(R.id.settings_fragment_new_messages_switch);
        Switch vibrationSwitch = view.findViewById(R.id.settings_fragment_vibration_switch);
        Switch soundSwitch = view.findViewById(R.id.settings_fragment_sound_switch);

        // Check permission
        newMatchesSwitch.setChecked(true);
        newMatchesSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                // Permissions
            }
        });

        newMessagesSwitch.setChecked(true);
        newMessagesSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                // Permissions
            }
        });

        vibrationSwitch.setChecked(true);
        vibrationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                // Permissions
            }
        });

        soundSwitch.setChecked(true);
        soundSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                // Permissions
            }
        });
    }

    private void initPrivacyTermsSection() {
        TextView privacyTextView = view.findViewById(R.id.settings_fragment_privacy_text_view);
        TextView termsTextView = view.findViewById(R.id.settings_fragment_terms_text_view);

        privacyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomTabNavigationCallback.goToPrivacyPolicy();
            }
        });

        termsTextView.setText(context.getText(R.string.settings_terms_and_conditions));
        termsTextView.setMovementMethod(LinkMovementMethod.getInstance());

    }

    private void initBottomButtonsSection() {
        Button shareButton = view.findViewById(R.id.settings_share_button);
        Button logoutButton = view.findViewById(R.id.settings_logout_button);
        Button deleteAccountButton = view.findViewById(R.id.settings_delete_account_button);

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomTabNavigationCallback.share();
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CloseDialog customDialog =
                        new CloseDialog(context, CloseDialog.LOGOUT_OPTION);
                if (customDialog.getWindow() != null) {
                    customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }
                customDialog.show();
            }
        });

        deleteAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CloseDialog customDialog =
                        new CloseDialog(context, CloseDialog.DELETE_ACCOUNT_OPTION);
                if (customDialog.getWindow() != null) {
                    customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }
                customDialog.show();
            }
        });
    }

    private void updateSettings(@PreferencesKey String key, Object value) {
        try {
            profileViewModel.attemptUpdateSettings(key, value);
        } catch (IllegalArgumentException ex) {
            // Do something with the meetDayError
            ex.printStackTrace();
        }
    }

}
