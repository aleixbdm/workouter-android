package com.aleixpellisa.mdpa.workouter.model;

public class Auth extends BaseModel {

    public static final String ACCESS_TOKEN = "access_token";
    public static final String USER_ID = "user_id";

    private String accessToken;

    private String userId;

    public Auth(String accessToken, String userId) {
        this.accessToken = accessToken;
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
