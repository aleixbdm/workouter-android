package com.aleixpellisa.mdpa.workouter.view.login;

import android.content.Context;

import com.aleixpellisa.mdpa.workouter.view.BaseFragment;

public class LoginBaseFragment extends BaseFragment {

    public ILoginFragmentToActivity fragmentToActivityCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentToActivityCallback = (ILoginFragmentToActivity) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement ILoginFragmentToActivity");
        }
    }

    @Override
    public void onDetach() {
        fragmentToActivityCallback = null;
        super.onDetach();
    }

}
