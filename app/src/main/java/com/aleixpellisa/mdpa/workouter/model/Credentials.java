package com.aleixpellisa.mdpa.workouter.model;

public class Credentials extends BaseModel {

    public static final int MIN_PASSWORD_LENGTH = 3;

    private String email;

    private String password;

    public Credentials(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
