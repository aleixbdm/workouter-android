package com.aleixpellisa.mdpa.workouter.builder;

import android.content.res.Resources;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.network.schema.AuthSchema;

import java.util.List;

public class AuthBuilder extends BaseBuilder {

    public static final String AUTH_ACCESS_TOKEN_MOCK = "xxxx.yyyy.zzzz";
    public static final String AUTH_USER_ID_MOCK = "userId1";

    private String accessToken;
    private String userId;

    public AuthBuilder(Auth auth) {
        if (auth == null) return;
        this.userId = auth.getUserId();
        this.accessToken = auth.getAccessToken();
    }

    public AuthBuilder(AuthSchema authSchema) {
        this.accessToken = authSchema.accessToken;
        this.userId = authSchema.userId;
    }

    public AuthBuilder(String accessToken, String userId) {
        this.accessToken = accessToken;
        this.userId = userId;
    }

    public AuthBuilder(Resources resources) {
        super.setResources(resources);
    }

    public Auth build() {
        return new Auth(accessToken, userId);
    }

    public Auth buildFromResources() {
        List<AuthSchema> authSchemaList = this.getSchemaListFromFile(R.raw.auth, AuthSchema.class);
        if (!authSchemaList.isEmpty()) {
            AuthSchema authSchema = authSchemaList.get(0);
            AuthBuilder authBuilder = new AuthBuilder(authSchema);
            return authBuilder.build();
        } else {
            return null;
        }
    }

    public AuthSchema buildSchema() {
        Auth auth = build();
        AuthSchema authSchema = new AuthSchema();
        authSchema.accessToken = auth.getAccessToken();
        authSchema.userId = auth.getUserId();
        return authSchema;
    }

    public static Auth buildEmptyAuth() {
        AuthBuilder authBuilder =
                new AuthBuilder("", "");
        return authBuilder.build();
    }

    public static Auth buildMockedAuth() {
        AuthBuilder authBuilder =
                new AuthBuilder(AUTH_ACCESS_TOKEN_MOCK, AUTH_USER_ID_MOCK);
        return authBuilder.build();
    }

}
