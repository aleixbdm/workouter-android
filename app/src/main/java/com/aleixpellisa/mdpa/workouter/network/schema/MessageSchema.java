package com.aleixpellisa.mdpa.workouter.network.schema;

import com.google.gson.annotations.SerializedName;

public class MessageSchema extends BaseSchema {

    @SerializedName("id")
    public String id;

    @SerializedName("chat_id")
    public String chatId;

    @SerializedName("sender_user_id")
    public String senderUserId;

    @SerializedName("message_text")
    public String messageText;

    @SerializedName("created_at")
    public Long createdAt;

}
