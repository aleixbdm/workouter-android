package com.aleixpellisa.mdpa.workouter.network.schema;

import com.aleixpellisa.mdpa.workouter.network.response.MessageResponse;
import com.aleixpellisa.mdpa.workouter.network.response.UserChatResponse;
import com.google.gson.annotations.SerializedName;

public class ChatSchema extends BaseSchema {

    @SerializedName("id")
    public String id;

    @SerializedName("candidate_user")
    public UserChatResponse candidateUser;

    @SerializedName("last_message")
    public MessageResponse lastMessage;

    @SerializedName("updated_at")
    public Long updatedAt;

}
