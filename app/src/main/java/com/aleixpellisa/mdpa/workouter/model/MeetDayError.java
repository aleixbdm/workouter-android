package com.aleixpellisa.mdpa.workouter.model;

public class MeetDayError {

    String message;
    boolean authenticated;

    public MeetDayError(String message) {
        this.message = message;
        this.authenticated = true;
    }

    public MeetDayError(String message, boolean authenticated) {
        this.message = message;
        this.authenticated = authenticated;
    }

    public String getMessage() {
        return message;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }
}
