package com.aleixpellisa.mdpa.workouter.network.schema;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CandidateSchema extends BaseSchema {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("image_urls")
    public List<String> imageUrls;

    @SerializedName("age")
    public Integer age;

    @SerializedName("skills")
    public List<SkillSchema> skills;

    @SerializedName("city")
    public String city;

}
