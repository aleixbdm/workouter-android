package com.aleixpellisa.mdpa.workouter.network.schema;

import com.google.gson.annotations.SerializedName;

public class AuthErrorSchema extends ErrorSchema {
    @SerializedName("authenticated")
    public Boolean authenticated;
}
