package com.aleixpellisa.mdpa.workouter.manager.refresher;

import com.aleixpellisa.mdpa.workouter.manager.PreferencesManager;
import com.aleixpellisa.mdpa.workouter.manager.observer.EventObserver;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.network.interceptor.AuthTokenInterceptor;
import com.aleixpellisa.mdpa.workouter.network.rest.AuthRest;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;

import java.util.concurrent.Callable;

import javax.inject.Inject;

public class TokenRefresher {

    @Inject
    PreferencesManager preferencesManager;

    @Inject
    AuthTokenInterceptor authTokenInterceptor;

    @Inject
    AuthRest authRest;

    @Inject
    TokenRefresher(PreferencesManager preferencesManager,
                   AuthTokenInterceptor authTokenInterceptor) {
        this.preferencesManager = preferencesManager;
        this.authTokenInterceptor = authTokenInterceptor;
    }

    public <T> Callable refreshTokenCallable(final Callable<Void> pendingFunction, final EventLiveData<Event<T>> eventLiveData) {
        return new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                refreshToken(pendingFunction, eventLiveData);
                return null;
            }
        };
    }

    private <T> void refreshToken(final Callable<Void> pendingFunction, final EventLiveData<Event<T>> eventLiveData) {
        EventObserver<Auth, T> refreshTokenObserver = new EventObserver<Auth, T>(null, eventLiveData) {
            @Override
            public void onSuccess(Event<Auth> event) {
                // Save auth
                Auth auth = event.data;
                preferencesManager.saveAuth(auth);
                authTokenInterceptor.setAccessToken(auth.getAccessToken());

                if (pendingFunction != null) {
                    try {
                        pendingFunction.call();
                    } catch (Exception ignored) {
                    }
                }
            }
        };
        Auth auth = preferencesManager.readAuth();
        String userId = auth != null ? auth.getUserId() : null;
        authRest.refreshToken(userId, refreshTokenObserver);
    }

}
