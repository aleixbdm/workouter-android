package com.aleixpellisa.mdpa.workouter.view.profile;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.view.BaseActivity;
import com.aleixpellisa.mdpa.workouter.view.dialog.CloseDialog;
import com.aleixpellisa.mdpa.workouter.view.dialog.DialogHandler;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;

import javax.inject.Inject;

public class EditProfileActivity extends BaseActivity implements ICloseEditProfileCallback, IProfileImageCallback {

    public static final int REQUEST_CODE = 101;

    @Inject
    DialogHandler dialogHandler;

    ProfileViewModel profileViewModel;

    private ProfileView profileView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        setupView(R.layout.fragment_profile);
    }

    @Override
    public void setupView(int layoutResID) {
        super.setupView(layoutResID);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        profileView =
                new ProfileView(this, getWindow().findViewById(android.R.id.content),
                        profileViewModel, dialogHandler, false, false, true);

        profileViewModel.getProfile().observe(this, new Observer<Event<Profile>>() {
            @Override
            public void onChanged(@Nullable Event<Profile> event) {
                if (event != null) {
                    switch (event.status) {
                        case Event.SUCCESS:
                            profileView.setupProfileView(event.data);
                            break;
                        case Event.ERROR:
                            dialogHandler.hideProgressDialogWithError(event.meetDayError);
                            break;
                        case Event.AUTH_ERROR:
                            break;
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.edit_profile_save_button:
                attemptSave();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        final CloseDialog customDialog =
                new CloseDialog(this, CloseDialog.CLOSE_EDIT_PROFILE_OPTION);
        if (customDialog.getWindow() != null) {
            customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        customDialog.show();
    }

    @Override
    public void closeEditProfile() {
        removeObservers();
        setResult(REQUEST_CODE, null);
        finish();
    }

    @Override
    public void imageAdded(String url) {
        profileView.addProfileImage(url);
    }

    private void attemptSave() {
        profileView.attemptUpdateProfile();
    }

    private void removeObservers() {
        profileViewModel.getProfile().removeObservers(this);
    }
}
