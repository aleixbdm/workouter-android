package com.aleixpellisa.mdpa.workouter.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.view.create.ICloseCreateProfileCallback;
import com.aleixpellisa.mdpa.workouter.view.main.IBottomTabNavigation;
import com.aleixpellisa.mdpa.workouter.view.profile.ICloseEditProfileCallback;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class CloseDialog extends Dialog {

    public static final int CLOSE_EDIT_PROFILE_OPTION = 0;
    public static final int CLOSE_CREATE_PROFILE_OPTION = 1;
    public static final int LOGOUT_OPTION = 2;
    public static final int DELETE_ACCOUNT_OPTION = 3;

    @IntDef({CLOSE_EDIT_PROFILE_OPTION, CLOSE_CREATE_PROFILE_OPTION, LOGOUT_OPTION, DELETE_ACCOUNT_OPTION})
    @Retention(RetentionPolicy.SOURCE)
    @interface CloseOption {
    }

    public Context context;
    private @CloseOption
    int closeOption;

    private TextView titleTextView;
    private TextView descriptionTextView;
    private Button confirmButton;

    public CloseDialog(Context context, @CloseOption int closeOption) {
        super(context);
        this.context = context;
        this.closeOption = closeOption;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.close_dialog);
        titleTextView = findViewById(R.id.close_dialog_title_text_view);
        descriptionTextView = findViewById(R.id.close_dialog_description_text_view);
        Button cancelButton = findViewById(R.id.close_dialog_cancel_button);
        confirmButton = findViewById(R.id.close_dialog_confirm_button);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        initDialogCloseOption();
    }

    private void initDialogCloseOption() {
        switch (closeOption) {
            case CLOSE_EDIT_PROFILE_OPTION:
                initCloseEditProfile();
                break;
            case CLOSE_CREATE_PROFILE_OPTION:
                initCloseCreateProfile();
                break;
            case LOGOUT_OPTION:
                initLogout();
                break;
            case DELETE_ACCOUNT_OPTION:
                initDeleteAccount();
                break;
        }
    }

    private void initCloseEditProfile() {
        titleTextView.setText(context.getString(R.string.close_dialog_exit_edit_profile_title));
        descriptionTextView.setText(context.getString(R.string.close_dialog_description_default));

        final ICloseEditProfileCallback closeEditProfileCallback;
        try {
            closeEditProfileCallback = (ICloseEditProfileCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement ICloseEditProfileCallback");
        }

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                closeEditProfileCallback.closeEditProfile();
            }
        });
    }

    private void initCloseCreateProfile() {
        titleTextView.setText(context.getString(R.string.close_dialog_cancel_create_profile_title));
        descriptionTextView.setText(context.getString(R.string.close_dialog_description_default));

        final ICloseCreateProfileCallback closeCreateProfileCallback;
        try {
            closeCreateProfileCallback = (ICloseCreateProfileCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement ICloseCreateProfileCallback");
        }

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                closeCreateProfileCallback.closeCreateProfile();
            }
        });
    }

    private void initLogout() {
        titleTextView.setText(context.getString(R.string.close_dialog_logout_title));
        descriptionTextView.setText(context.getString(R.string.close_dialog_description_logout));

        final IBottomTabNavigation bottomTabNavigation;
        try {
            bottomTabNavigation = (IBottomTabNavigation) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IBottomTabNavigation");
        }

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                bottomTabNavigation.logout();
            }
        });
    }

    private void initDeleteAccount() {
        titleTextView.setText(context.getString(R.string.close_dialog_delete_account_title));
        descriptionTextView.setText(context.getString(R.string.close_dialog_description_delete_account));

        final IBottomTabNavigation bottomTabNavigation;
        try {
            bottomTabNavigation = (IBottomTabNavigation) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IBottomTabNavigation");
        }

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                bottomTabNavigation.deleteAccount();
            }
        });
    }

}