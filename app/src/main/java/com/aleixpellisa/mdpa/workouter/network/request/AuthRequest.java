package com.aleixpellisa.mdpa.workouter.network.request;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class AuthRequest {
    @SerializedName("email")
    public String email;

    @SerializedName("password")
    public String password;
}

