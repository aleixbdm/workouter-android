package com.aleixpellisa.mdpa.workouter.builder;

import android.content.res.Resources;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.UserChat;
import com.aleixpellisa.mdpa.workouter.network.response.MessageResponse;
import com.aleixpellisa.mdpa.workouter.network.response.UserChatResponse;
import com.aleixpellisa.mdpa.workouter.network.schema.ChatSchema;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ChatBuilder extends BaseBuilder {

    public static final List<ChatSchema> CHAT_SCHEMA_LIST_MOCK = new ArrayList<ChatSchema>() {{
        ChatSchema chatSchema1 = new ChatSchema();
        chatSchema1.id = "ChatId1";
        chatSchema1.candidateUser = new UserChatResponse();
        UserChatBuilder userChatBuilder = new UserChatBuilder("userChatId1", "Name1", "FirstName1", "image1");
        chatSchema1.candidateUser.userChatSchema = userChatBuilder.buildSchema();
        chatSchema1.lastMessage = new MessageResponse();
        MessageBuilder messageBuilder = new MessageBuilder("senderUserId1", "Message1");
        chatSchema1.lastMessage.messageSchema = messageBuilder.buildSchema();
        chatSchema1.updatedAt = Calendar.getInstance().getTimeInMillis();
        add(chatSchema1);

        ChatSchema chatSchema2 = new ChatSchema();
        chatSchema2.id = "ChatId2";
        chatSchema2.candidateUser = new UserChatResponse();
        userChatBuilder = new UserChatBuilder("userChatId2", "Name2", "FirstName2", "image2");
        chatSchema2.candidateUser.userChatSchema = userChatBuilder.buildSchema();
        chatSchema2.lastMessage = new MessageResponse();
        messageBuilder = new MessageBuilder("senderUserId2", "Message2");
        chatSchema2.lastMessage.messageSchema = messageBuilder.buildSchema();
        chatSchema2.updatedAt = Calendar.getInstance().getTimeInMillis();
        add(chatSchema2);
    }};

    private List<ChatSchema> chatSchemaList;

    public String id;
    private UserChat userChat;
    private String lastMessage;
    private Long updatedAt;

    public ChatBuilder() {
        chatSchemaList = new ArrayList<>();
    }

    public ChatBuilder(List<ChatSchema> chatSchemaList) {
        this.chatSchemaList = chatSchemaList;
    }

    public ChatBuilder(Resources resources) {
        super.setResources(resources);
        chatSchemaList = this.getSchemaListFromFile(R.raw.chats, ChatSchema.class);
    }

    public ChatBuilder(Chat chat) {
        if (chat == null) return;
        this.id = chat.getId();
        this.userChat = chat.getUserChat();
        this.lastMessage = chat.getLastMessage();
        this.updatedAt = chat.getUpdatedAt();
    }

    public Chat build() {
        return new Chat(id, userChat, lastMessage, updatedAt);
    }

    public ChatSchema buildSchema() {
        Chat chat = build();
        ChatSchema chatSchema = new ChatSchema();
        chatSchema.id = chat.getId();
        UserChatResponse userChatResponse = new UserChatResponse();
        userChatResponse.userChatSchema = new UserChatBuilder(chat.getUserChat()).buildSchema();
        chatSchema.candidateUser = userChatResponse;
        MessageResponse messageResponse = new MessageResponse();
        // messageResponse.messageSchema = new MessageBuilder(chat.getLastMessage()).buildSchema();
        messageResponse.messageSchema = new MessageBuilder().buildSchema();
        chatSchema.lastMessage = messageResponse;
        chatSchema.updatedAt = chat.getUpdatedAt();
        return chatSchema;
    }

    public List<Chat> buildList() {
        List<Chat> chatList = new ArrayList<>();
        if (chatSchemaList != null) {
            for (ChatSchema chatSchema : chatSchemaList) {
                String lastMessage = chatSchema.lastMessage != null ? chatSchema.lastMessage.messageSchema.messageText : null;
                if (chatSchema.candidateUser != null) {
                    UserChatBuilder userChatBuilder = new UserChatBuilder(chatSchema.candidateUser.userChatSchema);

                    chatList.add(new Chat(
                            chatSchema.id,
                            userChatBuilder.build(),
                            lastMessage,
                            chatSchema.updatedAt
                    ));
                }
            }
        }
        return chatList;
    }

    public static List<Chat> buildEmptyChatList() {
        ChatBuilder chatBuilder =
                new ChatBuilder();
        return chatBuilder.buildList();
    }

    public static List<Chat> buildMockedChatList() {
        ChatBuilder chatBuilder =
                new ChatBuilder(CHAT_SCHEMA_LIST_MOCK);
        return chatBuilder.buildList();
    }

}
