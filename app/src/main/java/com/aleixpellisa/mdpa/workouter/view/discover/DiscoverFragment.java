package com.aleixpellisa.mdpa.workouter.view.discover;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.model.MeetDayError;
import com.aleixpellisa.mdpa.workouter.view.BaseFragment;
import com.aleixpellisa.mdpa.workouter.view.main.IBottomTabNavigation;
import com.aleixpellisa.mdpa.workouter.view.model.CandidateViewModel;
import com.aleixpellisa.mdpa.workouter.view.profile.CandidateProfileActivity;
import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.listeners.ItemRemovedListener;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class DiscoverFragment extends BaseFragment implements ISendEvaluationCallback {

    public static final String TAG = "DiscoverFragmentTag";

    CandidateViewModel candidateViewModel;

    @BindView(R.id.discover_fragment_information_text_view)
    TextView informationTextView;
    @BindView(R.id.discover_fragment_swipe_view)
    SwipePlaceHolderView swipeView;
    @BindView(R.id.layout_evaluation_button_layout)
    LinearLayout buttonLayout;
    @BindView(R.id.discover_error_layout)
    LinearLayout errorLayout;
    @BindView(R.id.discover_error_text_view)
    TextView errorTextView;
    @BindView(R.id.discover_error_button)
    Button errorButton;

    private Context context;
    private IBottomTabNavigation bottomTabNavigationCallback;

    private boolean notVisibleError = false;
    private Candidate candidateEvaluated;
    private LinkedList<Candidate> visibleCandidates = new LinkedList<>();
    private boolean isRebased = false;

    public static DiscoverFragment newInstance() {
        return new DiscoverFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_discover, container, false);

        setupView(view);

        return view;
    }

    @Override
    public void setupView(View view) {
        super.setupView(view);

        initViewModel();

        context = view.getContext();

        // Wait to button layout to be loaded to correctly fit the size of the candidate swipe view
        buttonLayout.post(new Runnable() {
            @Override
            public void run() {
                setupSwipeView();
            }
        });

        notifyFragmentReady();

        downloadCandidates();

        swipeView.addItemRemoveListener(new ItemRemovedListener() {
            @Override
            public void onItemRemoved(int count) {
                if (isRebased) {
                    isRebased = false;
                    swipeView.enableTouchSwipe();
                    swipeView.undoLastSwipe();
                } else {
                    visibleCandidates.removeFirst();
                    if (count < CandidateCard.VISIBLE_STACK_CANDIDATE_CARDS) {
                        downloadCandidates();
                    }
                }
            }
        });
    }

    private void initViewModel() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            candidateViewModel = ViewModelProviders.of(activity).get(CandidateViewModel.class);
        }
    }

    private void notifyFragmentReady() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            try {
                bottomTabNavigationCallback = (IBottomTabNavigation) activity;
                bottomTabNavigationCallback.fragmentReady(TAG);
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement IBottomTabNavigation");
            }
        }
    }

    public void downloadCandidates() {
        try {
            candidateViewModel.attemptDownloadCandidates();
            notVisibleError = false;
        } catch (IllegalAccessException e) {
            addError(new MeetDayError(getString(R.string.discover_error_not_visible)));
        }
    }

    private void setupSwipeView() {
        if (getActivity() != null) {
            int height = buttonLayout.getHeight();
            int bottomMargin = CandidateCardUtils.dpToPx(height);
            Point windowSize = CandidateCardUtils.getDisplaySize(getActivity().getWindowManager());
            swipeView.getBuilder()
                    .setDisplayViewCount(CandidateCard.VISIBLE_STACK_CANDIDATE_CARDS)
                    .setHeightSwipeDistFactor(10)
                    .setWidthSwipeDistFactor(5)
                    .setIsUndoEnabled(true)
                    .setSwipeDecor(new SwipeDecor()
                            .setViewWidth(windowSize.x)
                            .setViewHeight(windowSize.y - bottomMargin)
                            .setViewGravity(Gravity.TOP)
                            .setPaddingTop(20)
                            .setRelativeScale(0.01f)
                            .setSwipeMaxChangeAngle(2f)
                            .setSwipeInMsgLayoutId(R.layout.candidate_like_swipe_view)
                            .setSwipeOutMsgLayoutId(R.layout.candidate_dislike_swipe_view));
        }
    }

    public void addCandidates(List<Candidate> candidateList) {
        if (swipeView != null) {
            informationTextView.setVisibility(View.VISIBLE);
            swipeView.setVisibility(View.VISIBLE);
            buttonLayout.setVisibility(View.VISIBLE);
            errorLayout.setVisibility(View.GONE);
            for (Candidate candidate : candidateList) {
                if (!containsCandidate(candidate.getId())) {
                    visibleCandidates.addLast(candidate);
                    swipeView.addView(new CandidateCard(this, context, candidate));
                }
            }
        }
    }

    public void addError(MeetDayError meetDayError) {
        notVisibleError = getString(R.string.discover_error_not_visible).equals(meetDayError.getMessage());
        if (swipeView != null && swipeView.getChildCount() == 0) {
            informationTextView.setVisibility(View.GONE);
            swipeView.setVisibility(View.GONE);
            // Only invisible to get height correctly inside setupSwipeView()
            buttonLayout.setVisibility(View.INVISIBLE);
            errorLayout.setVisibility(View.VISIBLE);
            errorTextView.setText(meetDayError.getMessage());
            if (notVisibleError) {
                errorButton.setText(R.string.discover_error_not_visible_button);
            } else {
                errorButton.setText(R.string.discover_error_retry_button);
            }
        }
    }

    public void evaluationDone(Evaluation evaluation) {
        if (swipeView != null) {
            swipeView.enableTouchSwipe();
            if (evaluation.getEvaluationValue().equals(Evaluation.EVALUATION_MATCH) && candidateEvaluated != null) {
                final MatchDialogFragment customDialog = new MatchDialogFragment(context, candidateEvaluated);
                if (customDialog.getWindow() != null) {
                    customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }
                customDialog.show();
                candidateEvaluated = null;
            }
        }
    }

    public void rebaseEvaluation() {
        isRebased = true;
        candidateEvaluated = null;
    }

    public void notifyIsVisible() {
        if (notVisibleError) {
            this.downloadCandidates();
        }
    }

    @OnClick(R.id.discover_fragment_dislike_button)
    public void dislikeButtonClicked() {
        swipeView.doSwipe(false);
    }

    @OnClick(R.id.discover_fragment_like_button)
    public void likeButtonClicked() {
        swipeView.doSwipe(true);
    }

    @OnClick(R.id.discover_error_button)
    public void errorButtonClicked() {
        if (notVisibleError) {
            bottomTabNavigationCallback.goToSettings();
        } else {
            this.downloadCandidates();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == CandidateProfileActivity.REQUEST_CODE) {
            if (intent != null && intent.getExtras() != null) {
                Bundle data = intent.getExtras();
                if (data.getBoolean(CandidateProfileActivity.LIKE_PRESSED_KEY)) {
                    swipeView.doSwipe(true);
                } else if (data.getBoolean(CandidateProfileActivity.DISLIKE_PRESSED_KEY)) {
                    swipeView.doSwipe(false);
                }
            }
        }
    }

    @Override
    public void sendEvaluation(Candidate candidate, @Evaluation.EvaluationValue String evaluation) {
        candidateViewModel.sendEvaluation(candidate.getId(), evaluation);
        this.candidateEvaluated = candidate;
        swipeView.disableTouchSwipe();
    }

    private boolean containsCandidate(String candidateId) {
        for (Candidate candidate : visibleCandidates) {
            if (candidate.getId().equals(candidateId)) {
                return true;
            }
        }
        return false;
    }
}