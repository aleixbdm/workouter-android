package com.aleixpellisa.mdpa.workouter.network.response;

import com.aleixpellisa.mdpa.workouter.network.schema.AuthSchema;
import com.google.gson.annotations.SerializedName;

public class AuthResponse extends BaseResponse {
    @SerializedName("auth")
    public AuthSchema authSchema;
}
