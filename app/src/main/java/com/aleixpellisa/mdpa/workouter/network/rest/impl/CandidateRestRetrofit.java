package com.aleixpellisa.mdpa.workouter.network.rest.impl;

import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.aleixpellisa.mdpa.workouter.builder.CandidateBuilder;
import com.aleixpellisa.mdpa.workouter.builder.EvaluationBuilder;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.network.callback.RetrofitCallback;
import com.aleixpellisa.mdpa.workouter.network.request.EvaluationRequest;
import com.aleixpellisa.mdpa.workouter.network.response.CandidateResponse;
import com.aleixpellisa.mdpa.workouter.network.response.EvaluationResponse;
import com.aleixpellisa.mdpa.workouter.network.rest.CandidateRest;
import com.aleixpellisa.mdpa.workouter.network.rest.api.CandidateRestAPI;
import com.aleixpellisa.mdpa.workouter.network.schema.CandidateSchema;
import com.aleixpellisa.mdpa.workouter.network.schema.EvaluationSchema;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Retrofit;

@Singleton
public class CandidateRestRetrofit implements CandidateRest {

    private final static int DEFAULT_DISCOVER_LIMIT = 5;

    private CandidateRestAPI restAPI;

    private RetrofitCallback<List<CandidateResponse>, List<Candidate>> discoverCallback;
    private RetrofitCallback<EvaluationResponse, Evaluation> evaluateCallback;

    @Inject
    public CandidateRestRetrofit(Retrofit client) {
        restAPI = client.create(CandidateRestAPI.class);
    }

    @Override
    public void fetchCandidates(final Observer<Event<List<Candidate>>> responseObserver) {
        Call<List<CandidateResponse>> call = restAPI.discover(DEFAULT_DISCOVER_LIMIT);
        discoverCallback = new RetrofitCallback<List<CandidateResponse>, List<Candidate>>(responseObserver) {
            @Override
            public void onSuccess(@NonNull List<CandidateResponse> responseBody) {
                List<CandidateSchema> candidateSchemaList = new ArrayList<>();
                for (CandidateResponse candidateResponse : responseBody) {
                    candidateSchemaList.add(candidateResponse.candidateSchema);
                }
                CandidateBuilder candidateBuilder = new CandidateBuilder(candidateSchemaList);
                event = Event.success(candidateBuilder.buildList());
                responseObserver.onChanged(event);
            }
        };
        call.enqueue(discoverCallback);
    }

    @Override
    public void sendEvaluation(String candidateId, @Evaluation.EvaluationValue String evaluation,
                               final Observer<Event<Evaluation>> responseObserver) {
        EvaluationRequest evaluationRequest = new EvaluationRequest();
        evaluationRequest.candidateId = candidateId;
        evaluationRequest.evaluationValue = evaluation;
        Call<EvaluationResponse> call = restAPI.evaluate(evaluationRequest);
        evaluateCallback = new RetrofitCallback<EvaluationResponse, Evaluation>(responseObserver) {
            @Override
            public void onSuccess(@NonNull EvaluationResponse responseBody) {
                EvaluationSchema evaluationSchema = responseBody.evaluationSchema;
                if (evaluationSchema != null) {
                    EvaluationBuilder evaluationBuilder = new EvaluationBuilder(evaluationSchema);
                    event = Event.success(evaluationBuilder.build());
                }
                responseObserver.onChanged(event);
            }
        };
        call.enqueue(evaluateCallback);
    }

    /**
     * Testing section
     */

    @VisibleForTesting
    public RetrofitCallback<List<CandidateResponse>, List<Candidate>> getDiscoverCallback() {
        return discoverCallback;
    }

    @VisibleForTesting
    public RetrofitCallback<EvaluationResponse, Evaluation> getEvaluateCallback() {
        return evaluateCallback;
    }
}
