package com.aleixpellisa.mdpa.workouter.network.rest;

import android.arch.lifecycle.Observer;

import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.List;

public interface CandidateRest {
    void fetchCandidates(Observer<Event<List<Candidate>>> candidatesObserver);

    void sendEvaluation(String candidateId, String evaluation, Observer<Event<Evaluation>> evaluationObserver);
}
