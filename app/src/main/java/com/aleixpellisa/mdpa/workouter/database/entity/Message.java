package com.aleixpellisa.mdpa.workouter.database.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Chat.class,
        parentColumns = "id",
        childColumns = "chatId",
        onDelete = CASCADE),
        indices = {@Index("chatId")})
public class Message {

    @Ignore
    private final SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", Locale.FRANCE);

    @Ignore
    public static final String APP_MESSAGE_ID = "App";

    @Ignore
    public static final int CATEGORY_USER_MESSAGE = 0;
    @Ignore
    public static final int CATEGORY_APP_MESSAGE = 1;

    @IntDef({CATEGORY_USER_MESSAGE, CATEGORY_APP_MESSAGE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Category {
    }

    @PrimaryKey
    @NonNull
    private String id;

    private String chatId;

    private String senderUserId;

    private String messageText;

    private Long createdAt;

    public Message(String id, String chatId, String senderUserId, String messageText, Long createdAt) {
        this.id = id;
        this.chatId = chatId;
        this.senderUserId = senderUserId;
        this.messageText = messageText;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public String getChatId() {
        return chatId;
    }

    public String getSenderUserId() {
        return senderUserId;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Custom getters and setters
     */

    public Date getCreatedAtDate() {
        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        cal.setTimeInMillis(createdAt);
        return cal.getTime();
    }

    public String getFormattedDate(Date date) {
        return formatter.format(date);
    }

    public String getCurrentFormattedDate() {
        return getFormattedDate(getCreatedAtDate());
    }

    public @Category
    int getMessageCategory() {
        return APP_MESSAGE_ID.equals(senderUserId) ? CATEGORY_APP_MESSAGE : CATEGORY_USER_MESSAGE;
    }

}
