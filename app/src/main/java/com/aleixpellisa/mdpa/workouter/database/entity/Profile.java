package com.aleixpellisa.mdpa.workouter.database.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;

import com.aleixpellisa.mdpa.workouter.database.converters.MeetDayTypeConverters;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

@Entity
@Keep
public class Profile {

    public static final int MAX_IMAGE_URLS = 6;

    public static final int MAX_AGE = 65;
    public static final int MIN_AGE = 18;

    public static final String GENDER_MALE = "Male";
    public static final String GENDER_FEMALE = "Female";

    @StringDef({GENDER_MALE, GENDER_FEMALE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Gender {
    }

    @PrimaryKey
    @NonNull
    private String id;

    private String email;

    private String name;

    private String firstName;

    @TypeConverters(MeetDayTypeConverters.class)
    private List<String> imageUrls;

    private Integer age;

    private @Gender
    String gender;

    private String description;

    private String currentJob;

    private String studies;

    private String favouriteSong;

    @TypeConverters(MeetDayTypeConverters.class)
    private List<Skill> skills;

    @TypeConverters(MeetDayTypeConverters.class)
    private Settings settings;

    public Profile(String id, String email, String name, String firstName,
                   List<String> imageUrls, Integer age, String gender, String description,
                   String currentJob, String studies, String favouriteSong,
                   List<Skill> skills, Settings settings) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.firstName = firstName;
        this.imageUrls = imageUrls;
        this.age = age;
        this.gender = gender;
        this.description = description;
        this.currentJob = currentJob;
        this.studies = studies;
        this.favouriteSong = favouriteSong;
        this.skills = skills;
        this.settings = settings;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrentJob() {
        return currentJob;
    }

    public void setCurrentJob(String currentJob) {
        this.currentJob = currentJob;
    }

    public String getStudies() {
        return studies;
    }

    public void setStudies(String studies) {
        this.studies = studies;
    }

    public String getFavouriteSong() {
        return favouriteSong;
    }

    public void setFavouriteSong(String favouriteSong) {
        this.favouriteSong = favouriteSong;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

}
