package com.aleixpellisa.mdpa.workouter.view.discover;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.aleixpellisa.mdpa.workouter.database.entity.Skill;
import com.aleixpellisa.mdpa.workouter.view.helpers.SkillViewHelper;

import java.util.List;

public class SkillGridViewAdapter extends BaseAdapter {

    private final Context context;
    private final List<Skill> skills;

    SkillGridViewAdapter(Context context, List<Skill> skills) {
        this.context = context;
        this.skills = skills;
    }

    @Override
    public int getCount() {
        return skills.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Skill skill = skills.get(position);
        SkillViewHelper skillViewHelper = new SkillViewHelper(skill.getCategory());
        ImageView skillIcon = new ImageView(context);
        skillIcon.setImageDrawable(context.getDrawable(skillViewHelper.getSkillIcon()));
        return skillIcon;
    }

}
