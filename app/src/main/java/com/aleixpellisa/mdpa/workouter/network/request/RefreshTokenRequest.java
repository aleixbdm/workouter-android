package com.aleixpellisa.mdpa.workouter.network.request;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class RefreshTokenRequest {
    @SerializedName("user_id")
    public String userId;
}

