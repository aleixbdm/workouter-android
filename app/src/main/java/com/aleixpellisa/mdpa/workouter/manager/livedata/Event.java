package com.aleixpellisa.mdpa.workouter.manager.livedata;

import android.support.annotation.IntDef;

import com.aleixpellisa.mdpa.workouter.model.MeetDayError;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Event<T> {

    public static final int SUCCESS = 0;
    public static final int ERROR = 1;
    public static final int AUTH_ERROR = 2;

    @IntDef({SUCCESS, ERROR, AUTH_ERROR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Status {
    }

    public final @Status
    int status;

    public final T data;

    public final MeetDayError meetDayError;

    public Event(@Status int status, T data, MeetDayError meetDayError) {
        this.status = status;
        this.data = data;
        this.meetDayError = meetDayError;
    }

    public static <T> Event<T> success(T data) {
        return new Event<>(SUCCESS, data, null);
    }

    public static <T> Event<T> error(MeetDayError meetDayError) {
        return new Event<>(ERROR, null, meetDayError);
    }

    public static <T> Event<T> authError(MeetDayError meetDayError) {
        return new Event<>(AUTH_ERROR, null, meetDayError);
    }

}
