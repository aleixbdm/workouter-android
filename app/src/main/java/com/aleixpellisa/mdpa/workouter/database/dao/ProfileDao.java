package com.aleixpellisa.mdpa.workouter.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import com.aleixpellisa.mdpa.workouter.database.entity.Profile;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public abstract class ProfileDao {
    @Query("Select * from profile limit 1")
    public abstract LiveData<Profile> getProfile();

    @Insert(onConflict = REPLACE)
    public abstract void insert(Profile profile);

    @Update
    public abstract void update(Profile profile);

    @Query("Delete from profile")
    public abstract void deleteAll();

    @Transaction
    public void refreshDatabase(Profile profile) {
        deleteAll();
        insert(profile);
    }
}
