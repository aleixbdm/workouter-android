package com.aleixpellisa.mdpa.workouter.network.response;

import com.aleixpellisa.mdpa.workouter.network.schema.ProfileSchema;
import com.google.gson.annotations.SerializedName;

public class ProfileResponse extends BaseResponse {
    @SerializedName("user")
    public ProfileSchema profileSchema;
}
