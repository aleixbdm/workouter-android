package com.aleixpellisa.mdpa.workouter.database.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class UserChat implements Parcelable {

    private String id;

    private String name;

    private String firstName;

    private String imageUrl;

    public UserChat(String id, String name, String firstName, String imageUrl) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Parcelable part
     **/

    public static final String PARCEL_KEY = "UserChat";

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public UserChat createFromParcel(Parcel in) {
            return new UserChat(in);
        }

        public UserChat[] newArray(int size) {
            return new UserChat[size];
        }
    };

    public UserChat(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.firstName = in.readString();
        this.imageUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(firstName);
        parcel.writeString(imageUrl);
    }
}
