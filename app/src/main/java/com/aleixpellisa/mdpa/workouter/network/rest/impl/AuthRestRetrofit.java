package com.aleixpellisa.mdpa.workouter.network.rest.impl;

import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.aleixpellisa.mdpa.workouter.builder.AuthBuilder;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.network.callback.RetrofitCallback;
import com.aleixpellisa.mdpa.workouter.network.request.AuthRequest;
import com.aleixpellisa.mdpa.workouter.network.request.FacebookAuthRequest;
import com.aleixpellisa.mdpa.workouter.network.request.RefreshTokenRequest;
import com.aleixpellisa.mdpa.workouter.network.response.AuthResponse;
import com.aleixpellisa.mdpa.workouter.network.rest.AuthRest;
import com.aleixpellisa.mdpa.workouter.network.rest.api.AuthRestAPI;
import com.aleixpellisa.mdpa.workouter.network.schema.AuthSchema;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Retrofit;

@Singleton
public class AuthRestRetrofit implements AuthRest {

    private AuthRestAPI restAPI;

    private RetrofitCallback<AuthResponse, Auth> loginCallback;
    private RetrofitCallback<AuthResponse, Auth> facebookLoginCallback;
    private RetrofitCallback<AuthResponse, Auth> registerCallback;
    private RetrofitCallback<AuthResponse, Auth> refreshCallback;

    @Inject
    public AuthRestRetrofit(Retrofit client) {
        restAPI = client.create(AuthRestAPI.class);
    }

    @Override
    public void login(final String email, String password, final Observer<Event<Auth>> responseObserver) {
        AuthRequest authRequest = new AuthRequest();
        authRequest.email = email;
        authRequest.password = password;
        Call<AuthResponse> call = restAPI.login(authRequest);
        loginCallback = new RetrofitCallback<AuthResponse, Auth>(responseObserver) {
            @Override
            public void onSuccess(@NonNull AuthResponse responseBody) {
                AuthSchema authSchema = responseBody.authSchema;
                if (authSchema != null) {
                    AuthBuilder authBuilder = new AuthBuilder(authSchema);
                    event = Event.success(authBuilder.build());
                }
                responseObserver.onChanged(event);
            }
        };
        call.enqueue(loginCallback);
    }

    @Override
    public void facebookLogin(final String facebookAccessToken, final Observer<Event<Auth>> responseObserver) {
        FacebookAuthRequest facebookAuthRequest = new FacebookAuthRequest();
        facebookAuthRequest.facebookAccessToken = facebookAccessToken;
        Call<AuthResponse> call = restAPI.facebookLogin(facebookAuthRequest);
        facebookLoginCallback = new RetrofitCallback<AuthResponse, Auth>(responseObserver) {
            @Override
            public void onSuccess(@NonNull AuthResponse responseBody) {
                AuthSchema authSchema = responseBody.authSchema;
                if (authSchema != null) {
                    AuthBuilder authBuilder = new AuthBuilder(authSchema);
                    event = Event.success(authBuilder.build());
                }
                responseObserver.onChanged(event);
            }
        };
        call.enqueue(facebookLoginCallback);
    }

    @Override
    public void createAccount(String email, String password, final Observer<Event<Auth>> responseObserver) {
        AuthRequest authRequest = new AuthRequest();
        authRequest.email = email;
        authRequest.password = password;
        Call<AuthResponse> call = restAPI.register(authRequest);
        registerCallback = new RetrofitCallback<AuthResponse, Auth>(responseObserver) {
            @Override
            public void onSuccess(@NonNull AuthResponse responseBody) {
                AuthSchema authSchema = responseBody.authSchema;
                if (authSchema != null) {
                    AuthBuilder authBuilder = new AuthBuilder(authSchema);
                    event = Event.success(authBuilder.build());
                }
                responseObserver.onChanged(event);
            }
        };
        call.enqueue(registerCallback);
    }

    @Override
    public void refreshToken(String userId, final Observer<Event<Auth>> responseObserver) {
        RefreshTokenRequest refreshTokenRequest = new RefreshTokenRequest();
        refreshTokenRequest.userId = userId;
        Call<AuthResponse> call = restAPI.refreshToken(refreshTokenRequest);
        refreshCallback = new RetrofitCallback<AuthResponse, Auth>(responseObserver) {
            @Override
            public void onSuccess(@NonNull AuthResponse responseBody) {
                AuthSchema authSchema = responseBody.authSchema;
                if (authSchema != null) {
                    AuthBuilder authBuilder = new AuthBuilder(authSchema);
                    event = Event.success(authBuilder.build());
                }
                responseObserver.onChanged(event);
            }
        };
        call.enqueue(refreshCallback);
    }

    /**
     * Testing section
     */

    @VisibleForTesting
    public RetrofitCallback<AuthResponse, Auth> getLoginCallback() {
        return loginCallback;
    }

    @VisibleForTesting
    public RetrofitCallback<AuthResponse, Auth> getFacebookLoginCallback() {
        return facebookLoginCallback;
    }

    @VisibleForTesting
    public RetrofitCallback<AuthResponse, Auth> getRegisterCallback() {
        return registerCallback;
    }

    @VisibleForTesting
    public RetrofitCallback<AuthResponse, Auth> getRefreshCallback() {
        return refreshCallback;
    }
}
