package com.aleixpellisa.mdpa.workouter.model;

import android.support.annotation.Keep;

import com.google.gson.Gson;

@Keep
public class BaseModel {

    public static <T extends BaseModel> T fromJsonString(String var0, Class<T> type) {
        return new Gson().fromJson(var0, type);
    }

    public String toString() {
        return (new Gson()).toJson(this);
    }

}
