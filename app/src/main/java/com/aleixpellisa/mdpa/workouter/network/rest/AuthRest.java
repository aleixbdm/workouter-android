package com.aleixpellisa.mdpa.workouter.network.rest;

import android.arch.lifecycle.Observer;

import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

public interface AuthRest {
    void login(String email, String password, Observer<Event<Auth>> responseObserver);

    void facebookLogin(String facebookAccessToken, Observer<Event<Auth>> responseObserver);

    void createAccount(String email, String password, Observer<Event<Auth>> responseObserver);

    void refreshToken(String userId, Observer<Event<Auth>> responseObserver);
}
