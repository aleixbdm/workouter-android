package com.aleixpellisa.mdpa.workouter.view.create;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.view.input.ErrorChecker;
import com.aleixpellisa.mdpa.workouter.view.input.MeetDayTextInputLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.OnClick;

public class CreateAccountInitialFragment extends CreateAccountBaseFragment {

    static final String TAG = "CreateAccountInitialFragmentTag";

    private View view;

    @BindView(R.id.create_account_initial_fragment_name_text_input)
    TextInputLayout nameTextInputLayout;
    MeetDayTextInputLayout nameTextInput;

    @BindView(R.id.create_account_initial_fragment_first_name_text_input)
    TextInputLayout firstNameTextInputLayout;
    MeetDayTextInputLayout firstNameTextInput;

    @BindView(R.id.create_account_initial_fragment_add_photo_image_view)
    ImageView addPhotoImageView;
    String imageAdded;

    private boolean triedToAddImage;

    public Button leftButton;
    public Button rightButton;

    public static CreateAccountInitialFragment newInstance(Button leftButton, Button rightButton) {
        CreateAccountInitialFragment createAccountInitialFragment = new CreateAccountInitialFragment();
        createAccountInitialFragment.setArguments(leftButton, rightButton);
        return createAccountInitialFragment;
    }

    public void setArguments(Button leftButton, Button rightButton) {
        this.leftButton = leftButton;
        this.rightButton = rightButton;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_account_initial, container, false);

        setupView(view);

        return view;
    }

    @Override
    public void setupView(View view) {
        super.setupView(view);

        this.view = view;
        nameTextInput = new MeetDayTextInputLayout(nameTextInputLayout);
        firstNameTextInput = new MeetDayTextInputLayout(firstNameTextInputLayout);

        nameTextInput.initTextInputLayout(
                MeetDayTextInputLayout.CREATE_PROFILE_NAME_FIELD_KEY);
        firstNameTextInput.initTextInputLayout(
                MeetDayTextInputLayout.CREATE_PROFILE_FIRST_NAME_FIELD_KEY);

        configureBottomButtons();

        nameTextInput.getEditText().addTextChangedListener(getInputTextWatcher());

        firstNameTextInput.getEditText().addTextChangedListener(getInputTextWatcher());

        notifyFragmentReady();
    }

    private void notifyFragmentReady() {
        fragmentToActivityCallback.fragmentReady(TAG);
    }

    public void addProfile(Profile profile) {
        nameTextInput.getEditText().setText(profile.getName());
        firstNameTextInput.getEditText().setText(profile.getFirstName());
        if (profile.getImageUrls() != null && !profile.getImageUrls().isEmpty()) {
            imageAdded = profile.getImageUrls().get(0);
            loadImage();
        }
    }

    @OnClick(R.id.create_account_initial_fragment_add_photo_image_view)
    public void addPhoto() {
        // Also need to check permissions, if not granted, the user will be set with a default image
        if (imageAdded == null) {
            imageAdded = "https://www.w3schools.com/html/pulpitrock.jpg";
            loadImage();
        }
    }

    private void loadImage() {
        triedToAddImage = true;
        Glide.with(view.getContext()).load(imageAdded)
                .apply(RequestOptions.circleCropTransform())
                .into(addPhotoImageView);
        attemptEnableContinue();
    }

    private void configureBottomButtons() {
        leftButton.setText(R.string.create_account_cancel_button);
        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentToActivityCallback.cancelCreate();
            }
        });

        rightButton.setText(R.string.create_account_continue_button);
        rightButton.setEnabled(false);
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(getView());
                fragmentToActivityCallback.goToFillAge(
                        nameTextInput.retrieveMeetDayTextInputType().getValue(),
                        firstNameTextInput.retrieveMeetDayTextInputType().getValue(),
                        imageAdded
                );
            }
        });
    }

    private TextWatcher getInputTextWatcher() {
        return new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                attemptEnableContinue();
            }
        };
    }

    private void attemptEnableContinue() {
        ErrorChecker errorChecker = new ErrorChecker();

        fragmentToActivityCallback.setEnabledRightButton(
                !errorChecker.inputHasErrors(nameTextInput.retrieveMeetDayTextInputType())
                        && !errorChecker.inputHasErrors(firstNameTextInput.retrieveMeetDayTextInputType())
                        && triedToAddImage);
    }
}