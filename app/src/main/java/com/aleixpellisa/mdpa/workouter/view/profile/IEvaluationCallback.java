package com.aleixpellisa.mdpa.workouter.view.profile;

public interface IEvaluationCallback {

    void likePressed();

    void dislikePressed();

}
