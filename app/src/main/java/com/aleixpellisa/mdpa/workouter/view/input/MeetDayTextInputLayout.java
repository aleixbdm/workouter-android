package com.aleixpellisa.mdpa.workouter.view.input;

import android.content.Context;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.aleixpellisa.mdpa.workouter.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class MeetDayTextInputLayout {

    public static final int LOGIN_CREDENTIALS_EMAIL_FIELD_KEY = 0;
    public static final int LOGIN_CREDENTIALS_PASSWORD_FIELD_KEY = 1;
    public static final int CREATE_CREDENTIALS_EMAIL_FIELD_KEY = 2;
    public static final int CREATE_CREDENTIALS_PASSWORD_FIELD_KEY = 3;
    public static final int CREATE_CREDENTIALS_CONFIRM_PASSWORD_FIELD_KEY = 4;
    public static final int CREATE_PROFILE_NAME_FIELD_KEY = 5;
    public static final int CREATE_PROFILE_FIRST_NAME_FIELD_KEY = 6;
    public static final int PROFILE_NAME_FIELD_KEY = 7;
    public static final int PROFILE_FIRST_NAME_FIELD_KEY = 8;
    public static final int PROFILE_DESCRIPTION_FIELD_KEY = 9;
    public static final int PROFILE_STUDIES_FIELD_KEY = 10;
    public static final int PROFILE_CURRENT_JOB_FIELD_KEY = 11;
    public static final int PROFILE_FAV_SONG_FIELD_KEY = 22;

    @IntDef({LOGIN_CREDENTIALS_EMAIL_FIELD_KEY, LOGIN_CREDENTIALS_PASSWORD_FIELD_KEY,
            CREATE_CREDENTIALS_EMAIL_FIELD_KEY, CREATE_CREDENTIALS_PASSWORD_FIELD_KEY,
            CREATE_CREDENTIALS_CONFIRM_PASSWORD_FIELD_KEY,
            CREATE_PROFILE_NAME_FIELD_KEY, CREATE_PROFILE_FIRST_NAME_FIELD_KEY,
            PROFILE_NAME_FIELD_KEY, PROFILE_FIRST_NAME_FIELD_KEY,
            PROFILE_DESCRIPTION_FIELD_KEY, PROFILE_STUDIES_FIELD_KEY,
            PROFILE_CURRENT_JOB_FIELD_KEY, PROFILE_FAV_SONG_FIELD_KEY})
    @Retention(RetentionPolicy.SOURCE)
    private @interface FieldKey {
    }

    private @FieldKey
    int fieldKey;
    private TextInputLayout textInputLayout;
    private EditText editText;
    private Context context;
    private MeetDayTextInputType meetDayTextInputType;
    private boolean isFocusableOnChange;

    public MeetDayTextInputLayout(@NonNull TextInputLayout textInputLayout) {
        this.textInputLayout = textInputLayout;
        this.context = textInputLayout.getContext();
        if (textInputLayout.getEditText() != null) {
            this.editText = textInputLayout.getEditText();
        } else {
            // Should not get here
            this.editText = new EditText(context);
        }
        this.meetDayTextInputType = new MeetDayTextInputType();
    }

    public EditText getEditText() {
        return editText;
    }

    private String getEnteredText() {
        return editText.getText().toString();
    }

    public MeetDayTextInputType retrieveMeetDayTextInputType() {
        meetDayTextInputType.setValue(getEnteredText());
        return meetDayTextInputType;
    }

    public void initTextInputLayout(@FieldKey int fieldKey) {
        this.fieldKey = fieldKey;
        initHint();
        initRequired();
        initInputType();
        initIme();
        initIsEmail();
        initIsPassword();
        initIsCheckableOnChange();
    }

    public void initTextInputLayout(@FieldKey int fieldKey, String text) {
        this.fieldKey = fieldKey;
        editText.setText(text);
        initHint();
        initRequired();
        initInputType();
        initIme();
        initIsEmail();
        initIsPassword();
        initIsCheckableOnChange();
    }

    private void initHint() {
        switch (fieldKey) {
            case LOGIN_CREDENTIALS_EMAIL_FIELD_KEY:
            case CREATE_CREDENTIALS_EMAIL_FIELD_KEY:
                textInputLayout.setHint(context.getString(R.string.input_text_layout_prompt_email));
                break;
            case LOGIN_CREDENTIALS_PASSWORD_FIELD_KEY:
            case CREATE_CREDENTIALS_PASSWORD_FIELD_KEY:
                textInputLayout.setHint(context.getString(R.string.input_text_layout_prompt_password));
                break;
            case CREATE_CREDENTIALS_CONFIRM_PASSWORD_FIELD_KEY:
                textInputLayout.setHint(context.getString(R.string.input_text_layout_prompt_confirm_password));
                break;
            case CREATE_PROFILE_NAME_FIELD_KEY:
            case PROFILE_NAME_FIELD_KEY:
                textInputLayout.setHint(context.getString(R.string.input_text_layout_prompt_name));
                break;
            case CREATE_PROFILE_FIRST_NAME_FIELD_KEY:
            case PROFILE_FIRST_NAME_FIELD_KEY:
                textInputLayout.setHint(context.getString(R.string.input_text_layout_prompt_first_name));
                break;
            case PROFILE_CURRENT_JOB_FIELD_KEY:
                textInputLayout.setHint(context.getString(R.string.input_text_layout_prompt_current_job));
                break;
            case PROFILE_STUDIES_FIELD_KEY:
                textInputLayout.setHint(context.getString(R.string.input_text_layout_prompt_studies));
                break;
            case PROFILE_FAV_SONG_FIELD_KEY:
                textInputLayout.setHint(context.getString(R.string.input_text_layout_prompt_fav_song));
                break;
            case PROFILE_DESCRIPTION_FIELD_KEY:
                break;
        }
    }

    private void initRequired() {
        int[] keys = {
                LOGIN_CREDENTIALS_EMAIL_FIELD_KEY,
                LOGIN_CREDENTIALS_PASSWORD_FIELD_KEY,
                CREATE_CREDENTIALS_EMAIL_FIELD_KEY,
                CREATE_CREDENTIALS_PASSWORD_FIELD_KEY,
                CREATE_CREDENTIALS_CONFIRM_PASSWORD_FIELD_KEY,
                CREATE_PROFILE_NAME_FIELD_KEY,
                CREATE_PROFILE_FIRST_NAME_FIELD_KEY,
                PROFILE_NAME_FIELD_KEY,
                PROFILE_FIRST_NAME_FIELD_KEY
        };
        meetDayTextInputType.setRequired(contains(keys, fieldKey));
    }

    private void initInputType() {
        switch (fieldKey) {
            case LOGIN_CREDENTIALS_EMAIL_FIELD_KEY:
            case CREATE_CREDENTIALS_EMAIL_FIELD_KEY:
                editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;
            case LOGIN_CREDENTIALS_PASSWORD_FIELD_KEY:
            case CREATE_CREDENTIALS_PASSWORD_FIELD_KEY:
            case CREATE_CREDENTIALS_CONFIRM_PASSWORD_FIELD_KEY:
                editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
            case PROFILE_DESCRIPTION_FIELD_KEY:
                editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                break;
            case CREATE_PROFILE_FIRST_NAME_FIELD_KEY:
            case CREATE_PROFILE_NAME_FIELD_KEY:
            case PROFILE_CURRENT_JOB_FIELD_KEY:
            case PROFILE_FAV_SONG_FIELD_KEY:
            case PROFILE_FIRST_NAME_FIELD_KEY:
            case PROFILE_NAME_FIELD_KEY:
            case PROFILE_STUDIES_FIELD_KEY:
                break;
        }
    }

    private void initIme() {
        switch (fieldKey) {
            case LOGIN_CREDENTIALS_PASSWORD_FIELD_KEY:
                editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                editText.setImeActionLabel(context.getString(R.string.login_with_md_action_login), EditorInfo.IME_ACTION_DONE);
                break;
            case CREATE_CREDENTIALS_CONFIRM_PASSWORD_FIELD_KEY:
                editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                editText.setImeActionLabel(context.getString(R.string.login_create_account_action_create), EditorInfo.IME_ACTION_DONE);
                break;
            case CREATE_CREDENTIALS_EMAIL_FIELD_KEY:
            case CREATE_CREDENTIALS_PASSWORD_FIELD_KEY:
            case CREATE_PROFILE_FIRST_NAME_FIELD_KEY:
            case CREATE_PROFILE_NAME_FIELD_KEY:
            case LOGIN_CREDENTIALS_EMAIL_FIELD_KEY:
            case PROFILE_CURRENT_JOB_FIELD_KEY:
            case PROFILE_DESCRIPTION_FIELD_KEY:
            case PROFILE_FAV_SONG_FIELD_KEY:
            case PROFILE_FIRST_NAME_FIELD_KEY:
            case PROFILE_NAME_FIELD_KEY:
            case PROFILE_STUDIES_FIELD_KEY:
                break;
        }
    }

    private void initIsPassword() {
        int[] keys = {
                LOGIN_CREDENTIALS_PASSWORD_FIELD_KEY,
                CREATE_CREDENTIALS_PASSWORD_FIELD_KEY,
                CREATE_CREDENTIALS_CONFIRM_PASSWORD_FIELD_KEY
        };
        meetDayTextInputType.setPassword(contains(keys, fieldKey));
    }

    private void initIsEmail() {
        int[] keys = {
                CREATE_CREDENTIALS_EMAIL_FIELD_KEY,
                LOGIN_CREDENTIALS_EMAIL_FIELD_KEY
        };
        meetDayTextInputType.setEmail(contains(keys, fieldKey));
    }

    private void initIsCheckableOnChange() {
        int[] keys = {
                CREATE_PROFILE_NAME_FIELD_KEY,
                CREATE_PROFILE_FIRST_NAME_FIELD_KEY
        };
        isFocusableOnChange = contains(keys, fieldKey);
    }

    public void showErrorsIfNeeded() {
        ErrorChecker errorChecker = new ErrorChecker(context, editText);
        @ErrorChecker.ErrorKey int errorKey = errorChecker.getErrorKey(meetDayTextInputType);
        errorChecker.refreshUIErrors(context, editText, isFocusableOnChange, errorKey);
    }

    private boolean contains(final int[] array, final int v) {

        boolean result = false;

        for (int i : array) {
            if (i == v) {
                result = true;
                break;
            }
        }

        return result;
    }

}
