package com.aleixpellisa.mdpa.workouter.network.rest.impl;

import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.aleixpellisa.mdpa.workouter.builder.ChatBuilder;
import com.aleixpellisa.mdpa.workouter.builder.MessageBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.network.callback.RetrofitCallback;
import com.aleixpellisa.mdpa.workouter.network.request.SendMessageRequest;
import com.aleixpellisa.mdpa.workouter.network.response.ChatResponse;
import com.aleixpellisa.mdpa.workouter.network.response.MessageResponse;
import com.aleixpellisa.mdpa.workouter.network.rest.ChatRest;
import com.aleixpellisa.mdpa.workouter.network.rest.api.ChatRestAPI;
import com.aleixpellisa.mdpa.workouter.network.schema.ChatSchema;
import com.aleixpellisa.mdpa.workouter.network.schema.MessageSchema;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Retrofit;

@Singleton
public class ChatRestRetrofit implements ChatRest {

    private final static int DEFAULT_CHAT_MESSAGES_LIMIT = 20;

    private ChatRestAPI restAPI;

    private RetrofitCallback<List<ChatResponse>, List<Chat>> getChatsCallback;
    private RetrofitCallback<List<MessageResponse>, List<Message>> getChatMessagesCallback;
    private RetrofitCallback<MessageResponse, Message> sendMessageCallback;

    @Inject
    public ChatRestRetrofit(Retrofit client) {
        restAPI = client.create(ChatRestAPI.class);
    }

    @Override
    public void fetchChats(final Observer<Event<List<Chat>>> responseObserver) {
        Call<List<ChatResponse>> call = restAPI.getChats();
        getChatsCallback = new RetrofitCallback<List<ChatResponse>, List<Chat>>(responseObserver) {
            @Override
            public void onSuccess(@NonNull List<ChatResponse> responseBody) {
                List<ChatSchema> chatSchemaList = new ArrayList<>();
                for (ChatResponse chatResponse : responseBody) {
                    chatSchemaList.add(chatResponse.chatSchema);
                }
                ChatBuilder chatBuilder = new ChatBuilder(chatSchemaList);
                event = Event.success(chatBuilder.buildList());
                responseObserver.onChanged(event);
            }
        };
        call.enqueue(getChatsCallback);
    }

    @Override
    public void fetchChatMessages(String chatId, final Observer<Event<List<Message>>> responseObserver) {
        Call<List<MessageResponse>> call = restAPI.getChatMessages(chatId, DEFAULT_CHAT_MESSAGES_LIMIT);
        getChatMessagesCallback = new RetrofitCallback<List<MessageResponse>, List<Message>>(responseObserver) {
            @Override
            public void onSuccess(@NonNull List<MessageResponse> responseBody) {
                List<MessageSchema> messageSchemaList = new ArrayList<>();
                for (MessageResponse messageResponse : responseBody) {
                    messageSchemaList.add(messageResponse.messageSchema);
                }
                MessageBuilder messageBuilder = new MessageBuilder(messageSchemaList);
                event = Event.success(messageBuilder.buildList());
                responseObserver.onChanged(event);
            }
        };
        call.enqueue(getChatMessagesCallback);
    }

    @Override
    public void sendMessage(String chatId, String messageText, final Observer<Event<Message>> responseObserver) {
        SendMessageRequest sendMessageRequest = new SendMessageRequest();
        sendMessageRequest.messageText = messageText;
        Call<MessageResponse> call = restAPI.sendMessage(chatId, sendMessageRequest);
        sendMessageCallback = new RetrofitCallback<MessageResponse, Message>(responseObserver) {
            @Override
            public void onSuccess(@NonNull MessageResponse responseBody) {
                MessageSchema messageSchema = responseBody.messageSchema;
                MessageBuilder messageBuilder = new MessageBuilder(messageSchema);
                event = Event.success(messageBuilder.build());
                responseObserver.onChanged(event);
            }
        };
        call.enqueue(sendMessageCallback);
    }

    @VisibleForTesting
    public RetrofitCallback<List<ChatResponse>, List<Chat>> getGetChatsCallback() {
        return getChatsCallback;
    }

    @VisibleForTesting
    public RetrofitCallback<List<MessageResponse>, List<Message>> getGetChatMessagesCallback() {
        return getChatMessagesCallback;
    }

    @VisibleForTesting
    public RetrofitCallback<MessageResponse, Message> getSendMessageCallback() {
        return sendMessageCallback;
    }
}
