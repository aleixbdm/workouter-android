package com.aleixpellisa.mdpa.workouter.manager;

import android.support.annotation.StringDef;

import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({Auth.ACCESS_TOKEN, Auth.USER_ID, Settings.PREFERENCE_KEY, Settings.CITY_KEY,
        Settings.SEARCH_RANGE_KEY, Settings.YEAR_RANGE_MIN_KEY,
        Settings.YEAR_RANGE_MAX_KEY, Settings.VISIBLE_KEY})
@Retention(RetentionPolicy.SOURCE)
public @interface PreferencesKey {
}
