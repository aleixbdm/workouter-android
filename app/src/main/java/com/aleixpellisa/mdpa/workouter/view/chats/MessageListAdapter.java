package com.aleixpellisa.mdpa.workouter.view.chats;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.App;
import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.builder.MessageBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MessageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    class LeftViewHolder extends RecyclerView.ViewHolder {

        static final int VIEW_HOLDER_TYPE = 0;

        TextView message;
        TextView time;

        LeftViewHolder(View itemView) {
            super(itemView);

            message = itemView.findViewById(R.id.message_left_item_message);
            time = itemView.findViewById(R.id.message_left_item_time);

        }
    }

    class RightViewHolder extends LeftViewHolder {

        static final int VIEW_HOLDER_TYPE = 1;

        RightViewHolder(View itemView) {
            super(itemView);

            message = itemView.findViewById(R.id.message_right_item_message);
            time = itemView.findViewById(R.id.message_right_item_time);

        }
    }

    class CenterViewHolder extends LeftViewHolder {

        static final int VIEW_HOLDER_TYPE = 2;

        CenterViewHolder(View itemView) {
            super(itemView);

            message = itemView.findViewById(R.id.message_center_item_message);
            time = itemView.findViewById(R.id.message_center_item_time);

        }
    }

    private List<Message> values;
    private String userId;

    MessageListAdapter(List<Message> values, String userId) {
        this.values = values;
        this.userId = userId;
    }

    @Override
    public int getItemViewType(int position) {
        Message message = values.get(position);
        switch (message.getMessageCategory()) {
            case Message.CATEGORY_APP_MESSAGE:
                return CenterViewHolder.VIEW_HOLDER_TYPE;
            case Message.CATEGORY_USER_MESSAGE:
                if (message.getSenderUserId().equals(userId)) {
                    return RightViewHolder.VIEW_HOLDER_TYPE;
                } else {
                    return LeftViewHolder.VIEW_HOLDER_TYPE;
                }
            default:
                // Should not get here
                return -1;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        switch (viewType) {
            case LeftViewHolder.VIEW_HOLDER_TYPE:
                return new LeftViewHolder(inflater.inflate(R.layout.message_left_item_layout, parent, false));
            case CenterViewHolder.VIEW_HOLDER_TYPE:
                return new CenterViewHolder(inflater.inflate(R.layout.message_center_item_layout, parent, false));
            case RightViewHolder.VIEW_HOLDER_TYPE:
            default:
                return new RightViewHolder(inflater.inflate(R.layout.message_right_item_layout, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Message message = values.get(position);
        switch (holder.getItemViewType()) {
            case LeftViewHolder.VIEW_HOLDER_TYPE:
            case RightViewHolder.VIEW_HOLDER_TYPE:
                LeftViewHolder leftViewHolder = (LeftViewHolder) holder;
                leftViewHolder.message.setText(message.getMessageText());
                leftViewHolder.time.setText(message.getCurrentFormattedDate());
                break;
            case CenterViewHolder.VIEW_HOLDER_TYPE:
                CenterViewHolder centerViewHolder = (CenterViewHolder) holder;
                centerViewHolder.message.setText(message.getMessageText());
                centerViewHolder.time.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    void setValues(List<Message> values) {
        // TODO: Not efficient, but works...
        this.values = formatMessageList(values);
    }

    private List<Message> formatMessageList(List<Message> messages) {
        // If it's empty, add welcome message
        if (messages.isEmpty()) {
            messages.add(MessageBuilder.buildFirstAppMessage(App.getInstance()));
        }
        // Check to add day messages
        return addDayMessages(messages, 0);
    }

    // TODO: Refactor this code
    // Precondition: size >= 1
    private List<Message> addDayMessages(List<Message> messages, int position) {
        if (position == messages.size()) {
            if (!Message.APP_MESSAGE_ID.equals(messages.get(position - 1).getId())) {
                messages.add(MessageBuilder.buildDayMessage(messages.get(position - 1).getCreatedAtDate()));
            }
            return messages;
        }
        if (position < messages.size() - 1 && !Message.APP_MESSAGE_ID.equals(messages.get(position).getId())) {
            if (getDateDiff(
                    messages.get(position).getCreatedAtDate(),
                    messages.get(position + 1).getCreatedAtDate(),
                    TimeUnit.DAYS) > 0) {
                messages.add(position + 1, MessageBuilder.buildDayMessage(messages.get(position).getCreatedAtDate()));
                position = position + 1;
            }
            return addDayMessages(messages, position + 1);
        } else {
            return addDayMessages(messages, position + 1);
        }
    }

    private static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillis = date1.getTime() - date2.getTime();
        return timeUnit.convert(diffInMillis, TimeUnit.MILLISECONDS);
    }

}
