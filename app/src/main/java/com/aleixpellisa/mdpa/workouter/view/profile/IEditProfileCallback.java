package com.aleixpellisa.mdpa.workouter.view.profile;

public interface IEditProfileCallback {

    void goToEditProfile();

}
