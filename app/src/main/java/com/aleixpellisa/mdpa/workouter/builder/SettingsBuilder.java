package com.aleixpellisa.mdpa.workouter.builder;

import com.aleixpellisa.mdpa.workouter.manager.PreferencesKey;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.network.schema.SettingsSchema;

public class SettingsBuilder extends BaseBuilder {

    public static final @Settings.Preference
    String SETTINGS_PREFERENCE_MOCK = Settings.PREFERENCE_ALL;
    public static final String SETTINGS_CITY_MOCK = "SettingsCity";
    public static final Integer SETTINGS_SEARCH_RANGE_MOCK = 120;
    public static final Integer SETTINGS_YEAR_RANGE_MIN_MOCK = 18;
    public static final Integer SETTINGS_YEAR_RANGE_MAX_MOCK = 65;
    public static final Boolean SETTINGS_VISIBLE_MOCK = true;

    private @Settings.Preference
    String preference;

    private String city;

    private Integer searchRange;

    private Integer yearRangeMin;

    private Integer yearRangeMax;

    private Boolean visible;

    public SettingsBuilder(SettingsSchema settingsSchema) {
        this.preference = settingsSchema.preference;
        this.city = settingsSchema.city;
        this.searchRange = settingsSchema.searchRange;
        this.yearRangeMin = settingsSchema.yearRangeMin;
        this.yearRangeMax = settingsSchema.yearRangeMax;
        this.visible = settingsSchema.visible;
    }

    public SettingsBuilder(@Settings.Preference String preference, String city, Integer searchRange,
                           Integer yearRangeMin, Integer yearRangeMax, Boolean visible) {
        this.preference = preference;
        this.city = city;
        this.searchRange = searchRange;
        this.yearRangeMin = yearRangeMin;
        this.yearRangeMax = yearRangeMax;
        this.visible = visible;
    }

    public SettingsBuilder(Settings settings) {
        if (settings == null) return;
        this.preference = settings.getPreference();
        this.city = settings.getCity();
        this.searchRange = settings.getSearchRange();
        this.yearRangeMin = settings.getYearRangeMin();
        this.yearRangeMax = settings.getYearRangeMax();
        this.visible = settings.isVisible();
    }

    public void updateField(@PreferencesKey String key, Object value) {
        switch (key) {
            case Settings.PREFERENCE_KEY:
                preference = (String) value;
                break;
            case Settings.CITY_KEY:
                city = (String) value;
                break;
            case Settings.SEARCH_RANGE_KEY:
                searchRange = (Integer) value;
                break;
            case Settings.YEAR_RANGE_MIN_KEY:
                yearRangeMin = (Integer) value;
                break;
            case Settings.YEAR_RANGE_MAX_KEY:
                yearRangeMax = (Integer) value;
                break;
            case Settings.VISIBLE_KEY:
                visible = (Boolean) value;
                break;
        }
    }

    public Settings build() {
        return new Settings(preference, city, searchRange, yearRangeMin, yearRangeMax, visible);
    }

    public SettingsSchema buildSchema() {
        Settings settings = build();
        SettingsSchema settingsSchema = new SettingsSchema();
        settingsSchema.preference = settings.getPreference();
        settingsSchema.city = settings.getCity();
        settingsSchema.searchRange = settings.getSearchRange();
        settingsSchema.yearRangeMin = settings.getYearRangeMin();
        settingsSchema.yearRangeMax = settings.getYearRangeMax();
        settingsSchema.visible = settings.isVisible();
        return settingsSchema;
    }

    public static Settings buildEmptySettings() {
        SettingsBuilder settingsBuilder =
                new SettingsBuilder(
                        Settings.PREFERENCE_MALE,
                        "",
                        0,
                        0,
                        0,
                        false
                );
        return settingsBuilder.build();
    }

    public static Settings buildMockedSettings() {
        SettingsBuilder settingsBuilder =
                new SettingsBuilder(SETTINGS_PREFERENCE_MOCK,
                        SETTINGS_CITY_MOCK,
                        SETTINGS_SEARCH_RANGE_MOCK,
                        SETTINGS_YEAR_RANGE_MIN_MOCK,
                        SETTINGS_YEAR_RANGE_MAX_MOCK,
                        SETTINGS_VISIBLE_MOCK
                );
        return settingsBuilder.build();
    }

}
