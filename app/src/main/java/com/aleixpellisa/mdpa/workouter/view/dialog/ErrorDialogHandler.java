package com.aleixpellisa.mdpa.workouter.view.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.aleixpellisa.mdpa.workouter.model.MeetDayError;

public class ErrorDialogHandler {

    public static final String TAG = "ErrorDialog";

    private Context context;

    ErrorDialogHandler(@NonNull Context context) {
        this.context = context;
    }

    void show(@NonNull MeetDayError meetDayError) {
        Toast.makeText(context, meetDayError.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
