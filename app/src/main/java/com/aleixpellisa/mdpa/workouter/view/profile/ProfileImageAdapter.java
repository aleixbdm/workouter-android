package com.aleixpellisa.mdpa.workouter.view.profile;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.aleixpellisa.mdpa.workouter.database.entity.Profile;

import java.util.List;

public class ProfileImageAdapter extends FragmentStatePagerAdapter
        implements ViewPager.OnPageChangeListener {

    private Context context;
    private List<String> profileImageUrls;
    private boolean canAddNewPhotos;

    ProfileImageAdapter(Context context, FragmentManager fm, List<String> profileImageUrls,
                        boolean isEditing) {
        super(fm);
        this.context = context;
        this.profileImageUrls = profileImageUrls;
        this.canAddNewPhotos = isEditing && profileImageUrls.size() < Profile.MAX_IMAGE_URLS;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == profileImageUrls.size()) {
            return ProfileImageFragment.newInstance(context);
        }
        return ProfileImageFragment.newInstance(profileImageUrls.get(position));
    }

    @Override
    public int getCount() {
        int count = profileImageUrls.size();
        if (canAddNewPhotos) {
            count += 1;
        }
        return count;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
