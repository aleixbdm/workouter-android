package com.aleixpellisa.mdpa.workouter.view.profile;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.builder.ProfileBuilder;
import com.aleixpellisa.mdpa.workouter.builder.SkillBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Skill;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.view.dialog.DialogHandler;
import com.aleixpellisa.mdpa.workouter.view.helpers.GlideHelper;
import com.aleixpellisa.mdpa.workouter.view.helpers.SkillViewHelper;
import com.aleixpellisa.mdpa.workouter.view.input.MeetDayTextInputLayout;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;

import java.util.List;

import me.relex.circleindicator.CircleIndicator;

class ProfileView {

    private Context context;
    private View view;

    private ProfileViewModel profileViewModel;

    private DialogHandler dialogHandler;

    private boolean isFromCandidateCard;
    private boolean isFromCandidateChat;
    private boolean isEditing;
    private boolean isProfileBasic;

    private IEvaluationCallback evaluationCallback;
    private IEditProfileCallback editProfileCallback;
    private ICloseEditProfileCallback closeEditProfileCallback;

    private Profile currentProfile;

    private List<String> imagesUpdated;
    private MeetDayTextInputLayout profileNameTextInput;
    private MeetDayTextInputLayout profileFirstNameTextInput;
    private int ageUpdated;
    private String genderUpdated;
    private MeetDayTextInputLayout profileDescriptionTextInput;
    private MeetDayTextInputLayout profileCurrentJobTextInput;
    private MeetDayTextInputLayout profileStudiesTextInput;
    private MeetDayTextInputLayout profileFavSongTextInput;
    private List<Skill> skillsUpdated;

    private boolean attemptedUpdate;

    ProfileView(Activity activity, View view,
                ProfileViewModel profileViewModelParam, DialogHandler dialogHandler,
                boolean isFromCandidateCard, boolean isFromCandidateChat, boolean isEditing) {
        this.context = activity;
        this.view = view;
        this.profileViewModel = profileViewModelParam;
        this.dialogHandler = dialogHandler;
        this.isFromCandidateCard = isFromCandidateCard;
        this.isFromCandidateChat = isFromCandidateChat;
        this.isEditing = isEditing;
    }

    void setupProfileView(Profile profile) {
        if (attemptedUpdate) {
            closeEditProfileCallback.closeEditProfile();
            return;
        }
        setProfileBasic(profileViewModel.isProfileBasic(profile));
        initView(profile);
    }

    void setupCandidateProfileView(Candidate candidate) {
        setProfileBasic(false);
        ProfileBuilder profileBuilder = new ProfileBuilder(candidate);
        initView(profileBuilder.build());
    }

    void setupCandidateProfileView(Profile candidateProfile) {
        setProfileBasic(false);
        initView(candidateProfile);
    }

    private void setProfileBasic(boolean isProfileBasic) {
        this.isProfileBasic = isProfileBasic;
    }

    private void initView(Profile profile) {
        this.currentProfile = profile;
        initProfileImage(profile.getImageUrls());
        if (!isEditing) {
            initHeader(profile.getName(), profile.getAge(),
                    profile.getSettings().getCity(), profile.getGender());
        } else {
            initCloseEditProfileCallback();
            initEditBasicInformationSection(profile.getName(), profile.getFirstName(),
                    profile.getAge(), profile.getGender());
        }
        if (isProfileBasic) {
            initBasicMessage();
        }
        if (!isEditing) {
            initDescriptionSection(profile.getDescription());
            initExtraInformationSection(profile.getCurrentJob(), profile.getStudies(), profile.getFavouriteSong());
        } else {
            initEditExtraInformationSection(profile.getDescription(), profile.getCurrentJob(),
                    profile.getStudies(), profile.getFavouriteSong());
        }
        initSkillsSection(profile.getSkills());
        if (!isFromCandidateCard && !isFromCandidateChat && !isEditing) {
            initEditProfileButton();
        }
        if (isFromCandidateCard) {
            initEvaluationSection();
        }
    }

    private void initProfileImage(List<String> imageUrls) {
        this.imagesUpdated = imageUrls;
        ViewPager profileUserImagePager = view.findViewById(R.id.profile_fragment_user_image_pager);
        CircleIndicator profileUserImagePageIndicator = view.findViewById(R.id.profile_fragment_user_image_page_indicator);

        if (!imageUrls.isEmpty()) {
            if (context instanceof AppCompatActivity) {
                ProfileImageAdapter profileImageAdapter = new ProfileImageAdapter(
                        context,
                        ((AppCompatActivity) context).getSupportFragmentManager(),
                        imageUrls,
                        isEditing
                );
                profileUserImagePager.setAdapter(profileImageAdapter);
                profileUserImagePageIndicator.setViewPager(profileUserImagePager);
            }
        }
    }

    void addProfileImage(String url) {
        imagesUpdated.add(url);
        initProfileImage(imagesUpdated);
    }

    private void initHeader(String name, int age, String city, String gender) {
        LinearLayout profileBasicMessageLayout = view.findViewById(R.id.profile_fragment_header_layout);
        profileBasicMessageLayout.setVisibility(View.VISIBLE);

        TextView profileNameAgeTextView = view.findViewById(R.id.profile_fragment_name_age_text_view);
        TextView profileCityTextView = view.findViewById(R.id.profile_fragment_city_text_view);
        ImageView profileGenderImage = view.findViewById(R.id.profile_fragment_gender_image_view);

        profileNameAgeTextView.setText(
                context.getString(R.string.discover_candidate_name_age_description,
                        name, age));
        profileCityTextView.setText(city);

        GlideHelper.loadGenderImage(context, gender, profileGenderImage);
    }

    private void initCloseEditProfileCallback() {
        try {
            closeEditProfileCallback = (ICloseEditProfileCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement ICloseEditProfileCallback");
        }
    }

    private void initEditBasicInformationSection(String name, String firstName, int ageParam, final String genderParam) {
        LinearLayout profileEditBasicInformationLayout = view.findViewById(R.id.profile_fragment_edit_basic_information_layout);
        profileEditBasicInformationLayout.setVisibility(View.VISIBLE);

        profileNameTextInput =
                new MeetDayTextInputLayout((TextInputLayout) view.findViewById(R.id.profile_fragment_name_text_input));
        profileFirstNameTextInput =
                new MeetDayTextInputLayout((TextInputLayout) view.findViewById(R.id.profile_fragment_first_name_text_input));
        NumberPicker profileAgePicker = view.findViewById(R.id.profile_fragment_age_picker);
        final ImageButton genderMaleImageView = view.findViewById(R.id.profile_fragment_gender_male_image_view);
        final ImageButton genderFemaleImageView = view.findViewById(R.id.profile_fragment_gender_female_image_view);

        profileNameTextInput.initTextInputLayout(
                MeetDayTextInputLayout.PROFILE_NAME_FIELD_KEY, name);

        profileFirstNameTextInput.initTextInputLayout(
                MeetDayTextInputLayout.PROFILE_FIRST_NAME_FIELD_KEY, firstName);

        profileAgePicker.setMinValue(Profile.MIN_AGE);
        profileAgePicker.setMaxValue(Profile.MAX_AGE);
        profileAgePicker.setValue(ageParam);
        this.ageUpdated = ageParam;

        profileAgePicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                ageUpdated = numberPicker.getValue();
            }
        });

        genderMaleImageView.setActivated(genderParam.equals(Profile.GENDER_MALE));
        genderFemaleImageView.setActivated(genderParam.equals(Profile.GENDER_FEMALE));
        this.genderUpdated = genderParam;

        genderMaleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                genderUpdated = Profile.GENDER_MALE;
                genderMaleImageView.setActivated(genderUpdated.equals(Profile.GENDER_MALE));
                genderFemaleImageView.setActivated(genderUpdated.equals(Profile.GENDER_FEMALE));
            }
        });

        genderFemaleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                genderUpdated = Profile.GENDER_FEMALE;
                genderMaleImageView.setActivated(genderUpdated.equals(Profile.GENDER_MALE));
                genderFemaleImageView.setActivated(genderUpdated.equals(Profile.GENDER_FEMALE));
            }
        });

    }

    private void initBasicMessage() {
        LinearLayout profileBasicMessageLayout = view.findViewById(R.id.profile_fragment_basic_message_layout);
        profileBasicMessageLayout.setVisibility(View.VISIBLE);
    }

    private void initDescriptionSection(String description) {
        if (description != null && !description.isEmpty()) {
            LinearLayout profileDescriptionLayout = view.findViewById(R.id.profile_fragment_description_layout);
            TextView profileDescriptionTextView = view.findViewById(R.id.profile_fragment_description_text_view);

            profileDescriptionLayout.setVisibility(View.VISIBLE);
            profileDescriptionTextView.setText(description);
        }
    }

    private void initExtraInformationSection(String currentJob, String studies, String favSong) {
        LinearLayout profileExtraInformationLayout = view.findViewById(R.id.profile_fragment_extra_information_layout);

        if (currentJob != null && !currentJob.isEmpty()) {
            profileExtraInformationLayout.setVisibility(View.VISIBLE);

            TextView profileCurrentJobHeader = view.findViewById(R.id.profile_fragment_current_job_header);
            TextView profileCurrentJobTextView = view.findViewById(R.id.profile_fragment_current_job_text_view);

            profileCurrentJobHeader.setVisibility(View.VISIBLE);
            profileCurrentJobTextView.setVisibility(View.VISIBLE);
            profileCurrentJobTextView.setText(currentJob);
        }

        if (studies != null && !studies.isEmpty()) {
            profileExtraInformationLayout.setVisibility(View.VISIBLE);

            TextView profileStudiesHeader = view.findViewById(R.id.profile_fragment_studies_header);
            TextView profileStudiesTextView = view.findViewById(R.id.profile_fragment_studies_text_view);

            profileStudiesHeader.setVisibility(View.VISIBLE);
            profileStudiesTextView.setVisibility(View.VISIBLE);
            profileStudiesTextView.setText(studies);
        }

        if (favSong != null && !favSong.isEmpty()) {
            profileExtraInformationLayout.setVisibility(View.VISIBLE);

            TextView profileFavSongHeader = view.findViewById(R.id.profile_fragment_fav_song_header);
            TextView profileFavSongTextView = view.findViewById(R.id.profile_fragment_fav_song_text_view);

            profileFavSongHeader.setVisibility(View.VISIBLE);
            profileFavSongTextView.setVisibility(View.VISIBLE);
            profileFavSongTextView.setText(favSong);
        }
    }

    private void initEditExtraInformationSection(String description, String currentJob, String studies, String favSong) {
        LinearLayout profileEditExtraInformationLayout = view.findViewById(R.id.profile_fragment_edit_extra_information_layout);
        profileEditExtraInformationLayout.setVisibility(View.VISIBLE);

        profileDescriptionTextInput =
                new MeetDayTextInputLayout((TextInputLayout) view.findViewById(R.id.profile_fragment_description_text_input));
        profileCurrentJobTextInput =
                new MeetDayTextInputLayout((TextInputLayout) view.findViewById(R.id.profile_fragment_current_job_text_input));
        profileStudiesTextInput =
                new MeetDayTextInputLayout((TextInputLayout) view.findViewById(R.id.profile_fragment_studies_text_input));
        profileFavSongTextInput =
                new MeetDayTextInputLayout((TextInputLayout) view.findViewById(R.id.profile_fragment_fav_song_text_input));

        profileDescriptionTextInput.initTextInputLayout(
                MeetDayTextInputLayout.PROFILE_DESCRIPTION_FIELD_KEY, description);

        profileCurrentJobTextInput.initTextInputLayout(
                MeetDayTextInputLayout.PROFILE_CURRENT_JOB_FIELD_KEY, currentJob);

        profileStudiesTextInput.initTextInputLayout(
                MeetDayTextInputLayout.PROFILE_STUDIES_FIELD_KEY, studies);

        profileFavSongTextInput.initTextInputLayout(
                MeetDayTextInputLayout.PROFILE_FAV_SONG_FIELD_KEY, favSong);

    }

    private void initSkillsSection(List<Skill> skillList) {
        List<Skill> validSkillList;
        if (isEditing) {
            if (skillList == null || skillList.isEmpty()) {
                validSkillList = SkillBuilder.buildInitialSkillList();
            } else {
                validSkillList = skillList;
            }
            skillsUpdated = validSkillList;
        } else {
            validSkillList = skillList;
        }
        if (validSkillList != null && !validSkillList.isEmpty()) {
            LinearLayout profileSkillsLayout = view.findViewById(R.id.profile_fragment_skills_layout);
            profileSkillsLayout.setVisibility(View.VISIBLE);
            for (Skill skill : validSkillList) {
                initSkillLayout(skill, validSkillList.indexOf(skill));
            }
        }
    }

    private void initSkillLayout(Skill skill, final int index) {
        SkillViewHelper skillViewHelper = new SkillViewHelper(skill.getCategory());
        LinearLayout skillLayout = view.findViewById(skillViewHelper.getSkillLayout());
        TextView skillTextView = skillLayout.findViewById(R.id.layout_profile_skill_text_view);
        SeekBar skillSeekBar = skillLayout.findViewById(R.id.layout_profile_skill_seek_bar);
        Drawable skillIcon = context.getDrawable(skillViewHelper.getSkillIcon());

        skillLayout.setVisibility(View.VISIBLE);
        skillTextView.setText(context.getString(skillViewHelper.getSkillName()));
        skillTextView.setCompoundDrawablesWithIntrinsicBounds(null, skillIcon, null, null);
        skillSeekBar.setProgress(skill.getValue());
        skillSeekBar.setEnabled(isEditing);

        if (isEditing) {
            TextView skillHelpTextView = view.findViewById(R.id.profile_fragment_skill_help_text_view);
            skillHelpTextView.setVisibility(View.VISIBLE);

            skillSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    Skill skillUpdate = skillsUpdated.get(index);
                    skillUpdate.setValue(i);
                    skillsUpdated.set(index, skillUpdate);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });
        }
    }

    private void initEditProfileButton() {
        Button profileEditButton = view.findViewById(R.id.profile_fragment_edit_button);
        profileEditButton.setVisibility(View.VISIBLE);

        try {
            editProfileCallback = (IEditProfileCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IEditProfileCallback");
        }

        profileEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editProfileCallback.goToEditProfile();
            }
        });
    }

    private void initEvaluationSection() {
        LinearLayout profileEvaluationLayout = view.findViewById(R.id.layout_evaluation_button_layout);
        profileEvaluationLayout.setVisibility(View.VISIBLE);

        try {
            evaluationCallback = (IEvaluationCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement ISendEvaluationCallback");
        }

        view.findViewById(R.id.discover_fragment_dislike_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evaluationCallback.dislikePressed();
            }
        });

        view.findViewById(R.id.discover_fragment_like_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evaluationCallback.likePressed();
            }
        });
    }

    void attemptUpdateProfile() {
        try {
            if (dialogHandler != null) {
                dialogHandler.showProgressDialog();
            }
            attemptedUpdate = true;
            profileViewModel.attemptUpdateProfile(currentProfile, imagesUpdated, profileNameTextInput.retrieveMeetDayTextInputType(),
                    profileFirstNameTextInput.retrieveMeetDayTextInputType(),
                    ageUpdated, genderUpdated, profileDescriptionTextInput.retrieveMeetDayTextInputType(),
                    profileCurrentJobTextInput.retrieveMeetDayTextInputType(),
                    profileStudiesTextInput.retrieveMeetDayTextInputType(),
                    profileFavSongTextInput.retrieveMeetDayTextInputType(), skillsUpdated);
        } catch (IllegalArgumentException ex) {
            if (dialogHandler != null) {
                dialogHandler.hideProgressDialog();
            }
            attemptedUpdate = false;
            profileNameTextInput.showErrorsIfNeeded();
            profileFirstNameTextInput.showErrorsIfNeeded();
            profileDescriptionTextInput.showErrorsIfNeeded();
            profileCurrentJobTextInput.showErrorsIfNeeded();
            profileStudiesTextInput.showErrorsIfNeeded();
            profileFavSongTextInput.showErrorsIfNeeded();
        }
    }

}
