package com.aleixpellisa.mdpa.workouter.network.schema;

import com.google.gson.annotations.SerializedName;

public class UserChatSchema extends BaseSchema {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("first_name")
    public String firstName;

    @SerializedName("image_url")
    public String imageUrl;
}
