package com.aleixpellisa.mdpa.workouter.view.discover;

import com.aleixpellisa.mdpa.workouter.model.Candidate;

public interface ISendEvaluationCallback {
    void sendEvaluation(Candidate candidate, String evaluation);
}
