package com.aleixpellisa.mdpa.workouter.network.schema;

import com.google.gson.annotations.SerializedName;

public class ErrorSchema extends BaseSchema {
    @SerializedName("message")
    public String message;
}
