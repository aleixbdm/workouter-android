package com.aleixpellisa.mdpa.workouter.network.schema;

import com.google.gson.annotations.SerializedName;

public class SkillSchema extends BaseSchema {

    @SerializedName("value")
    public Integer value;

    @SerializedName("category")
    public String category;

}
