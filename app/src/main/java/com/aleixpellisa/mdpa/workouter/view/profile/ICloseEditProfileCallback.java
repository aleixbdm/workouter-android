package com.aleixpellisa.mdpa.workouter.view.profile;

public interface ICloseEditProfileCallback {

    void closeEditProfile();

}
