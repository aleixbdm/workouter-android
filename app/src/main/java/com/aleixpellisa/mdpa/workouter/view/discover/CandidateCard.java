package com.aleixpellisa.mdpa.workouter.view.discover;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.view.main.IBottomTabNavigation;
import com.bumptech.glide.Glide;
import com.google.common.annotations.VisibleForTesting;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeHead;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeView;

@Layout(R.layout.candidate_card)
public class CandidateCard {

    public static int VISIBLE_STACK_CANDIDATE_CARDS = 3;

    @View(R.id.candidate_card_image_view)
    private ImageView imageView;

    @View(R.id.candidate_card_name_age_text_view)
    private TextView nameAgeTextView;

    @View(R.id.candidate_card_city_text_view)
    private TextView cityTextView;

    @View(R.id.candidate_card_skills_grid_view)
    private GridView skillsGridView;

    @SwipeView
    private android.view.View cardView;

    private Candidate candidate;
    private Context context;

    private IBottomTabNavigation bottomTabNavigationCallback;
    private ISendEvaluationCallback sendEvaluationCallback;

    // To not go to the candidate profile when swiping from button
    private boolean enabled = true;

    CandidateCard(Fragment fragment, Context context, Candidate candidate) {
        this.context = context;
        this.candidate = candidate;

        try {
            sendEvaluationCallback = (ISendEvaluationCallback) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IBottomTabNavigation");
        }

        try {
            bottomTabNavigationCallback = (IBottomTabNavigation) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IBottomTabNavigation");
        }

    }

    @Resolve
    private void onResolved() {
        if (!candidate.getImageUrls().isEmpty()) {
            Glide.with(context).load(candidate.getImageUrls().get(0))
                    .into(imageView);
        }
        nameAgeTextView.setText(context.getString(
                R.string.discover_candidate_name_age_description, candidate.getName(), candidate.getAge()));
        cityTextView.setText(candidate.getCity());

        if (candidate.getSkills() != null) {
            SkillGridViewAdapter skillGridViewAdapter = new SkillGridViewAdapter(context, candidate.getTopSkills());
            skillsGridView.setAdapter(skillGridViewAdapter);
        }
    }

    @SwipeHead
    private void onSwipeHeadCard() {
        enabled = true;
        if (!candidate.getImageUrls().isEmpty()) {
            Glide.with(context).load(candidate.getImageUrls().get(0))
                    .into(imageView);
        }
        cardView.invalidate();
    }

    @Click(R.id.candidate_card_view)
    private void onClick() {
        if (enabled) {
            bottomTabNavigationCallback.goToCandidateProfileFromCard(candidate);
        }
    }

    @SwipeOut
    private void onSwipedOut() {
        enabled = false;
        sendEvaluationCallback.sendEvaluation(candidate, Evaluation.EVALUATION_DISLIKE);
    }

    @SwipeIn
    private void onSwipeIn() {
        enabled = false;
        sendEvaluationCallback.sendEvaluation(candidate, Evaluation.EVALUATION_LIKE);
    }

    /**
     * Testing section
     */

    @VisibleForTesting
    public Candidate getCandidate() {
        return candidate;
    }

    @VisibleForTesting
    public TextView getCityTextView() {
        return cityTextView;
    }
}
