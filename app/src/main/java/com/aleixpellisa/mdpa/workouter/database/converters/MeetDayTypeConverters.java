package com.aleixpellisa.mdpa.workouter.database.converters;

import android.arch.persistence.room.TypeConverter;

import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.database.entity.Skill;
import com.aleixpellisa.mdpa.workouter.database.entity.UserChat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class MeetDayTypeConverters {

    @TypeConverter
    public static List<String> stringToStringList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<String>>() {
        }.getType();

        Gson gson = new Gson();
        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someStringListToString(List<String> someStrings) {
        Gson gson = new Gson();
        return gson.toJson(someStrings);
    }

    @TypeConverter
    public static List<Skill> stringToSkillList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<Skill>>() {
        }.getType();

        Gson gson = new Gson();
        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String skillListToString(List<Skill> someSkills) {
        Gson gson = new Gson();
        return gson.toJson(someSkills);
    }

    @TypeConverter
    public static Settings stringToSettings(String data) {
        if (data == null) {
            return null;
        }

        Type listType = new TypeToken<Settings>() {
        }.getType();

        Gson gson = new Gson();
        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String settingsToString(Settings someSettings) {
        Gson gson = new Gson();
        return gson.toJson(someSettings);
    }

    @TypeConverter
    public static UserChat stringToUserChat(String data) {
        if (data == null) {
            return null;
        }

        Type listType = new TypeToken<UserChat>() {
        }.getType();

        Gson gson = new Gson();
        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String userChatToString(UserChat someUserChat) {
        Gson gson = new Gson();
        return gson.toJson(someUserChat);
    }

}
