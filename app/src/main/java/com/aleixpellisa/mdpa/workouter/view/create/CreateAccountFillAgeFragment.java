package com.aleixpellisa.mdpa.workouter.view.create;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.NumberPicker;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.builder.ProfileBuilder;
import com.aleixpellisa.mdpa.workouter.builder.SettingsBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;

import butterknife.BindView;

public class CreateAccountFillAgeFragment extends CreateAccountBaseFragment {

    static final String TAG = "CreateAccountFillAgeFragmentTag";

    @BindView(R.id.create_account_fill_age_fragment_age_picker)
    NumberPicker profileAgePicker;

    @BindView(R.id.create_account_fill_age_fragment_gender_male_image_view)
    ImageButton genderMaleImageView;
    @BindView(R.id.create_account_fill_age_fragment_gender_female_image_view)
    ImageButton genderFemaleImageView;
    @BindView(R.id.create_account_fill_age_fragment_preference_male_image_view)
    ImageButton preferenceMaleImageView;
    @BindView(R.id.create_account_fill_age_fragment_preference_female_image_view)
    ImageButton preferenceFemaleImageView;
    @BindView(R.id.create_account_fill_age_fragment_preference_all_image_view)
    ImageButton preferenceAllImageView;

    public Button leftButton;
    public Button rightButton;

    private int age;
    private @Profile.Gender
    String gender;
    private @Settings.Preference
    String preference;

    public static CreateAccountFillAgeFragment newInstance(Button leftButton, Button rightButton) {
        CreateAccountFillAgeFragment createAccountFillAgeFragment = new CreateAccountFillAgeFragment();
        createAccountFillAgeFragment.setArguments(leftButton, rightButton);
        return createAccountFillAgeFragment;
    }

    public void setArguments(Button leftButton, Button rightButton) {
        this.leftButton = leftButton;
        this.rightButton = rightButton;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_account_fill_age, container, false);

        setupView(view);

        return view;
    }

    @Override
    public void setupView(final View view) {
        super.setupView(view);

        initViewComponents();
        configureBottomButtons();
    }

    private void initViewComponents() {
        profileAgePicker.setMinValue(Profile.MIN_AGE);
        profileAgePicker.setMaxValue(Profile.MAX_AGE);
        age = ProfileBuilder.PROFILE_AGE_MOCK;
        profileAgePicker.setValue(age);

        profileAgePicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                age = numberPicker.getValue();
            }
        });

        gender = ProfileBuilder.PROFILE_GENDER_MOCK;
        genderMaleImageView.setActivated(gender.equals(Profile.GENDER_MALE));
        genderFemaleImageView.setActivated(gender.equals(Profile.GENDER_FEMALE));

        genderMaleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateGender(Profile.GENDER_MALE);
            }
        });

        genderFemaleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateGender(Profile.GENDER_FEMALE);
            }
        });

        preference = SettingsBuilder.SETTINGS_PREFERENCE_MOCK;
        preferenceMaleImageView.setActivated(preference.equals(Settings.PREFERENCE_MALE));
        preferenceFemaleImageView.setActivated(preference.equals(Settings.PREFERENCE_FEMALE));
        preferenceAllImageView.setActivated(preference.equals(Settings.PREFERENCE_ALL));

        preferenceMaleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePreference(Settings.PREFERENCE_MALE);
            }
        });

        preferenceFemaleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePreference(Settings.PREFERENCE_FEMALE);
            }
        });

        preferenceAllImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePreference(Settings.PREFERENCE_ALL);
            }
        });

    }

    private void updateGender(@Profile.Gender String gender) {
        this.gender = gender;
        genderMaleImageView.setActivated(gender.equals(Profile.GENDER_MALE));
        genderFemaleImageView.setActivated(gender.equals(Profile.GENDER_FEMALE));
    }

    private void updatePreference(@Settings.Preference String preference) {
        this.preference = preference;
        preferenceMaleImageView.setActivated(preference.equals(Settings.PREFERENCE_MALE));
        preferenceFemaleImageView.setActivated(preference.equals(Settings.PREFERENCE_FEMALE));
        preferenceAllImageView.setActivated(preference.equals(Settings.PREFERENCE_ALL));
    }

    private void configureBottomButtons() {
        leftButton.setText(R.string.create_account_later_button);
        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentToActivityCallback.attemptFinishCreate();
            }
        });

        rightButton.setText(R.string.create_account_continue_button);
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentToActivityCallback.goToFillLocation(age, gender, preference);
            }
        });
    }

}