package com.aleixpellisa.mdpa.workouter.view.input;

public class MeetDayTextInputType {

    private String value;
    private boolean isRequired;
    private boolean isEmail;
    private boolean isPassword;

    public static MeetDayTextInputType notRequired(String value) {
        MeetDayTextInputType inputType = new MeetDayTextInputType();
        inputType.setValue(value);
        return inputType;
    }

    public static MeetDayTextInputType required(String value) {
        MeetDayTextInputType inputType = new MeetDayTextInputType();
        inputType.setRequired(true);
        inputType.setValue(value);
        return inputType;
    }

    public static MeetDayTextInputType email(String value) {
        MeetDayTextInputType inputType = required(value);
        inputType.setEmail(true);
        return inputType;
    }

    public static MeetDayTextInputType password(String value) {
        MeetDayTextInputType inputType = required(value);
        inputType.setPassword(true);
        return inputType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }

    public boolean isEmail() {
        return isEmail;
    }

    public void setEmail(boolean email) {
        isEmail = email;
    }

    public boolean isPassword() {
        return isPassword;
    }

    public void setPassword(boolean password) {
        isPassword = password;
    }

}
