package com.aleixpellisa.mdpa.workouter.network.request;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class UpdateSettingsRequest {
    @SerializedName("preference")
    public String preference;

    @SerializedName("city")
    public String city;

    @SerializedName("search_range")
    public Integer searchRange;

    @SerializedName("year_range_min")
    public Integer yearRangeMin;

    @SerializedName("year_range_max")
    public Integer yearRangeMax;

    @SerializedName("visible")
    public String visible;
}

