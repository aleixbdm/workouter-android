package com.aleixpellisa.mdpa.workouter.manager;

import android.arch.lifecycle.LiveData;

import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.List;

public interface ProfileManager {

    void startObservers();

    Auth getAuth();

    Settings getSettings();

    LiveData<Event<Profile>> getProfile();

    LiveData<Event<Profile>> getCandidateProfile();

    void login(String email, String password);

    void facebookLogin(String accessToken);

    void createAccount(String email, String password);

    void fetchProfile();

    boolean isProfileBasic(Profile profile);

    void createProfile(String name, String firstName, List<String> imageUrls, Integer age,
                       @Profile.Gender String gender, @Settings.Preference String preference, String city);

    void updateProfile(Profile profile);

    void updateSettings(@PreferencesKey String key, Object value);

    void deleteProfile();

    void fetchCandidateProfile(String candidateId);

    void clearAll();

}
