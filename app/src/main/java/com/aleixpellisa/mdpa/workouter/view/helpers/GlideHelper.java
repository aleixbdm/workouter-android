package com.aleixpellisa.mdpa.workouter.view.helpers;

import android.content.Context;
import android.widget.ImageView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.bumptech.glide.Glide;

public class GlideHelper {

    public static void loadGenderImage(Context context, @Profile.Gender String gender, ImageView imageView) {
        if (gender == null) {
            return;
        }
        if (gender.equals(Profile.GENDER_MALE)) {
            Glide.with(context).load(R.drawable.ic_gender_male).into(imageView);
        } else {
            Glide.with(context).load(R.drawable.ic_gender_female).into(imageView);
        }
    }

}
