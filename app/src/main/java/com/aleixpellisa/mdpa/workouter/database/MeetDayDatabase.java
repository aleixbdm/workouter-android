package com.aleixpellisa.mdpa.workouter.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.aleixpellisa.mdpa.workouter.database.dao.ChatDao;
import com.aleixpellisa.mdpa.workouter.database.dao.MessageDao;
import com.aleixpellisa.mdpa.workouter.database.dao.ProfileDao;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;

@Database(entities = {Profile.class, Chat.class, Message.class}, version = 1)
public abstract class MeetDayDatabase extends RoomDatabase {

    public abstract ProfileDao getProfileDao();

    public abstract ChatDao getChatDao();

    public abstract MessageDao getMessageDao();

}
