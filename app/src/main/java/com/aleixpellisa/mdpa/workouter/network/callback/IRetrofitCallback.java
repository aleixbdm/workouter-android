package com.aleixpellisa.mdpa.workouter.network.callback;

import android.support.annotation.NonNull;

import com.aleixpellisa.mdpa.workouter.model.MeetDayError;

public interface IRetrofitCallback<R> {
    void onSuccess(@NonNull R responseBody);

    void onError(@NonNull MeetDayError meetDayError);
}
