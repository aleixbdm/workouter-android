package com.aleixpellisa.mdpa.workouter.view.chats;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.UserChat;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.view.BasePushActivity;
import com.aleixpellisa.mdpa.workouter.view.input.MeetDayTextInputType;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.view.model.ChatViewModel;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ChatActivity extends BasePushActivity {

    ChatViewModel chatViewModel;

    ProfileViewModel profileViewModel;

    @BindView(R.id.chat_message_list)
    RecyclerView messageRecyclerView;

    @BindView(R.id.chat_enter_message_edit_text)
    EditText enterMessageEditText;

    @BindView(R.id.chat_send_message_button)
    ImageButton sendMessageButton;

    private MessageListAdapter messageListAdapter;
    private List<Message> messageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        chatViewModel = ViewModelProviders.of(this).get(ChatViewModel.class);
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        Bundle data = getIntent().getExtras();
        if (data != null) {
            Chat chat = data.getParcelable(Chat.PARCEL_KEY);
            if (chat != null) {
                chatViewModel.enterChat(chat.getId());
            }
            UserChat userChat = data.getParcelable(UserChat.PARCEL_KEY);
            if (userChat != null) {
                setTitle(this.getString(R.string.chat_activity_title, userChat.getName()));
            }
        }

        setupView(R.layout.activity_chat);
    }

    @Override
    public void setupView(int layoutResID) {
        super.setupView(layoutResID);

        Auth auth = profileViewModel.getAuth();
        if (auth == null || auth.getUserId().isEmpty()) {
            // Should not continue
            return;
        }
        String userId = auth.getUserId();
        messageList = new ArrayList<>();
        messageListAdapter = new MessageListAdapter(messageList, userId);
        messageRecyclerView.setAdapter(messageListAdapter);
        messageRecyclerView.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true));

        enterMessageEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    sendMessage();
                    return true;
                }
                return false;
            }
        });

        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });

        setupChatViewModel();

        chatViewModel.fetchChatMessages();
    }

    private void setupChatViewModel() {
        chatViewModel.getChatMessageList().observe(this, new Observer<Event<List<Message>>>() {
            @Override
            public void onChanged(@Nullable Event<List<Message>> event) {
                if (event != null) {
                    switch (event.status) {
                        case Event.SUCCESS:
                            messageList = event.data;
                            messageListAdapter.setValues(messageList);
                            messageListAdapter.notifyDataSetChanged();
                            // Optional
                            // messageRecyclerView.smoothScrollToPosition(0);
                            break;
                        case Event.ERROR:
                        case Event.AUTH_ERROR:
                            break;
                    }
                }
            }
        });
        chatViewModel.getChatMessage().observe(this, new Observer<Event<Message>>() {
            @Override
            public void onChanged(@Nullable Event<Message> event) {
                if (event != null) {
                    switch (event.status) {
                        case Event.SUCCESS:
                            messageList.add(0, event.data);
                            messageListAdapter.setValues(messageList);
                            messageListAdapter.notifyDataSetChanged();
                            // Optional
                            // messageRecyclerView.smoothScrollToPosition(0);
                            break;
                        case Event.ERROR:
                        case Event.AUTH_ERROR:
                            break;
                    }
                }
            }
        });
    }

    private void sendMessage() {
        MeetDayTextInputType meetDayTextInputType = new MeetDayTextInputType();
        meetDayTextInputType.setValue(enterMessageEditText.getText().toString());
        meetDayTextInputType.setRequired(true);
        try {
            chatViewModel.attemptSendMessage(meetDayTextInputType);
            enterMessageEditText.setText("");
        } catch (IllegalArgumentException ignore) {
            // Nothing to do, the text was empty
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        chatViewModel.leaveChat();
        removeObservers();
    }

    private void removeObservers() {
        chatViewModel.getChatMessageList().removeObservers(this);
        chatViewModel.getChatMessage().removeObservers(this);
    }
}
