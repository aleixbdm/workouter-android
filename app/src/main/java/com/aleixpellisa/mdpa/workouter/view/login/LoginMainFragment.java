package com.aleixpellisa.mdpa.workouter.view.login;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.model.MeetDayError;
import com.aleixpellisa.mdpa.workouter.view.dialog.DialogHandler;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginMainFragment extends LoginBaseFragment {

    static final String TAG = "LoginMainFragmentTag";

    @Inject
    DialogHandler dialogHandler;

    ProfileViewModel profileViewModel;

    @BindView(R.id.login_real_login_with_fb_button)
    LoginButton fbLoginButton;

    CallbackManager callbackManager;

    public static LoginMainFragment newInstance() {
        return new LoginMainFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_login, container, false);

        setupView(view);

        return view;
    }

    @Override
    public void setupView(View view) {
        super.setupView(view);

        initViewModel();

        callbackManager = CallbackManager.Factory.create();
        fbLoginButton.setReadPermissions("email", "public_profile", "user_birthday");
        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                profileViewModel.attemptFacebookLogin(loginResult.getAccessToken().getToken());
            }

            @Override
            public void onCancel() {
                dialogHandler.hideProgressDialog();
            }

            @Override
            public void onError(FacebookException exception) {
                dialogHandler.hideProgressDialogWithError(new MeetDayError(exception.getMessage()));
            }
        });
    }

    private void initViewModel() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            profileViewModel = ViewModelProviders.of(activity).get(ProfileViewModel.class);
        }
    }

    @OnClick(R.id.login_login_with_fb_button)
    public void loginWithFbButtonClicked() {
        dialogHandler.showProgressDialog();
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken == null) {
            fbLoginButton.performClick();
        } else {
            profileViewModel.attemptFacebookLogin(accessToken.getToken());
        }
    }

    @OnClick(R.id.login_login_with_md_button)
    public void loginWithMDButtonClicked() {
        fragmentToActivityCallback.goToLoginWithMeetDay();
    }

    @OnClick(R.id.login_create_account_text_view)
    public void createAccountButtonClicked() {
        fragmentToActivityCallback.goToLoginCreateAccount();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}