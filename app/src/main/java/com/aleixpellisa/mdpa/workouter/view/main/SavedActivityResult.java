package com.aleixpellisa.mdpa.workouter.view.main;

import android.content.Intent;

class SavedActivityResult {

    int requestCode;
    int resultCode;
    Intent intent;

    SavedActivityResult(int requestCode, int resultCode, Intent intent) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.intent = intent;
    }
}
