package com.aleixpellisa.mdpa.workouter.network.response;

import android.support.annotation.Keep;

import com.google.gson.Gson;

@Keep
public class BaseResponse {

    public static <T extends BaseResponse> T fromJson(String var0, Class<T> type) {
        return new Gson().fromJson(var0, type);
    }

    public String toString() {
        return (new Gson()).toJson(this);
    }
}
