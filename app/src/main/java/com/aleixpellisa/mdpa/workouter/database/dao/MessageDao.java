package com.aleixpellisa.mdpa.workouter.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.aleixpellisa.mdpa.workouter.database.entity.Message;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public abstract class MessageDao {
    @Query("Select * from message where chatId = :chatId order by createdAt desc")
    public abstract LiveData<List<Message>> getMessagesForChat(String chatId);

    @Insert(onConflict = REPLACE)
    public abstract void insert(List<Message> messageList);

    @Insert(onConflict = REPLACE)
    public abstract void insert(Message message);
}
