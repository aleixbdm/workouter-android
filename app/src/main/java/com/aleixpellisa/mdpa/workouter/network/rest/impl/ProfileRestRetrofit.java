package com.aleixpellisa.mdpa.workouter.network.rest.impl;

import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.aleixpellisa.mdpa.workouter.builder.BasicProfileBuilder;
import com.aleixpellisa.mdpa.workouter.builder.ProfileBuilder;
import com.aleixpellisa.mdpa.workouter.builder.SettingsBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesKey;
import com.aleixpellisa.mdpa.workouter.network.callback.RetrofitCallback;
import com.aleixpellisa.mdpa.workouter.network.request.CreateProfileRequest;
import com.aleixpellisa.mdpa.workouter.network.request.UpdateProfileRequest;
import com.aleixpellisa.mdpa.workouter.network.request.UpdateSettingsRequest;
import com.aleixpellisa.mdpa.workouter.network.response.ProfileResponse;
import com.aleixpellisa.mdpa.workouter.network.rest.ProfileRest;
import com.aleixpellisa.mdpa.workouter.network.rest.api.ProfileRestAPI;
import com.aleixpellisa.mdpa.workouter.network.schema.ProfileSchema;
import com.aleixpellisa.mdpa.workouter.network.schema.SettingsSchema;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Retrofit;

@Singleton
public class ProfileRestRetrofit implements ProfileRest {

    private ProfileRestAPI restAPI;

    private RetrofitCallback<ProfileResponse, Profile> fetchProfileCallback;
    private RetrofitCallback<ProfileResponse, Profile> createProfileCallback;
    private RetrofitCallback<ProfileResponse, Profile> updateProfileCallback;
    private RetrofitCallback<ProfileResponse, Profile> updateSettingsCallback;
    private RetrofitCallback<ProfileResponse, Profile> deleteProfileCallback;
    private RetrofitCallback<ProfileResponse, Profile> fetchCandidateProfileCallback;

    @Inject
    public ProfileRestRetrofit(Retrofit client) {
        restAPI = client.create(ProfileRestAPI.class);
    }

    @Override
    public void fetchProfile(final Observer<Event<Profile>> responseObserver) {
        Call<ProfileResponse> call = restAPI.getProfile();
        fetchProfileCallback = initProfileCallback(responseObserver);
        call.enqueue(fetchProfileCallback);
    }

    @Override
    public void createProfile(String name, String firstName, List<String> imageUrls, Integer age,
                              String gender, String preference, String city,
                              final Observer<Event<Profile>> responseObserver) {
        CreateProfileRequest createProfileRequest = new CreateProfileRequest();
        createProfileRequest.createProfileSchema = new BasicProfileBuilder(name, firstName,
                imageUrls, age, gender, preference, city).buildSchema();

        Call<ProfileResponse> call = restAPI.createProfile(createProfileRequest);
        createProfileCallback = initProfileCallback(responseObserver);
        call.enqueue(createProfileCallback);
    }

    @Override
    public void updateProfile(Profile profile, final Observer<Event<Profile>> responseObserver) {
        UpdateProfileRequest updateProfileRequest = new UpdateProfileRequest();
        updateProfileRequest.profileSchema = new ProfileBuilder(profile).buildSchema();

        Call<ProfileResponse> call = restAPI.updateProfile(updateProfileRequest);
        updateProfileCallback = initProfileCallback(responseObserver);
        call.enqueue(updateProfileCallback);
    }

    @Override
    public void updateSettings(@PreferencesKey String key, Object value, final Observer<Event<Profile>> responseObserver) {
        UpdateSettingsRequest updateSettingsRequest = new UpdateSettingsRequest();
        SettingsBuilder settingsBuilder = new SettingsBuilder(new SettingsSchema());
        settingsBuilder.updateField(key, value);
        SettingsSchema settingsSchema = settingsBuilder.buildSchema();
        updateSettingsRequest.preference = settingsSchema.preference;
        updateSettingsRequest.city = settingsSchema.city;
        updateSettingsRequest.searchRange = settingsSchema.searchRange;
        updateSettingsRequest.yearRangeMin = settingsSchema.yearRangeMin;
        updateSettingsRequest.yearRangeMax = settingsSchema.yearRangeMax;
        updateSettingsRequest.visible = settingsSchema.visible != null ? settingsSchema.visible.toString() : null;

        Call<ProfileResponse> call = restAPI.updateSettings(updateSettingsRequest);
        updateSettingsCallback = initProfileCallback(responseObserver);
        call.enqueue(updateSettingsCallback);
    }

    @Override
    public void deleteProfile(final Observer<Event<Profile>> responseObserver) {
        Call<ProfileResponse> call = restAPI.deleteProfile();
        deleteProfileCallback = initProfileCallback(responseObserver);
        call.enqueue(deleteProfileCallback);
    }

    @Override
    public void fetchCandidateProfile(String candidateId, Observer<Event<Profile>> responseObserver) {
        Call<ProfileResponse> call = restAPI.getCandidateProfile(candidateId);
        fetchCandidateProfileCallback = initProfileCallback(responseObserver);
        call.enqueue(fetchCandidateProfileCallback);
    }

    private RetrofitCallback<ProfileResponse, Profile> initProfileCallback(final Observer<Event<Profile>> responseObserver) {
        return new RetrofitCallback<ProfileResponse, Profile>(responseObserver) {
            @Override
            public void onSuccess(@NonNull ProfileResponse responseBody) {
                ProfileSchema profileSchema = responseBody.profileSchema;
                if (profileSchema != null) {
                    ProfileBuilder profileBuilder = new ProfileBuilder(profileSchema);
                    event = Event.success(profileBuilder.build());
                }
                responseObserver.onChanged(event);
            }
        };
    }

    /**
     * Testing section
     */

    @VisibleForTesting
    public RetrofitCallback<ProfileResponse, Profile> getFetchProfileCallback() {
        return fetchProfileCallback;
    }

    @VisibleForTesting
    public RetrofitCallback<ProfileResponse, Profile> getCreateProfileCallback() {
        return createProfileCallback;
    }

    @VisibleForTesting
    public RetrofitCallback<ProfileResponse, Profile> getUpdateProfileCallback() {
        return updateProfileCallback;
    }

    @VisibleForTesting
    public RetrofitCallback<ProfileResponse, Profile> getUpdateSettingsCallback() {
        return updateSettingsCallback;
    }

    @VisibleForTesting
    public RetrofitCallback<ProfileResponse, Profile> getDeleteProfileCallback() {
        return deleteProfileCallback;
    }

    @VisibleForTesting
    public RetrofitCallback<ProfileResponse, Profile> getFetchCandidateProfileCallback() {
        return fetchCandidateProfileCallback;
    }
}
