package com.aleixpellisa.mdpa.workouter.database.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;

import com.aleixpellisa.mdpa.workouter.database.converters.MeetDayTypeConverters;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@Entity
@Keep
public class Chat implements Parcelable {

    @PrimaryKey
    @NonNull
    private String id;

    @TypeConverters(MeetDayTypeConverters.class)
    private UserChat userChat;

    private String lastMessage;

    private Long updatedAt;

    public Chat(@NonNull String id, UserChat userChat, String lastMessage, Long updatedAt) {
        this.id = id;
        this.userChat = userChat;
        this.lastMessage = lastMessage;
        this.updatedAt = updatedAt;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public UserChat getUserChat() {
        return userChat;
    }

    public void setUserChat(UserChat userChat) {
        this.userChat = userChat;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Custom getters and setters
     */

    private Date getUpdatedAtDate() {
        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        cal.setTimeInMillis(updatedAt);
        return cal.getTime();
    }

    public String getCurrentFormattedDate() {
        PrettyTime p = new PrettyTime();
        return p.format(getUpdatedAtDate());
    }

    /**
     * Parcelable part
     **/

    public static final String PARCEL_KEY = "Chat";

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Chat createFromParcel(Parcel in) {
            return new Chat(in);
        }

        public Chat[] newArray(int size) {
            return new Chat[size];
        }
    };

    public Chat(Parcel in) {
        this.id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
    }
}
