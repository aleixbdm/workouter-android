package com.aleixpellisa.mdpa.workouter.network.schema;

import android.support.annotation.Keep;

import com.google.gson.Gson;

@Keep
public class BaseSchema {
    public static <T extends BaseSchema> T fromJson(String var0, Class<T> type) {
        return new Gson().fromJson(var0, type);
    }

    public String toString() {
        return (new Gson()).toJson(this);
    }
}
