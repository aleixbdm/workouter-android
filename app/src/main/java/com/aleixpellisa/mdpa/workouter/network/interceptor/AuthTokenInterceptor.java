package com.aleixpellisa.mdpa.workouter.network.interceptor;

import java.io.IOException;

import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

@Singleton
public class AuthTokenInterceptor implements Interceptor {

    private static final String AuthHeaderKey = "auth";

    private String accessToken;

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        Request.Builder requestBuilder = request.newBuilder();

        if (request.header(AuthHeaderKey) == null && accessToken != null) {
            requestBuilder.addHeader(AuthHeaderKey, accessToken);
        }

        return chain.proceed(requestBuilder.build());
    }
}