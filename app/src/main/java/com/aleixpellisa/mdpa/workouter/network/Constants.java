package com.aleixpellisa.mdpa.workouter.network;

import com.aleixpellisa.mdpa.workouter.BuildConfig;

public final class Constants {

    static public String TEST_API_BASE = "http://meetday.test.com";
    static public String API_BASE = BuildConfig.API_BASE;

}
