package com.aleixpellisa.mdpa.workouter.view.login;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.inputmethod.InputMethodManager;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.view.BaseActivity;
import com.aleixpellisa.mdpa.workouter.view.create.CreateAccountActivity;
import com.aleixpellisa.mdpa.workouter.view.dialog.DialogHandler;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.view.main.MainBottomTabActivity;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;

import java.util.List;

import javax.inject.Inject;

public class LoginActivity extends BaseActivity implements ILoginFragmentToActivity {

    @Inject
    DialogHandler dialogHandler;

    private ProfileViewModel profileViewModel;

    private FragmentManager fragmentManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        checkIfNeedToLaunch();

        setupView(R.layout.activity_login);
    }

    private void checkIfNeedToLaunch() {
        // check auth to launch directly the other activity
        Auth auth = profileViewModel.getAuth();
        if (auth != null && auth.getAccessToken() != null) {
            retrievedProfile();
        }
    }

    @Override
    public void setupView(int layoutResID) {
        super.setupView(layoutResID);

        // Hide action bar
        if (getSupportActionBar() != null) getSupportActionBar().hide();

        setupProfileViewModel();

        loadInitialFragment();
    }

    private void setupProfileViewModel() {
        profileViewModel.getProfile().observe(this, new Observer<Event<Profile>>() {
            @Override
            public void onChanged(@Nullable Event<Profile> event) {
                if (event != null) {
                    switch (event.status) {
                        case Event.SUCCESS:
                            retrievedProfile();
                            break;
                        case Event.ERROR:
                        case Event.AUTH_ERROR:
                            dialogHandler.hideProgressDialogWithError(event.meetDayError);
                            break;
                    }
                }
            }
        });
    }

    private void loadInitialFragment() {
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.login_fragment_container, LoginMainFragment.newInstance(),
                LoginMainFragment.TAG);
        fragmentTransaction.commit();
    }

    private void retrievedProfile() {
        hideKeyboard();
        Settings settings = profileViewModel.getSettings();
        if (settings != null && settings.getCity() != null) {
            // Checking city, for example
            goToDiscover();
        } else {
            goToCreateAccount();
        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
        }
    }

    @Override
    public void goToLoginWithMeetDay() {
        if (isDestroyed()) {
            return;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.right_side_in, R.anim.left_side_out);
        ft.replace(R.id.login_fragment_container, LoginWithMeetDayFragment.newInstance(),
                LoginWithMeetDayFragment.TAG);
        ft.commit();
    }

    @Override
    public void goToLoginCreateAccount() {
        if (isDestroyed()) {
            return;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.bottom_side_in, R.anim.up_side_out);
        ft.replace(R.id.login_fragment_container, LoginCreateAccountFragment.newInstance(),
                LoginCreateAccountFragment.TAG);
        ft.commit();
    }

    @Override
    public void returnFromLoginWithMD() {
        if (isDestroyed()) {
            return;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.setCustomAnimations(R.anim.left_side_in, R.anim.right_side_out);
        ft.replace(R.id.login_fragment_container, LoginMainFragment.newInstance(),
                LoginMainFragment.TAG);
        ft.commit();
    }

    @Override
    public void returnFromCreateAccount() {
        if (isDestroyed()) {
            return;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.setCustomAnimations(R.anim.up_side_in, R.anim.bottom_side_out);
        ft.replace(R.id.login_fragment_container, LoginMainFragment.newInstance(),
                LoginMainFragment.TAG);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        String activeFragmentTag = getActiveFragmentTag();
        if (activeFragmentTag != null && activeFragmentTag.equals(LoginWithMeetDayFragment.TAG)) {
            returnFromLoginWithMD();
        } else if (activeFragmentTag != null && activeFragmentTag.equals(LoginCreateAccountFragment.TAG)) {
            returnFromCreateAccount();
        } else {
            super.onBackPressed();
        }
    }

    private Fragment getActiveFragment() {
        List<Fragment> fragmentList = fragmentManager.getFragments();
        for (Fragment fragment : fragmentList) {
            if (fragment.isVisible()) {
                return fragment;
            }
        }
        return null;
    }

    private String getActiveFragmentTag() {
        Fragment fragment = getActiveFragment();
        if (fragment != null && fragment.getTag() != null) {
            return fragment.getTag();
        }
        return null;
    }

    private void goToDiscover() {
        this.removeObservers();
        Intent intent = new Intent(this, MainBottomTabActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void goToCreateAccount() {
        this.removeObservers();
        Intent intent = new Intent(this, CreateAccountActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void removeObservers() {
        profileViewModel.getProfile().removeObservers(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment activeFragment = getActiveFragment();
        if (activeFragment != null) {
            activeFragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
