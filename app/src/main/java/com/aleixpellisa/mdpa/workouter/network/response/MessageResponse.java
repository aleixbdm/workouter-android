package com.aleixpellisa.mdpa.workouter.network.response;

import com.aleixpellisa.mdpa.workouter.network.schema.MessageSchema;
import com.google.gson.annotations.SerializedName;

public class MessageResponse extends BaseResponse {
    @SerializedName("message_chat")
    public MessageSchema messageSchema;
}
