package com.aleixpellisa.mdpa.workouter.view;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.aleixpellisa.mdpa.workouter.view.dialog.DialogHandler;

import butterknife.ButterKnife;
import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        final BaseActivity activity = this;
        Scope scope = Toothpick.openScopes(getApplication(), this);
        scope.installModules(new Module() {{
            bind(DialogHandler.class).toInstance(new DialogHandler(activity));
        }});
        Toothpick.inject(this, scope);
        super.onCreate(savedInstanceState);
    }

    protected void setupView(@LayoutRes int layoutResID) {
        setContentView(layoutResID);
        ButterKnife.bind(this);
    }

}
