package com.aleixpellisa.mdpa.workouter;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.SharedPreferences;

import com.aleixpellisa.mdpa.workouter.database.MeetDayDatabase;
import com.aleixpellisa.mdpa.workouter.manager.CandidateManager;
import com.aleixpellisa.mdpa.workouter.manager.ChatManager;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesManager;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.manager.impl.CandidateManagerImpl;
import com.aleixpellisa.mdpa.workouter.manager.impl.ChatManagerImpl;
import com.aleixpellisa.mdpa.workouter.manager.impl.PreferencesManagerImpl;
import com.aleixpellisa.mdpa.workouter.manager.impl.ProfileManagerImpl;
import com.aleixpellisa.mdpa.workouter.network.Constants;
import com.aleixpellisa.mdpa.workouter.network.interceptor.AuthTokenInterceptor;
import com.aleixpellisa.mdpa.workouter.network.rest.AuthRest;
import com.aleixpellisa.mdpa.workouter.network.rest.CandidateRest;
import com.aleixpellisa.mdpa.workouter.network.rest.ChatRest;
import com.aleixpellisa.mdpa.workouter.network.rest.ProfileRest;
import com.aleixpellisa.mdpa.workouter.network.rest.impl.AuthRestRetrofit;
import com.aleixpellisa.mdpa.workouter.network.rest.impl.CandidateRestRetrofit;
import com.aleixpellisa.mdpa.workouter.network.rest.impl.ChatRestRetrofit;
import com.aleixpellisa.mdpa.workouter.network.rest.impl.ProfileRestRetrofit;
import com.facebook.appevents.AppEventsLogger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

public class App extends Application {

    private static App mInstance;

    public static synchronized App getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;

        // Facebook
        AppEventsLogger.activateApp(this);

        // ToothPick
        Scope applicationScope = Toothpick.openScope(this);
        installToothPickModules(applicationScope);

    }

    public void installToothPickModules(Scope scope) {
        scope.installModules(new Module() {{
            // Manager
            bind(CandidateManager.class).to(CandidateManagerImpl.class);
            bind(ChatManager.class).to(ChatManagerImpl.class);
            bind(ProfileManager.class).to(ProfileManagerImpl.class);
            bind(PreferencesManager.class).to(PreferencesManagerImpl.class);
            // Data
            bind(SharedPreferences.class).toInstance(createSharedPreferencesInstance());
            bind(MeetDayDatabase.class).toInstance(createDatabaseInstance());
            // Network
            AuthTokenInterceptor authTokenInterceptorInstance = new AuthTokenInterceptor();
            bind(AuthTokenInterceptor.class).toInstance(authTokenInterceptorInstance);
            bind(Retrofit.class).toInstance(getRetrofitClient(authTokenInterceptorInstance));
            bind(AuthRest.class).to(AuthRestRetrofit.class);
            bind(ProfileRest.class).to(ProfileRestRetrofit.class);
            bind(CandidateRest.class).to(CandidateRestRetrofit.class);
            bind(ChatRest.class).to(ChatRestRetrofit.class);
        }});
    }

    private SharedPreferences createSharedPreferencesInstance() {
        return getSharedPreferences(PreferencesManagerImpl.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
    }

    private MeetDayDatabase createDatabaseInstance() {
        return Room.databaseBuilder(getApplicationContext(),
                MeetDayDatabase.class, "meetday-database").build();
    }

    private Retrofit getRetrofitClient(AuthTokenInterceptor authTokenInterceptor) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.addInterceptor(authTokenInterceptor);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(Constants.API_BASE)
                .client(httpClient.build())
                .build();
    }
}
