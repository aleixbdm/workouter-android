package com.aleixpellisa.mdpa.workouter.model;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Evaluation extends BaseModel {

    public final static String EVALUATION_LIKE = "Like";
    public final static String EVALUATION_DISLIKE = "Dislike";
    public final static String EVALUATION_MATCH = "Match";

    @StringDef({EVALUATION_LIKE, EVALUATION_DISLIKE, EVALUATION_MATCH})
    @Retention(RetentionPolicy.SOURCE)
    public @interface EvaluationValue {
    }

    private String id;

    private String senderUserId;

    private String receiverUserId;

    private String evaluationValue;

    public Evaluation(String id, String senderUserId, String receiverUserId, String evaluationValue) {
        this.id = id;
        this.senderUserId = senderUserId;
        this.receiverUserId = receiverUserId;
        this.evaluationValue = evaluationValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(String senderUserId) {
        this.senderUserId = senderUserId;
    }

    public String getReceiverUserId() {
        return receiverUserId;
    }

    public void setReceiverUserId(String receiverUserId) {
        this.receiverUserId = receiverUserId;
    }

    public String getEvaluationValue() {
        return evaluationValue;
    }

    public void setEvaluationValue(String evaluationValue) {
        this.evaluationValue = evaluationValue;
    }
}
