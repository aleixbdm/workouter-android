package com.aleixpellisa.mdpa.workouter.network.request;

import android.support.annotation.Keep;

import com.aleixpellisa.mdpa.workouter.network.schema.CreateProfileSchema;
import com.google.gson.annotations.SerializedName;

@Keep
public class CreateProfileRequest {
    @SerializedName("user_basic_profile")
    public CreateProfileSchema createProfileSchema;
}

