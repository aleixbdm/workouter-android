package com.aleixpellisa.mdpa.workouter.view.create;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.aleixpellisa.mdpa.workouter.R;

public class CreateAccountFinishCreateFragment extends CreateAccountBaseFragment {

    static final String TAG = "CreateAccountFinishCreateFragmentTag";

    public Button leftButton;
    public Button rightButton;

    public static CreateAccountFinishCreateFragment newInstance(Button leftButton, Button rightButton) {
        CreateAccountFinishCreateFragment createAccountFillAgeFragment = new CreateAccountFinishCreateFragment();
        createAccountFillAgeFragment.setArguments(leftButton, rightButton);
        return createAccountFillAgeFragment;
    }

    public void setArguments(Button leftButton, Button rightButton) {
        this.leftButton = leftButton;
        this.rightButton = rightButton;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_account_finish_create, container, false);

        setupView(view);

        return view;
    }

    @Override
    public void setupView(View view) {
        super.setupView(view);

        configureBottomButtons();

    }

    private void configureBottomButtons() {
        leftButton.setVisibility(View.GONE);

        rightButton.setText(R.string.create_account_finish_button);
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentToActivityCallback.attemptFinishCreate();
            }
        });
    }

}