package com.aleixpellisa.mdpa.workouter.manager.observer;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;

import java.util.concurrent.Callable;

public abstract class EventObserver<E, ELD> implements Observer<Event<E>>, IEventObserver<E> {

    private @Nullable
    Callable refreshToken;

    private @Nullable
    EventLiveData<Event<ELD>> eventLiveData;

    protected EventObserver(@Nullable Callable refreshToken, @Nullable EventLiveData<Event<ELD>> eventLiveData) {
        this.refreshToken = refreshToken;
        this.eventLiveData = eventLiveData;
    }

    @Override
    public void onChanged(@Nullable Event<E> event) {
        if (event != null) {
            switch (event.status) {
                case Event.SUCCESS:
                    onSuccess(event);
                    break;
                case Event.ERROR:
                    onError(event);
                    break;
                case Event.AUTH_ERROR:
                    if (refreshToken != null) {
                        try {
                            refreshToken.call();
                        } catch (Exception ignored) {
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onError(Event<E> event) {
        if (eventLiveData != null) {
            Event<ELD> errorEvent = Event.error(event.meetDayError);
            eventLiveData.postValue(errorEvent);
        }
    }
}
