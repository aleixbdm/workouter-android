package com.aleixpellisa.mdpa.workouter.view.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.UserChat;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.model.MeetDayError;
import com.aleixpellisa.mdpa.workouter.view.BaseActivity;
import com.aleixpellisa.mdpa.workouter.view.chats.ChatActivity;
import com.aleixpellisa.mdpa.workouter.view.chats.ChatsFragment;
import com.aleixpellisa.mdpa.workouter.view.dialog.DialogHandler;
import com.aleixpellisa.mdpa.workouter.view.discover.DiscoverFragment;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.view.login.LoginActivity;
import com.aleixpellisa.mdpa.workouter.view.model.CandidateViewModel;
import com.aleixpellisa.mdpa.workouter.view.model.ChatViewModel;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;
import com.aleixpellisa.mdpa.workouter.view.profile.CandidateProfileActivity;
import com.aleixpellisa.mdpa.workouter.view.profile.EditProfileActivity;
import com.aleixpellisa.mdpa.workouter.view.profile.IEditProfileCallback;
import com.aleixpellisa.mdpa.workouter.view.profile.ProfileFragment;
import com.aleixpellisa.mdpa.workouter.view.settings.SettingsFragment;
import com.aleixpellisa.mdpa.workouter.view.terms.PrivacyActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class MainBottomTabActivity
        extends BaseActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener,
        IBottomTabNavigation, IEditProfileCallback {

    private static int DISCOVER_POSITION = 0;
    private static int CHATS_POSITION = 1;
    private static int PROFILE_POSITION = 2;
    private static int SETTINGS_POSITION = 3;

    @Inject
    DialogHandler dialogHandler;

    CandidateViewModel candidateViewModel;

    ChatViewModel chatViewModel;

    ProfileViewModel profileViewModel;

    @BindView(R.id.main_bottom_tab_view_pager)
    NoSwipePager viewPager;

    @BindView(R.id.main_bottom_tab_bottom_navigation)
    BottomNavigationView bottomNavigationView;

    private SavedActivityResult savedActivityResult;

    private BottomBarAdapter bottomBarAdapter;

    private boolean deletedProfile = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        candidateViewModel = ViewModelProviders.of(this).get(CandidateViewModel.class);
        chatViewModel = ViewModelProviders.of(this).get(ChatViewModel.class);
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        setupView(R.layout.activity_main_bottom_tab);
    }

    @Override
    public void setupView(int layoutResID) {
        super.setupView(layoutResID);

        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        viewPager.setPagingEnabled(false);
        bottomBarAdapter = new BottomBarAdapter(getSupportFragmentManager());

        bottomBarAdapter.addFragments(DiscoverFragment.newInstance());
        bottomBarAdapter.addFragments(ChatsFragment.newInstance());
        bottomBarAdapter.addFragments(ProfileFragment.newInstance());
        bottomBarAdapter.addFragments(SettingsFragment.newInstance());

        viewPager.setAdapter(bottomBarAdapter);

        setTitle(getString(R.string.main_bottom_tab_discover_title));

        // Workaround to keep all fragments loaded
        viewPager.setOffscreenPageLimit(bottomBarAdapter.getCount() - 1);

        goToDiscover();
    }

    private void setupDiscoverViewModel() {
        candidateViewModel.getCandidateList().observe(this, new Observer<Event<List<Candidate>>>() {
            @Override
            public void onChanged(@Nullable Event<List<Candidate>> listEvent) {
                if (listEvent != null && getDiscoverFragment() != null) {
                    if (listEvent.data != null) {
                        getDiscoverFragment().addCandidates(listEvent.data);
                    } else if (listEvent.meetDayError != null) {
                        getDiscoverFragment().addError(listEvent.meetDayError);
                    }
                }
            }
        });
        candidateViewModel.getEvaluation().observe(this, new Observer<Event<Evaluation>>() {
            @Override
            public void onChanged(@Nullable Event<Evaluation> evaluationEvent) {
                if (evaluationEvent != null && getDiscoverFragment() != null) {
                    if (evaluationEvent.data != null) {
                        getDiscoverFragment().evaluationDone(evaluationEvent.data);
                    } else {
                        dialogHandler.showErrorDialog(new MeetDayError(getString(R.string.discover_error_not_evaluated)));
                        getDiscoverFragment().rebaseEvaluation();
                    }
                }
            }
        });
    }

    private void setupChatViewModel() {
        chatViewModel.getChatList().observe(this, new Observer<Event<List<Chat>>>() {
            @Override
            public void onChanged(@Nullable Event<List<Chat>> listEvent) {
                if (listEvent != null && listEvent.data != null && getChatsFragment() != null) {
                    getChatsFragment().addChats(listEvent.data);
                }
            }
        });
    }

    private void setupProfileViewModel() {
        profileViewModel.getProfile().observe(this, new Observer<Event<Profile>>() {
            @Override
            public void onChanged(@Nullable Event<Profile> event) {
                if (event != null && event.data != null && getProfileFragment() != null) {
                    if (deletedProfile) {
                        logout();
                        dialogHandler.hideProgressDialog();
                        return;
                    }

                    getProfileFragment().addProfile(event.data);
                    if (getDiscoverFragment() != null && event.data.getSettings().isVisible()) {
                        getDiscoverFragment().notifyIsVisible();
                    }
                }
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.isChecked()) {
            return true;
        }
        int position = 0;
        switch (item.getItemId()) {
            case R.id.main_bottom_tab_action_discover:
                position = DISCOVER_POSITION;
                break;

            case R.id.main_bottom_tab_action_chats:
                position = CHATS_POSITION;
                break;

            case R.id.main_bottom_tab_action_profile:
                position = PROFILE_POSITION;
                break;

            case R.id.main_bottom_tab_action_settings:
                position = SETTINGS_POSITION;
                break;
        }

        item.setChecked(true);
        setTitle(item.getTitle());

        viewPager.setCurrentItem(position, false);
        return true;
    }

    @Override
    public void fragmentReady(String fragmentTag) {
        switch (fragmentTag) {
            case DiscoverFragment.TAG:
                setupDiscoverViewModel();
                break;
            case ChatsFragment.TAG:
                setupChatViewModel();
                break;
            case ProfileFragment.TAG:
                setupProfileViewModel();
                break;
        }
    }

    @Override
    public void goToDiscover() {
        onNavigationItemSelected(
                bottomNavigationView.getMenu().findItem(R.id.main_bottom_tab_action_discover));
    }

    @Override
    public void goToChats() {
        onNavigationItemSelected(
                bottomNavigationView.getMenu().findItem(R.id.main_bottom_tab_action_chats));
    }

    @Override
    public void goToChat(Chat chat) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(Chat.PARCEL_KEY, chat);
        intent.putExtra(UserChat.PARCEL_KEY, chat.getUserChat());
        startActivity(intent);
    }

    @Override
    public void goToProfile() {
        onNavigationItemSelected(
                bottomNavigationView.getMenu().findItem(R.id.main_bottom_tab_action_profile));
    }

    @Override
    public void goToCandidateProfileFromCard(Candidate candidate) {
        Intent intent = new Intent(this, CandidateProfileActivity.class);
        intent.putExtra(CandidateProfileActivity.FROM_CANDIDATE_CARD_KEY, true);
        intent.putExtra(Candidate.PARCEL_KEY, candidate);
        startActivityForResult(intent, CandidateProfileActivity.REQUEST_CODE);
    }

    @Override
    public void goToSettings() {
        onNavigationItemSelected(
                bottomNavigationView.getMenu().findItem(R.id.main_bottom_tab_action_settings));
    }

    @Override
    public void goToPrivacyPolicy() {
        Intent intent = new Intent(this, PrivacyActivity.class);
        startActivity(intent);
    }

    @Override
    public void share() {
        // TODO: Share
    }

    @Override
    public void logout() {
        removeObservers();
        clearAll();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void deleteAccount() {
        deletedProfile = true;
        dialogHandler.showProgressDialog();
        profileViewModel.deleteProfile();
    }

    @Override
    public void goToEditProfile() {
        profileViewModel.getProfile().removeObservers(this);
        Intent intent = new Intent(this, EditProfileActivity.class);
        startActivityForResult(intent, EditProfileActivity.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        savedActivityResult = new SavedActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onEnterAnimationComplete() {
        super.onEnterAnimationComplete();
        if (savedActivityResult != null) {
            if (savedActivityResult.resultCode == CandidateProfileActivity.REQUEST_CODE) {
                Fragment fragment = getDiscoverFragment();
                if (fragment != null) {
                    fragment.onActivityResult(savedActivityResult.requestCode,
                            savedActivityResult.resultCode,
                            savedActivityResult.intent);
                }
            }
            if (savedActivityResult.resultCode == EditProfileActivity.REQUEST_CODE) {
                profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
                this.setupProfileViewModel();
            }
            savedActivityResult = null;

        }
    }

    private DiscoverFragment getDiscoverFragment() {
        Fragment fragment = bottomBarAdapter.getItem(DISCOVER_POSITION);
        if (fragment instanceof DiscoverFragment) {
            return (DiscoverFragment) fragment;
        }
        return null;
    }

    private ChatsFragment getChatsFragment() {
        Fragment fragment = bottomBarAdapter.getItem(CHATS_POSITION);
        if (fragment instanceof ChatsFragment) {
            return (ChatsFragment) fragment;
        }
        return null;
    }

    private ProfileFragment getProfileFragment() {
        Fragment fragment = bottomBarAdapter.getItem(PROFILE_POSITION);
        if (fragment instanceof ProfileFragment) {
            return (ProfileFragment) fragment;
        }
        return null;
    }

    private SettingsFragment getSettingsFragment() {
        Fragment fragment = bottomBarAdapter.getItem(SETTINGS_POSITION);
        if (fragment instanceof SettingsFragment) {
            return (SettingsFragment) fragment;
        }
        return null;
    }

    private void clearAll() {
        candidateViewModel.clearAll();
        chatViewModel.clearAll();
        profileViewModel.clearAll();
    }

    private void removeObservers() {
        candidateViewModel.getCandidateList().removeObservers(this);
        chatViewModel.getChatList().removeObservers(this);
        profileViewModel.getProfile().removeObservers(this);
    }

}
