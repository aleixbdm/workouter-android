package com.aleixpellisa.mdpa.workouter.view.discover;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.view.main.IBottomTabNavigation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class MatchDialogFragment extends Dialog {

    public Context context;
    private Candidate candidate;

    private IBottomTabNavigation bottomTabNavigationCallback;

    MatchDialogFragment(Context context, Candidate candidate) {
        super(context);
        this.context = context;
        this.candidate = candidate;

        try {
            bottomTabNavigationCallback = (IBottomTabNavigation) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IBottomTabNavigation");
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.match_dialog);
        TextView matchDialogDescription = findViewById(R.id.match_dialog_description_text_view);
        ImageView candidateImageView = findViewById(R.id.match_dialog_candidate_image_view);
        Button goChatsButton = findViewById(R.id.match_dialog_go_chats_button);

        matchDialogDescription.setText(context.getString(R.string.match_dialog_description, candidate.getName()));

        if (!candidate.getImageUrls().isEmpty()) {
            Glide.with(context).load(candidate.getImageUrls().get(0)).apply(RequestOptions.circleCropTransform()).into(candidateImageView);
        }

        goChatsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                bottomTabNavigationCallback.goToChats();
            }
        });
    }

}