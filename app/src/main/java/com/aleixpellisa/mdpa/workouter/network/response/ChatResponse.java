package com.aleixpellisa.mdpa.workouter.network.response;

import com.aleixpellisa.mdpa.workouter.network.schema.ChatSchema;
import com.google.gson.annotations.SerializedName;

public class ChatResponse extends BaseResponse {
    @SerializedName("chat")
    public ChatSchema chatSchema;
}
