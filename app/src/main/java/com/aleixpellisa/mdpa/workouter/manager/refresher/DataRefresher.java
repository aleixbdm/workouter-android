package com.aleixpellisa.mdpa.workouter.manager.refresher;

import android.os.AsyncTask;

import com.aleixpellisa.mdpa.workouter.BuildConfig;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

public class DataRefresher {

    private RefreshData refreshData;

    @Inject
    DataRefresher() {}

    public void refreshData(Callable<Void> functionToCall) {
        if (refreshData == null || refreshData.isCancelled()) {
            refreshData = new RefreshData();
            refreshData.setFunctionToCall(functionToCall);
            ExecutorService executor = Executors.newFixedThreadPool(1);
            refreshData.executeOnExecutor(executor, BuildConfig.REFRESH_TIME);
        }
    }

    public void stop(){
        if (refreshData != null) {
            refreshData.cancel(true);
            refreshData = null;
        }
    }

    /**
     * AsyncTask section
     */

    private static class RefreshData extends AsyncTask<Integer, Void, Void> {

        private Callable<Void> functionToCall;

        @Override
        protected Void doInBackground(Integer... msValues) {
            Callable<Void> function = functionToCall;
            if (function != null) {
                while (!isCancelled()) {
                    try {
                        Thread.sleep(Long.valueOf(msValues[0]));
                        function.call();
                    } catch (Exception ignored) {
                    }
                }
            }
            return null;
        }

        void setFunctionToCall(Callable<Void> functionToCall) {
            this.functionToCall = functionToCall;
        }
    }


}
