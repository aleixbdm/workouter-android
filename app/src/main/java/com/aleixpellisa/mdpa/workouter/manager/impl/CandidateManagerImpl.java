package com.aleixpellisa.mdpa.workouter.manager.impl;

import android.arch.lifecycle.LiveData;
import android.support.annotation.VisibleForTesting;

import com.aleixpellisa.mdpa.workouter.manager.CandidateManager;
import com.aleixpellisa.mdpa.workouter.manager.observer.EventObserver;
import com.aleixpellisa.mdpa.workouter.manager.refresher.DataRefresher;
import com.aleixpellisa.mdpa.workouter.manager.refresher.TokenRefresher;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.network.rest.CandidateRest;
import com.aleixpellisa.mdpa.workouter.view.discover.CandidateCard;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class CandidateManagerImpl implements CandidateManager {

    @Inject
    CandidateRest candidateRest;

    @Inject
    TokenRefresher tokenRefresher;

    @Inject
    DataRefresher candidatesDataRefresher;

    private EventLiveData<Event<List<Candidate>>> candidateListLiveData;

    private EventLiveData<Event<Evaluation>> evaluationLiveData;

    private EventObserver<List<Candidate>, List<Candidate>> fetchCandidatesObserver;

    private EventObserver<Evaluation, Evaluation> sendEvaluationObserver;

    CandidateManagerImpl() {
        this.candidateListLiveData = new EventLiveData<>();
        this.evaluationLiveData = new EventLiveData<>();
    }

    @Override
    public LiveData<Event<List<Candidate>>> getCandidateList() {
        return candidateListLiveData;
    }

    @Override
    public LiveData<Event<Evaluation>> getEvaluation() {
        return evaluationLiveData;
    }

    @Override
    public void fetchCandidates() {
        Callable refreshToken = tokenRefresher.refreshTokenCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                fetchCandidates();
                return null;
            }
        }, candidateListLiveData);
        fetchCandidatesObserver = new EventObserver<List<Candidate>, List<Candidate>>
                (refreshToken, candidateListLiveData) {
            @Override
            public void onSuccess(Event<List<Candidate>> event) {
                // Sync candidates
                candidateListLiveData.postValue(event);

                if (event.data.size() < CandidateCard.VISIBLE_STACK_CANDIDATE_CARDS) {
                    refreshCandidates();
                } else {
                    candidatesDataRefresher.stop();
                }
            }

            @Override
            public void onError(Event<List<Candidate>> event) {
                super.onError(event);
                refreshCandidates();
            }
        };
        candidateRest.fetchCandidates(fetchCandidatesObserver);
    }

    @Override
    public void sendEvaluation(final String candidateId, final @Evaluation.EvaluationValue String evaluation) {
        Callable refreshToken = tokenRefresher.refreshTokenCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                sendEvaluation(candidateId, evaluation);
                return null;
            }
        }, evaluationLiveData);
        sendEvaluationObserver = new EventObserver<Evaluation, Evaluation>
                (refreshToken, evaluationLiveData) {
            @Override
            public void onSuccess(Event<Evaluation> event) {
                // Sync evaluation
                evaluationLiveData.postValue(event);
            }
        };
        candidateRest.sendEvaluation(candidateId, evaluation, sendEvaluationObserver);
    }

    @Override
    public void clearAll() {
        stopRefreshCandidates();
    }

    private void refreshCandidates() {
        candidatesDataRefresher.refreshData(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                fetchCandidates();
                return null;
            }
        });
    }

    private void stopRefreshCandidates() {
        candidatesDataRefresher.stop();
    }

    /**
     * Testing section
     */

    @VisibleForTesting
    public EventObserver<List<Candidate>, List<Candidate>> getFetchCandidatesObserver() {
        return fetchCandidatesObserver;
    }

    @VisibleForTesting
    public EventObserver<Evaluation, Evaluation> getSendEvaluationObserver() {
        return sendEvaluationObserver;
    }
}
