package com.aleixpellisa.mdpa.workouter.view.create;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.builder.ProfileBuilder;
import com.aleixpellisa.mdpa.workouter.builder.SettingsBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.view.BaseActivity;
import com.aleixpellisa.mdpa.workouter.view.dialog.CloseDialog;
import com.aleixpellisa.mdpa.workouter.view.dialog.DialogHandler;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.view.login.LoginActivity;
import com.aleixpellisa.mdpa.workouter.view.main.MainBottomTabActivity;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class CreateAccountActivity extends BaseActivity implements
        ICreateAccountFragmentToActivity, ICloseCreateProfileCallback {

    @Inject
    DialogHandler dialogHandler;

    ProfileViewModel profileViewModel;

    private FragmentManager fragmentManager;

    @BindView(R.id.create_account_name_text_view)
    TextView nameTextView;

    @BindView(R.id.create_account_profile_image_view)
    ImageView profileImageView;

    @BindView(R.id.create_account_left_button)
    Button leftButton;

    @BindView(R.id.create_account_right_button)
    Button rightButton;

    private String name;
    private String firstName;
    private String image;
    private int age = ProfileBuilder.PROFILE_AGE_MOCK;
    private @Profile.Gender
    String gender = ProfileBuilder.PROFILE_GENDER_MOCK;
    private @Settings.Preference
    String preference = SettingsBuilder.SETTINGS_PREFERENCE_MOCK;
    private String city = SettingsBuilder.SETTINGS_CITY_MOCK;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        setupView(R.layout.activity_create_account);
    }

    @Override
    public void setupView(int layoutResID) {
        super.setupView(layoutResID);

        // Hide action bar
        if (getSupportActionBar() != null) getSupportActionBar().hide();

        loadInitialFragment();
    }

    private void setupProfileViewModel() {
        profileViewModel.getProfile().observe(this, new Observer<Event<Profile>>() {
            @Override
            public void onChanged(@Nullable Event<Profile> event) {
                if (event != null) {
                    switch (event.status) {
                        case Event.SUCCESS:
                            Fragment activeFragment = getActiveFragment();
                            if (activeFragment != null && CreateAccountInitialFragment.TAG.equals(activeFragment.getTag())) {
                                CreateAccountInitialFragment fragment = (CreateAccountInitialFragment) getActiveFragment();
                                fragment.addProfile(event.data);
                            }
                            // City for example
                            if (event.data.getSettings().getCity() != null) {
                                goToDiscover();
                            }
                            break;
                        case Event.ERROR:
                            if ("database profile empty".equals(event.meetDayError.getMessage())) {
                                // Deleted profile
                                goToLogin();
                            }
                        case Event.AUTH_ERROR:
                            dialogHandler.hideProgressDialogWithError(event.meetDayError);
                            break;
                    }
                }
            }
        });
    }

    private void loadInitialFragment() {
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.create_account_fragment_container,
                CreateAccountInitialFragment.newInstance(leftButton, rightButton),
                CreateAccountInitialFragment.TAG);
        fragmentTransaction.commit();
    }

    private void initViewComponents(String name, String image) {
        nameTextView.setVisibility(View.VISIBLE);
        profileImageView.setVisibility(View.VISIBLE);
        nameTextView.setText(name);
        if (image != null && !image.isEmpty()) {
            Glide.with(this).load(image)
                    .apply(RequestOptions.circleCropTransform())
                    .into(profileImageView);
        }
    }

    @Override
    public void goToFillAge(String name, String firstName, String image) {
        initViewComponents(name, image);
        this.name = name;
        this.firstName = firstName;
        this.image = image;

        if (isDestroyed()) {
            return;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.right_side_in, R.anim.left_side_out);
        ft.replace(R.id.create_account_fragment_container,
                CreateAccountFillAgeFragment.newInstance(leftButton, rightButton),
                CreateAccountFillAgeFragment.TAG);
        ft.commit();
    }

    @Override
    public void goToFillLocation(int age, @Profile.Gender String gender, @Settings.Preference String preference) {
        this.age = age;
        this.gender = gender;
        this.preference = preference;
        if (isDestroyed()) {
            return;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.right_side_in, R.anim.left_side_out);
        ft.replace(R.id.create_account_fragment_container,
                CreateAccountFillLocationFragment.newInstance(leftButton, rightButton),
                CreateAccountFillLocationFragment.TAG);
        ft.commit();
    }

    @Override
    public void goToFinishCreate(String city) {
        this.city = city;
        if (isDestroyed()) {
            return;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.right_side_in, R.anim.left_side_out);
        ft.replace(R.id.create_account_fragment_container,
                CreateAccountFinishCreateFragment.newInstance(leftButton, rightButton),
                CreateAccountFinishCreateFragment.TAG);
        ft.commit();
    }

    @Override
    public void fragmentReady(String fragmentTag) {
        switch (fragmentTag) {
            case CreateAccountInitialFragment.TAG:
                setupProfileViewModel();
                break;
        }
    }

    @Override
    public void setEnabledRightButton(boolean enabled) {
        rightButton.setEnabled(enabled);
    }

    @Override
    public void cancelCreate() {
        onBackPressed();
    }

    @Override
    public void attemptFinishCreate() {
        try {
            dialogHandler.showProgressDialog();

            List<String> imageUrls = new ArrayList<>();
            imageUrls.add(image);
            profileViewModel.attemptCreateProfile(name, firstName, imageUrls, age, gender, preference, city);
        } catch (IllegalArgumentException ignored) {
            dialogHandler.hideProgressDialog();
        }
    }

    @Override
    public void returnFromFillLocation() {
        if (isDestroyed()) {
            return;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.setCustomAnimations(R.anim.left_side_in, R.anim.right_side_out);
        ft.replace(R.id.create_account_fragment_container,
                CreateAccountFillAgeFragment.newInstance(leftButton, rightButton),
                CreateAccountFillAgeFragment.TAG);
        ft.commit();
    }

    @Override
    public void returnFromFinishCreate() {
        if (isDestroyed()) {
            return;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.setCustomAnimations(R.anim.left_side_in, R.anim.right_side_out);
        ft.replace(R.id.create_account_fragment_container,
                CreateAccountFillLocationFragment.newInstance(leftButton, rightButton),
                CreateAccountFillLocationFragment.TAG);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        String activeFragmentTag = getActiveFragmentTag();
        if (activeFragmentTag != null) {
            switch (activeFragmentTag) {
                case CreateAccountInitialFragment.TAG:
                case CreateAccountFillAgeFragment.TAG:
                    final CloseDialog customDialog =
                            new CloseDialog(this, CloseDialog.CLOSE_CREATE_PROFILE_OPTION);
                    if (customDialog.getWindow() != null) {
                        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    }
                    customDialog.show();
                    break;
                case CreateAccountFillLocationFragment.TAG:
                    returnFromFillLocation();
                    break;
                case CreateAccountFinishCreateFragment.TAG:
                    returnFromFinishCreate();
                    break;
                default:
                    closeCreateProfile();
            }
        }
    }

    private Fragment getActiveFragment() {
        List<Fragment> fragmentList = fragmentManager.getFragments();
        for (Fragment fragment : fragmentList) {
            if (fragment.isVisible()) {
                return fragment;
            }
        }
        return null;
    }

    private String getActiveFragmentTag() {
        Fragment fragment = getActiveFragment();
        if (fragment != null && fragment.getTag() != null) {
            return fragment.getTag();
        }
        return null;
    }

    @Override
    public void closeCreateProfile() {
        profileViewModel.clearAll();
    }

    private void goToDiscover() {
        this.removeObservers();
        Intent intent = new Intent(this, MainBottomTabActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void goToLogin() {
        this.removeObservers();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void removeObservers() {
        profileViewModel.getProfile().removeObservers(this);
    }

}
