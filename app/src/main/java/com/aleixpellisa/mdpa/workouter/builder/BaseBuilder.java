package com.aleixpellisa.mdpa.workouter.builder;

import android.content.res.Resources;
import android.util.Log;

import com.aleixpellisa.mdpa.workouter.network.schema.BaseSchema;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class BaseBuilder {

    private Resources resources;

    public void setResources(Resources resources) {
        this.resources = resources;
    }

    <T extends BaseSchema> List<T> getSchemaListFromFile(int file, Class<T> type) {
        List<T> schemaList = new ArrayList<>();
        try {
            String jsonContent = readJsonFile(file);
            if (jsonContent.isEmpty()) {
                return schemaList;
            }

            JSONArray jsonArray = new JSONArray(jsonContent);
            for (int index = 0; index < jsonArray.length(); ++index) {
                JSONObject jsonObject = jsonArray.getJSONObject(index);
                T schema = type.cast(BaseSchema.fromJson(jsonObject.toString(), type));
                schemaList.add(schema);
            }
        } catch (Exception e) {
            Log.e(this.getClass().getName(), e.getMessage());
        }
        return schemaList;
    }

    private String readJsonFile(int file) {
        InputStream is = resources.openRawResource(file);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (Exception e) {
            Log.e(this.getClass().getName(), e.getMessage());
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.e(this.getClass().getName(), e.getMessage());
            }
        }

        return writer.toString();
    }

}
