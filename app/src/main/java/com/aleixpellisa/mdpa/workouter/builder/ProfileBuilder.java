package com.aleixpellisa.mdpa.workouter.builder;

import android.content.res.Resources;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.database.entity.Skill;
import com.aleixpellisa.mdpa.workouter.network.schema.ProfileSchema;

import java.util.ArrayList;
import java.util.List;

public class ProfileBuilder extends BaseBuilder {

    public static final String PROFILE_ID_MOCK = "ProfileId1";
    public static final String PROFILE_EMAIL_MOCK = "Email1";
    public static final String PROFILE_NAME_MOCK = "Name1";
    public static final String PROFILE_FIRST_NAME_MOCK = "FirstName1";
    public static final List<String> PROFILE_IMAGES_MOCK = new ArrayList<String>() {{
        add("image1.1");
        add("image1.2");
    }};
    public static final Integer PROFILE_AGE_MOCK = 20;
    public static final @Profile.Gender
    String PROFILE_GENDER_MOCK = Profile.GENDER_MALE;
    public static final String PROFILE_DESCRIPTION_MOCK = "Description1";
    public static final String PROFILE_CURRENT_JOB_MOCK = "CurrentJob1";
    public static final String PROFILE_STUDIES_MOCK = "Studies1";
    public static final String PROFILE_FAV_SONG_MOCK = "FavSong1";
    public static final List<Skill> PROFILE_SKILLS_MOCK = SkillBuilder.buildMockedSkillList();
    public static final Settings PROFILE_SETTINGS_MOCK = SettingsBuilder.buildMockedSettings();

    public static final String CANDIDATE_PROFILE_ID_MOCK = "CandidateId2";
    public static final String CANDIDATE_PROFILE_NAME_MOCK = "Name2";
    public static final String CANDIDATE_PROFILE_FIRST_NAME_MOCK = "FirstName2";
    public static final List<String> CANDIDATE_PROFILE_IMAGES_MOCK = new ArrayList<String>() {{
        add("image2.1");
        add("image2.2");
    }};
    public static final Integer CANDIDATE_PROFILE_AGE_MOCK = 22;
    public static final @Profile.Gender
    String CANDIDATE_PROFILE_GENDER_MOCK = Profile.GENDER_FEMALE;
    public static final String CANDIDATE_PROFILE_DESCRIPTION_MOCK = "Description2";
    public static final String CANDIDATE_PROFILE_CURRENT_JOB_MOCK = "CurrentJob2";
    public static final String CANDIDATE_PROFILE_STUDIES_MOCK = "Studies2";
    public static final String CANDIDATE_PROFILE_FAV_SONG_MOCK = "FavSong2";
    public static final List<Skill> CANDIDATE_PROFILE_SKILLS_MOCK = SkillBuilder.buildMockedSkillList();
    public static final Settings CANDIDATE_PROFILE_SETTINGS_MOCK = SettingsBuilder.buildMockedSettings();

    private String id;
    private String email;
    private String name;
    private String firstName;
    private List<String> imageUrls;
    private Integer age;
    private @Profile.Gender
    String gender;
    private String description;
    private String currentJob;
    private String studies;
    private String favouriteSong;
    private List<Skill> skills;
    private Settings settings;

    public ProfileBuilder(ProfileSchema profileSchema) {
        this.id = profileSchema.id;
        this.email = profileSchema.email;
        this.name = profileSchema.name;
        this.firstName = profileSchema.firstName;
        this.imageUrls = profileSchema.imageUrls;
        this.age = profileSchema.age;
        this.gender = profileSchema.gender;
        this.description = profileSchema.description;
        this.currentJob = profileSchema.currentJob;
        this.studies = profileSchema.studies;
        this.favouriteSong = profileSchema.favouriteSong;
        SkillBuilder skillBuilder = new SkillBuilder(profileSchema.skills);
        this.skills = skillBuilder.buildList();
        SettingsBuilder settingsBuilder = new SettingsBuilder(profileSchema.settings);
        this.settings = settingsBuilder.build();
    }

    public ProfileBuilder(String id, String name, String firstName,
                          String image, Integer age, @Profile.Gender String gender,
                          @Settings.Preference String preference, String city) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.imageUrls = new ArrayList<>();
        this.imageUrls.add(image);
        this.age = age;
        this.gender = gender;
        this.skills = new ArrayList<>();
        SettingsBuilder settingsBuilder = new SettingsBuilder(preference, city,
                null, null, null, true);
        this.settings = settingsBuilder.build();
    }

    public ProfileBuilder(String id, String email, String name, String firstName,
                          List<String> imageUrls, Integer age, @Profile.Gender String gender,
                          String description, String currentJob, String studies,
                          String favouriteSong, List<Skill> skills, Settings settings) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.firstName = firstName;
        this.imageUrls = imageUrls;
        this.age = age;
        this.gender = gender;
        this.description = description;
        this.currentJob = currentJob;
        this.studies = studies;
        this.favouriteSong = favouriteSong;
        this.skills = skills;
        this.settings = settings;
    }

    public ProfileBuilder(Candidate candidate) {
        if (candidate == null) return;
        this.id = candidate.getId();
        this.name = candidate.getName();
        this.imageUrls = candidate.getImageUrls();
        this.age = candidate.getAge();
        this.skills = candidate.getSkills();
        SettingsBuilder settingsBuilder =
                new SettingsBuilder(null, candidate.getCity(), null,
                        null, null, true);
        this.settings = settingsBuilder.build();
    }

    public ProfileBuilder(Profile profile) {
        if (profile == null) return;
        this.id = profile.getId();
        this.email = profile.getEmail();
        this.name = profile.getName();
        this.firstName = profile.getFirstName();
        this.imageUrls = profile.getImageUrls();
        this.age = profile.getAge();
        this.gender = profile.getGender();
        this.description = profile.getDescription();
        this.currentJob = profile.getCurrentJob();
        this.studies = profile.getStudies();
        this.favouriteSong = profile.getFavouriteSong();
        this.skills = profile.getSkills();
        this.settings = profile.getSettings();
    }

    public ProfileBuilder(Resources resources) {
        super.setResources(resources);
    }

    public Profile build() {
        return new Profile(id, email, name, firstName, imageUrls, age,
                gender, description, currentJob, studies, favouriteSong, skills, settings);
    }

    public Profile buildFromResources() {
        List<ProfileSchema> profileSchemaList = this.getSchemaListFromFile(R.raw.profile, ProfileSchema.class);
        if (!profileSchemaList.isEmpty()) {
            ProfileSchema profileSchema = profileSchemaList.get(0);
            ProfileBuilder profileBuilder = new ProfileBuilder(profileSchema);
            return profileBuilder.build();
        } else {
            return null;
        }
    }

    public ProfileSchema buildSchema() {
        Profile profile = build();
        ProfileSchema profileSchema = new ProfileSchema();
        profileSchema.id = profile.getId();
        profileSchema.email = profile.getEmail();
        profileSchema.name = profile.getName();
        profileSchema.firstName = profile.getFirstName();
        profileSchema.imageUrls = profile.getImageUrls();
        profileSchema.age = profile.getAge();
        profileSchema.gender = profile.getGender();
        profileSchema.description = profile.getDescription();
        profileSchema.currentJob = profile.getCurrentJob();
        profileSchema.studies = profile.getStudies();
        profileSchema.favouriteSong = profile.getFavouriteSong();
        if (profile.getSkills() != null) {
            for (Skill skill : profile.getSkills()) {
                SkillBuilder skillBuilder = new SkillBuilder(skill);
                if (profileSchema.skills == null) profileSchema.skills = new ArrayList<>();
                profileSchema.skills.add(skillBuilder.buildSchema());
            }
        }
        SettingsBuilder settingsBuilder = new SettingsBuilder(profile.getSettings());
        profileSchema.settings = settingsBuilder.buildSchema();
        return profileSchema;
    }

    public static Profile buildBasicProfile() {
        ProfileBuilder profileBuilder =
                new ProfileBuilder(PROFILE_ID_MOCK, PROFILE_EMAIL_MOCK, PROFILE_NAME_MOCK,
                        null, PROFILE_IMAGES_MOCK, PROFILE_AGE_MOCK, PROFILE_GENDER_MOCK, null,
                        null, null, null, null, SettingsBuilder.buildEmptySettings());
        return profileBuilder.build();
    }

    public static Profile buildMockedProfile() {
        ProfileBuilder profileBuilder =
                new ProfileBuilder(PROFILE_ID_MOCK, PROFILE_EMAIL_MOCK, PROFILE_NAME_MOCK,
                        PROFILE_FIRST_NAME_MOCK, PROFILE_IMAGES_MOCK, PROFILE_AGE_MOCK,
                        PROFILE_GENDER_MOCK, PROFILE_DESCRIPTION_MOCK, PROFILE_CURRENT_JOB_MOCK,
                        PROFILE_STUDIES_MOCK, PROFILE_FAV_SONG_MOCK, PROFILE_SKILLS_MOCK,
                        PROFILE_SETTINGS_MOCK);
        return profileBuilder.build();
    }

    public static Profile buildMockedCandidateProfile() {
        ProfileBuilder profileBuilder =
                new ProfileBuilder(CANDIDATE_PROFILE_ID_MOCK, null, CANDIDATE_PROFILE_NAME_MOCK,
                        CANDIDATE_PROFILE_FIRST_NAME_MOCK, CANDIDATE_PROFILE_IMAGES_MOCK,
                        CANDIDATE_PROFILE_AGE_MOCK, CANDIDATE_PROFILE_GENDER_MOCK,
                        CANDIDATE_PROFILE_DESCRIPTION_MOCK, CANDIDATE_PROFILE_CURRENT_JOB_MOCK,
                        CANDIDATE_PROFILE_STUDIES_MOCK, CANDIDATE_PROFILE_FAV_SONG_MOCK,
                        CANDIDATE_PROFILE_SKILLS_MOCK, CANDIDATE_PROFILE_SETTINGS_MOCK);
        return profileBuilder.build();
    }

}
