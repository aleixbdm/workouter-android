package com.aleixpellisa.mdpa.workouter.network.schema;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateProfileSchema extends BaseSchema {
    @SerializedName("name")
    public String name;

    @SerializedName("first_name")
    public String firstName;

    @SerializedName("image_urls")
    public List<String> imageUrls;

    @SerializedName("age")
    public Integer age;

    @SerializedName("gender")
    public String gender;

    @SerializedName("preference")
    public String preference;

    @SerializedName("city")
    public String city;
}
