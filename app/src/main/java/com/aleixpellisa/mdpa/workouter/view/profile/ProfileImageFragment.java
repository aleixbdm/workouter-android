package com.aleixpellisa.mdpa.workouter.view.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aleixpellisa.mdpa.workouter.R;
import com.bumptech.glide.Glide;

public class ProfileImageFragment extends Fragment {

    private IProfileImageCallback profileImageCallback;
    private String imageUrl;

    public static ProfileImageFragment newInstance(Context context) {
        ProfileImageFragment profileImageFragment = new ProfileImageFragment();
        profileImageFragment.setArguments(context);
        return profileImageFragment;
    }

    public static ProfileImageFragment newInstance(String imageUrl) {
        ProfileImageFragment profileImageFragment = new ProfileImageFragment();
        profileImageFragment.setArguments(imageUrl);
        return profileImageFragment;
    }

    public void setArguments(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setArguments(Context context) {
        try {
            profileImageCallback = (IProfileImageCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IProfileImageCallback");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.item_profile_image, container, false);

        ImageView imageView = rootView.findViewById(R.id.item_profile_image_view);

        if (imageUrl != null) {
            Glide.with(rootView.getContext()).load(imageUrl).into(imageView);
        } else {
            // TODO: use external library
            // add new image to the profile
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    profileImageCallback.imageAdded("https://www.w3schools.com/html/pulpitrock.jpg");
                }
            });

        }

        return rootView;
    }
}

