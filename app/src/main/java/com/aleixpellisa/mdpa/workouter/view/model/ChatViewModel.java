package com.aleixpellisa.mdpa.workouter.view.model;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.aleixpellisa.mdpa.workouter.App;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.manager.ChatManager;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.view.input.MeetDayTextInputType;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.google.common.annotations.VisibleForTesting;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import toothpick.Scope;
import toothpick.Toothpick;

@Singleton
public class ChatViewModel extends BaseViewModel {

    @Inject
    ChatManager chatManager;

    private String currentChatId;

    @Inject
    public ChatViewModel(@NonNull Application application) {
        super(application);
        // Supporting tests
        if (application instanceof App) {
            Scope scope = Toothpick.openScopes(getApplication(), this);
            Toothpick.inject(this, scope);
            chatManager.startObservers();
        }
    }

    public LiveData<Event<List<Chat>>> getChatList() {
        return chatManager.getChatList();
    }

    public LiveData<Event<List<Message>>> getChatMessageList() {
        return chatManager.getChatMessageList();
    }

    public LiveData<Event<Message>> getChatMessage() {
        return chatManager.getChatMessage();
    }

    public void fetchChats() {
        chatManager.fetchChats();
    }

    public void enterChat(String chatId) {
        currentChatId = chatId;
        chatManager.enterChat(chatId);
    }

    public void fetchChatMessages() {
        chatManager.fetchChatMessages(currentChatId);
    }

    public void applyChatsFilter(String filter) {
        // List<Chat> chatListFiltered = chatManager.retrieveChatsFiltered(this.chatList.getValue(), filter);
        // this.chatList.setValue(chatListFiltered);
    }

    public void attemptSendMessage(MeetDayTextInputType inputSendMessage) throws IllegalArgumentException {
        checkInputErrors(inputSendMessage);

        chatManager.sendMessage(currentChatId, inputSendMessage.getValue());
    }

    public void leaveChat() {
        currentChatId = null;
        chatManager.leaveChat();
    }

    public void clearAll() {
        chatManager.clearAll();
    }

    /**
     * Testing section
     */

    @VisibleForTesting
    public void setCurrentChatId(String currentChatId) {
        this.currentChatId = currentChatId;
    }

}
