package com.aleixpellisa.mdpa.workouter.view.dialog;

import android.support.annotation.NonNull;

import com.aleixpellisa.mdpa.workouter.model.MeetDayError;
import com.aleixpellisa.mdpa.workouter.view.BaseActivity;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DialogHandler {

    private ProgressDialogHandler progressDialogHandler;
    private ErrorDialogHandler errorDialogHandler;

    @Inject
    public DialogHandler(@NonNull BaseActivity activity) {
        progressDialogHandler = new ProgressDialogHandler(activity);
        errorDialogHandler = new ErrorDialogHandler(activity);
    }

    public void showProgressDialog() {
        progressDialogHandler.show("Loading...");
    }

    public void hideProgressDialog() {
        progressDialogHandler.hide();
    }

    public void hideProgressDialogWithError(MeetDayError meetDayError) {
        boolean isVisible = progressDialogHandler.isVisible();
        progressDialogHandler.hide();
        if (meetDayError != null && isVisible) {
            errorDialogHandler.show(meetDayError);
        }
    }

    public void showErrorDialog(MeetDayError meetDayError) {
        errorDialogHandler.show(meetDayError);
    }

}
