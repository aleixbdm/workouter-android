package com.aleixpellisa.mdpa.workouter.testers;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.interactions.InputLayoutInteractions;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class LoginTester {

    /**
     * Actions
     */

    public void goToLoginWithMeetDay() {
        onView(withId(R.id.login_login_with_md_button)).perform(click());
    }

    public void goToLoginCreateAccount() {
        onView(withId(R.id.login_create_account_text_view)).perform(click());
    }

    public void returnFromLoginWithMD() {
        onView(withId(R.id.login_with_md_back_button)).perform(click());
    }

    public void returnFromCreateAccount() {
        onView(withId(R.id.login_create_account_back_button)).perform(click());
    }

    public void attemptLogin() {
        onView(withId(R.id.login_with_md_login_button)).perform(click());
    }

    public void attemptCreateAccount() {
        onView(withId(R.id.login_create_account_create_button)).perform(click());
    }

    public void writeLoginWithMDEmail(String stringToBeTyped) {
        InputLayoutInteractions.onEditTextWithinTilWithId(R.id.login_with_md_email_text_input)
                .perform(typeText(stringToBeTyped), closeSoftKeyboard());
    }

    public void writeLoginWithMDPassword(String stringToBeTyped) {
        InputLayoutInteractions.onEditTextWithinTilWithId(R.id.login_with_md_password_text_input)
                .perform(typeText(stringToBeTyped), closeSoftKeyboard());
    }

    public void writeLoginCreateAccountEmail(String stringToBeTyped) {
        InputLayoutInteractions.onEditTextWithinTilWithId(R.id.login_create_account_email_text_input)
                .perform(typeText(stringToBeTyped), closeSoftKeyboard());
    }

    public void writeLoginCreateAccountPassword(String stringToBeTyped) {
        InputLayoutInteractions.onEditTextWithinTilWithId(R.id.login_create_account_password_text_input)
                .perform(typeText(stringToBeTyped), closeSoftKeyboard());
    }

    public void writeLoginCreateAccountConfirmPassword(String stringToBeTyped) {
        InputLayoutInteractions.onEditTextWithinTilWithId(R.id.login_create_account_confirm_password_text_input)
                .perform(typeText(stringToBeTyped), closeSoftKeyboard());
    }

    public void acceptLoginCreateAccountTerms() {
        onView(withId(R.id.login_create_account_register_accept_terms_checkbox)).perform(click());
    }

}
