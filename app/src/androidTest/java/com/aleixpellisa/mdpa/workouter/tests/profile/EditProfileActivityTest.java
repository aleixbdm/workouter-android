//package com.aleixpellisa.mdpa.meetday.tests.profile;
//
//import android.support.test.filters.MediumTest;
//import android.support.test.runner.AndroidJUnit4;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.manager.ManagerListener;
//import com.aleixpellisa.mdpa.meetday.matchers.InputLayoutMatcher;
//import com.aleixpellisa.mdpa.meetday.matchers.ProfileArgumentMatcher;
//import com.aleixpellisa.mdpa.meetday.database.entity.Profile;
//import com.aleixpellisa.mdpa.meetday.testers.ProfileTester;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.assertion.ViewAssertions.matches;
//import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.argThat;
//import static org.mockito.Mockito.verify;
//import static org.mockito.internal.verification.VerificationModeFactory.times;
//
//@RunWith(AndroidJUnit4.class)
//@MediumTest
//public class EditProfileActivityTest extends BaseInstrumentationTest {
//
//    private ProfileTester tester;
//
//    @Before
//    public void setup() {
//        super.setup();
//
//        tester = new ProfileTester(this);
//        tester.whenAuthenticated();
//    }
//
//    @After
//    public void tearDown() {
//        super.tearDown();
//    }
//
//    /**
//     * Navigation tests
//     **/
//
//    @Test
//    public void checkEditProfileIsDisplayed() {
//        tester.launchEditProfileActivity();
//
//        onView(withId(R.id.edit_profile_save_button)).check(matches(isDisplayed()));
//    }
//
//    @Test
//    public void checkLaunchEditProfileWithExistingProfileDisplaysProfile() {
//        Profile profile = tester.getProfile();
//        tester.launchEditProfileActivityWithProfile(profile);
//
//        onView(withId(R.id.profile_fragment_name_text_input)).check
//                (matches(InputLayoutMatcher.withText(profile.getName())));
//    }
//
//    @Test
//    public void checkLaunchEditProfileWithNotExistingProfileNotDisplaysProfile() {
//        tester.launchEditProfileActivity();
//
//        onView(withId(R.id.profile_fragment_name_text_input)).check
//                (matches(InputLayoutMatcher.withText("")));
//    }
//
//    /**
//     * Update name
//     **/
//
//    @Test
//    public void checkChangeNameUpdatesProfileName() {
//        Profile profile = tester.getProfile();
//        tester.launchEditProfileActivityWithProfile(profile);
//
//        String name = "newName";
//        tester.changeProfileName(name);
//        profile.setName(name);
//
//        tester.saveProfile();
//
//        verify(profileManagerMock, times(1))
//                .updateProfile(argThat(new ProfileArgumentMatcher(profile)), any(ManagerListener.class));
//    }
//
//}
