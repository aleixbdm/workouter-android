package com.aleixpellisa.mdpa.workouter.testers;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.uiautomator.UiDevice;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.aleixpellisa.mdpa.workouter.App;
import com.aleixpellisa.mdpa.workouter.matchers.ToastMatcher;
import com.aleixpellisa.mdpa.workouter.view.BaseActivity;

import java.util.concurrent.Callable;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class CommonTester {

    private ActivityTestRule<BaseActivity> activityTestRule;
    private App app;

    public CommonTester(ActivityTestRule<BaseActivity> activityTestRule, App app) {
        this.activityTestRule = activityTestRule;
        this.app = app;
    }

    /**
     * Getters
     */

    public Fragment getCurrentFragment(int containerId) {
        FragmentManager fragmentManager = activityTestRule.getActivity().getSupportFragmentManager();
        return fragmentManager.findFragmentById(containerId);
    }

    /**
     * Actions
     */

    public void launchActivity(Class activityClass) {
        this.launchActivityWithIntent(new Intent(app.getApplicationContext(), activityClass));
    }

    public void launchActivityWithIntent(Intent intent) {
        activityTestRule.launchActivity(intent);
    }

    public void willAppearLoadingDialog(final Callable<Void> functionThatWillMakeAppear) {
        // In some emulators... the progress dialog need to be dismissed in order to
        // continue with the test, this function works with the precondition that a
        // loading dialog will be appeared.
        final UiDevice uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        final int waitTime = 1000;
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(waitTime);
                    uiDevice.pressBack();
                } catch (InterruptedException ignored) {}
            }
        }).start();
        try {
            functionThatWillMakeAppear.call();
        } catch (Exception ignored) {
        }
        waitToSee(waitTime + 500);
    }

    public void waitToSee(int millis) {
        // Wait to see results, sometimes is needed for animations...
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks
     */

    public void isToastDisplayedWithText(String text) {
        onView(withText(text)).inRoot(new ToastMatcher()).check(matches(isDisplayed()));
    }

}
