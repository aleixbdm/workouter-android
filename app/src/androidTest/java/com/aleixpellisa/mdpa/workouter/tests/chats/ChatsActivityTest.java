//package com.aleixpellisa.mdpa.meetday.tests.chats;
//
//import android.support.test.filters.MediumTest;
//import android.support.test.runner.AndroidJUnit4;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.builder.MessageBuilder;
//import com.aleixpellisa.mdpa.meetday.matchers.RecyclerViewMatcher;
//import com.aleixpellisa.mdpa.meetday.model.Message;
//import com.aleixpellisa.mdpa.meetday.testers.ChatsTester;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import java.util.List;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.assertion.ViewAssertions.matches;
//import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static android.support.test.espresso.matcher.ViewMatchers.withText;
//
//@RunWith(AndroidJUnit4.class)
//@MediumTest
//public class ChatsActivityTest extends BaseInstrumentationTest {
//
//    private ChatsTester tester;
//
//    @Before
//    public void setup() {
//        super.setup();
//
//        tester = new ChatsTester(this);
//        tester.whenAuthenticated();
//    }
//
//    @After
//    public void tearDown() {
//        super.tearDown();
//    }
//
//    /**
//     * Navigation tests
//     **/
//
//    @Test
//    public void checkChatMessageListIsDisplayed() {
//        tester.launchActivity();
//
//        onView(withId(R.id.chat_message_list)).check(matches(isDisplayed()));
//    }
//
//    /**
//     * Download messages from chat
//     **/
//
//    @Test
//    public void checkDownloadMessagesFromExistingChatReturnsMessagesDisplaysMessageList() {
//        Chat chat = tester.getChatList().get(0);
//        String chatId = chat.getId();
//        List<Message> messageList = tester.getMessageList();
//        tester.whenDownloadMessagesFromChatSucceed(chatId, true);
//        tester.whenRetrieveMessages(messageList);
//
//        tester.launchActivityWithChat(chat);
//
//        int index = messageList.size() - 1;
//        Message message = messageList.get(index);
//        tester.scrollMessageListTo(index);
//        onView(withText(message.getMessageText())).check(matches(isDisplayed()));
//        onView(withId(R.id.chat_message_list)).check
//                (matches(RecyclerViewMatcher.hasNItems(messageList.size())));
//    }
//
//    @Test
//    public void checkDownloadMessagesFromExistingChatFailsDisplaysEmptyMessageList() {
//        Chat chat = tester.getChatList().get(0);
//        String chatId = chat.getId();
//        tester.whenDownloadMessagesFromChatSucceed(chatId, false);
//
//        tester.launchActivityWithChat(chat);
//
//        onView(withId(R.id.chat_message_list)).check
//                (matches(RecyclerViewMatcher.isEmpty()));
//    }
//
//    @Test
//    public void checkDownloadMessagesFromExistingChatReturnsEmptyListDisplaysEmptyMessageList() {
//        Chat chat = tester.getChatList().get(0);
//        String chatId = chat.getId();
//        tester.whenDownloadMessagesFromChatSucceed(chatId, true);
//        tester.whenRetrieveMessages(MessageBuilder.buildEmptyMessageList());
//
//        tester.launchActivityWithChat(chat);
//
//        onView(withId(R.id.chat_message_list)).check
//                (matches(RecyclerViewMatcher.isEmpty()));
//    }
//
//    @Test
//    public void checkLaunchChatActivityWithoutChatDownloadMessagesFromChatIsNotCalledAndDisplaysEmptyMessageList() {
//        tester.launchActivity();
//
//        onView(withId(R.id.chat_message_list)).check
//                (matches(RecyclerViewMatcher.isEmpty()));
//    }
//
//    /**
//     * Send message
//     **/
//
//    @Test
//    public void checkSendMessageSucceedDisplaysMessageListWithMessageAdded() {
//        Chat chat = tester.getChatList().get(0);
//        String chatId = chat.getId();
//        String messageToSend = tester.getMessageToSend();
//        List<Message> messageList = tester.getMessageList();
//        tester.whenDownloadMessagesFromChatSucceed(chatId, true);
//        tester.whenRetrieveMessages(messageList);
//        tester.whenSendMessageSucceed(messageList, chatId, messageToSend, true);
//
//        tester.launchActivityWithChat(chat);
//
//        tester.writeMessage(messageToSend);
//        tester.pressSendMessage();
//
//        int size = messageList.size();
//        onView(withId(R.id.chat_message_list)).check
//                (matches(RecyclerViewMatcher.hasNItems(size + 1)));
//    }
//
//    @Test
//    public void checkSendEmptyMessageSucceedDisplaysMessageListWithoutMessageAdded() {
//        Chat chat = tester.getChatList().get(0);
//        String chatId = chat.getId();
//        String messageToSend = "";
//        List<Message> messageList = tester.getMessageList();
//        tester.whenDownloadMessagesFromChatSucceed(chatId, true);
//        tester.whenRetrieveMessages(messageList);
//        tester.whenSendMessageSucceed(messageList, chatId, messageToSend, true);
//
//        tester.launchActivityWithChat(chat);
//
//        tester.writeMessage(messageToSend);
//        tester.pressSendMessage();
//
//        int size = messageList.size();
//        onView(withId(R.id.chat_message_list)).check
//                (matches(RecyclerViewMatcher.hasNItems(size)));
//    }
//
//    @Test
//    public void checkSendMessageFailsDisplaysMessageListWithoutMessageAdded() {
//        Chat chat = tester.getChatList().get(0);
//        String chatId = chat.getId();
//        String messageToSend = tester.getMessageToSend();
//        List<Message> messageList = tester.getMessageList();
//        tester.whenDownloadMessagesFromChatSucceed(chatId, true);
//        tester.whenRetrieveMessages(messageList);
//        tester.whenSendMessageSucceed(messageList, chatId, messageToSend, false);
//
//        tester.launchActivityWithChat(chat);
//
//        tester.writeMessage(messageToSend);
//        tester.pressSendMessage();
//
//        int size = messageList.size();
//        onView(withId(R.id.chat_message_list)).check
//                (matches(RecyclerViewMatcher.hasNItems(size)));
//    }
//
//}
