//package com.aleixpellisa.mdpa.meetday.tests.profile;
//
//import android.support.test.filters.MediumTest;
//import android.support.test.runner.AndroidJUnit4;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.model.Candidate;
//import com.aleixpellisa.mdpa.meetday.testers.ProfileTester;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.assertion.ViewAssertions.matches;
//import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static android.support.test.espresso.matcher.ViewMatchers.withText;
//import static org.hamcrest.core.IsNot.not;
//
//@RunWith(AndroidJUnit4.class)
//@MediumTest
//public class CandidateProfileActivityTest extends BaseInstrumentationTest {
//
//    private ProfileTester tester;
//
//    @Before
//    public void setup() {
//        super.setup();
//
//        tester = new ProfileTester(this);
//        tester.whenAuthenticated();
//    }
//
//    @After
//    public void tearDown() {
//        super.tearDown();
//    }
//
//    /**
//     * Navigation tests
//     **/
//
//    @Test
//    public void checkCandidateProfileIsDisplayed() {
//        tester.launchCandidateProfileActivity();
//
//        onView(withId(R.id.profile_fragment_user_image_pager)).check(matches(isDisplayed()));
//    }
//
//    @Test
//    public void checkLaunchCandidateProfileWithExistingCandidateDisplaysCandidateProfile() {
//        Candidate candidate = tester.getCandidate();
//        tester.launchCandidateProfileActivityWithCandidateFromCandidateCard(candidate);
//
//        onView(withId(R.id.profile_fragment_city_text_view)).check
//                (matches(withText(candidate.getCity())));
//    }
//
//    @Test
//    public void checkLaunchCandidateProfileWithNotExistingCandidateNotDisplaysCandidateProfile() {
//        tester.launchCandidateProfileActivity();
//
//        onView(withId(R.id.profile_fragment_city_text_view)).check
//                (matches(withText("")));
//    }
//
//    @Test
//    public void checkLaunchCandidateProfileFromCandidateCardDisplaysEvaluationSection() {
//        Candidate candidate = tester.getCandidate();
//        tester.launchCandidateProfileActivityWithCandidateFromCandidateCard(candidate);
//
//        onView(withId(R.id.layout_evaluation_button_layout)).check
//                (matches(isDisplayed()));
//    }
//
//    @Test
//    public void checkLaunchCandidateProfileFromCandidateChatNotDisplaysEvaluationSection() {
//        Candidate candidate = tester.getCandidate();
//        tester.launchCandidateProfileActivityWithCandidateFromCandidateChat(candidate);
//
//        onView(withId(R.id.layout_evaluation_button_layout)).check
//                (matches(not(isDisplayed())));
//    }
//
//}
