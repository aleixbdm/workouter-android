//package com.aleixpellisa.mdpa.meetday.tests.main;
//
//import android.support.test.filters.MediumTest;
//import android.support.test.runner.AndroidJUnit4;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//import com.aleixpellisa.mdpa.meetday.testers.CommonTester;
//import com.aleixpellisa.mdpa.meetday.testers.MainTester;
//import com.aleixpellisa.mdpa.meetday.view.chats.ChatsFragment;
//import com.aleixpellisa.mdpa.meetday.view.discover.DiscoverFragment;
//import com.aleixpellisa.mdpa.meetday.view.main.MainBottomTabActivity;
//import com.aleixpellisa.mdpa.meetday.view.profile.ProfileFragment;
//import com.aleixpellisa.mdpa.meetday.view.settings.SettingsFragment;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import static org.hamcrest.Matchers.instanceOf;
//import static org.junit.Assert.assertThat;
//import static org.junit.Assert.assertTrue;
//
//@RunWith(AndroidJUnit4.class)
//@MediumTest
//public class MainBottomTabActivityTest extends BaseInstrumentationTest {
//
//    private CommonTester commonTester;
//    private MainTester mainTester;
//
//    @Before
//    public void setup() {
//        super.setup();
//
//        commonTester = new CommonTester(this);
//        mainTester = new MainTester();
//
//        commonTester.launchActivity(MainBottomTabActivity.class);
//    }
//
//    @After
//    public void tearDown() {
//        super.tearDown();
//    }
//
//    /**
//     * Navigation tests
//     **/
//
//    @Test
//    public void checkDiscoverFragmentIsDisplayed() {
//        assertTrue(commonTester.fragmentIsVisible(DiscoverFragment.TAG));
//    }
//
//    @Test
//    public void goToDiscoverLoadsProperFragment() {
//        mainTester.goToDiscover();
//
//        assertThat(commonTester.getCurrentFragment(R.id.main_bottom_tab_view_pager), instanceOf(DiscoverFragment.class));
//    }
//
//    @Test
//    public void goToChatsLoadsProperFragment() {
//        mainTester.goToChats();
//
//        assertThat(commonTester.getCurrentFragment(R.id.main_bottom_tab_view_pager), instanceOf(ChatsFragment.class));
//    }
//
//    @Test
//    public void goToProfileLoadsProperFragment() {
//        mainTester.goToProfile();
//
//        assertThat(commonTester.getCurrentFragment(R.id.main_bottom_tab_view_pager), instanceOf(ProfileFragment.class));
//    }
//
//    @Test
//    public void goToSettingsLoadsProperFragment() {
//        mainTester.goToSettings();
//
//        assertThat(commonTester.getCurrentFragment(R.id.main_bottom_tab_view_pager), instanceOf(SettingsFragment.class));
//    }
//
//}
