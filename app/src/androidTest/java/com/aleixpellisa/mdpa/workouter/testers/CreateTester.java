//package com.aleixpellisa.mdpa.meetday.testers;
//
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.builder.AuthBuilder;
//import com.aleixpellisa.mdpa.meetday.builder.CandidateBuilder;
//import com.aleixpellisa.mdpa.meetday.builder.ProfileBuilder;
//import com.aleixpellisa.mdpa.meetday.interactions.InputLayoutInteractions;
//import com.aleixpellisa.mdpa.meetday.manager.ManagerListener;
//import com.aleixpellisa.mdpa.meetday.model.Candidate;
//import com.aleixpellisa.mdpa.meetday.database.entity.Profile;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//import com.aleixpellisa.mdpa.meetday.view.create.CreateAccountActivity;
//
//import org.mockito.invocation.InvocationOnMock;
//import org.mockito.stubbing.Answer;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.action.ViewActions.click;
//import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
//import static android.support.test.espresso.action.ViewActions.scrollTo;
//import static android.support.test.espresso.action.ViewActions.typeText;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.doAnswer;
//import static org.mockito.Mockito.when;
//
//public class CreateTester {
//
//    private BaseInstrumentationTest test;
//
//    private CommonTester commonTester;
//    private AuthTester authTester;
//
//    public CreateTester(BaseInstrumentationTest test) {
//        this.test = test;
//        this.commonTester = new CommonTester(test);
//        this.authTester = new AuthTester(test);
//    }
//
//    /**
//     * Getters
//     */
//
//    public Fragment getCurrentFragment(int containerId) {
//        FragmentManager fragment = test.activityTestRule.getActivity().getSupportFragmentManager();
//        return fragment.findFragmentById(containerId);
//    }
//
//    public Profile getProfile() {
//        ProfileBuilder profileBuilder = new ProfileBuilder(test.getApplication().getResources());
//        return profileBuilder.buildFromResources();
//    }
//
//    public Candidate getCandidate() {
//        CandidateBuilder candidateBuilder = new CandidateBuilder(test.getApplication().getResources());
//        return candidateBuilder.buildList().get(0);
//    }
//
//    /**
//     * Preconditions
//     */
//
//    public void whenAuthenticated() {
//        AuthBuilder authBuilder = new AuthBuilder(test.getApplication().getResources());
//        when(test.preferencesManagerMock.readAuth()).thenReturn(authBuilder.buildFromResources());
//    }
//
//    public void whenInitialFragmentDone(String name, String firstName) {
//        writeName(name);
//        writeFirstName(firstName);
//        addImage();
//        pressRightButton();
//    }
//
//    public void whenUpdateProfileSucceed(final boolean succeed) {
//        doAnswer(new Answer() {
//            @Override
//            public Object answer(InvocationOnMock invocation) throws Throwable {
//                ManagerListener callback = (ManagerListener) invocation.getArguments()[1];
//                if (succeed) {
//                    callback.onSucceed();
//                } else {
//                    callback.onError(new Error());
//                }
//                return null;
//            }
//        }).when(test.profileManagerMock).updateProfile(any(Profile.class), any(ManagerListener.class));
//    }
//
//    public void whenRetrieveProfile(Profile profile) {
//        when(test.profileManagerMock.retrieveProfile()).thenReturn(profile);
//    }
//
//    /**
//     * Actions
//     */
//
//    public void launchCreateAccountActivity() {
//        commonTester.launchActivity(CreateAccountActivity.class);
//    }
//
//    public void writeName(String name) {
//        InputLayoutInteractions.onEditTextWithinTilWithId(R.id.create_account_initial_fragment_name_text_input)
//                .perform(scrollTo(), typeText(name), closeSoftKeyboard());
//    }
//
//    public void writeFirstName(String firstName) {
//        InputLayoutInteractions.onEditTextWithinTilWithId(R.id.create_account_initial_fragment_first_name_text_input)
//                .perform(scrollTo(), typeText(firstName), closeSoftKeyboard());
//    }
//
//    public void addImage() {
//        onView(withId(R.id.create_account_initial_fragment_add_photo_image_view))
//                .perform(scrollTo(), click());
//    }
//
//    public void pressLeftButton() {
//        onView(withId(R.id.create_account_left_button)).perform(click());
//    }
//
//    public void pressRightButton() {
//        onView(withId(R.id.create_account_right_button)).perform(click());
//    }
//
//}
