//package com.aleixpellisa.mdpa.meetday.tests.create;
//
//import android.support.test.filters.MediumTest;
//import android.support.test.runner.AndroidJUnit4;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.matchers.InputLayoutMatcher;
//import com.aleixpellisa.mdpa.meetday.testers.CreateTester;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//import com.aleixpellisa.mdpa.meetday.view.create.CreateAccountFillAgeFragment;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.assertion.ViewAssertions.matches;
//import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static org.hamcrest.Matchers.instanceOf;
//import static org.hamcrest.core.IsNot.not;
//import static org.junit.Assert.assertThat;
//
//@RunWith(AndroidJUnit4.class)
//@MediumTest
//public class CreateAccountInitialFragmentTest extends BaseInstrumentationTest {
//
//    private CreateTester tester;
//
//    @Before
//    public void setup() {
//        super.setup();
//
//        tester = new CreateTester(this);
//        tester.launchCreateAccountActivity();
//    }
//
//    @After
//    public void tearDown() {
//        super.tearDown();
//    }
//
//    /**
//     * Input tests
//     **/
//
//    @Test
//    public void checkInputsAreEmpty() {
//        onView(withId(R.id.create_account_initial_fragment_name_text_input)).check
//                (matches(InputLayoutMatcher.withText("")));
//        onView(withId(R.id.create_account_initial_fragment_first_name_text_input)).check
//                (matches(InputLayoutMatcher.withText("")));
//    }
//
//    @Test
//    public void checkInputsCorrectHint() {
//        onView(withId(R.id.create_account_initial_fragment_name_text_input)).check
//                (matches(InputLayoutMatcher.withHint(activityTestRule.getActivity().getString(R.string.input_text_layout_prompt_name))));
//        onView(withId(R.id.create_account_initial_fragment_first_name_text_input)).check
//                (matches(InputLayoutMatcher.withHint(activityTestRule.getActivity().getString(R.string.input_text_layout_prompt_first_name))));
//    }
//
//    @Test
//    public void checkRightButtonIsDisabledWithEmptyInputs() {
//        onView(withId(R.id.create_account_right_button)).check
//                (matches(not(isEnabled())));
//    }
//
//    @Test
//    public void enterCorrectParametersEnablesRightButton() {
//        tester.writeName("name");
//        tester.writeFirstName("firstName");
//        tester.addImage();
//
//        onView(withId(R.id.create_account_right_button)).check
//                (matches(isEnabled()));
//
//    }
//
//    @Test
//    public void pressRightButtonGoToFillAge() {
//        tester.writeName("name");
//        tester.writeFirstName("firstName");
//        tester.addImage();
//
//        tester.pressRightButton();
//
//        assertThat(tester.getCurrentFragment(R.id.create_account_fragment_container),
//                instanceOf(CreateAccountFillAgeFragment.class));
//    }
//
//}
