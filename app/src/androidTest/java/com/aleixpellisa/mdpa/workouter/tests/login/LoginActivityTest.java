package com.aleixpellisa.mdpa.workouter.tests.login;

import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.testers.LoginTester;
import com.aleixpellisa.mdpa.workouter.tests.BaseInstrumentationTest;
import com.aleixpellisa.mdpa.workouter.view.login.LoginActivity;
import com.aleixpellisa.mdpa.workouter.view.login.LoginCreateAccountFragment;
import com.aleixpellisa.mdpa.workouter.view.login.LoginMainFragment;
import com.aleixpellisa.mdpa.workouter.view.login.LoginWithMeetDayFragment;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.pressBack;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
@MediumTest
public class LoginActivityTest extends BaseInstrumentationTest {

    private LoginTester loginTester;

    @Before
    public void setup() {
        super.setup();

        loginTester = new LoginTester();

        commonTester.launchActivity(LoginActivity.class);
    }

    @After
    public void tearDown() {
        super.tearDown();
    }

    /**
     * Navigation tests
     **/

    @Test
    public void checkLoginMainFragmentIsDisplayed() {
        assertThat(commonTester.getCurrentFragment(R.id.login_fragment_container), instanceOf(LoginMainFragment.class));
    }

    @Test
    public void goToLoginWithMeetDayLoadsProperFragment() {
        loginTester.goToLoginWithMeetDay();

        assertThat(commonTester.getCurrentFragment(R.id.login_fragment_container), instanceOf(LoginWithMeetDayFragment.class));
    }

    @Test
    public void goToLoginCreateAccountLoadsProperFragment() {
        loginTester.goToLoginCreateAccount();

        assertThat(commonTester.getCurrentFragment(R.id.login_fragment_container), instanceOf(LoginCreateAccountFragment.class));
    }

    @Test
    public void returnFromLoginWithMDLoadsProperFragment() {
        loginTester.goToLoginWithMeetDay();
        loginTester.returnFromLoginWithMD();

        assertThat(commonTester.getCurrentFragment(R.id.login_fragment_container), instanceOf(LoginMainFragment.class));
    }

    @Test
    public void returnFromCreateAccountLoadsProperFragment() {
        loginTester.goToLoginCreateAccount();
        loginTester.returnFromCreateAccount();

        assertThat(commonTester.getCurrentFragment(R.id.login_fragment_container), instanceOf(LoginMainFragment.class));
    }

    @Test
    public void pressBackFromLoginWithMeetDayLoadsProperFragment() {
        loginTester.goToLoginWithMeetDay();
        pressBack();

        assertThat(commonTester.getCurrentFragment(R.id.login_fragment_container), instanceOf(LoginMainFragment.class));
    }

    @Test
    public void pressBackFromCreateAccountLoadsProperFragment() {
        loginTester.goToLoginCreateAccount();
        pressBack();

        assertThat(commonTester.getCurrentFragment(R.id.login_fragment_container), instanceOf(LoginMainFragment.class));
    }

}
