//package com.aleixpellisa.mdpa.meetday.tests.settings;
//
//import android.support.test.filters.MediumTest;
//import android.support.test.runner.AndroidJUnit4;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.builder.AuthBuilder;
//import com.aleixpellisa.mdpa.meetday.builder.ProfileBuilder;
//import com.aleixpellisa.mdpa.meetday.manager.ManagerListener;
//import com.aleixpellisa.mdpa.meetday.database.entity.Profile;
//import com.aleixpellisa.mdpa.meetday.database.entity.Settings;
//import com.aleixpellisa.mdpa.meetday.testers.CommonTester;
//import com.aleixpellisa.mdpa.meetday.testers.MainTester;
//import com.aleixpellisa.mdpa.meetday.testers.ProfileTester;
//import com.aleixpellisa.mdpa.meetday.testers.SettingsTester;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//import com.aleixpellisa.mdpa.meetday.view.main.MainBottomTabActivity;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.assertion.ViewAssertions.matches;
//import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static android.support.test.espresso.matcher.ViewMatchers.withText;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.eq;
//import static org.mockito.Mockito.verify;
//import static org.mockito.internal.verification.VerificationModeFactory.times;
//
//@RunWith(AndroidJUnit4.class)
//@MediumTest
//public class SettingsFragmentTest extends BaseInstrumentationTest {
//
//    private Class activityClass;
//
//    private CommonTester commonTester;
//    private MainTester mainTester;
//    private AuthTester authTester;
//    private ProfileTester profileTester;
//    private SettingsTester settingsTester;
//
//    @Before
//    public void setup() {
//        super.setup();
//
//        activityClass = MainBottomTabActivity.class;
//
//        commonTester = new CommonTester(this);
//        mainTester = new MainTester(this);
//        authTester = new AuthTester(this);
//        profileTester = new ProfileTester(this);
//        settingsTester = new SettingsTester(this);
//
//        authTester.whenRetrieveAuthentication(AuthBuilder.buildMockedAuth());
//    }
//
//    @After
//    public void tearDown() {
//        super.tearDown();
//    }
//
//    /**
//     * Navigation tests
//     **/
//
//    @Test
//    public void checkDownloadProfileReturnsProfileDisplaysSettings() {
//        profileTester.whenDownloadProfileSucceed(true);
//        ProfileBuilder profileBuilder = new ProfileBuilder(application.getResources());
//        Profile profile = profileBuilder.buildFromResources();
//        profileTester.whenRetrieveProfile(profile);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToSettings();
//
//        onView(withId(R.id.settings_fragment_location_switch)).check(matches(isDisplayed()));
//
//        onView(withId(R.id.settings_fragment_city_text_view)).check
//                (matches(withText(profile.getSettings().getCity())));
//    }
//
//    @Test
//    public void checkDownloadProfileFailsNotDisplaysProfile() {
//        profileTester.whenDownloadProfileSucceed(false);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToSettings();
//
//        onView(withId(R.id.settings_fragment_city_text_view)).check
//                (matches(withText("")));
//    }
//
//    @Test
//    public void goToPrivacyPolicyDisplaysPrivacyActivity() {
//        profileTester.whenDownloadProfileSucceed(true);
//        ProfileBuilder profileBuilder = new ProfileBuilder(application.getResources());
//        Profile profile = profileBuilder.buildFromResources();
//        profileTester.whenRetrieveProfile(profile);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToSettings();
//
//        settingsTester.goToPrivacyPolicy();
//
//        onView(withId(R.id.privacy_text)).check(matches(isDisplayed()));
//    }
//
//    @Test
//    public void goToTermsAndConditionsDisplaysTermsActivity() {
//        profileTester.whenDownloadProfileSucceed(true);
//        ProfileBuilder profileBuilder = new ProfileBuilder(application.getResources());
//        Profile profile = profileBuilder.buildFromResources();
//        profileTester.whenRetrieveProfile(profile);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToSettings();
//
//        settingsTester.goToTerms();
//
//        onView(withId(R.id.terms_text)).check(matches(isDisplayed()));
//    }
//
//    @Test
//    public void logoutClearAllDataAndDisplaysLoginActivity() {
//        profileTester.whenDownloadProfileSucceed(true);
//        ProfileBuilder profileBuilder = new ProfileBuilder(application.getResources());
//        Profile profile = profileBuilder.buildFromResources();
//        profileTester.whenRetrieveProfile(profile);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToSettings();
//
//        authTester.whenRetrieveAuthentication(null);
//
//        settingsTester.logout();
//
//        verify(preferencesManagerMock, times(1)).clearAll();
//        onView(withId(R.id.login_fragment_container)).check(matches(isDisplayed()));
//    }
//
//    /**
//     * Update search range
//     **/
//
//    @Test
//    public void checkChangeSearchRangeUpdatesSearchRangeSettings() {
//        profileTester.whenDownloadProfileSucceed(true);
//        ProfileBuilder profileBuilder = new ProfileBuilder(application.getResources());
//        Profile profile = profileBuilder.buildFromResources();
//        profileTester.whenRetrieveProfile(profile);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToSettings();
//
//        int progress = 10;
//        settingsTester.changeSearchRangeBar(progress);
//
//        verify(profileManagerMock, times(1))
//                .updateSettings(eq(Settings.SEARCH_RANGE_KEY), eq(progress), any(ManagerListener.class));
//    }
//
//    /**
//     * Update preference
//     **/
//
//    @Test
//    public void checkUpdatePreferenceMaleUpdatesPreferenceSettings() {
//        profileTester.whenDownloadProfileSucceed(true);
//        ProfileBuilder profileBuilder = new ProfileBuilder(application.getResources());
//        Profile profile = profileBuilder.buildFromResources();
//        profileTester.whenRetrieveProfile(profile);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToSettings();
//
//        @Settings.Preference String preference = Settings.PREFERENCE_MALE;
//        settingsTester.changePreference(preference);
//
//        verify(profileManagerMock, times(1))
//                .updateSettings(eq(Settings.PREFERENCE_KEY), eq(preference), any(ManagerListener.class));
//    }
//
//    @Test
//    public void checkUpdatePreferenceFemaleUpdatesPreferenceSettings() {
//        profileTester.whenDownloadProfileSucceed(true);
//        ProfileBuilder profileBuilder = new ProfileBuilder(application.getResources());
//        Profile profile = profileBuilder.buildFromResources();
//        profileTester.whenRetrieveProfile(profile);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToSettings();
//
//        @Settings.Preference String preference = Settings.PREFERENCE_FEMALE;
//        settingsTester.changePreference(preference);
//
//        verify(profileManagerMock, times(1))
//                .updateSettings(eq(Settings.PREFERENCE_KEY), eq(preference), any(ManagerListener.class));
//    }
//
//    @Test
//    public void checkUpdatePreferenceAllUpdatesPreferenceSettings() {
//        profileTester.whenDownloadProfileSucceed(true);
//        ProfileBuilder profileBuilder = new ProfileBuilder(application.getResources());
//        Profile profile = profileBuilder.buildFromResources();
//        profileTester.whenRetrieveProfile(profile);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToSettings();
//
//        @Settings.Preference String preference = Settings.PREFERENCE_ALL;
//        settingsTester.changePreference(preference);
//
//        verify(profileManagerMock, times(1))
//                .updateSettings(eq(Settings.PREFERENCE_KEY), eq(preference), any(ManagerListener.class));
//    }
//
//    /**
//     * Update search range
//     **/
//
//    @Test
//    public void checkChangeYearRangeUpdatesYearRangeSettings() {
//        profileTester.whenDownloadProfileSucceed(true);
//        ProfileBuilder profileBuilder = new ProfileBuilder(application.getResources());
//        Profile profile = profileBuilder.buildFromResources();
//        profileTester.whenRetrieveProfile(profile);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToSettings();
//
//        int progressLeft = 31;
//        int progressRight = 35;
//        settingsTester.changeYearRangeBar(progressLeft, progressRight);
//
//        verify(profileManagerMock, times(1))
//                .updateSettings(eq(Settings.YEAR_RANGE_MIN_KEY), eq(progressLeft), any(ManagerListener.class));
//        verify(profileManagerMock, times(1))
//                .updateSettings(eq(Settings.YEAR_RANGE_MAX_KEY), eq(progressRight), any(ManagerListener.class));
//    }
//
//    /**
//     * Update visibility
//     */
//
//    @Test
//    public void checkChangeVisibilityUpdatesVisibilitySettings() {
//        profileTester.whenDownloadProfileSucceed(true);
//        ProfileBuilder profileBuilder = new ProfileBuilder(application.getResources());
//        Profile profile = profileBuilder.buildFromResources();
//        profileTester.whenRetrieveProfile(profile);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToSettings();
//
//        boolean previousVisibility = profile.getSettings().isVisible();
//        settingsTester.switchVisibility();
//
//        verify(profileManagerMock, times(1))
//                .updateSettings(eq(Settings.VISIBLE_KEY), eq(!previousVisibility), any(ManagerListener.class));
//    }
//
//}
