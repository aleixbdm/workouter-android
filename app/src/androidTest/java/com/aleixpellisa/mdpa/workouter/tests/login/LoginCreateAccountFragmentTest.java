package com.aleixpellisa.mdpa.workouter.tests.login;

import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.matchers.CheckboxMatcher;
import com.aleixpellisa.mdpa.workouter.matchers.InputLayoutMatcher;
import com.aleixpellisa.mdpa.workouter.testers.LoginTester;
import com.aleixpellisa.mdpa.workouter.tests.BaseInstrumentationTest;
import com.aleixpellisa.mdpa.workouter.view.login.LoginActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.Callable;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static values.Events.fetchProfileErrorEvent;
import static values.Events.fetchProfileEvent;
import static values.Inputs.email;
import static values.Inputs.encryptedPassword;
import static values.Inputs.password;
import static values.Inputs.wrongConfirmPassword;
import static values.Inputs.wrongEmail;
import static values.Inputs.wrongPassword;

@RunWith(AndroidJUnit4.class)
@MediumTest
public class LoginCreateAccountFragmentTest extends BaseInstrumentationTest {

    private LoginTester loginTester;

    @Before
    public void setup() {
        super.setup();

        loginTester = new LoginTester();

        commonTester.launchActivity(LoginActivity.class);
    }

    @After
    public void tearDown() {
        super.tearDown();
    }

    /**
     * Input tests
     **/

    @Test
    public void checkInputsAreEmpty() {
        loginTester.goToLoginCreateAccount();

        onView(withId(R.id.login_create_account_email_text_input)).check
                (matches(InputLayoutMatcher.withText("")));
        onView(withId(R.id.login_create_account_password_text_input)).check
                (matches(InputLayoutMatcher.withText("")));
        onView(withId(R.id.login_create_account_confirm_password_text_input)).check
                (matches(InputLayoutMatcher.withText("")));
        onView(withId(R.id.login_create_account_register_accept_terms_checkbox)).check
                (matches(not(isChecked())));
    }

    @Test
    public void checkInputsCorrectHint() {
        loginTester.goToLoginCreateAccount();

        onView(withId(R.id.login_create_account_email_text_input)).check
                (matches(InputLayoutMatcher.withHint(activityTestRule.getActivity().getString(R.string.input_text_layout_prompt_email))));
        onView(withId(R.id.login_create_account_password_text_input)).check
                (matches(InputLayoutMatcher.withHint(activityTestRule.getActivity().getString(R.string.input_text_layout_prompt_password))));
        onView(withId(R.id.login_create_account_confirm_password_text_input)).check
                (matches(InputLayoutMatcher.withHint(activityTestRule.getActivity().getString(R.string.input_text_layout_prompt_confirm_password))));
    }

    @Test
    public void createAccountWithEmptyInputs() {
        loginTester.goToLoginCreateAccount();
        loginTester.attemptCreateAccount();

        onView(withId(R.id.login_create_account_email_text_input)).check
                (matches(InputLayoutMatcher.withError(activityTestRule.getActivity().getString(R.string.input_text_layout_error_field_required))));
        onView(withId(R.id.login_create_account_password_text_input)).check
                (matches(InputLayoutMatcher.withError(activityTestRule.getActivity().getString(R.string.input_text_layout_error_field_required))));
        onView(withId(R.id.login_create_account_confirm_password_text_input)).check
                (matches(InputLayoutMatcher.withError(activityTestRule.getActivity().getString(R.string.input_text_layout_error_field_required))));
        onView(withId(R.id.login_create_account_register_accept_terms_checkbox)).check
                (matches(CheckboxMatcher.withError(activityTestRule.getActivity().getString(R.string.login_create_account_error_must_accept_terms))));
    }

    @Test
    public void createAccountWithIncorrectEmail() {
        loginTester.goToLoginCreateAccount();
        loginTester.writeLoginCreateAccountEmail(wrongEmail);
        loginTester.attemptCreateAccount();

        onView(withId(R.id.login_create_account_email_text_input)).check
                (matches(InputLayoutMatcher.withError(activityTestRule.getActivity().getString(R.string.input_text_layout_error_email_incorrect))));
    }

    @Test
    public void createAccountWithShortPassword() {
        loginTester.goToLoginCreateAccount();
        loginTester.writeLoginCreateAccountPassword(wrongPassword);
        loginTester.attemptCreateAccount();

        onView(withId(R.id.login_create_account_password_text_input)).check
                (matches(InputLayoutMatcher.withError(activityTestRule.getActivity().getString(R.string.input_text_layout_error_password_short))));
    }

    @Test
    public void createAccountWithShortConfirmPassword() {
        loginTester.goToLoginCreateAccount();
        loginTester.writeLoginCreateAccountConfirmPassword(wrongPassword);
        loginTester.attemptCreateAccount();

        onView(withId(R.id.login_create_account_confirm_password_text_input)).check
                (matches(InputLayoutMatcher.withError(activityTestRule.getActivity().getString(R.string.input_text_layout_error_password_short))));
    }

    @Test
    public void createAccountWithoutMatchingPasswords() {
        loginTester.goToLoginCreateAccount();
        loginTester.writeLoginCreateAccountPassword(password);
        loginTester.writeLoginCreateAccountConfirmPassword(wrongConfirmPassword);
        loginTester.attemptCreateAccount();

        onView(withId(R.id.login_create_account_confirm_password_text_input)).check
                (matches(InputLayoutMatcher.withError(activityTestRule.getActivity().getString(R.string.login_create_account_error_passwords_match))));
    }

    @Test
    public void checkCreateAccountForwardsCredentials() {
        loginTester.goToLoginCreateAccount();
        loginTester.writeLoginCreateAccountEmail(email);
        loginTester.writeLoginCreateAccountPassword(password);
        loginTester.writeLoginCreateAccountConfirmPassword(password);
        loginTester.acceptLoginCreateAccountTerms();
        commonTester.willAppearLoadingDialog(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                loginTester.attemptCreateAccount();
                return null;
            }
        });

        verify(profileManagerMock, times(1))
                .createAccount(
                        eq(email),
                        eq(encryptedPassword));
    }

    @Test
    public void createAccountWithCorrectCredentialsGoToCreateAccount() {
        loginTester.goToLoginCreateAccount();
        loginTester.writeLoginCreateAccountEmail(email);
        loginTester.writeLoginCreateAccountPassword(password);
        loginTester.writeLoginCreateAccountConfirmPassword(password);
        loginTester.acceptLoginCreateAccountTerms();
        commonTester.willAppearLoadingDialog(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                loginTester.attemptCreateAccount();
                return null;
            }
        });
        profileViewModelTester.profileFromManager(fetchProfileEvent);

        onView(withId(R.id.create_account_fragment_container)).check(matches(isDisplayed()));
    }

    @Test
    public void createAccountWithIncorrectCredentialsShowsError() {
        loginTester.goToLoginCreateAccount();
        loginTester.writeLoginCreateAccountEmail(email);
        loginTester.writeLoginCreateAccountPassword(password);
        loginTester.writeLoginCreateAccountConfirmPassword(password);
        loginTester.acceptLoginCreateAccountTerms();
        commonTester.willAppearLoadingDialog(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                loginTester.attemptCreateAccount();
                return null;
            }
        });
        profileViewModelTester.profileFromManager(fetchProfileErrorEvent);

        commonTester.isToastDisplayedWithText(fetchProfileErrorEvent.meetDayError.getMessage());
    }

}
