package com.aleixpellisa.mdpa.workouter.tests;

import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;

import com.aleixpellisa.mdpa.workouter.App;
import com.aleixpellisa.mdpa.workouter.manager.CandidateManager;
import com.aleixpellisa.mdpa.workouter.manager.ChatManager;
import com.aleixpellisa.mdpa.workouter.manager.PreferencesManager;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.testers.CommonTester;
import com.aleixpellisa.mdpa.workouter.view.BaseActivity;

import org.junit.Rule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import testers.viewmodel.CandidateViewModelInstrumentedTester;
import testers.viewmodel.ChatViewModelInstrumentedTester;
import testers.viewmodel.ProfileViewModelInstrumentedTester;
import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

public class BaseInstrumentationTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public ActivityTestRule<BaseActivity> activityTestRule =
            new ActivityTestRule<>(BaseActivity.class, false, false);

    @Mock
    protected CandidateManager candidateManagerMock;

    @Mock
    protected ChatManager chatManagerMock;

    @Mock
    protected PreferencesManager preferencesManagerMock;

    @Mock
    protected ProfileManager profileManagerMock;

    @Mock
    protected SharedPreferences sharedPreferencesMock;

    protected CommonTester commonTester;

    protected ProfileViewModelInstrumentedTester profileViewModelTester;

    protected CandidateViewModelInstrumentedTester candidateViewModelTester;

    protected ChatViewModelInstrumentedTester chatViewModelTester;

    private static App application;
    private static Scope applicationScope;

    public void setup() {
        application = (App) InstrumentationRegistry.getTargetContext().getApplicationContext();
        applicationScope = Toothpick.openScope(application);

        // We need to install the modules and launch the activity manually
        applicationScope.installTestModules(new Module() {{
            bind(CandidateManager.class).toInstance(candidateManagerMock);
            bind(ChatManager.class).toInstance(chatManagerMock);
            bind(PreferencesManager.class).toInstance(preferencesManagerMock);
            bind(ProfileManager.class).toInstance(profileManagerMock);
            bind(SharedPreferences.class).toInstance(sharedPreferencesMock);
        }});

        commonTester = new CommonTester(activityTestRule, application);
        profileViewModelTester = new ProfileViewModelInstrumentedTester(profileManagerMock);
        candidateViewModelTester = new CandidateViewModelInstrumentedTester(profileManagerMock, candidateManagerMock);
        chatViewModelTester = new ChatViewModelInstrumentedTester(chatManagerMock);
    }

    public void tearDown() {
        Toothpick.reset(applicationScope);
        application.installToothPickModules(applicationScope);
    }

}
