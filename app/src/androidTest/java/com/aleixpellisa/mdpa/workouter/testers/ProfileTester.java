//package com.aleixpellisa.mdpa.meetday.testers;
//
//import android.content.Intent;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.builder.AuthBuilder;
//import com.aleixpellisa.mdpa.meetday.builder.CandidateBuilder;
//import com.aleixpellisa.mdpa.meetday.builder.ProfileBuilder;
//import com.aleixpellisa.mdpa.meetday.interactions.InputLayoutInteractions;
//import com.aleixpellisa.mdpa.meetday.manager.ManagerListener;
//import com.aleixpellisa.mdpa.meetday.model.Candidate;
//import com.aleixpellisa.mdpa.meetday.database.entity.Profile;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//import com.aleixpellisa.mdpa.meetday.view.profile.CandidateProfileActivity;
//import com.aleixpellisa.mdpa.meetday.view.profile.EditProfileActivity;
//
//import org.mockito.invocation.InvocationOnMock;
//import org.mockito.stubbing.Answer;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.action.ViewActions.clearText;
//import static android.support.test.espresso.action.ViewActions.click;
//import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
//import static android.support.test.espresso.action.ViewActions.scrollTo;
//import static android.support.test.espresso.action.ViewActions.typeText;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.eq;
//import static org.mockito.Mockito.doAnswer;
//import static org.mockito.Mockito.when;
//
//public class ProfileTester {
//
//    private BaseInstrumentationTest test;
//
//    private CommonTester commonTester;
//    private AuthTester authTester;
//
//    public ProfileTester(BaseInstrumentationTest test) {
//        this.test = test;
//        this.commonTester = new CommonTester(test);
//        this.authTester = new AuthTester(test);
//    }
//
//    /**
//     * Getters
//     */
//
//    public Profile getProfile() {
//        ProfileBuilder profileBuilder = new ProfileBuilder(test.getApplication().getResources());
//        return profileBuilder.buildFromResources();
//    }
//
//    public Candidate getCandidate() {
//        CandidateBuilder candidateBuilder = new CandidateBuilder(test.getApplication().getResources());
//        return candidateBuilder.buildList().get(0);
//    }
//
//    /**
//     * Preconditions
//     */
//
//    public void whenAuthenticated() {
//        AuthBuilder authBuilder = new AuthBuilder(test.getApplication().getResources());
//        authTester.whenRetrieveAuthentication(authBuilder.buildFromResources());
//    }
//
//    public void whenDownloadProfileSucceed(final boolean succeed) {
//        doAnswer(new Answer() {
//            @Override
//            public Object answer(InvocationOnMock invocation) throws Throwable {
//                ManagerListener callback = (ManagerListener) invocation.getArguments()[0];
//                if (succeed) {
//                    callback.onSucceed();
//                } else {
//                    callback.onError(new Error());
//                }
//                return null;
//            }
//        }).when(test.profileManagerMock).downloadProfile(any(ManagerListener.class));
//    }
//
//    public void whenDownloadCandidateProfileSucceed(String candidateId, final boolean succeed) {
//        doAnswer(new Answer() {
//            @Override
//            public Object answer(InvocationOnMock invocation) throws Throwable {
//                ManagerListener callback = (ManagerListener) invocation.getArguments()[1];
//                if (succeed) {
//                    callback.onSucceed();
//                } else {
//                    callback.onError(new Error());
//                }
//                return null;
//            }
//        }).when(test.profileManagerMock).downloadCandidateProfile(eq(candidateId), any(ManagerListener.class));
//    }
//
//    public void whenRetrieveProfile(Profile profile) {
//        when(test.profileManagerMock.retrieveProfile()).thenReturn(profile);
//    }
//
//    public void whenRetrieveCandidateProfile(Profile candidateProfile) {
//        when(test.profileManagerMock.retrieveCandidateProfile()).thenReturn(candidateProfile);
//    }
//
//    public void whenProfileIsBasic(boolean isBasic) {
//        when(test.profileManagerMock.isProfileBasic())
//                .thenReturn(isBasic);
//    }
//
//    /**
//     * Actions
//     */
//
//    public void launchCandidateProfileActivity() {
//        commonTester.launchActivity(CandidateProfileActivity.class);
//    }
//
//    public void launchEditProfileActivity() {
//        commonTester.launchActivity(EditProfileActivity.class);
//    }
//
//    public void launchCandidateProfileActivityWithCandidateFromCandidateCard(Candidate candidate) {
//        Intent intent = new Intent(test.getApplication().getApplicationContext(), CandidateProfileActivity.class);
//        intent.putExtra(CandidateProfileActivity.FROM_CANDIDATE_CARD_KEY, true);
//        intent.putExtra(Candidate.PARCEL_KEY, candidate);
//        commonTester.launchActivityWithIntent(intent);
//    }
//
//    public void launchCandidateProfileActivityWithCandidateFromCandidateChat(Candidate candidate) {
//        Intent intent = new Intent(test.getApplication().getApplicationContext(), CandidateProfileActivity.class);
//        intent.putExtra(CandidateProfileActivity.FROM_CANDIDATE_CHAT_KEY, true);
//        intent.putExtra(Candidate.PARCEL_KEY, candidate);
//        commonTester.launchActivityWithIntent(intent);
//    }
//
//    public void launchEditProfileActivityWithProfile(Profile profile) {
//        this.whenDownloadProfileSucceed(true);
//        this.whenRetrieveProfile(profile);
//        this.launchEditProfileActivity();
//    }
//
//    public void goToEditProfile() {
//        onView(withId(R.id.profile_fragment_edit_button)).perform(scrollTo(), click());
//    }
//
//    public void changeProfileName(String name) {
//        InputLayoutInteractions.onEditTextWithinTilWithId(R.id.profile_fragment_name_text_input)
//                .perform(scrollTo(), clearText(), closeSoftKeyboard());
//        InputLayoutInteractions.onEditTextWithinTilWithId(R.id.profile_fragment_name_text_input)
//                .perform(scrollTo(), typeText(name), closeSoftKeyboard());
//    }
//
//    public void saveProfile() {
//        onView(withId(R.id.edit_profile_save_button)).perform(click());
//    }
//
//}
