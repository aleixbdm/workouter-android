package com.aleixpellisa.mdpa.workouter.testers;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.interactions.RangeBarInteractions;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.tests.BaseInstrumentationTest;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class SettingsTester {

    private BaseInstrumentationTest test;

    public SettingsTester(BaseInstrumentationTest test) {
        this.test = test;
    }

    /**
     * Actions
     */

    public void changeSearchRangeBar(int progress) {
        onView(withId(R.id.settings_fragment_search_range_bar))
                .perform(scrollTo(), RangeBarInteractions.setSeekProgress(progress));
    }

    public void changePreference(@Settings.Preference String preference) {
        int resourceId = 0;
        switch (preference) {
            case Settings.PREFERENCE_MALE:
                resourceId = R.id.settings_fragment_gender_male_image_view;
                break;
            case Settings.PREFERENCE_FEMALE:
                resourceId = R.id.settings_fragment_gender_female_image_view;
                break;
            case Settings.PREFERENCE_ALL:
                resourceId = R.id.settings_fragment_gender_all_image_view;
                break;
        }
        onView(withId(resourceId))
                .perform(scrollTo(), click());
    }

    public void changeYearRangeBar(int leftProgress, int rightProgress) {
        onView(withId(R.id.settings_fragment_year_range_bar))
                .perform(scrollTo(), RangeBarInteractions.setRangeProgress(leftProgress, rightProgress));
    }

    public void switchVisibility() {
        onView(withId(R.id.settings_fragment_visible_switch))
                .perform(scrollTo(), click());
    }

    public void goToPrivacyPolicy() {
        onView(withId(R.id.settings_fragment_privacy_text_view))
                .perform(scrollTo(), click());
    }

    public void goToTerms() {
        onView(withId(R.id.settings_fragment_terms_text_view))
                .perform(scrollTo(), click());
    }

    public void share() {
        onView(withId(R.id.settings_share_button))
                .perform(scrollTo(), click());
    }

    public void logout() {
        onView(withId(R.id.settings_logout_button))
                .perform(scrollTo(), click());
    }

    public void deleteAccount() {
        onView(withId(R.id.settings_delete_account_button))
                .perform(scrollTo(), click());
    }

}
