package com.aleixpellisa.mdpa.workouter.tests.login;

import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.matchers.InputLayoutMatcher;
import com.aleixpellisa.mdpa.workouter.testers.LoginTester;
import com.aleixpellisa.mdpa.workouter.tests.BaseInstrumentationTest;
import com.aleixpellisa.mdpa.workouter.view.login.LoginActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.Callable;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static values.Events.fetchProfileErrorEvent;
import static values.Events.fetchProfileEvent;
import static values.Inputs.email;
import static values.Inputs.encryptedPassword;
import static values.Inputs.password;
import static values.Inputs.settings;
import static values.Inputs.wrongEmail;
import static values.Inputs.wrongPassword;

@RunWith(AndroidJUnit4.class)
@MediumTest
public class LoginWithMDFragmentTest extends BaseInstrumentationTest {

    private LoginTester loginTester;

    @Before
    public void setup() {
        super.setup();

        loginTester = new LoginTester();

        commonTester.launchActivity(LoginActivity.class);
    }

    @After
    public void tearDown() {
        super.tearDown();
    }

    /**
     * Input tests
     **/

    @Test
    public void checkInputsAreEmpty() {
        loginTester.goToLoginWithMeetDay();

        onView(withId(R.id.login_with_md_email_text_input)).check
                (matches(InputLayoutMatcher.withText("")));
        onView(withId(R.id.login_with_md_password_text_input)).check
                (matches(InputLayoutMatcher.withText("")));
    }

    @Test
    public void checkInputsCorrectHint() {
        loginTester.goToLoginWithMeetDay();

        onView(withId(R.id.login_with_md_email_text_input)).check
                (matches(InputLayoutMatcher.withHint(activityTestRule.getActivity().getString(R.string.input_text_layout_prompt_email))));
        onView(withId(R.id.login_with_md_password_text_input)).check
                (matches(InputLayoutMatcher.withHint(activityTestRule.getActivity().getString(R.string.input_text_layout_prompt_password))));
    }

    @Test
    public void loginWithEmptyInputs() {
        loginTester.goToLoginWithMeetDay();
        loginTester.attemptLogin();

        onView(withId(R.id.login_with_md_email_text_input)).check
                (matches(InputLayoutMatcher.withError(activityTestRule.getActivity().getString(R.string.input_text_layout_error_field_required))));
        onView(withId(R.id.login_with_md_password_text_input)).check
                (matches(InputLayoutMatcher.withError(activityTestRule.getActivity().getString(R.string.input_text_layout_error_field_required))));
    }

    @Test
    public void loginWithIncorrectEmail() {
        loginTester.goToLoginWithMeetDay();
        loginTester.writeLoginWithMDEmail(wrongEmail);
        loginTester.attemptLogin();

        onView(withId(R.id.login_with_md_email_text_input)).check
                (matches(InputLayoutMatcher.withError(activityTestRule.getActivity().getString(R.string.input_text_layout_error_email_incorrect))));
    }

    @Test
    public void loginWithShortPassword() {
        loginTester.goToLoginWithMeetDay();
        loginTester.writeLoginWithMDPassword(wrongPassword);
        loginTester.attemptLogin();

        onView(withId(R.id.login_with_md_password_text_input)).check
                (matches(InputLayoutMatcher.withError(activityTestRule.getActivity().getString(R.string.input_text_layout_error_password_short))));
    }

    @Test
    public void checkLoginForwardsCredentials() {
        loginTester.goToLoginWithMeetDay();
        loginTester.writeLoginWithMDEmail(email);
        loginTester.writeLoginWithMDPassword(password);
        commonTester.willAppearLoadingDialog(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                loginTester.attemptLogin();
                return null;
            }
        });

        verify(profileManagerMock, times(1))
                .login(eq(email),
                        eq(encryptedPassword));
    }

    @Test
    public void loginWithCorrectCredentialsForUnfinishedProfileGoToCreateAccount() {
        loginTester.goToLoginWithMeetDay();
        loginTester.writeLoginWithMDEmail(email);
        loginTester.writeLoginWithMDPassword(password);
        commonTester.willAppearLoadingDialog(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                loginTester.attemptLogin();
                return null;
            }
        });
        profileViewModelTester.settingsFromManager(null);
        profileViewModelTester.profileFromManager(fetchProfileEvent);

        onView(withId(R.id.create_account_fragment_container)).check(matches(isDisplayed()));
    }

    @Test
    public void loginWithCorrectCredentialsForFinishedProfileGoToMain() {
        loginTester.goToLoginWithMeetDay();
        loginTester.writeLoginWithMDEmail(email);
        loginTester.writeLoginWithMDPassword(password);
        commonTester.willAppearLoadingDialog(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                loginTester.attemptLogin();
                return null;
            }
        });
        profileViewModelTester.settingsFromManager(settings);
        profileViewModelTester.profileFromManager(fetchProfileEvent);

        onView(withId(R.id.main_bottom_tab_bottom_navigation)).check(matches(isDisplayed()));
    }

    @Test
    public void loginWithIncorrectCredentialsShowsError() {
        loginTester.goToLoginWithMeetDay();
        loginTester.writeLoginWithMDEmail(email);
        loginTester.writeLoginWithMDPassword(password);
        commonTester.willAppearLoadingDialog(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                loginTester.attemptLogin();
                return null;
            }
        });
        loginTester.attemptLogin();
        profileViewModelTester.profileFromManager(fetchProfileErrorEvent);

        commonTester.isToastDisplayedWithText(fetchProfileErrorEvent.meetDayError.getMessage());
    }

}
