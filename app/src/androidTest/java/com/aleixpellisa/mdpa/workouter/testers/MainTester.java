package com.aleixpellisa.mdpa.workouter.testers;

import com.aleixpellisa.mdpa.workouter.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class MainTester {

    /**
     * Actions
     */

    public void goToDiscover() {
        onView(withId(R.id.main_bottom_tab_action_discover)).perform(click());
    }

    public void goToChats() {
        onView(withId(R.id.main_bottom_tab_action_chats)).perform(click());
    }

    public void goToProfile() {
        onView(withId(R.id.main_bottom_tab_action_profile)).perform(click());
    }

    public void goToSettings() {
        onView(withId(R.id.main_bottom_tab_action_settings)).perform(click());
    }

}
