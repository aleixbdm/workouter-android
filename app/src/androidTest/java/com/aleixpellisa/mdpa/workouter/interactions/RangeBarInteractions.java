package com.aleixpellisa.mdpa.workouter.interactions;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.matcher.ViewMatchers;
import android.view.View;

import com.appyvet.materialrangebar.RangeBar;

import org.hamcrest.Matcher;

public class RangeBarInteractions {

    public static ViewAction setSeekProgress(final int progress) {
        return new ViewAction() {
            @Override
            public void perform(UiController uiController, View view) {
                ((RangeBar) view).setSeekPinByValue(progress);
            }

            @Override
            public String getDescription() {
                return "Set a progress";
            }

            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isAssignableFrom(RangeBar.class);
            }
        };
    }

    public static ViewAction setRangeProgress(final int leftProgress, final int rightProgress) {
        return new ViewAction() {
            @Override
            public void perform(UiController uiController, View view) {
                ((RangeBar) view).setRangePinsByValue(leftProgress, rightProgress);
            }

            @Override
            public String getDescription() {
                return "Set a progress";
            }

            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isAssignableFrom(RangeBar.class);
            }
        };
    }

}
