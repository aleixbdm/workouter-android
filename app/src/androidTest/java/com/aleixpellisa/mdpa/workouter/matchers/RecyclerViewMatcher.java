package com.aleixpellisa.mdpa.workouter.matchers;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class RecyclerViewMatcher {

    public static Matcher<View> isEmpty() {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                RecyclerView recyclerView = getRecyclerView(view);
                if (recyclerView == null) {
                    return true;
                }
                int count = recyclerView.getAdapter().getItemCount();
                return count == 0;
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    public static Matcher<View> hasNItems(final int numberOfItems) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                RecyclerView recyclerView = getRecyclerView(view);
                if (recyclerView == null) {
                    return true;
                }
                int count = recyclerView.getAdapter().getItemCount();
                return count == numberOfItems;
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    private static RecyclerView getRecyclerView(View view) {
        if (!(view instanceof RecyclerView)) {
            return null;
        }
        return (RecyclerView) view;
    }

}
