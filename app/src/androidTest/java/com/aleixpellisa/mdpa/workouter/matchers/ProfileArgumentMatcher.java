package com.aleixpellisa.mdpa.workouter.matchers;

import com.aleixpellisa.mdpa.workouter.database.entity.Profile;

import org.mockito.ArgumentMatcher;

public class ProfileArgumentMatcher implements ArgumentMatcher<Profile> {

    private Profile profile;

    public ProfileArgumentMatcher(Profile profile) {
        this.profile = profile;
    }

    public boolean matches(Profile profile) {
        return this.profile.getName().equals(profile.getName());
    }

    public String toString() {
        //printed in verification errors
        return "[list of 2 elements]";
    }

}
