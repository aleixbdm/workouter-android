package com.aleixpellisa.mdpa.workouter.tests.discover;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;

import com.aleixpellisa.mdpa.workouter.R;
import com.aleixpellisa.mdpa.workouter.builder.AuthBuilder;
import com.aleixpellisa.mdpa.workouter.matchers.SwipePlaceHolderViewMatcher;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.testers.DiscoverTester;
import com.aleixpellisa.mdpa.workouter.tests.BaseInstrumentationTest;
import com.aleixpellisa.mdpa.workouter.view.main.MainBottomTabActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static values.Events.fetchCandidatesErrorEvent;
import static values.Events.fetchCandidatesEvent;
import static values.Events.fetchEmptyCandidatesEvent;
import static values.Inputs.notVisible;
import static values.Inputs.visible;

@RunWith(AndroidJUnit4.class)
@MediumTest
public class DiscoverFragmentTest extends BaseInstrumentationTest {

    private Class activityClass;

    private DiscoverTester discoverTester;

    @Before
    public void setup() {
        super.setup();

        activityClass = MainBottomTabActivity.class;
        discoverTester = new DiscoverTester();

        profileViewModelTester.authFromManager(AuthBuilder.buildMockedAuth());
    }

    @After
    public void tearDown() {
        super.tearDown();
    }

    /**
     * Navigation tests
     **/

    @Test
    public void checkWhenVisibleAndDownloadCandidatesReturnsCandidatesDisplaysCandidate() {
        candidateViewModelTester.isVisibleFromManager(visible);

        commonTester.launchActivity(activityClass);

        onView(withId(R.id.discover_fragment_swipe_view)).check(matches(isDisplayed()));

        candidateViewModelTester.candidateListFromManager(fetchCandidatesEvent);

        commonTester.waitToSee(1000);

        int index = 0;
        Candidate candidate = fetchCandidatesEvent.data.get(index);
        onView(withId(R.id.discover_fragment_swipe_view)).check
                (matches(SwipePlaceHolderViewMatcher.withCandidate(candidate, index)));
        onView(withId(R.id.discover_fragment_swipe_view)).check
                (matches(SwipePlaceHolderViewMatcher.withCity(candidate.getCity(), index)));
    }

    @Test
    public void checkWhenNotVisibleNotDisplaysCandidate() {
        candidateViewModelTester.isVisibleFromManager(notVisible);

        commonTester.launchActivity(activityClass);

        onView(withId(R.id.discover_fragment_swipe_view)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    @Test
    public void checkWhenVisibleAndDownloadCandidatesFailsNotDisplaysCandidate() {
        candidateViewModelTester.isVisibleFromManager(visible);

        commonTester.launchActivity(activityClass);

        candidateViewModelTester.candidateListFromManager(fetchCandidatesErrorEvent);

        onView(withId(R.id.discover_fragment_swipe_view)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    @Test
    public void checkWhenVisibleAndDownloadCandidatesReturnsEmptyListNotDisplaysCandidate() {
        candidateViewModelTester.isVisibleFromManager(visible);

        commonTester.launchActivity(activityClass);

        onView(withId(R.id.discover_fragment_swipe_view)).check(matches(isDisplayed()));

        candidateViewModelTester.candidateListFromManager(fetchEmptyCandidatesEvent);

        commonTester.waitToSee(1000);

        onView(withId(R.id.discover_fragment_swipe_view)).check
                (matches(SwipePlaceHolderViewMatcher.isEmpty()));
    }

    @Test
    public void clickOnCandidateGoesToCandidateProfile() {
        candidateViewModelTester.isVisibleFromManager(visible);

        commonTester.launchActivity(activityClass);

        onView(withId(R.id.discover_fragment_swipe_view)).check(matches(isDisplayed()));

        candidateViewModelTester.candidateListFromManager(fetchCandidatesEvent);

        commonTester.waitToSee(1000);

        discoverTester.goToCandidateProfile();

        onView(withId(R.id.profile_fragment_user_image_pager)).check(matches(isDisplayed()));
    }

    /**
     * Like & Dislike
     **/

    @Test
    public void pressLikeButtonSendsLike() {
        candidateViewModelTester.isVisibleFromManager(visible);

        commonTester.launchActivity(activityClass);

        onView(withId(R.id.discover_fragment_swipe_view)).check(matches(isDisplayed()));

        candidateViewModelTester.candidateListFromManager(fetchCandidatesEvent);

        commonTester.waitToSee(1000);

        Candidate candidate = fetchCandidatesEvent.data.get(0);

        discoverTester.pressLikeButton();

        verify(candidateManagerMock, times(1))
                .sendEvaluation(eq(candidate.getId()),
                        eq(Evaluation.EVALUATION_LIKE));
    }

    @Test
    public void pressLikeButtonLikesTheCandidateAndDisplaysNextCandidate() {
        candidateViewModelTester.isVisibleFromManager(visible);

        commonTester.launchActivity(activityClass);

        onView(withId(R.id.discover_fragment_swipe_view)).check(matches(isDisplayed()));

        candidateViewModelTester.candidateListFromManager(fetchCandidatesEvent);

        commonTester.waitToSee(1000);

        Candidate nextCandidate = fetchCandidatesEvent.data.get(1);

        discoverTester.pressLikeButton();

        commonTester.waitToSee(1000);

        int index = 0;
        onView(withId(R.id.discover_fragment_swipe_view)).check
                (matches(SwipePlaceHolderViewMatcher.withCandidate(nextCandidate, index)));
        onView(withId(R.id.discover_fragment_swipe_view)).check
                (matches(SwipePlaceHolderViewMatcher.withCity(nextCandidate.getCity(), index)));
    }

    @Test
    public void swipeRightLikesTheCandidateAndDisplaysNextCandidate() {
        candidateViewModelTester.isVisibleFromManager(visible);

        commonTester.launchActivity(activityClass);

        onView(withId(R.id.discover_fragment_swipe_view)).check(matches(isDisplayed()));

        candidateViewModelTester.candidateListFromManager(fetchCandidatesEvent);

        commonTester.waitToSee(1000);

        Candidate nextCandidate = fetchCandidatesEvent.data.get(1);

        discoverTester.swipeRightToLike();

        commonTester.waitToSee(1000);

        int index = 0;
        onView(withId(R.id.discover_fragment_swipe_view)).check
                (matches(SwipePlaceHolderViewMatcher.withCandidate(nextCandidate, index)));
        onView(withId(R.id.discover_fragment_swipe_view)).check
                (matches(SwipePlaceHolderViewMatcher.withCity(nextCandidate.getCity(), index)));
    }

    @Test
    public void pressDislikeButtonSendsDislike() {
        candidateViewModelTester.isVisibleFromManager(visible);

        commonTester.launchActivity(activityClass);

        onView(withId(R.id.discover_fragment_swipe_view)).check(matches(isDisplayed()));

        candidateViewModelTester.candidateListFromManager(fetchCandidatesEvent);

        commonTester.waitToSee(1000);

        Candidate candidate = fetchCandidatesEvent.data.get(0);

        discoverTester.pressDislikeButton();

        verify(candidateManagerMock, times(1))
                .sendEvaluation(eq(candidate.getId()),
                        eq(Evaluation.EVALUATION_DISLIKE));
    }

    @Test
    public void pressDislikeButtonDislikesTheCandidateAndDisplaysNextCandidate() {
        candidateViewModelTester.isVisibleFromManager(visible);

        commonTester.launchActivity(activityClass);

        onView(withId(R.id.discover_fragment_swipe_view)).check(matches(isDisplayed()));

        candidateViewModelTester.candidateListFromManager(fetchCandidatesEvent);

        commonTester.waitToSee(1000);

        Candidate nextCandidate = fetchCandidatesEvent.data.get(1);

        discoverTester.pressDislikeButton();

        commonTester.waitToSee(1000);

        int index = 0;
        onView(withId(R.id.discover_fragment_swipe_view)).check
                (matches(SwipePlaceHolderViewMatcher.withCandidate(nextCandidate, index)));
        onView(withId(R.id.discover_fragment_swipe_view)).check
                (matches(SwipePlaceHolderViewMatcher.withCity(nextCandidate.getCity(), index)));
    }

    @Test
    public void swipeLeftDislikesTheCandidateAndDisplaysNextCandidate() {
        candidateViewModelTester.isVisibleFromManager(visible);

        commonTester.launchActivity(activityClass);

        onView(withId(R.id.discover_fragment_swipe_view)).check(matches(isDisplayed()));

        candidateViewModelTester.candidateListFromManager(fetchCandidatesEvent);

        commonTester.waitToSee(1000);

        Candidate nextCandidate = fetchCandidatesEvent.data.get(1);

        discoverTester.swipeLeftToDislike();

        commonTester.waitToSee(1000);

        int index = 0;
        onView(withId(R.id.discover_fragment_swipe_view)).check
                (matches(SwipePlaceHolderViewMatcher.withCandidate(nextCandidate, index)));
        onView(withId(R.id.discover_fragment_swipe_view)).check
                (matches(SwipePlaceHolderViewMatcher.withCity(nextCandidate.getCity(), index)));
    }
}
