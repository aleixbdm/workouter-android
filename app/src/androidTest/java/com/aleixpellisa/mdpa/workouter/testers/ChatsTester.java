//package com.aleixpellisa.mdpa.meetday.testers;
//
//import android.content.Intent;
//import android.support.test.espresso.contrib.RecyclerViewActions;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.builder.AuthBuilder;
//import com.aleixpellisa.mdpa.meetday.builder.ChatBuilder;
//import com.aleixpellisa.mdpa.meetday.builder.MessageBuilder;
//import com.aleixpellisa.mdpa.meetday.database.entity.Chat;
//import com.aleixpellisa.mdpa.meetday.manager.ManagerListener;
//import com.aleixpellisa.mdpa.meetday.manager.impl.ChatManagerImpl;
//import com.aleixpellisa.mdpa.meetday.model.Message;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//import com.aleixpellisa.mdpa.meetday.view.chats.ChatActivity;
//
//import org.mockito.ArgumentMatchers;
//import org.mockito.Mockito;
//import org.mockito.invocation.InvocationOnMock;
//import org.mockito.stubbing.Answer;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.action.ViewActions.click;
//import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
//import static android.support.test.espresso.action.ViewActions.typeText;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.eq;
//import static org.mockito.Mockito.doAnswer;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
//public class ChatsTester {
//
//    private BaseInstrumentationTest test;
//
//    private CommonTester commonTester;
//    private ProfileTester profileTester;
//
//    public ChatsTester(BaseInstrumentationTest test) {
//        this.test = test;
//        this.commonTester = new CommonTester(test);
//        this.profileTester = new ProfileTester(test);
//    }
//
//    /**
//     * Getters
//     */
//
//    public List<Chat> getChatList() {
//        ChatBuilder chatBuilder = new ChatBuilder(test.getApplication().getResources());
//        return chatBuilder.buildList();
//    }
//
//    public List<Message> getMessageList() {
//        MessageBuilder messageBuilder = new MessageBuilder(test.getApplication().getResources());
//        return messageBuilder.buildList();
//    }
//
//    public String getMessageToSend() {
//        return "Test";
//    }
//
//    /**
//     * Preconditions
//     */
//
//    public void whenAuthenticated() {
//        AuthBuilder authBuilder = new AuthBuilder(test.getApplication().getResources());
//        profileTester.whenRetrieveAuthentication(authBuilder.buildFromResources());
//    }
//
//    public void whenDownloadChatsSucceed(final boolean succeed) {
//        doAnswer(new Answer() {
//            @Override
//            public Object answer(InvocationOnMock invocation) throws Throwable {
//                ManagerListener callback = (ManagerListener) invocation.getArguments()[0];
//                if (succeed) {
//                    callback.onSucceed();
//                } else {
//                    callback.onError(new Error());
//                }
//                return null;
//            }
//        }).when(test.chatManagerMock).downloadChats(any(ManagerListener.class));
//    }
//
//    public void whenRetrieveChats(List<Chat> chatList) {
//        when(test.chatManagerMock.retrieveChats()).thenReturn(chatList);
//    }
//
//    public void whenRetrieveChatsFiltered() {
//        final ChatManagerImpl chatManagerImplMock = mock(ChatManagerImpl.class, Mockito.CALLS_REAL_METHODS);
//        when(test.chatManagerMock.retrieveChatsFiltered(ArgumentMatchers.<List<Chat>>any(), any(String.class)))
//                .then(new Answer() {
//                    @SuppressWarnings("unchecked")
//                    @Override
//                    public Object answer(InvocationOnMock invocation) throws Throwable {
//                        List<Chat> chatList = (List<Chat>) invocation.getArguments()[0];
//                        String filter = (String) invocation.getArguments()[1];
//                        return chatManagerImplMock
//                                .retrieveChatsFiltered(chatList, filter);
//                    }
//                });
//    }
//
//    public void whenDownloadMessagesFromChatSucceed(String chatId, final boolean succeed) {
//        doAnswer(new Answer() {
//            @Override
//            public Object answer(InvocationOnMock invocation) throws Throwable {
//                ManagerListener callback = (ManagerListener) invocation.getArguments()[1];
//                if (succeed) {
//                    callback.onSucceed();
//                } else {
//                    callback.onError(new Error());
//                }
//                return null;
//            }
//        }).when(test.chatManagerMock).downloadMessagesFromChat(eq(chatId), any(ManagerListener.class));
//    }
//
//    public void whenRetrieveMessages(List<Message> messageList) {
//        when(test.chatManagerMock.retrieveMessages()).thenReturn(messageList);
//    }
//
//    public void whenSendMessageSucceed(List<Message> previousMessageList, String chatId, String messageText, final boolean succeed) {
//        AuthBuilder authBuilder = new AuthBuilder(test.getApplication().getResources());
//        MessageBuilder messageBuilder = new MessageBuilder(authBuilder.buildFromResources().getUserId(), messageText);
//        final List<Message> messageListWithMessageAdded = new ArrayList<>();
//        messageListWithMessageAdded.addAll(previousMessageList);
//        messageListWithMessageAdded.add(messageBuilder.build());
//        doAnswer(new Answer() {
//            @Override
//            public Object answer(InvocationOnMock invocation) throws Throwable {
//                // Modify the return list
//                whenRetrieveMessages(messageListWithMessageAdded);
//                ManagerListener callback = (ManagerListener) invocation.getArguments()[2];
//                if (succeed) {
//                    callback.onSucceed();
//                } else {
//                    callback.onError(new Error());
//                }
//                return null;
//            }
//        }).when(test.chatManagerMock).sendMessageToChat(eq(chatId), eq(messageText), any(ManagerListener.class));
//    }
//
//    /**
//     * Actions
//     */
//
//    public void launchActivity() {
//        commonTester.launchActivity(ChatActivity.class);
//    }
//
//    public void launchActivityWithChat(Chat chat) {
//        Intent intent = new Intent(test.getApplication().getApplicationContext(), ChatActivity.class);
//        intent.putExtra(Chat.PARCEL_KEY, chat);
//        commonTester.launchActivityWithIntent(intent);
//    }
//
//    public void scrollChatListTo(int position) {
//        onView(withId(R.id.chats_fragment_chats_list))
//                .perform(RecyclerViewActions.scrollToPosition(position));
//    }
//
//    public void scrollMessageListTo(int position) {
//        onView(withId(R.id.chat_message_list))
//                .perform(RecyclerViewActions.scrollToPosition(position));
//    }
//
//    public void goToChat(int position) {
//        onView(withId(R.id.chats_fragment_chats_list))
//                .perform(RecyclerViewActions.actionOnItemAtPosition(position, click()));
//    }
//
//    public void writeFilter(String filter) {
//        onView(withId(R.id.chats_fragment_search_bar))
//                .perform(click());
//        onView(withId(R.id.chats_fragment_search_bar))
//                .perform(typeText(filter), closeSoftKeyboard());
//    }
//
//    public void writeMessage(String message) {
//        onView(withId(R.id.chat_enter_message_edit_text))
//                .perform(typeText(message), closeSoftKeyboard());
//    }
//
//    public void pressSendMessage() {
//        onView(withId(R.id.chat_send_message_button))
//                .perform(click());
//    }
//
//}
