package com.aleixpellisa.mdpa.workouter.matchers;

import android.view.View;
import android.widget.CheckBox;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class CheckboxMatcher {

    public static Matcher<View> withError(final String expected) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                CheckBox checkBox = getCheckBox(view);
                if (checkBox == null) {
                    return false;
                }

                String actual = checkBox.getError().toString();
                return actual.equals(expected);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    private static CheckBox getCheckBox(View view) {
        if (!(view instanceof CheckBox)) {
            return null;
        }
        return (CheckBox) view;
    }

}
