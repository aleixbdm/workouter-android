//package com.aleixpellisa.mdpa.meetday.tests.profile;
//
//import android.support.test.espresso.matcher.ViewMatchers;
//import android.support.test.filters.MediumTest;
//import android.support.test.runner.AndroidJUnit4;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.builder.AuthBuilder;
//import com.aleixpellisa.mdpa.meetday.builder.ProfileBuilder;
//import com.aleixpellisa.mdpa.meetday.database.entity.Profile;
//import com.aleixpellisa.mdpa.meetday.testers.CommonTester;
//import com.aleixpellisa.mdpa.meetday.testers.MainTester;
//import com.aleixpellisa.mdpa.meetday.testers.ProfileTester;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//import com.aleixpellisa.mdpa.meetday.view.main.MainBottomTabActivity;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.action.ViewActions.scrollTo;
//import static android.support.test.espresso.assertion.ViewAssertions.matches;
//import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
//import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static android.support.test.espresso.matcher.ViewMatchers.withText;
//
//@RunWith(AndroidJUnit4.class)
//@MediumTest
//public class ProfileFragmentTest extends BaseInstrumentationTest {
//
//    private Class activityClass;
//
//    private CommonTester commonTester;
//    private MainTester mainTester;
//    private ProfileTester profileTester;
//
//    @Before
//    public void setup() {
//        super.setup();
//
//        activityClass = MainBottomTabActivity.class;
//
//        commonTester = new CommonTester(this);
//        mainTester = new MainTester(this);
//        AuthTester authTester = new AuthTester(this);
//        profileTester = new ProfileTester(this);
//
//        authTester.whenRetrieveAuthentication(AuthBuilder.buildMockedAuth());
//    }
//
//    @After
//    public void tearDown() {
//        super.tearDown();
//    }
//
//    /**
//     * Navigation tests
//     **/
//
//    @Test
//    public void checkDownloadProfileReturnsProfileDisplaysProfile() {
//        profileTester.whenDownloadProfileSucceed(true);
//        ProfileBuilder profileBuilder = new ProfileBuilder(application.getResources());
//        Profile profile = profileBuilder.buildFromResources();
//        profileTester.whenRetrieveProfile(profile);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToProfile();
//
//        onView(withId(R.id.profile_fragment_user_image_pager)).check(matches(isDisplayed()));
//
//        onView(withId(R.id.profile_fragment_city_text_view)).check
//                (matches(withText(profile.getSettings().getCity())));
//    }
//
//    @Test
//    public void checkDownloadProfileFailsNotDisplaysProfile() {
//        profileTester.whenDownloadProfileSucceed(false);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToProfile();
//
//        onView(withId(R.id.profile_fragment_city_text_view)).check
//                (matches(withText("")));
//    }
//
//    @Test
//    public void goToEditProfileDisplaysEditProfile() {
//        profileTester.whenDownloadProfileSucceed(true);
//        ProfileBuilder profileBuilder = new ProfileBuilder(application.getResources());
//        Profile profile = profileBuilder.buildFromResources();
//        profileTester.whenRetrieveProfile(profile);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToProfile();
//
//        profileTester.goToEditProfile();
//
//        onView(withId(R.id.edit_profile_save_button)).check(matches(isDisplayed()));
//    }
//
//    /**
//     * Basic profile
//     **/
//
//    @Test
//    public void checkDownloadProfileReturnsBasicProfileDisplaysBasicMessage() {
//        profileTester.whenDownloadProfileSucceed(true);
//        profileTester.whenRetrieveProfile(ProfileBuilder.buildBasicProfile());
//        profileTester.whenProfileIsBasic(true);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToProfile();
//
//        onView(withId(R.id.profile_fragment_basic_message_layout))
//                .perform(scrollTo())
//                .check(matches(isDisplayed()));
//    }
//
//    @Test
//    public void checkDownloadProfileReturnsNotBasicProfileNotDisplaysBasicMessage() {
//        profileTester.whenDownloadProfileSucceed(true);
//        profileTester.whenRetrieveProfile(ProfileBuilder.buildBasicProfile());
//        profileTester.whenProfileIsBasic(false);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToProfile();
//
//        onView(withId(R.id.profile_fragment_basic_message_layout))
//                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
//    }
//
//}
