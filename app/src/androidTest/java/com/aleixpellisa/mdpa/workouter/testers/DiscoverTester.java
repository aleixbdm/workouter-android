package com.aleixpellisa.mdpa.workouter.testers;

import android.support.test.espresso.matcher.ViewMatchers;

import com.aleixpellisa.mdpa.workouter.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.allOf;

public class DiscoverTester {

    /**
     * Actions
     */

    public void pressDislikeButton() {
        onView(allOf(withId(R.id.discover_fragment_dislike_button),
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
                .perform(click());
    }

    public void swipeLeftToDislike() {
        onView(withId(R.id.discover_fragment_swipe_view)).perform(swipeLeft());
    }

    public void pressLikeButton() {
        onView(allOf(withId(R.id.discover_fragment_like_button),
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
                .perform(click());
    }

    public void swipeRightToLike() {
        onView(withId(R.id.discover_fragment_swipe_view)).perform(swipeRight());
    }

    public void goToCandidateProfile() {
        onView(withId(R.id.discover_fragment_swipe_view)).perform(click());
    }

}
