package com.aleixpellisa.mdpa.workouter.matchers;

import android.view.View;
import android.widget.TextView;

import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.view.discover.CandidateCard;
import com.mindorks.placeholderview.SwipePlaceHolderView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.List;

public class SwipePlaceHolderViewMatcher {

    public static Matcher<View> isEmpty() {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                CandidateCard candidateCard = getCandidateCard(view, 0);
                return candidateCard == null;
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    public static Matcher<View> withCandidate(final Candidate candidate, final int index) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                CandidateCard candidateCard = getCandidateCard(view, index);
                if (candidateCard == null) {
                    return false;
                }

                Candidate candidateCardCandidate = candidateCard.getCandidate();
                return candidateCardCandidate.getId().equals(candidate.getId());
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    public static Matcher<View> withCity(final String expected, final int index) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                CandidateCard candidateCard = getCandidateCard(view, index);
                if (candidateCard == null) {
                    return false;
                }

                TextView cityTextView = candidateCard.getCityTextView();
                return cityTextView.getText().equals(expected);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    private static CandidateCard getCandidateCard(View view, int index) {
        if (!(view instanceof SwipePlaceHolderView)) {
            return null;
        }
        SwipePlaceHolderView swipePlaceHolderView = (SwipePlaceHolderView) view;
        List<Object> resolvers = swipePlaceHolderView.getAllResolvers();
        if (resolvers.isEmpty()) {
            return null;
        }
        Object resolver = resolvers.get(index);
        if (!(resolver instanceof CandidateCard)) {
            return null;
        }
        return (CandidateCard) resolver;
    }

}
