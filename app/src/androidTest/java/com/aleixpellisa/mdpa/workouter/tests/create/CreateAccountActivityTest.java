//package com.aleixpellisa.mdpa.meetday.tests.create;
//
//import android.support.test.filters.MediumTest;
//import android.support.test.runner.AndroidJUnit4;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.testers.CreateTester;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//import com.aleixpellisa.mdpa.meetday.view.create.CreateAccountInitialFragment;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.assertion.ViewAssertions.matches;
//import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static org.hamcrest.Matchers.instanceOf;
//import static org.junit.Assert.assertThat;
//
//@RunWith(AndroidJUnit4.class)
//@MediumTest
//public class CreateAccountActivityTest extends BaseInstrumentationTest {
//
//    private CreateTester tester;
//
//    @Before
//    public void setup() {
//        super.setup();
//
//        tester = new CreateTester(this);
//        tester.launchCreateAccountActivity();
//    }
//
//    @After
//    public void tearDown() {
//        super.tearDown();
//    }
//
//    /**
//     * Navigation tests
//     **/
//
//    @Test
//    public void checkCreateAccountIsDisplayed() {
//        onView(withId(R.id.create_account_fragment_container)).check(matches(isDisplayed()));
//    }
//
//    @Test
//    public void checkCreateAccountInitialFragmentIsDisplayed() {
//        assertThat(tester.getCurrentFragment(R.id.create_account_fragment_container),
//                instanceOf(CreateAccountInitialFragment.class));
//    }
//
//}
