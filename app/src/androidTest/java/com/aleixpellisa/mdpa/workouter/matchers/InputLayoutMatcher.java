package com.aleixpellisa.mdpa.workouter.matchers;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class InputLayoutMatcher {

    public static Matcher<View> withText(final String expected) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                EditText editText = getEditText(view);
                if (editText == null) {
                    return false;
                }

                String actual = editText.getText().toString();
                return actual.equals(expected);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    public static Matcher<View> withHint(final String expected) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                TextInputLayout textInputLayout = getTextInputLayout(view);
                if (textInputLayout == null) {
                    return false;
                }
                CharSequence charSequence = textInputLayout.getHint();
                if (charSequence == null) {
                    return false;
                }

                String actual = charSequence.toString();
                return actual.equals(expected);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    public static Matcher<View> withError(final String expected) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                EditText editText = getEditText(view);
                if (editText == null) {
                    return false;
                }

                String actual = editText.getError().toString();
                return actual.equals(expected);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    private static TextInputLayout getTextInputLayout(View view) {
        if (!(view instanceof TextInputLayout)) {
            return null;
        }
        return (TextInputLayout) view;
    }

    private static EditText getEditText(View view) {
        TextInputLayout textInputLayout = getTextInputLayout(view);
        if (textInputLayout == null) {
            return null;
        }
        return textInputLayout.getEditText();
    }

}
