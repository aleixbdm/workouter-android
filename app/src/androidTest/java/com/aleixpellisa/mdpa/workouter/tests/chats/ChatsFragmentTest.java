//package com.aleixpellisa.mdpa.meetday.tests.chats;
//
//import android.support.test.filters.MediumTest;
//import android.support.test.runner.AndroidJUnit4;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.builder.AuthBuilder;
//import com.aleixpellisa.mdpa.meetday.builder.ChatBuilder;
//import com.aleixpellisa.mdpa.meetday.matchers.RecyclerViewMatcher;
//import com.aleixpellisa.mdpa.meetday.testers.ChatsTester;
//import com.aleixpellisa.mdpa.meetday.testers.CommonTester;
//import com.aleixpellisa.mdpa.meetday.testers.MainTester;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//import com.aleixpellisa.mdpa.meetday.view.main.MainBottomTabActivity;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import java.util.List;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.assertion.ViewAssertions.matches;
//import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static android.support.test.espresso.matcher.ViewMatchers.withText;
//
//@RunWith(AndroidJUnit4.class)
//@MediumTest
//public class ChatsFragmentTest extends BaseInstrumentationTest {
//
//    private Class activityClass;
//
//    private CommonTester commonTester;
//    private MainTester mainTester;
//    private ChatsTester chatsTester;
//
//    @Before
//    public void setup() {
//        super.setup();
//
//        activityClass = MainBottomTabActivity.class;
//
//        commonTester = new CommonTester(this);
//        mainTester = new MainTester(this);
//        AuthTester authTester = new AuthTester(this);
//        chatsTester = new ChatsTester(this);
//
//        authTester.whenRetrieveAuthentication(AuthBuilder.buildMockedAuth());
//    }
//
//    @After
//    public void tearDown() {
//        super.tearDown();
//    }
//
//    /**
//     * Navigation tests
//     **/
//
//    @Test
//    public void checkDownloadChatsReturnsChatsDisplaysChatList() {
//        chatsTester.whenDownloadChatsSucceed(true);
//        ChatBuilder chatBuilder = new ChatBuilder(application.getResources());
//        List<Chat> chatList = chatBuilder.buildList();
//        chatsTester.whenRetrieveChats(chatList);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToChats();
//
//        onView(withId(R.id.chats_fragment_chats_list)).check(matches(isDisplayed()));
//
//        int index = chatList.size() - 1;
//        Chat chat = chatList.get(index);
//        chatsTester.scrollChatListTo(index);
//        onView(withText(chat.getUserName())).check(matches(isDisplayed()));
//        onView(withId(R.id.chats_fragment_chats_list)).check
//                (matches(RecyclerViewMatcher.hasNItems(chatList.size())));
//    }
//
//    @Test
//    public void checkDownloadChatsFailsDisplaysEmptyChatList() {
//        chatsTester.whenDownloadChatsSucceed(false);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToChats();
//
//        onView(withId(R.id.chats_fragment_chats_list)).check
//                (matches(RecyclerViewMatcher.isEmpty()));
//    }
//
//    @Test
//    public void checkDownloadChatsReturnsEmptyListDisplaysEmptyChatList() {
//        chatsTester.whenDownloadChatsSucceed(true);
//        chatsTester.whenRetrieveChats(ChatBuilder.buildEmptyChatList());
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToChats();
//
//        onView(withId(R.id.chats_fragment_chats_list)).check
//                (matches(RecyclerViewMatcher.isEmpty()));
//    }
//
//    @Test
//    public void clickOnChatItemGoesToChat() {
//        chatsTester.whenDownloadChatsSucceed(true);
//        ChatBuilder chatBuilder = new ChatBuilder(application.getResources());
//        List<Chat> chatList = chatBuilder.buildList();
//        chatsTester.whenRetrieveChats(chatList);
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToChats();
//
//        int index = 0;
//        chatsTester.goToChat(index);
//
//        onView(withId(R.id.chat_message_list)).check(matches(isDisplayed()));
//    }
//
//    /**
//     * Filter chats
//     **/
//
//    @Test
//    public void applyExistingChatNameAsFilterDisplaysFilteredChatList() {
//        chatsTester.whenDownloadChatsSucceed(true);
//        ChatBuilder chatBuilder = new ChatBuilder(application.getResources());
//        List<Chat> chatList = chatBuilder.buildList();
//        chatsTester.whenRetrieveChats(chatList);
//        int index = 0;
//        Chat chat = chatList.get(index);
//        String filter = chat.getUserName();
//        chatsTester.whenRetrieveChatsFiltered();
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToChats();
//
//        chatsTester.writeFilter(filter);
//
//        chatsTester.scrollChatListTo(index);
//        onView(withId(R.id.chats_fragment_chats_list)).check
//                (matches(RecyclerViewMatcher.hasNItems(1)));
//    }
//
//    @Test
//    public void applyNotExistingChatNameAsFilterDisplaysEmptyChatList() {
//        chatsTester.whenDownloadChatsSucceed(true);
//        ChatBuilder chatBuilder = new ChatBuilder(application.getResources());
//        List<Chat> chatList = chatBuilder.buildList();
//        chatsTester.whenRetrieveChats(chatList);
//        int index = 0;
//        Chat chat = chatList.get(index);
//        String filter = chat.getUserName() + "z";
//        chatsTester.whenRetrieveChatsFiltered();
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToChats();
//
//        chatsTester.writeFilter(filter);
//
//        onView(withId(R.id.chats_fragment_chats_list)).check
//                (matches(RecyclerViewMatcher.isEmpty()));
//    }
//
//    @Test
//    public void applyEmptyFilterDisplaysNotFilteredChatList() {
//        chatsTester.whenDownloadChatsSucceed(true);
//        ChatBuilder chatBuilder = new ChatBuilder(application.getResources());
//        List<Chat> chatList = chatBuilder.buildList();
//        chatsTester.whenRetrieveChats(chatList);
//        int index = chatList.size() - 1;
//        Chat chat = chatList.get(index);
//        String filter = "";
//        chatsTester.whenRetrieveChatsFiltered();
//
//        commonTester.launchActivity(activityClass);
//        mainTester.goToChats();
//
//        chatsTester.writeFilter(filter);
//
//        chatsTester.scrollChatListTo(index);
//        onView(withText(chat.getUserName())).check(matches(isDisplayed()));
//        onView(withId(R.id.chats_fragment_chats_list)).check
//                (matches(RecyclerViewMatcher.hasNItems(chatList.size())));
//    }
//
//}
