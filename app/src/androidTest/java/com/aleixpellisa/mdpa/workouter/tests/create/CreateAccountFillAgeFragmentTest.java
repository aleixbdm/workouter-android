//package com.aleixpellisa.mdpa.meetday.tests.create;
//
//import android.support.test.filters.MediumTest;
//import android.support.test.runner.AndroidJUnit4;
//
//import com.aleixpellisa.mdpa.meetday.R;
//import com.aleixpellisa.mdpa.meetday.manager.ManagerListener;
//import com.aleixpellisa.mdpa.meetday.matchers.ProfileArgumentMatcher;
//import com.aleixpellisa.mdpa.meetday.database.entity.Profile;
//import com.aleixpellisa.mdpa.meetday.testers.CreateTester;
//import com.aleixpellisa.mdpa.meetday.tests.BaseInstrumentationTest;
//import com.aleixpellisa.mdpa.meetday.view.create.CreateAccountFillAgeFragment;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.assertion.ViewAssertions.matches;
//import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static org.hamcrest.Matchers.instanceOf;
//import static org.junit.Assert.assertThat;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.argThat;
//import static org.mockito.Mockito.verify;
//import static org.mockito.internal.verification.VerificationModeFactory.times;
//
//@RunWith(AndroidJUnit4.class)
//@MediumTest
//public class CreateAccountFillAgeFragmentTest extends BaseInstrumentationTest {
//
//    private CreateTester tester;
//
//    @Before
//    public void setup() {
//        super.setup();
//
//        tester = new CreateTester(this);
//        tester.launchCreateAccountActivity();
//    }
//
//    @After
//    public void tearDown() {
//        super.tearDown();
//    }
//
//    /**
//     * Input tests
//     **/
//
//    @Test
//    public void pressLeftButtonAttemptCreateAccount() {
//        tester.whenAuthenticated();
//        String name = "createName";
//        Profile profile = tester.getProfile();
//        profile.setName(name);
//        tester.whenInitialFragmentDone(name, "createFirstName");
//
//        tester.pressLeftButton();
//
//        verify(profileManagerMock, times(1))
//                .updateProfile(argThat(new ProfileArgumentMatcher(profile)), any(ManagerListener.class));
//    }
//
//    @Test
//    public void pressLeftButtonAttemptCreateAccountSucceedGoToDiscover() {
//        tester.whenAuthenticated();
//        String name = "createName";
//        Profile profile = tester.getProfile();
//        profile.setName(name);
//        tester.whenInitialFragmentDone(name, "createFirstName");
//        tester.whenUpdateProfileSucceed(true);
//        tester.whenRetrieveProfile(profile);
//
//        tester.pressLeftButton();
//
//        onView(withId(R.id.main_bottom_tab_fragment_container)).check(matches(isDisplayed()));
//    }
//
//    @Test
//    public void pressLeftButtonAttemptCreateAccountFailsNotGoToDiscover() {
//        tester.whenAuthenticated();
//        String name = "createName";
//        Profile profile = tester.getProfile();
//        profile.setName(name);
//        tester.whenInitialFragmentDone(name, "createFirstName");
//        tester.whenUpdateProfileSucceed(false);
//        tester.whenRetrieveProfile(profile);
//
//        tester.pressLeftButton();
//
//        assertThat(tester.getCurrentFragment(R.id.create_account_fragment_container),
//                instanceOf(CreateAccountFillAgeFragment.class));
//    }
//
//}
