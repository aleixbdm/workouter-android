package com.aleixpellisa.mdpa.workouter.viewmodel;

import android.app.Application;

import com.aleixpellisa.mdpa.workouter.BaseUnitTest;
import com.aleixpellisa.mdpa.workouter.manager.CandidateManager;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;
import com.aleixpellisa.mdpa.workouter.view.model.CandidateViewModel;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import javax.inject.Inject;

import testers.viewmodel.CandidateViewModelUnitTester;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static values.Events.fetchCandidatesErrorEvent;
import static values.Events.fetchCandidatesEvent;
import static values.Events.sendEvaluationErrorEvent;
import static values.Events.sendEvaluationEvent;
import static values.Inputs.candidateId;
import static values.Inputs.evaluation;
import static values.Inputs.notVisible;
import static values.Inputs.visible;

@RunWith(MockitoJUnitRunner.class)
public class CandidateViewModelTest extends BaseUnitTest {

    @Mock
    @SuppressWarnings("unused")
    private EventLiveData<Event<List<Candidate>>> candidateListLiveDataMock;

    @Mock
    @SuppressWarnings("unused")
    private EventLiveData<Event<Evaluation>> evaluationLiveDataMock;

    @Mock
    @SuppressWarnings("unused")
    private Application testApplication;

    @Mock
    @SuppressWarnings("unused")
    private ProfileManager profileManagerMock;

    @Mock
    private CandidateManager candidateManagerMock;

    @Inject
    CandidateViewModel candidateViewModel;

    @Inject
    CandidateViewModelUnitTester tester;

    @Before
    public void setup() {
        super.setup();
    }

    /**
     * Get candidate list
     */

    @Test
    public void checkReceiveCandidateListWhenReturnedFromManager() {
        tester.candidateListFromManager(fetchCandidatesEvent);

        Event<List<Candidate>> event = candidateViewModel.getCandidateList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertTrue(!event.data.isEmpty());
        Assert.assertEquals(event.data, fetchCandidatesEvent.data);
    }

    @Test
    public void checkNotReceiveCandidateListWhenErrorReturnedFromManager() {
        tester.candidateListFromManager(fetchCandidatesErrorEvent);

        Event<List<Candidate>> event = candidateViewModel.getCandidateList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchCandidatesErrorEvent.meetDayError.getMessage());
    }

    /**
     * Get evaluation
     */

    @Test
    public void checkReceiveEvaluationWhenReturnedFromManager() {
        tester.evaluationFromManager(sendEvaluationEvent);

        Event<Evaluation> event = candidateViewModel.getEvaluation().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data, sendEvaluationEvent.data);
    }

    @Test
    public void checkNotReceiveEvaluationWhenErrorReturnedFromManager() {
        tester.evaluationFromManager(sendEvaluationErrorEvent);

        Event<Evaluation> event = candidateViewModel.getEvaluation().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), sendEvaluationErrorEvent.meetDayError.getMessage());
    }

    /**
     * Attempt download candidates
     */

    @Test(expected = IllegalAccessException.class)
    public void checkAttemptDownloadCandidatesUserIsVisible() throws Exception {
        tester.isVisibleFromManager(notVisible);
        candidateViewModel.attemptDownloadCandidates();
    }

    @Test
    public void checkAttemptDownloadCandidatesIsForwardedProperly() throws Exception {
        tester.isVisibleFromManager(visible);
        candidateViewModel.attemptDownloadCandidates();
        verify(candidateManagerMock, times(1))
                .fetchCandidates();
    }

    /**
     * Send evaluation
     */

    @Test
    public void checkSendEvaluationIsForwardedProperly() throws Exception {
        candidateViewModel.sendEvaluation(candidateId, evaluation);
        verify(candidateManagerMock, times(1))
                .sendEvaluation(
                        eq(candidateId),
                        eq(evaluation));
    }

    /**
     * Clear all
     */

    @Test
    public void checkClearAllIsForwardedProperly() throws Exception {
        candidateViewModel.clearAll();

        verify(candidateManagerMock, times(1))
                .clearAll();
    }

}
