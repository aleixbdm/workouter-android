package com.aleixpellisa.mdpa.workouter.manager;

import android.arch.lifecycle.LiveData;

import com.aleixpellisa.mdpa.workouter.BaseUnitTest;
import com.aleixpellisa.mdpa.workouter.database.MeetDayDatabase;
import com.aleixpellisa.mdpa.workouter.database.dao.ProfileDao;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.manager.impl.ProfileManagerImpl;
import com.aleixpellisa.mdpa.workouter.manager.refresher.TokenRefresher;
import com.aleixpellisa.mdpa.workouter.network.interceptor.AuthTokenInterceptor;
import com.aleixpellisa.mdpa.workouter.network.rest.AuthRest;
import com.aleixpellisa.mdpa.workouter.network.rest.ProfileRest;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.inject.Inject;

import testers.manager.ProfileManagerTester;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static values.Events.createAccountErrorEvent;
import static values.Events.createAccountEvent;
import static values.Events.createProfileErrorEvent;
import static values.Events.createProfileEvent;
import static values.Events.deleteProfileErrorEvent;
import static values.Events.deleteProfileEvent;
import static values.Events.facebookLoginErrorEvent;
import static values.Events.facebookLoginEvent;
import static values.Events.fetchCandidateProfileErrorEvent;
import static values.Events.fetchCandidateProfileEvent;
import static values.Events.fetchProfileErrorEvent;
import static values.Events.fetchProfileEvent;
import static values.Events.loginErrorEvent;
import static values.Events.loginEvent;
import static values.Events.updateProfileErrorEvent;
import static values.Events.updateProfileEvent;
import static values.Events.updateSettingsErrorEvent;
import static values.Events.updateSettingsEvent;
import static values.Inputs.ageUpdated;
import static values.Inputs.auth;
import static values.Inputs.basicProfile;
import static values.Inputs.candidateId;
import static values.Inputs.cityUpdated;
import static values.Inputs.email;
import static values.Inputs.facebookAccessToken;
import static values.Inputs.firstNameUpdated;
import static values.Inputs.genderUpdated;
import static values.Inputs.imagesUpdated;
import static values.Inputs.nameUpdated;
import static values.Inputs.password;
import static values.Inputs.preferenceUpdated;
import static values.Inputs.profile;
import static values.Inputs.settings;
import static values.Inputs.updateSettingsKey;
import static values.Inputs.updateSettingsValue;

public class ProfileManagerImplTest extends BaseUnitTest {

    @Mock
    private PreferencesManager preferencesManagerMock;

    @Mock
    private MeetDayDatabase meetDayDatabaseMock;

    @Mock
    @SuppressWarnings("unused")
    private AuthTokenInterceptor authTokenInterceptorMock;

    @Mock
    @SuppressWarnings("unused")
    private AuthRest authRestMock;

    @Mock
    @SuppressWarnings("unused")
    private ProfileRest profileRestMock;

    @Mock
    private ProfileDao profileDaoMock;

    @Mock
    @SuppressWarnings("unused")
    private TokenRefresher tokenRefresherMock;

    @Mock
    private LiveData<Profile> profileLiveDataMock;

    @Inject
    public ProfileManagerImpl manager;

    @Inject
    public ProfileManagerTester tester;

    @Before
    public void setup() {
        when(profileDaoMock.getProfile()).thenReturn(profileLiveDataMock);
        when(meetDayDatabaseMock.getProfileDao()).thenReturn(profileDaoMock);

        super.setup();

        manager.startObservers();
    }

    /**
     * Get Auth
     */

    @Test
    public void checkReceiveAuthWhenExistsInPreferences() {
        tester.authInPreferences(auth);

        Assert.assertEquals(manager.getAuth(), auth);
    }

    @Test
    public void checkNotReceiveAuthWhenNotExistsInPreferences() {
        tester.authInPreferences(null);

        Assert.assertNull(manager.getAuth());
    }

    /**
     * Get Settings
     */

    @Test
    public void checkReceiveSettingsWhenExistsInPreferences() {
        tester.settingsInPreferences(settings);

        Assert.assertEquals(manager.getSettings(), settings);
    }

    @Test
    public void checkNotReceiveSettingsWhenNotExistsInPreferences() {
        tester.settingsInPreferences(null);

        Assert.assertNull(manager.getSettings());
    }

    /**
     * Get Profile
     */

    @Test
    public void checkReceiveProfileWhenExistsInDatabase() {
        tester.profileInDatabase(profile);

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data, profile);
    }

    @Test
    public void checkNotReceiveProfileWhenNotExistsInDatabase() {
        tester.profileInDatabase(null);

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
    }

    /**
     * Login
     */

    @Test
    public void checkReceiveAuthWhenLoginSucceed() {
        tester.loginReturnsAuth(loginEvent);

        manager.login(email, password);

        verify(preferencesManagerMock, times(1)).saveAuth(loginEvent.data);
        Assert.assertEquals(manager.getAuth(), loginEvent.data);
    }

    @Test
    public void checkReceiveErrorWhenLoginFails() {
        tester.loginReturnsAuth(loginErrorEvent);

        manager.login(email, password);

        Assert.assertNull(manager.getAuth());

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), loginErrorEvent.meetDayError.getMessage());
    }

    /**
     * Facebook Login
     */

    @Test
    public void checkReceiveAuthWhenFacebookLoginSucceed() {
        tester.facebookLoginReturnsAuth(facebookLoginEvent);

        manager.facebookLogin(facebookAccessToken);

        verify(preferencesManagerMock, times(1)).saveAuth(facebookLoginEvent.data);
        Assert.assertEquals(manager.getAuth(), facebookLoginEvent.data);
    }

    @Test
    public void checkReceiveErrorWhenFacebookLoginFails() {
        tester.facebookLoginReturnsAuth(facebookLoginErrorEvent);

        manager.facebookLogin(facebookAccessToken);

        Assert.assertNull(manager.getAuth());

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), facebookLoginErrorEvent.meetDayError.getMessage());
    }

    /**
     * Create account
     */

    @Test
    public void checkReceiveAuthWhenCreateAccountSucceed() {
        tester.createAccountReturnsAuth(createAccountEvent);

        manager.createAccount(email, password);

        verify(preferencesManagerMock, times(1)).saveAuth(createAccountEvent.data);
        Assert.assertEquals(manager.getAuth(), createAccountEvent.data);
    }

    @Test
    public void checkReceiveErrorWhenSendCreateAccountCredentialsFails() {
        tester.createAccountReturnsAuth(createAccountErrorEvent);

        manager.createAccount(email, password);

        Assert.assertNull(manager.getAuth());

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), createAccountErrorEvent.meetDayError.getMessage());
    }

    /**
     * Fetch Profile
     */

    @Test
    public void checkReceiveProfileWhenFetchProfileSucceed() {
        tester.fetchProfileReturnsProfile(fetchProfileEvent);

        manager.fetchProfile();

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data, fetchProfileEvent.data);
    }

    @Test
    public void checkReceiveErrorWhenFetchProfileFails() {
        tester.fetchProfileReturnsProfile(fetchProfileErrorEvent);

        manager.fetchProfile();

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchProfileErrorEvent.meetDayError.getMessage());
    }

    /**
     * Is profile basic
     */

    @Test
    public void checkProfileIsBasicWhenHasNoSkills() {
        Assert.assertTrue(manager.isProfileBasic(basicProfile));
    }

    @Test
    public void checkProfileIsBasicWhenHasSkills() {
        Assert.assertFalse(manager.isProfileBasic(profile));
    }

    /**
     * Create profile
     */

    @Test
    public void checkReceiveProfileCreatedWhenCreateProfileSucceed() {
        tester.createProfileReturnsProfileCreated(nameUpdated, firstNameUpdated, imagesUpdated, ageUpdated,
                genderUpdated, preferenceUpdated, cityUpdated, createProfileEvent);

        manager.createProfile(nameUpdated, firstNameUpdated, imagesUpdated, ageUpdated,
                genderUpdated, preferenceUpdated, cityUpdated);

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data, createProfileEvent.data);
    }

    @Test
    public void checkReceiveErrorWhenCreateProfileFails() {
        tester.createProfileReturnsProfileCreated(nameUpdated, firstNameUpdated, imagesUpdated, ageUpdated,
                genderUpdated, preferenceUpdated, cityUpdated, createProfileErrorEvent);

        manager.createProfile(nameUpdated, firstNameUpdated, imagesUpdated, ageUpdated,
                genderUpdated, preferenceUpdated, cityUpdated);

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), createProfileErrorEvent.meetDayError.getMessage());
    }

    /**
     * Update profile
     */

    @Test
    public void checkReceiveProfileUpdatedWhenUpdateProfileSucceed() {
        tester.updateProfileReturnsProfileUpdated(updateProfileEvent);

        manager.updateProfile(updateProfileEvent.data);

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data, updateProfileEvent.data);
    }

    @Test
    public void checkReceiveErrorWhenUpdateProfileFails() {
        tester.updateProfileReturnsProfileUpdated(updateProfileErrorEvent);

        manager.updateProfile(updateProfileErrorEvent.data);

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), updateProfileErrorEvent.meetDayError.getMessage());
    }

    /**
     * Update settings
     */

    @Test
    public void checkReceiveProfileUpdatedWhenUpdateSettingsSucceed() {
        tester.updateSettingsReturnsProfileUpdated(updateSettingsKey, updateSettingsValue, updateSettingsEvent);

        manager.updateSettings(updateSettingsKey, updateSettingsValue);

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data, updateSettingsEvent.data);
    }

    @Test
    public void checkReceiveErrorWhenUpdateSettingsFails() {
        tester.updateSettingsReturnsProfileUpdated(updateSettingsKey, updateSettingsValue, updateSettingsErrorEvent);

        manager.updateSettings(updateSettingsKey, updateSettingsValue);

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), updateSettingsErrorEvent.meetDayError.getMessage());
    }

    /**
     * Delete Profile
     */

    @Test
    public void checkReceiveProfileWhenDeleteProfileSucceed() {
        tester.deleteProfileReturnsProfileDeleted(deleteProfileEvent);

        manager.deleteProfile();

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data, deleteProfileEvent.data);
    }

    @Test
    public void checkReceiveErrorWhenDeleteProfileFails() {
        tester.deleteProfileReturnsProfileDeleted(deleteProfileErrorEvent);

        manager.deleteProfile();

        Event<Profile> event = manager.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), deleteProfileErrorEvent.meetDayError.getMessage());
    }

    /**
     * Fetch Candidate Profile
     */

    @Test
    public void checkReceiveCandidateProfileWhenFetchCandidateProfileSucceed() {
        tester.fetchCandidateProfileReturnsCandidateProfile(candidateId, fetchCandidateProfileEvent);

        manager.fetchCandidateProfile(candidateId);

        Event<Profile> event = manager.getCandidateProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getId(), candidateId);
        Assert.assertEquals(event.data, fetchCandidateProfileEvent.data);
    }

    @Test
    public void checkReceiveErrorWhenFetchCandidateProfileFails() {
        tester.fetchCandidateProfileReturnsCandidateProfile(candidateId, fetchCandidateProfileErrorEvent);

        manager.fetchCandidateProfile(candidateId);

        Event<Profile> event = manager.getCandidateProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchCandidateProfileErrorEvent.meetDayError.getMessage());
    }

}
