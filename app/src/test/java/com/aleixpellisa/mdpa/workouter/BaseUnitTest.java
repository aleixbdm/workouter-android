package com.aleixpellisa.mdpa.workouter;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import org.junit.Rule;
import org.junit.rules.TestRule;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import toothpick.testing.ToothPickRule;

public class BaseUnitTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public TestRule testRule = new InstantTaskExecutorRule();

    @Rule
    public ToothPickRule toothPickRule = new ToothPickRule(this, "BaseUnitTest");

    protected void setup() {
        toothPickRule.inject(this);
    }
}
