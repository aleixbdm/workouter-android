package com.aleixpellisa.mdpa.workouter.network;

import android.arch.lifecycle.Observer;

import com.aleixpellisa.mdpa.workouter.BaseUnitTest;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.network.rest.impl.ChatRestRetrofit;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import testers.network.ChatRestTester;

import static values.Events.fetchChatMessagesErrorEvent;
import static values.Events.fetchChatMessagesEvent;
import static values.Events.fetchChatsErrorEvent;
import static values.Events.fetchChatsEvent;
import static values.Events.sendMessageErrorEvent;
import static values.Events.sendMessageEvent;
import static values.MockClient.retrofitClientMock;

public class ChatRestRetrofitTest extends BaseUnitTest {

    @Mock
    private Observer<Event<List<Chat>>> chatsResponseObserverMock;

    @Mock
    private Observer<Event<List<Message>>> chatMessagesResponseObserverMock;

    @Mock
    private Observer<Event<Message>> messageResponseObserverMock;

    private ChatRestRetrofit retrofit;

    private ChatRestTester tester;

    @Before
    public void setup() {
        super.setup();

        retrofit = new ChatRestRetrofit(retrofitClientMock);
        tester = new ChatRestTester(retrofit);
    }

    /**
     * Fetch Chats
     */

    @Test
    public void checkReceiveChatsWhenFetchChatsSucceed() {
        retrofit.fetchChats(chatsResponseObserverMock);

        tester.fetchChatsReturnsChats(fetchChatsEvent);

        Event<List<Chat>> event = tester.captureEventFromResponseObserver(chatsResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertTrue(!event.data.isEmpty());
        Assert.assertEquals(event.data.get(0).getId(), fetchChatsEvent.data.get(0).getId());
    }

    @Test
    public void checkReceiveErrorWhenFetchChatsFails() {
        retrofit.fetchChats(chatsResponseObserverMock);

        tester.fetchChatsReturnsChats(fetchChatsErrorEvent);

        Event<List<Chat>> event = tester.captureEventFromResponseObserver(chatsResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchChatsErrorEvent.meetDayError.getMessage());
    }

    /**
     * Fetch Chat Messages
     */

    private static final String chatId = fetchChatMessagesEvent.data.get(0).getId();

    @Test
    public void checkReceiveChatMessagesWhenFetchChatMessagesSucceed() {
        retrofit.fetchChatMessages(chatId, chatMessagesResponseObserverMock);

        tester.fetchChatMessagesReturnsChatMessages(fetchChatMessagesEvent);

        Event<List<Message>> event = tester.captureEventFromResponseObserver(chatMessagesResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertTrue(!event.data.isEmpty());
        Assert.assertEquals(event.data.get(0).getId(), fetchChatMessagesEvent.data.get(0).getId());
    }

    @Test
    public void checkReceiveErrorWhenFetchChatMessagesFails() {
        retrofit.fetchChatMessages(chatId, chatMessagesResponseObserverMock);

        tester.fetchChatMessagesReturnsChatMessages(fetchChatMessagesErrorEvent);

        Event<List<Message>> event = tester.captureEventFromResponseObserver(chatMessagesResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchChatMessagesErrorEvent.meetDayError.getMessage());
    }

    /**
     * Send Message
     */

    private static final String messageText = "Message text";

    @Test
    public void checkReceiveChatMessageWhenSendMessageSucceed() {
        retrofit.sendMessage(chatId, messageText, messageResponseObserverMock);

        tester.sendMessageReturnsMessage(sendMessageEvent);

        Event<Message> event = tester.captureEventFromResponseObserver(messageResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getId(), sendMessageEvent.data.getId());
    }

    @Test
    public void checkReceiveErrorWhenSendMessageFails() {
        retrofit.sendMessage(chatId, messageText, messageResponseObserverMock);

        tester.sendMessageReturnsMessage(sendMessageErrorEvent);

        Event<Message> event = tester.captureEventFromResponseObserver(messageResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), sendMessageErrorEvent.meetDayError.getMessage());
    }

}
