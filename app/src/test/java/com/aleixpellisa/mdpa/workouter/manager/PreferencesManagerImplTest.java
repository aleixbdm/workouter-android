package com.aleixpellisa.mdpa.workouter.manager;

import android.content.SharedPreferences;

import com.aleixpellisa.mdpa.workouter.BaseUnitTest;
import com.aleixpellisa.mdpa.workouter.builder.AuthBuilder;
import com.aleixpellisa.mdpa.workouter.builder.SettingsBuilder;
import com.aleixpellisa.mdpa.workouter.database.entity.Settings;
import com.aleixpellisa.mdpa.workouter.manager.impl.PreferencesManagerImpl;
import com.aleixpellisa.mdpa.workouter.model.Auth;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.inject.Inject;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PreferencesManagerImplTest extends BaseUnitTest {

    private static final String accessToken = AuthBuilder.AUTH_ACCESS_TOKEN_MOCK;
    private static final String userId = AuthBuilder.AUTH_USER_ID_MOCK;
    private static final Auth auth = AuthBuilder.buildMockedAuth();
    private static final String preference = SettingsBuilder.SETTINGS_PREFERENCE_MOCK;
    private static final String city = SettingsBuilder.SETTINGS_CITY_MOCK;
    private static final int searchRange = SettingsBuilder.SETTINGS_SEARCH_RANGE_MOCK;
    private static final int yearRangeMax = SettingsBuilder.SETTINGS_YEAR_RANGE_MAX_MOCK;
    private static final int yearRangeMin = SettingsBuilder.SETTINGS_YEAR_RANGE_MIN_MOCK;
    private static final boolean visible = SettingsBuilder.SETTINGS_VISIBLE_MOCK;
    private static final Settings settings = SettingsBuilder.buildMockedSettings();
    private static final String defaultString = null;
    private static final int defaultInt = 0;
    private static final boolean defaultBoolean = false;

    @Mock
    private SharedPreferences sharedPreferencesMock;

    @Mock
    private SharedPreferences.Editor sharedPreferencesEditorMock;

    @Inject
    PreferencesManagerImpl settingsManager;

    @Before
    public void setup() {
        super.setup();
    }

    /**
     * Read & Write Auth
     */

    @Test
    public void writeAuthWorksProperly() {
        when(sharedPreferencesMock.edit()).thenReturn(sharedPreferencesEditorMock);

        Auth auth = AuthBuilder.buildMockedAuth();
        settingsManager.saveAuth(auth);

        verify(sharedPreferencesMock, times(2)).edit();
        verify(sharedPreferencesEditorMock, times(1))
                .putString(eq(Auth.ACCESS_TOKEN), eq(auth.getAccessToken()));
        verify(sharedPreferencesEditorMock, times(1))
                .putString(eq(Auth.USER_ID), eq(auth.getUserId()));
        verify(sharedPreferencesEditorMock, times(2)).apply();
    }

    @Test
    public void readExistingAuthReturnsAuth() {
        when(sharedPreferencesMock.getString(Auth.ACCESS_TOKEN, null)).thenReturn(accessToken);
        when(sharedPreferencesMock.getString(Auth.USER_ID, null)).thenReturn(userId);

        Auth returnedValue = settingsManager.readAuth();
        assertEquals(auth.getAccessToken(), returnedValue.getAccessToken());
        assertEquals(auth.getUserId(), returnedValue.getUserId());
    }

    @Test
    public void readNotExistingAuthReturnsDefaultInvalidAuth() {
        Auth returnedValue = settingsManager.readAuth();

        verify(sharedPreferencesMock, times(1)).getString(eq(Auth.ACCESS_TOKEN), eq(defaultString));
        verify(sharedPreferencesMock, times(1)).getString(eq(Auth.USER_ID), eq(defaultString));
        assertNull(returnedValue.getAccessToken());
        assertNull(returnedValue.getUserId());
    }

    /**
     * Read & Write Settings
     */

    @Test
    public void writeSettingsWorksProperly() {
        when(sharedPreferencesMock.edit()).thenReturn(sharedPreferencesEditorMock);

        Settings settings = SettingsBuilder.buildMockedSettings();
        settingsManager.saveSettings(settings);

        verify(sharedPreferencesMock, times(6)).edit();
        verify(sharedPreferencesEditorMock, times(1))
                .putString(eq(Settings.PREFERENCE_KEY), eq(settings.getPreference()));
        verify(sharedPreferencesEditorMock, times(1))
                .putString(eq(Settings.CITY_KEY), eq(settings.getCity()));
        verify(sharedPreferencesEditorMock, times(1))
                .putInt(eq(Settings.SEARCH_RANGE_KEY), eq(settings.getSearchRange()));
        verify(sharedPreferencesEditorMock, times(1))
                .putInt(eq(Settings.YEAR_RANGE_MIN_KEY), eq(settings.getYearRangeMin()));
        verify(sharedPreferencesEditorMock, times(1))
                .putInt(eq(Settings.YEAR_RANGE_MAX_KEY), eq(settings.getYearRangeMax()));
        verify(sharedPreferencesEditorMock, times(1))
                .putBoolean(eq(Settings.VISIBLE_KEY), eq(settings.isVisible()));
        verify(sharedPreferencesEditorMock, times(6)).apply();
    }

    @Test
    public void readExistingSettingsReturnsSettings() {
        when(sharedPreferencesMock.getString(Settings.PREFERENCE_KEY, null)).thenReturn(preference);
        when(sharedPreferencesMock.getString(Settings.CITY_KEY, null)).thenReturn(city);
        when(sharedPreferencesMock.getInt(eq(Settings.SEARCH_RANGE_KEY), anyInt())).thenReturn(searchRange);
        when(sharedPreferencesMock.getInt(eq(Settings.YEAR_RANGE_MAX_KEY), anyInt())).thenReturn(yearRangeMax);
        when(sharedPreferencesMock.getInt(eq(Settings.YEAR_RANGE_MIN_KEY), anyInt())).thenReturn(yearRangeMin);
        when(sharedPreferencesMock.getBoolean(eq(Settings.VISIBLE_KEY), anyBoolean())).thenReturn(visible);

        Settings returnedValue = settingsManager.readSettings();
        assertEquals(settings.getCity(), returnedValue.getCity());
        assertEquals(settings.getPreference(), returnedValue.getPreference());
        assertEquals(settings.getSearchRange(), returnedValue.getSearchRange());
        assertEquals(settings.getYearRangeMin(), returnedValue.getYearRangeMin());
        assertEquals(settings.getYearRangeMax(), returnedValue.getYearRangeMax());
        assertEquals(settings.isVisible(), returnedValue.isVisible());
    }

    @Test
    public void readNotExistingSettingsReturnsDefaultInvalidSettings() {
        Settings returnedValue = settingsManager.readSettings();

        verify(sharedPreferencesMock, times(1)).getString(eq(Settings.PREFERENCE_KEY), eq(defaultString));
        verify(sharedPreferencesMock, times(1)).getString(eq(Settings.CITY_KEY), eq(defaultString));
        verify(sharedPreferencesMock, times(1)).getInt(eq(Settings.SEARCH_RANGE_KEY), eq(defaultInt));
        verify(sharedPreferencesMock, times(1)).getInt(eq(Settings.YEAR_RANGE_MIN_KEY), eq(defaultInt));
        verify(sharedPreferencesMock, times(1)).getInt(eq(Settings.YEAR_RANGE_MAX_KEY), eq(defaultInt));
        verify(sharedPreferencesMock, times(1)).getBoolean(eq(Settings.VISIBLE_KEY), eq(defaultBoolean));
        assertNull(returnedValue.getCity());
        assertNull(returnedValue.getPreference());
        assertNull(returnedValue.getSearchRange());
        assertNull(returnedValue.getYearRangeMin());
        assertNull(returnedValue.getYearRangeMax());
        assertFalse(returnedValue.isVisible());
    }

    /**
     * Clear all
     */

    @Test
    public void clearAllWorksProperly() {
        when(sharedPreferencesMock.edit()).thenReturn(sharedPreferencesEditorMock);

        Auth auth = AuthBuilder.buildMockedAuth();
        settingsManager.saveAuth(auth);

        settingsManager.clearAll();

        Auth returnedValue = settingsManager.readAuth();
        assertNull(returnedValue.getAccessToken());
        assertNull(returnedValue.getUserId());
    }

}
