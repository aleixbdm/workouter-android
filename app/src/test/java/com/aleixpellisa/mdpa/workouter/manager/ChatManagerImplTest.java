package com.aleixpellisa.mdpa.workouter.manager;

import android.arch.lifecycle.LiveData;

import com.aleixpellisa.mdpa.workouter.BaseUnitTest;
import com.aleixpellisa.mdpa.workouter.builder.AuthBuilder;
import com.aleixpellisa.mdpa.workouter.builder.ChatBuilder;
import com.aleixpellisa.mdpa.workouter.builder.MessageBuilder;
import com.aleixpellisa.mdpa.workouter.database.MeetDayDatabase;
import com.aleixpellisa.mdpa.workouter.database.dao.ChatDao;
import com.aleixpellisa.mdpa.workouter.database.dao.MessageDao;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.manager.impl.ChatManagerImpl;
import com.aleixpellisa.mdpa.workouter.manager.refresher.DataRefresher;
import com.aleixpellisa.mdpa.workouter.manager.refresher.TokenRefresher;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.network.interceptor.AuthTokenInterceptor;
import com.aleixpellisa.mdpa.workouter.network.rest.ChatRest;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import javax.inject.Inject;

import testers.manager.ChatManagerTester;

import static org.mockito.Mockito.when;
import static values.Events.fetchChatMessagesErrorEvent;
import static values.Events.fetchChatMessagesEvent;
import static values.Events.fetchChatsErrorEvent;
import static values.Events.fetchChatsEvent;
import static values.Events.sendMessageErrorEvent;

public class ChatManagerImplTest extends BaseUnitTest {

    private static final String chatId = fetchChatMessagesEvent.data.get(0).getId();

    @Mock
    @SuppressWarnings("unused")
    private PreferencesManager preferencesManagerMock;

    @Mock
    private MeetDayDatabase meetDayDatabaseMock;

    @Mock
    @SuppressWarnings("unused")
    private AuthTokenInterceptor authTokenInterceptorMock;

    @Mock
    @SuppressWarnings("unused")
    private ChatRest chatRestMock;

    @Mock
    private ChatDao chatDaoMock;

    @Mock
    private MessageDao messageDaoMock;

    @Mock
    @SuppressWarnings("unused")
    private DataRefresher chatsDataRefresherMock;

    @Mock
    @SuppressWarnings("unused")
    private DataRefresher chatMessagesDataRefresherMock;

    @Mock
    @SuppressWarnings("unused")
    private TokenRefresher tokenRefresherMock;

    @Mock
    private LiveData<List<Chat>> chatsLiveDataMock;

    @Mock
    private LiveData<List<Message>> chatMessagesLiveDataMock;

    @Inject
    public ChatManagerImpl manager;

    @Inject
    public ChatManagerTester tester;

    @Before
    public void setup() {
        when(chatDaoMock.getChats()).thenReturn(chatsLiveDataMock);
        when(meetDayDatabaseMock.getChatDao()).thenReturn(chatDaoMock);
        when(messageDaoMock.getMessagesForChat(chatId)).thenReturn(chatMessagesLiveDataMock);
        when(meetDayDatabaseMock.getMessageDao()).thenReturn(messageDaoMock);

        super.setup();

        manager.startObservers();
        manager.enterChat(chatId);
    }

    /**
     * Get Chat List
     */

    private static final List<Chat> chatList = ChatBuilder.buildMockedChatList();

    @Test
    public void checkReceiveChatListWhenExistsInDatabase() {
        tester.chatListInDatabase(chatList);

        Event<List<Chat>> event = manager.getChatList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data, chatList);
    }

    @Test
    public void checkNotReceiveChatListWhenNotExistsInDatabase() {
        tester.chatListInDatabase(null);

        Event<List<Chat>> event = manager.getChatList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
    }

    /**
     * Get Chat Message List
     */

    private static final List<Message> chatMessageList = MessageBuilder.buildMockedMessageList();

    @Test
    public void checkReceiveChatMessageListWhenExistsInDatabase() {
        tester.chatMessageListInDatabase(chatMessageList);

        Event<List<Message>> event = manager.getChatMessageList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data, chatMessageList);
    }

    @Test
    public void checkNotReceiveChatMessageListWhenNotExistsInDatabase() {
        tester.chatMessageListInDatabase(null);

        Event<List<Message>> event = manager.getChatMessageList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
    }

    /**
     * Get Chat List Filtered
     */

    private static final String filter = chatList.get(0).getUserChat().getName();
    private static final String incorrectFilter = chatList.get(0).getUserChat().getName() + "z";
    private static final String emptyFilter = "";

    @Test
    public void checkReceiveChatListFilteredWhenCorrectFilter() {
        tester.chatListInDatabase(chatList);

        List<Chat> chatListFiltered = manager.getChatListFiltered(chatList, filter);
        Assert.assertNotNull(chatListFiltered);
        Assert.assertTrue(!chatListFiltered.isEmpty());
        Assert.assertEquals(chatListFiltered.get(0), chatList.get(0));
    }

    @Test
    public void checkNotReceiveChatListFilteredWhenIncorrectFilter() {
        tester.chatListInDatabase(chatList);

        List<Chat> chatListFiltered = manager.getChatListFiltered(chatList, incorrectFilter);
        Assert.assertNotNull(chatListFiltered);
        Assert.assertTrue(chatListFiltered.isEmpty());
    }

    @Test
    public void checkReceiveChatListNotFilteredWhenEmptyFilter() {
        tester.chatListInDatabase(chatList);

        List<Chat> chatListFiltered = manager.getChatListFiltered(chatList, emptyFilter);
        Assert.assertNotNull(chatListFiltered);
        Assert.assertEquals(chatListFiltered, chatList);
    }

    /**
     * Fetch Chats
     */

    @Test
    public void checkReceiveChatsWhenFetchChatsSucceed() {
        tester.fetchChatsReturnsChats(fetchChatsEvent);

        manager.fetchChats();

        Event<List<Chat>> event = manager.getChatList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertTrue(!event.data.isEmpty());
        Assert.assertEquals(event.data.get(0).getId(), fetchChatsEvent.data.get(0).getId());
    }

    @Test
    public void checkReceiveErrorWhenFetchChatsFails() {
        tester.fetchChatsReturnsChats(fetchChatsErrorEvent);

        manager.fetchChats();

        Event<List<Chat>> event = manager.getChatList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchChatsErrorEvent.meetDayError.getMessage());
    }

    /**
     * Fetch Chat Messages
     */

    @Test
    public void checkReceiveChatMessagesWhenFetchChatMessagesSucceed() {
        tester.fetchChatMessagesReturnsChatMessages(chatId, fetchChatMessagesEvent);

        manager.fetchChatMessages(chatId);

        Event<List<Message>> event = manager.getChatMessageList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertTrue(!event.data.isEmpty());
        Assert.assertEquals(event.data.get(0).getId(), fetchChatMessagesEvent.data.get(0).getId());
    }

    @Test
    public void checkReceiveErrorWhenFetchChatMessagesFails() {
        tester.fetchChatMessagesReturnsChatMessages(chatId, fetchChatMessagesErrorEvent);

        manager.fetchChatMessages(chatId);

        Event<List<Message>> event = manager.getChatMessageList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchChatMessagesErrorEvent.meetDayError.getMessage());
    }

    /**
     * Send Message
     */

    private static final Auth auth = AuthBuilder.buildEmptyAuth();
    private static final String messageText = "Message text";

    @Test
    public void checkReceiveChatMessageInstantlyWhenAuthenticated() {
        tester.authInPreferences(auth);

        manager.sendMessage(chatId, messageText);

        Event<Message> event = manager.getChatMessage().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getSenderUserId(), auth.getUserId());
        Assert.assertEquals(event.data.getMessageText(), messageText);
    }

    @Test
    public void checkReceiveErrorWhenSendMessageFails() {
        tester.sendChatMessageReturnsChatMessage(chatId, messageText, sendMessageErrorEvent);

        manager.sendMessage(chatId, messageText);

        Event<Message> event = manager.getChatMessage().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), sendMessageErrorEvent.meetDayError.getMessage());
    }
}