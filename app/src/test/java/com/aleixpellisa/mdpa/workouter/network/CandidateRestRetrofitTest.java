package com.aleixpellisa.mdpa.workouter.network;

import android.arch.lifecycle.Observer;

import com.aleixpellisa.mdpa.workouter.BaseUnitTest;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.network.rest.impl.CandidateRestRetrofit;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import testers.network.CandidateRestTester;

import static values.Events.fetchCandidatesErrorEvent;
import static values.Events.fetchCandidatesEvent;
import static values.Events.sendEvaluationErrorEvent;
import static values.Events.sendEvaluationEvent;
import static values.MockClient.retrofitClientMock;

public class CandidateRestRetrofitTest extends BaseUnitTest {

    @Mock
    private Observer<Event<List<Candidate>>> candidatesResponseObserverMock;

    @Mock
    private Observer<Event<Evaluation>> evaluationResponseObserverMock;

    private CandidateRestRetrofit retrofit;

    private CandidateRestTester tester;

    @Before
    public void setup() {
        super.setup();

        retrofit = new CandidateRestRetrofit(retrofitClientMock);
        tester = new CandidateRestTester(retrofit);
    }

    /**
     * Fetch Candidates
     */

    @Test
    public void checkReceiveCandidatesWhenFetchCandidatesSucceed() {
        retrofit.fetchCandidates(candidatesResponseObserverMock);

        tester.fetchCandidatesReturnsCandidates(fetchCandidatesEvent);

        Event<List<Candidate>> event = tester.captureEventFromResponseObserver(candidatesResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertTrue(!event.data.isEmpty());
        Assert.assertEquals(event.data.get(0).getId(), fetchCandidatesEvent.data.get(0).getId());
    }

    @Test
    public void checkReceiveErrorWhenFetchCandidatesFails() {
        retrofit.fetchCandidates(candidatesResponseObserverMock);

        tester.fetchCandidatesReturnsCandidates(fetchCandidatesErrorEvent);

        Event<List<Candidate>> event = tester.captureEventFromResponseObserver(candidatesResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchCandidatesErrorEvent.meetDayError.getMessage());
    }

    /**
     * Send Evaluation
     */

    private static final String candidateId = fetchCandidatesEvent.data.get(0).getId();
    private static final String evaluationValue = sendEvaluationEvent.data.getEvaluationValue();

    @Test
    public void checkReceiveEvaluationWhenSendEvaluationSucceed() {
        retrofit.sendEvaluation(candidateId, evaluationValue, evaluationResponseObserverMock);

        tester.sendEvaluationReturnsEvaluation(sendEvaluationEvent);

        Event<Evaluation> event = tester.captureEventFromResponseObserver(evaluationResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getId(), sendEvaluationEvent.data.getId());
    }

    @Test
    public void checkReceiveErrorWhenSendEvaluationFails() {
        retrofit.sendEvaluation(candidateId, evaluationValue, evaluationResponseObserverMock);

        tester.sendEvaluationReturnsEvaluation(sendEvaluationErrorEvent);

        Event<Evaluation> event = tester.captureEventFromResponseObserver(evaluationResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), sendEvaluationErrorEvent.meetDayError.getMessage());
    }

}
