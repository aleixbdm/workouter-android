package com.aleixpellisa.mdpa.workouter.network;

import android.arch.lifecycle.Observer;

import com.aleixpellisa.mdpa.workouter.BaseUnitTest;
import com.aleixpellisa.mdpa.workouter.model.Auth;
import com.aleixpellisa.mdpa.workouter.network.rest.impl.AuthRestRetrofit;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import testers.network.AuthRestTester;

import static values.Events.createAccountErrorEvent;
import static values.Events.createAccountEvent;
import static values.Events.facebookLoginErrorEvent;
import static values.Events.facebookLoginEvent;
import static values.Events.loginErrorEvent;
import static values.Events.loginEvent;
import static values.Events.refreshTokenErrorEvent;
import static values.Events.refreshTokenEvent;
import static values.Inputs.*;
import static values.MockClient.retrofitClientMock;

public class AuthRestRetrofitTest extends BaseUnitTest {

    @Mock
    private Observer<Event<Auth>> authResponseObserverMock;

    private AuthRestRetrofit retrofit;

    private AuthRestTester tester;

    @Before
    public void setup() {
        super.setup();

        retrofit = new AuthRestRetrofit(retrofitClientMock);
        tester = new AuthRestTester(retrofit);
    }

    /**
     * Login
     */

    @Test
    public void checkReceiveAuthWhenLoginSucceed() {
        retrofit.login(email, password, authResponseObserverMock);

        tester.loginReturnsAuth(loginEvent);

        Event<Auth> event = tester.captureEventFromResponseObserver(authResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getUserId(), loginEvent.data.getUserId());
        Assert.assertEquals(event.data.getAccessToken(), loginEvent.data.getAccessToken());
    }

    @Test
    public void checkReceiveErrorWhenLoginFails() {
        retrofit.login(email, password, authResponseObserverMock);

        tester.loginReturnsAuth(loginErrorEvent);

        Event<Auth> event = tester.captureEventFromResponseObserver(authResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), loginErrorEvent.meetDayError.getMessage());
    }

    /**
     * Facebook Login
     */

    @Test
    public void checkReceiveAuthWhenFacebookLoginSucceed() {
        retrofit.facebookLogin(facebookAccessToken, authResponseObserverMock);

        tester.facebookLoginReturnsAuth(facebookLoginEvent);

        Event<Auth> event = tester.captureEventFromResponseObserver(authResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getUserId(), facebookLoginEvent.data.getUserId());
        Assert.assertEquals(event.data.getAccessToken(), facebookLoginEvent.data.getAccessToken());
    }

    @Test
    public void checkReceiveErrorWhenFacebookLoginFails() {
        retrofit.facebookLogin(facebookAccessToken, authResponseObserverMock);

        tester.facebookLoginReturnsAuth(facebookLoginErrorEvent);

        Event<Auth> event = tester.captureEventFromResponseObserver(authResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), facebookLoginErrorEvent.meetDayError.getMessage());
    }

    /**
     * Create Account
     */

    @Test
    public void checkReceiveAuthWhenCreateAccountSucceed() {
        retrofit.createAccount(email, password, authResponseObserverMock);

        tester.createAccountReturnsAuth(createAccountEvent);

        Event<Auth> event = tester.captureEventFromResponseObserver(authResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getUserId(), createAccountEvent.data.getUserId());
        Assert.assertEquals(event.data.getAccessToken(), createAccountEvent.data.getAccessToken());
    }

    @Test
    public void checkReceiveErrorWhenCreateAccountFails() {
        retrofit.createAccount(email, password, authResponseObserverMock);

        tester.createAccountReturnsAuth(createAccountErrorEvent);

        Event<Auth> event = tester.captureEventFromResponseObserver(authResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), createAccountErrorEvent.meetDayError.getMessage());
    }

    /**
     * Refresh token
     */

    @Test
    public void checkReceiveAuthWhenRefreshTokenSucceed() {
        retrofit.refreshToken(auth.getUserId(), authResponseObserverMock);

        tester.refreshTokenReturnsAuth(refreshTokenEvent);

        Event<Auth> event = tester.captureEventFromResponseObserver(authResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getUserId(), refreshTokenEvent.data.getUserId());
        Assert.assertEquals(event.data.getAccessToken(), refreshTokenEvent.data.getAccessToken());
    }

    @Test
    public void checkReceiveErrorWhenRefreshTokenFails() {
        retrofit.refreshToken(auth.getUserId(), authResponseObserverMock);

        tester.refreshTokenReturnsAuth(refreshTokenErrorEvent);

        Event<Auth> event = tester.captureEventFromResponseObserver(authResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), refreshTokenErrorEvent.meetDayError.getMessage());
    }

}
