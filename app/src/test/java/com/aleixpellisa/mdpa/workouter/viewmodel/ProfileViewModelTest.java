package com.aleixpellisa.mdpa.workouter.viewmodel;

import android.app.Application;

import com.aleixpellisa.mdpa.workouter.BaseUnitTest;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.manager.ProfileManager;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;
import com.aleixpellisa.mdpa.workouter.view.model.ProfileViewModel;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import javax.inject.Inject;

import testers.viewmodel.ProfileViewModelUnitTester;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static values.Events.fetchCandidateProfileErrorEvent;
import static values.Events.fetchCandidateProfileEvent;
import static values.Events.fetchProfileErrorEvent;
import static values.Events.fetchProfileEvent;
import static values.Inputs.acceptedTermsAndConditions;
import static values.Inputs.ageUpdated;
import static values.Inputs.auth;
import static values.Inputs.basicProfile;
import static values.Inputs.settings;
import static values.Inputs.candidateId;
import static values.Inputs.cityUpdated;
import static values.Inputs.emailInput;
import static values.Inputs.emptyEmailInput;
import static values.Inputs.emptyPasswordInput;
import static values.Inputs.encryptedPassword;
import static values.Inputs.firstNameUpdated;
import static values.Inputs.genderUpdated;
import static values.Inputs.imagesUpdated;
import static values.Inputs.nameUpdated;
import static values.Inputs.notAcceptedTermsAndConditions;
import static values.Inputs.passwordInput;
import static values.Inputs.preferenceUpdated;
import static values.Inputs.profile;
import static values.Inputs.profileCurrentJobTextInput;
import static values.Inputs.profileDescriptionTextInput;
import static values.Inputs.profileEmptyTextInput;
import static values.Inputs.profileFavSongTextInput;
import static values.Inputs.profileFirstNameTextInput;
import static values.Inputs.profileNameTextInput;
import static values.Inputs.profileStudiesTextInput;
import static values.Inputs.skillsUpdated;
import static values.Inputs.updateSettingsKey;
import static values.Inputs.updateSettingsValue;
import static values.Inputs.wrongConfirmPasswordInput;
import static values.Inputs.wrongEmailInput;
import static values.Inputs.wrongPasswordInput;

@RunWith(MockitoJUnitRunner.class)
public class ProfileViewModelTest extends BaseUnitTest {

    @Mock
    @SuppressWarnings("unused")
    private EventLiveData<Event<Profile>> profileLiveDataMock;

    @Mock
    @SuppressWarnings("unused")
    private EventLiveData<Event<Profile>> candidateProfileLiveDataMock;

    @Mock
    @SuppressWarnings("unused")
    private Application testApplication;

    @Mock
    private ProfileManager profileManagerMock;

    @Inject
    ProfileViewModel profileViewModel;

    @Inject
    ProfileViewModelUnitTester tester;

    @Before
    public void setup() {
        super.setup();
    }

    /**
     * Get auth
     */

    @Test
    public void checkReceiveAuthWhenReturnedFromManager() {
        tester.authFromManager(auth);

        Assert.assertEquals(profileViewModel.getAuth(), auth);
    }

    @Test
    public void checkNotReceiveAuthWhenNotReturnedFromManager() {
        tester.authFromManager(null);

        Assert.assertNull(profileViewModel.getAuth());
    }

    /**
     * Get settings
     */

    @Test
    public void checkReceiveSettingsWhenReturnedFromManager() {
        tester.settingsFromManager(settings);

        Assert.assertEquals(profileViewModel.getSettings(), settings);
    }

    @Test
    public void checkNotReceiveSettingsWhenNotReturnedFromManager() {
        tester.settingsFromManager(null);

        Assert.assertNull(profileViewModel.getSettings());
    }

    /**
     * Get profile
     */

    @Test
    public void checkReceiveProfileWhenReturnedFromManager() {
        tester.profileFromManager(fetchProfileEvent);

        Event<Profile> event = profileViewModel.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data, fetchProfileEvent.data);
    }

    @Test
    public void checkNotReceiveProfileWhenErrorReturnedFromManager() {
        tester.profileFromManager(fetchProfileErrorEvent);

        Event<Profile> event = profileViewModel.getProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchProfileErrorEvent.meetDayError.getMessage());
    }

    /**
     * Get candidate profile
     */

    @Test
    public void checkReceiveCandidateProfileWhenReturnedFromManager() {
        tester.candidateProfileFromManager(fetchCandidateProfileEvent);

        Event<Profile> event = profileViewModel.getCandidateProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data, fetchCandidateProfileEvent.data);
    }

    @Test
    public void checkNotReceiveCandidateProfileWhenErrorReturnedFromManager() {
        tester.candidateProfileFromManager(fetchCandidateProfileErrorEvent);

        Event<Profile> event = profileViewModel.getCandidateProfile().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchCandidateProfileErrorEvent.meetDayError.getMessage());
    }

    /**
     * Attempt login
     */

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptLoginParametersAreNotNull() throws Exception {
        profileViewModel.attemptLogin(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptLoginParametersAreNotEmpty() throws Exception {
        profileViewModel.attemptLogin(emptyEmailInput, emptyPasswordInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptLoginEmailIncorrectFormat() throws Exception {
        profileViewModel.attemptLogin(wrongEmailInput, passwordInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptLoginPasswordTooShort() throws Exception {
        profileViewModel.attemptLogin(emailInput, wrongPasswordInput);
    }

    @Test
    public void checkAttemptLoginEmailIsForwardedProperly() throws Exception {
        profileViewModel.attemptLogin(emailInput, passwordInput);
        verify(profileManagerMock, times(1))
                .login(eq(emailInput.getValue()),
                        anyString());
    }

    @Test
    public void checkAttemptLoginPasswordForwardedIsSha256Encrypted() throws Exception {
        profileViewModel.attemptLogin(emailInput, passwordInput);
        verify(profileManagerMock, times(1))
                .login(anyString(),
                        eq(encryptedPassword));
    }

    /**
     * Attempt create account
     */

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptCreateAccountParametersAreNotNull() throws Exception {
        profileViewModel.attemptCreateAccount(null, null,
                null, notAcceptedTermsAndConditions);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptCreateAccountParametersAreNotEmpty() throws Exception {
        profileViewModel.attemptCreateAccount(emptyEmailInput, emptyPasswordInput,
                emptyPasswordInput, notAcceptedTermsAndConditions);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptCreateAccountEmailIncorrectFormat() throws Exception {
        profileViewModel.attemptCreateAccount(wrongEmailInput, passwordInput,
                passwordInput, acceptedTermsAndConditions);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptCreateAccountPasswordTooShort() throws Exception {
        profileViewModel.attemptCreateAccount(emailInput, wrongPasswordInput,
                passwordInput, acceptedTermsAndConditions);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptCreateAccountPasswordsNotMatch() throws Exception {
        profileViewModel.attemptCreateAccount(emailInput, passwordInput,
                wrongConfirmPasswordInput, acceptedTermsAndConditions);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptCreateAccountMustAcceptTerms() throws Exception {
        profileViewModel.attemptCreateAccount(emailInput, passwordInput,
                passwordInput, notAcceptedTermsAndConditions);
    }

    @Test
    public void checkAttemptCreateAccountEmailIsForwardedProperly() throws Exception {
        profileViewModel.attemptCreateAccount(emailInput, passwordInput,
                passwordInput, acceptedTermsAndConditions);
        verify(profileManagerMock, times(1))
                .createAccount(eq(emailInput.getValue()),
                        anyString());
    }

    @Test
    public void checkAttemptCreateAccountPasswordForwardedIsSha256Encrypted() throws Exception {
        profileViewModel.attemptCreateAccount(emailInput, passwordInput,
                passwordInput, acceptedTermsAndConditions);
        verify(profileManagerMock, times(1))
                .createAccount(anyString(),
                        eq(encryptedPassword));
    }

    /**
     * Fetch profile
     */

    @Test
    public void checkFetchProfileIsForwardedProperly() throws Exception {
        profileViewModel.fetchProfile();
        verify(profileManagerMock, times(1))
                .fetchProfile();
    }

    /**
     * Basic profile
     */

    @Test
    public void checkBasicProfileIsForwardedProperly() throws Exception {
        profileViewModel.isProfileBasic(basicProfile);
        verify(profileManagerMock, times(1))
                .isProfileBasic(basicProfile);
    }

    /**
     * Fetch candidate profile
     */

    @Test(expected = IllegalArgumentException.class)
    public void checkFetchCandidateProfileParametersAreNotNull() throws Exception {
        profileViewModel.fetchCandidateProfile(null);
    }

    @Test
    public void checkFetchCandidateProfileIsForwardedProperly() throws Exception {
        profileViewModel.fetchCandidateProfile(candidateId);
        verify(profileManagerMock, times(1))
                .fetchCandidateProfile(candidateId);
    }

    /**
     * Create profile
     */

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptCreateProfileParametersAreNotNull() throws Exception {
        profileViewModel.attemptCreateProfile(null, null, null, 0,
                null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptCreateProfileParametersAreNotEmpty() throws Exception {
        profileViewModel.attemptCreateProfile("", "", new ArrayList<String>(), 0,
                "", "", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptCreateProfileAgeMinRequired() throws Exception {
        profileViewModel.attemptCreateProfile(nameUpdated, firstNameUpdated, imagesUpdated,
                17, genderUpdated, preferenceUpdated, cityUpdated);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptCreateProfileAgeMaxRequired() throws Exception {
        profileViewModel.attemptCreateProfile(nameUpdated, firstNameUpdated, imagesUpdated,
                66, genderUpdated, preferenceUpdated, cityUpdated);
    }

    @Test
    public void checkAttemptCreateProfileIsForwardedProperly() throws Exception {
        profileViewModel.attemptCreateProfile(nameUpdated, firstNameUpdated, imagesUpdated,
                ageUpdated, genderUpdated, preferenceUpdated, cityUpdated);

        verify(profileManagerMock, times(1))
                .createProfile(nameUpdated, firstNameUpdated, imagesUpdated,
                        ageUpdated, genderUpdated, preferenceUpdated, cityUpdated);
    }

    /**
     * Update profile
     */

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptUpdateProfileParametersAreNotNull() throws Exception {
        profileViewModel.attemptUpdateProfile(profile, null, null,
                null, 0, null, null,
                null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptUpdateProfileWithEmptyParameters() throws Exception {
        profileViewModel.attemptUpdateProfile(profile, imagesUpdated, profileEmptyTextInput,
                profileEmptyTextInput, ageUpdated, genderUpdated, profileEmptyTextInput,
                profileEmptyTextInput, profileEmptyTextInput, profileEmptyTextInput, skillsUpdated);
    }

    @Test
    public void checkAttemptUpdateProfileIsForwardedProperly() throws Exception {
        profileViewModel.attemptUpdateProfile(profile, imagesUpdated, profileNameTextInput,
                profileFirstNameTextInput, ageUpdated, genderUpdated, profileDescriptionTextInput,
                profileCurrentJobTextInput, profileStudiesTextInput, profileFavSongTextInput, skillsUpdated);

        verify(profileManagerMock, times(1))
                .updateProfile(
                        any(Profile.class));
    }

    /**
     * Update settings
     */

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptUpdateSettingsWithNull() throws Exception {
        profileViewModel.attemptUpdateSettings(updateSettingsKey, null);
    }

    @Test
    public void checkAttemptUpdateSettingsIsForwardedProperly() throws Exception {
        profileViewModel.attemptUpdateSettings(updateSettingsKey, updateSettingsValue);

        verify(profileManagerMock, times(1))
                .updateSettings(
                        eq(updateSettingsKey),
                        eq(updateSettingsValue));
    }

    /**
     * Clear all
     */

    @Test
    public void checkClearAllIsForwardedProperly() throws Exception {
        profileViewModel.clearAll();

        verify(profileManagerMock, times(1))
                .clearAll();
    }

}
