package com.aleixpellisa.mdpa.workouter.viewmodel;

import android.app.Application;

import com.aleixpellisa.mdpa.workouter.BaseUnitTest;
import com.aleixpellisa.mdpa.workouter.database.entity.Chat;
import com.aleixpellisa.mdpa.workouter.database.entity.Message;
import com.aleixpellisa.mdpa.workouter.manager.ChatManager;
import com.aleixpellisa.mdpa.workouter.view.input.MeetDayTextInputType;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;
import com.aleixpellisa.mdpa.workouter.manager.livedata.EventLiveData;
import com.aleixpellisa.mdpa.workouter.view.model.ChatViewModel;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import javax.inject.Inject;

import testers.viewmodel.ChatViewModelUnitTester;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static values.Events.fetchChatMessagesErrorEvent;
import static values.Events.fetchChatMessagesEvent;
import static values.Events.fetchChatsErrorEvent;
import static values.Events.fetchChatsEvent;
import static values.Events.sendMessageErrorEvent;
import static values.Events.sendMessageEvent;

@RunWith(MockitoJUnitRunner.class)
public class ChatViewModelTest extends BaseUnitTest {

    private static final MeetDayTextInputType messageInput = new MeetDayTextInputType();

    @Mock
    @SuppressWarnings("unused")
    private EventLiveData<Event<List<Chat>>> chatListLiveDataMock;

    @Mock
    @SuppressWarnings("unused")
    private EventLiveData<Event<List<Message>>> chatMessageListLiveDataMock;

    @Mock
    @SuppressWarnings("unused")
    private EventLiveData<Event<Message>> chatMessageLiveDataMock;

    @Mock
    @SuppressWarnings("unused")
    private Application testApplication;

    @Mock
    private ChatManager chatManagerMock;

    @Inject
    ChatViewModel chatViewModel;

    @Inject
    ChatViewModelUnitTester tester;

    @BeforeClass
    public static void setupBeforeClass() {
        messageInput.setRequired(true);
    }

    @Before
    public void setup() {
        super.setup();
    }

    /**
     * Get chat list
     */

    @Test
    public void checkReceiveChatListWhenReturnedFromManager() {
        tester.chatListFromManager(fetchChatsEvent);

        Event<List<Chat>> event = chatViewModel.getChatList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertTrue(!event.data.isEmpty());
        Assert.assertEquals(event.data, fetchChatsEvent.data);
    }

    @Test
    public void checkNotReceiveChatListWhenErrorReturnedFromManager() {
        tester.chatListFromManager(fetchChatsErrorEvent);

        Event<List<Chat>> event = chatViewModel.getChatList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchChatsErrorEvent.meetDayError.getMessage());
    }

    /**
     * Get chat message list
     */

    @Test
    public void checkReceiveChatMessageListWhenReturnedFromManager() {
        tester.chatMessageListFromManager(fetchChatMessagesEvent);

        Event<List<Message>> event = chatViewModel.getChatMessageList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertTrue(!event.data.isEmpty());
        Assert.assertEquals(event.data, fetchChatMessagesEvent.data);
    }

    @Test
    public void checkNotReceiveChatMessagesWhenErrorReturnedFromManager() {
        tester.chatMessageListFromManager(fetchChatMessagesErrorEvent);

        Event<List<Message>> event = chatViewModel.getChatMessageList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchChatMessagesErrorEvent.meetDayError.getMessage());
    }

    /**
     * Get chat message
     */

    @Test
    public void checkReceiveChatMessageWhenReturnedFromManager() {
        tester.chatMessageFromManager(sendMessageEvent);

        Event<Message> event = chatViewModel.getChatMessage().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data, sendMessageEvent.data);
    }

    @Test
    public void checkNotReceiveChatMessageWhenErrorReturnedFromManager() {
        tester.chatMessageFromManager(sendMessageErrorEvent);

        Event<Message> event = chatViewModel.getChatMessage().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), sendMessageErrorEvent.meetDayError.getMessage());
    }

    /**
     * Fetch chats
     */

    @Test
    public void checkFetchChatsIsForwardedProperly() throws Exception {
        chatViewModel.fetchChats();
        verify(chatManagerMock, times(1))
                .fetchChats();
    }

    /**
     * Enter chat
     */

    private static final String chatId = fetchChatMessagesEvent.data.get(0).getId();

    @Test
    public void checkEnterChatIsForwardedProperly() throws Exception {
        chatViewModel.enterChat(chatId);

        verify(chatManagerMock, times(1))
                .enterChat(eq(chatId));
    }

    /**
     * Fetch chat messages
     */

    @Test
    public void checkFetchChatMessagesIsForwardedProperly() throws Exception {
        tester.setCurrentChatId(chatId);
        chatViewModel.fetchChatMessages();
        verify(chatManagerMock, times(1))
                .fetchChatMessages(eq(chatId));
    }

    /**
     * Send Message
     */

    private static final String messageText = "Message text";

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptSendMessageParametersAreNotNull() throws Exception {
        chatViewModel.attemptSendMessage(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAttemptSendMessageParametersAreNotEmpty() throws Exception {
        messageInput.setValue("");
        chatViewModel.attemptSendMessage(messageInput);
    }

    @Test
    public void checkAttemptSendMessageIsForwardedProperly() throws Exception {
        tester.setCurrentChatId(chatId);
        messageInput.setValue(messageText);
        chatViewModel.attemptSendMessage(messageInput);
        verify(chatManagerMock, times(1))
                .sendMessage(eq(chatId), eq(messageText));
    }

    /**
     * Leave chat
     */

    @Test
    public void checkLeaveChatIsForwardedProperly() throws Exception {
        chatViewModel.leaveChat();

        verify(chatManagerMock, times(1))
                .leaveChat();
    }

    /**
     * Clear all
     */

    @Test
    public void checkClearAllIsForwardedProperly() throws Exception {
        chatViewModel.clearAll();

        verify(chatManagerMock, times(1))
                .clearAll();
    }

}
