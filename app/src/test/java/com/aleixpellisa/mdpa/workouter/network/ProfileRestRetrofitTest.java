package com.aleixpellisa.mdpa.workouter.network;

import android.arch.lifecycle.Observer;

import com.aleixpellisa.mdpa.workouter.BaseUnitTest;
import com.aleixpellisa.mdpa.workouter.database.entity.Profile;
import com.aleixpellisa.mdpa.workouter.network.rest.impl.ProfileRestRetrofit;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import testers.network.ProfileRestTester;

import static values.Events.createProfileErrorEvent;
import static values.Events.createProfileEvent;
import static values.Events.deleteProfileErrorEvent;
import static values.Events.deleteProfileEvent;
import static values.Events.fetchCandidateProfileErrorEvent;
import static values.Events.fetchCandidateProfileEvent;
import static values.Events.fetchProfileErrorEvent;
import static values.Events.fetchProfileEvent;
import static values.Events.updateProfileErrorEvent;
import static values.Events.updateProfileEvent;
import static values.Events.updateSettingsErrorEvent;
import static values.Events.updateSettingsEvent;
import static values.Inputs.ageUpdated;
import static values.Inputs.candidateId;
import static values.Inputs.cityUpdated;
import static values.Inputs.firstNameUpdated;
import static values.Inputs.genderUpdated;
import static values.Inputs.imagesUpdated;
import static values.Inputs.nameUpdated;
import static values.Inputs.preferenceUpdated;
import static values.Inputs.updateSettingsKey;
import static values.Inputs.updateSettingsValue;
import static values.MockClient.retrofitClientMock;

public class ProfileRestRetrofitTest extends BaseUnitTest {

    @Mock
    private Observer<Event<Profile>> profileResponseObserverMock;

    private ProfileRestRetrofit retrofit;

    private ProfileRestTester tester;

    @Before
    public void setup() {
        super.setup();

        retrofit = new ProfileRestRetrofit(retrofitClientMock);
        tester = new ProfileRestTester(retrofit);
    }

    /**
     * Fetch Profile
     */

    @Test
    public void checkReceiveProfileWhenFetchProfileSucceed() {
        retrofit.fetchProfile(profileResponseObserverMock);

        tester.fetchProfileReturnsProfile(fetchProfileEvent);

        Event<Profile> event = tester.captureEventFromResponseObserver(profileResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getId(), fetchProfileEvent.data.getId());
    }

    @Test
    public void checkReceiveErrorWhenFetchProfileFails() {
        retrofit.fetchProfile(profileResponseObserverMock);

        tester.fetchProfileReturnsProfile(fetchProfileErrorEvent);

        Event<Profile> event = tester.captureEventFromResponseObserver(profileResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchProfileErrorEvent.meetDayError.getMessage());
    }

    /**
     * Create Profile
     */

    @Test
    public void checkReceiveProfileWhenCreateProfileSucceed() {
        retrofit.createProfile(nameUpdated, firstNameUpdated, imagesUpdated, ageUpdated,
                genderUpdated, preferenceUpdated, cityUpdated, profileResponseObserverMock);

        tester.createProfileReturnsProfile(createProfileEvent);

        Event<Profile> event = tester.captureEventFromResponseObserver(profileResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getId(), createProfileEvent.data.getId());
    }

    @Test
    public void checkReceiveErrorWhenCreateProfileFails() {
        retrofit.createProfile(nameUpdated, firstNameUpdated, imagesUpdated, ageUpdated,
                genderUpdated, preferenceUpdated, cityUpdated, profileResponseObserverMock);

        tester.createProfileReturnsProfile(createProfileErrorEvent);

        Event<Profile> event = tester.captureEventFromResponseObserver(profileResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), createProfileErrorEvent.meetDayError.getMessage());
    }

    /**
     * Update Profile
     */

    @Test
    public void checkReceiveProfileWhenUpdateProfileSucceed() {
        retrofit.updateProfile(updateProfileEvent.data, profileResponseObserverMock);

        tester.updateProfileReturnsProfile(updateProfileEvent);

        Event<Profile> event = tester.captureEventFromResponseObserver(profileResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getId(), updateProfileEvent.data.getId());
    }

    @Test
    public void checkReceiveErrorWhenUpdateProfileFails() {
        retrofit.updateProfile(updateProfileEvent.data, profileResponseObserverMock);

        tester.updateProfileReturnsProfile(updateProfileErrorEvent);

        Event<Profile> event = tester.captureEventFromResponseObserver(profileResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), updateProfileErrorEvent.meetDayError.getMessage());
    }

    /**
     * Update Settings
     */

    @Test
    public void checkReceiveProfileWhenUpdateSettingsSucceed() {
        retrofit.updateSettings(updateSettingsKey, updateSettingsValue, profileResponseObserverMock);

        tester.updateSettingsReturnsProfile(updateSettingsEvent);

        Event<Profile> event = tester.captureEventFromResponseObserver(profileResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getId(), updateSettingsEvent.data.getId());
    }

    @Test
    public void checkReceiveErrorWhenUpdateSettingsFails() {
        retrofit.updateSettings(updateSettingsKey, updateSettingsValue, profileResponseObserverMock);

        tester.updateSettingsReturnsProfile(updateSettingsErrorEvent);

        Event<Profile> event = tester.captureEventFromResponseObserver(profileResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), updateSettingsErrorEvent.meetDayError.getMessage());
    }

    /**
     * Delete Profile
     */

    @Test
    public void checkReceiveProfileWhenDeleteProfileSucceed() {
        retrofit.deleteProfile(profileResponseObserverMock);

        tester.deleteProfileReturnsProfile(deleteProfileEvent);

        Event<Profile> event = tester.captureEventFromResponseObserver(profileResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getId(), deleteProfileEvent.data.getId());
    }

    @Test
    public void checkReceiveErrorWhenDeleteProfileFails() {
        retrofit.deleteProfile(profileResponseObserverMock);

        tester.deleteProfileReturnsProfile(deleteProfileErrorEvent);

        Event<Profile> event = tester.captureEventFromResponseObserver(profileResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), deleteProfileErrorEvent.meetDayError.getMessage());
    }

    /**
     * Fetch Candidate Profile
     */

    @Test
    public void checkReceiveProfileWhenFetchCandidateProfileSucceed() {
        retrofit.fetchCandidateProfile(candidateId, profileResponseObserverMock);

        tester.fetchCandidateProfileReturnsProfile(fetchCandidateProfileEvent);

        Event<Profile> event = tester.captureEventFromResponseObserver(profileResponseObserverMock);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getId(), fetchCandidateProfileEvent.data.getId());
    }

    @Test
    public void checkReceiveErrorWhenFetchCandidateProfileFails() {
        retrofit.fetchCandidateProfile(candidateId, profileResponseObserverMock);

        tester.fetchCandidateProfileReturnsProfile(fetchCandidateProfileErrorEvent);

        Event<Profile> event = tester.captureEventFromResponseObserver(profileResponseObserverMock);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchCandidateProfileErrorEvent.meetDayError.getMessage());
    }

}
