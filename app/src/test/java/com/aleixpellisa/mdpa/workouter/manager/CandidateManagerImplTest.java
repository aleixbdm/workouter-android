package com.aleixpellisa.mdpa.workouter.manager;

import com.aleixpellisa.mdpa.workouter.BaseUnitTest;
import com.aleixpellisa.mdpa.workouter.manager.impl.CandidateManagerImpl;
import com.aleixpellisa.mdpa.workouter.manager.refresher.DataRefresher;
import com.aleixpellisa.mdpa.workouter.manager.refresher.TokenRefresher;
import com.aleixpellisa.mdpa.workouter.model.Candidate;
import com.aleixpellisa.mdpa.workouter.model.Evaluation;
import com.aleixpellisa.mdpa.workouter.network.interceptor.AuthTokenInterceptor;
import com.aleixpellisa.mdpa.workouter.network.rest.CandidateRest;
import com.aleixpellisa.mdpa.workouter.manager.livedata.Event;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import javax.inject.Inject;

import testers.manager.CandidateManagerTester;

import static values.Events.fetchCandidatesErrorEvent;
import static values.Events.fetchCandidatesEvent;
import static values.Events.sendEvaluationErrorEvent;
import static values.Events.sendEvaluationEvent;

public class CandidateManagerImplTest extends BaseUnitTest {

    @Mock
    @SuppressWarnings("unused")
    private AuthTokenInterceptor authTokenInterceptorMock;

    @Mock
    @SuppressWarnings("unused")
    private CandidateRest candidateRestMock;

    @Mock
    @SuppressWarnings("unused")
    private DataRefresher candidatesDataRefresherMock;

    @Mock
    @SuppressWarnings("unused")
    private TokenRefresher tokenRefresherMock;

    @Inject
    public CandidateManagerImpl manager;

    @Inject
    public CandidateManagerTester tester;

    @Before
    public void setup() {
        super.setup();
    }

    /**
     * Fetch Candidates
     */

    @Test
    public void checkReceiveCandidatesWhenFetchCandidatesSucceed() {
        tester.fetchCandidatesReturnsCandidates(fetchCandidatesEvent);

        manager.fetchCandidates();

        Event<List<Candidate>> event = manager.getCandidateList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertTrue(!event.data.isEmpty());
        Assert.assertEquals(event.data.get(0).getId(), fetchCandidatesEvent.data.get(0).getId());
    }

    @Test
    public void checkReceiveErrorWhenFetchCandidatesFails() {
        tester.fetchCandidatesReturnsCandidates(fetchCandidatesErrorEvent);

        manager.fetchCandidates();

        Event<List<Candidate>> event = manager.getCandidateList().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), fetchCandidatesErrorEvent.meetDayError.getMessage());
    }

    /**
     * Send Evaluation
     */

    private static final String candidateId = fetchCandidatesEvent.data.get(0).getId();
    private static final String evaluationValue = sendEvaluationEvent.data.getEvaluationValue();

    @Test
    public void checkReceiveEvaluationWhenSendEvaluationSucceed() {
        tester.sendEvaluationReturnsEvaluation(candidateId, evaluationValue, sendEvaluationEvent);

        manager.sendEvaluation(candidateId, evaluationValue);

        Event<Evaluation> event = manager.getEvaluation().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.SUCCESS);
        Assert.assertEquals(event.data.getId(), sendEvaluationEvent.data.getId());
    }

    @Test
    public void checkReceiveErrorWhenSendEvaluationFails() {
        tester.sendEvaluationReturnsEvaluation(candidateId, evaluationValue, sendEvaluationErrorEvent);

        manager.sendEvaluation(candidateId, evaluationValue);

        Event<Evaluation> event = manager.getEvaluation().getValue();
        Assert.assertNotNull(event);
        Assert.assertEquals(event.status, Event.ERROR);
        Assert.assertEquals(event.meetDayError.getMessage(), sendEvaluationErrorEvent.meetDayError.getMessage());
    }
}